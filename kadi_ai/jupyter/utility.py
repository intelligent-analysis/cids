# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Utility functions to integrate KadiAI dashboards into Jupyter. Part of KadAI."""
import functools
from enum import Enum

import ipywidgets as widgets
from ipyfilechooser import FileChooser
from IPython.core.display import display
from IPython.core.display import HTML
from IPython.display import Javascript

from ..dash import dashboard_data
from ..dash import dashboard_model


class Dashboard(Enum):
    DEFINE_DATA = "dashboard_data"
    DEFINE_MODEL = "dashboard_model"


def setup_dashboard():
    display(HTML("<style>.container { width:100% !important; }</style>"))
    add_class()
    execute_cells("execute_cell")
    display(HTML("<style>.hide_cell .input{display:none;}</style"))
    display(HTML("<style>.hide_output .output_wrapper{display:none;}</style"))


def add_class():
    display(
        Javascript(
            "\
                var number_cells = Jupyter.notebook.get_cell_elements().length;\
                for (var i = 0; i < number_cells; i++) {\
                    var tags = Jupyter.notebook.get_cell(i).metadata.tags;\
                    var cell = Jupyter.notebook.get_cell_elements()[i];\
                    $(cell).addClass(tags);\
                }"
        )
    )


def execute_cells(tag):
    display(
        Javascript(
            "\
                    var number_cells = Jupyter.notebook.get_cell_elements().length;\
                    for (var i = 0; i < number_cells; i++) {\
                        var tags = Jupyter.notebook.get_cell(i).metadata.tags;\
                        var tag = "
            + '"'
            + tag
            + '"'
            + ";\
                        if (tags !== undefined && tags.includes(tag)){\
                            Jupyter.notebook.execute_cells([i]);\
                        }\
                    }"
        )
    )


def on_button_clicked(b, tag="dashboard_data"):
    my_js = (
        "\
    var number_cells = Jupyter.notebook.get_cell_elements().length;\
    for (var i = 0; i < number_cells; i++) {\
                        var tags = Jupyter.notebook.get_cell(i).metadata.tags;\
                        var tag = "
        + '"'
        + tag
        + '"'
        + ";\
                        if (tags !== undefined && tags.includes(tag)){\
                            Jupyter.notebook.execute_cells([i]);\
                        }\
    }"
    )
    display(Javascript(my_js))


def get_button(tag):
    label = tag.replace("_", " ")
    btn = widgets.Button(
        description=f"Run {label}",
        disabled=False,
        button_style="",
        tooltip=f"Run {label}",
        icon="check",
    )
    btn.on_click(functools.partial(on_button_clicked, tag=tag))
    return btn


def get_app(
    dashboard_type, project_dir=".", project_name="test", src_identifier="test-record"
):
    if dashboard_type == Dashboard.DEFINE_DATA:
        return dashboard_data.setup_dash(
            project_dir=project_dir,
            project_name=project_name,
            src_identifier=src_identifier,
            force=True,
        )
    if dashboard_type == Dashboard.DEFINE_MODEL:
        return dashboard_model.setup_dash(
            project_dir=project_dir,
            project_name=project_name,
            tfrecord_identifier=src_identifier,
        )
    raise ValueError(f"Unknown dashboard type: {dashboard_type}")


def get_apps(project_dir=".", project_name="test", src_identifier="test-record"):
    app_dashboard_data = dashboard_data.setup_dash(
        project_dir=project_dir,
        project_name=project_name,
        src_identifier=src_identifier,
        force=True,
    )
    app_dashboard_model = dashboard_model.setup_dash(
        project_dir=project_dir,
        project_name=project_name,
        tfrecord_identifier=src_identifier,
    )
    return app_dashboard_data, app_dashboard_model


def get_widgets():
    input_project_directory = widgets.HTML(
        value="Please choose a project directory",
        placeholder="Please choose a project directory",
    )

    file_chooser = FileChooser(".")
    file_chooser.default_path = "."

    input_project_name = widgets.HTML(
        value="Please choose a project name",
        placeholder="Please choose a project name",
    )

    project_name = widgets.Text(
        value="test", placeholder="test", description="Name:", disabled=False
    )

    input_identifier = widgets.HTML(
        value="Please choose an identifier",
        placeholder="Please choose an identifier",
    )

    identifier_name = widgets.Text(
        value="test-record",
        placeholder="test-record",
        description="Identifier:",
        disabled=False,
    )
    return (
        input_project_directory,
        file_chooser,
        input_project_name,
        project_name,
        input_identifier,
        identifier_name,
    )


def display_dashboard(port=8889):
    url = f"http://localhost:{port:d}"
    width = "100%"
    height = 900
    scrolling = "yes"
    iframe = (
        f'<iframe src="{url}" width={width} height={height:d}'
        + f" scrolling={scrolling}></iframe>"
    )
    return display.display_html(iframe, raw=True)


if __name__ == "__main__":
    print("main")
