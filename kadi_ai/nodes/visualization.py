# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import json
import os

import matplotlib.pyplot
import numpy
import pandas


def factorize_data_frame(original_df):
    """Factorize each non-numeric column of a dataframe.

    This is necessary for plotting, because pyplot and plotly can only
    handle numeric values. Numeric values start by 1.

    Args:
        original_df: Input dataframe to be factorized.

    Return:
        factorized_df: Factorized copy of the original_df.
    """
    factorized_df = original_df.copy()
    non_numeric_column_keys = factorized_df.select_dtypes(exclude="number").keys()
    for key in non_numeric_column_keys:
        factorized_df[key] = pandas.factorize(original_df[key])[0] + 1

    return factorized_df


def get_yaxis_bounds(factorized_df):
    """Calculate the bounds for each y-axis in the factorized dataframe
    and scale them for plotting.

    Args:
        factorized_df: Input dataframe as a factorized dataframe
        (use factorize_data_frame() before).

    Return:
        lower_bounds: List of minimum values for the y-axis.
        upper_bounds: List of maximum values for the y-axis.
    """
    lower_bounds = factorized_df.min(axis=0)
    upper_bounds = factorized_df.max(axis=0)
    delta_bounds = upper_bounds - lower_bounds

    # If lower and upper bounds are equal, delta should be 1 at least.
    # This ensures the propper visualization with one data set.
    delta_bounds[delta_bounds == 0] = 1

    # Add some addidional spacing to each y-axis, so that the values on the
    # bounds of the y-axis are plotted correctly.
    lower_bounds -= delta_bounds * 0.05
    upper_bounds += delta_bounds * 0.05

    return lower_bounds, upper_bounds


def get_sampling_dataframe(factorized_df, lower_bounds, upper_bounds):
    """Calculate the sampling points based on the bounds and scaling of the y-axis.
    This function is specific to static plots.

    Args:
        factorized_df: Factorized dataframe (result of factorize_data_frame()).
        lower_bounds: List of minimum values for the y-axis
                        (result of get_yaxis_bounds()).
        uppder_bounds: List of maximum values for the y-axis
                        (result of get_yaxis_bounds()).

    Return:
        sampling_df: Dataframe containing the correct values for the y-axis
          in a copy of the factorized_df.
    """
    delta_bounds = upper_bounds - lower_bounds

    sampling_df = factorized_df.copy()
    sampling_df.iloc[:, 1:] = (
        (factorized_df.iloc[:, 1:] - lower_bounds[1:]) / delta_bounds[1:]
    ) * delta_bounds[0] + lower_bounds[0]

    return sampling_df


def convert_hex_to_rgb(hex_str):
    """Convert a hex string to the corresponding rgb-value.
    Example: #FFFFFF will be converted to 255, 255, 255.

    Args:
        hex: Hexadecimal string for the conversion.

    Return:
        List of the red, green and blue values in range 0 to 255.
    """
    # Convert the individual strings "FF" "FF" "FF" to an integer
    #   by passing the base 16
    return [int(hex_str[i : i + 2], 16) for i in range(1, 6, 2)]


def create_color_gradient(start_color, stop_color, steps):
    """Create a color gradient as a list of hexadecimal color strings.

    Args:
        start_color: Start color of the gradient.
        stop_color: Final color of the gradient.
        steps: Number of intermediate colors between the start and stop color.

    Return:
        List of color strings.
    """
    start_color_rgb = numpy.array(convert_hex_to_rgb(start_color)) / 255
    stop_color_rgb = numpy.array(convert_hex_to_rgb(stop_color)) / 255

    mix_pcts = [x / (steps - 1) for x in range(steps)]
    rgb_colors = [
        ((1 - mix) * start_color_rgb + (mix * stop_color_rgb)) for mix in mix_pcts
    ]

    return [
        "#" + "".join([format(int(round(val * 255)), "02x") for val in item])
        for item in rgb_colors
    ]


def get_color_by_score(score, min_score, max_score):
    """This function returns a color based on a score and its bounds.

    Args:
        score: Score to request the color for.
        min_score: Lower bound of the scores in the plot.
        max_score: Upper bound of the scores in the plot.

    Return:
        Representing color as a hexadezimal string.
    """
    # Color resolution is 100 values over the score range
    tick_colors = create_color_gradient("#450a5c", "#21948b", 51)
    # Remove last color because of the duplicate
    tick_colors.pop(-1)
    tick_colors.extend(create_color_gradient("#21948b", "#dfe31b", 51))

    if max_score == min_score:
        score_as_index = 100
    else:
        score_as_index = int((score - min_score) / ((max_score - min_score) / 100))

    return tick_colors[score_as_index]


def plot_parallel_coordinates(data_df, title="Parallel Coordinates", score_axis=None):
    """Create a parallel coordinates plot.

    Args:
        data_df:    Dataframe as data source for the plot.
                    Labels are based on the column indices.
        title:      Title text of the plot.
        score_axis: Axis which should be selected as a score reference.
                    This creates a score-based gradient plot.
                    If None, every hyperprameter (included score) is handled
                    in the same way. This creates a plot highlighting the last
                    raw of the dataframe (correcponding to the last trial).

    Return:
        figure: Figure object of the plot.
    """
    # Check if data frame is valid
    if data_df.shape[1] < 2:
        raise ValueError("Error:" / "The dataframe should contain at least two axes!")

    # Check if score_axis is invalid
    if score_axis is not None:
        if not (score_axis in data_df.columns) or not (
            pandas.api.types.is_numeric_dtype(data_df[score_axis])
        ):
            raise ValueError(
                f'Error: The axis "{score_axis}"'
                / "is not valid to be used as a score!"
            )

    # Prepare the data
    factorized_df = factorize_data_frame(data_df)
    lower_bounds, upper_bounds = get_yaxis_bounds(factorized_df)
    sampling_df = get_sampling_dataframe(factorized_df, lower_bounds, upper_bounds)

    # Create the required plot and axes
    figure, host_axis = matplotlib.pyplot.subplots(figsize=(10, 4))
    axes = [host_axis] + [host_axis.twinx() for i in range(sampling_df.shape[1] - 1)]
    score_label_index = 0

    # Format the plot
    for i, axis in enumerate(axes):
        axis.set_ylim(lower_bounds[i], upper_bounds[i])
        axis.spines["top"].set_visible(False)
        axis.spines["bottom"].set_visible(False)
        axis.spines["right"].set_color("#154C79")
        axis.tick_params(axis="y", colors="#154C79")

        if axis == host_axis:
            axis.spines["left"].set_color("#154C79")
            axis.tick_params(axis="x", colors="#154C79")
        else:
            axis.spines["left"].set_visible(False)
            axis.yaxis.set_ticks_position("right")
            axis.spines["right"].set_position(("axes", i / (sampling_df.shape[1] - 1)))

        if score_axis == data_df.keys()[i]:
            score_label_index = i
            axis.spines["right"].set_linewidth(2)
            axis.spines["right"].set_color("#b80434")
            axis.tick_params(axis="y", colors="#b80434")

        # Create ticks and labels for the non-numerical axes
        if not pandas.api.types.is_numeric_dtype(data_df.iloc[:, i]):
            axis.yaxis.set_ticks(range(1, len(data_df.iloc[:, i].unique()) + 1))
            axis.yaxis.set_ticklabels(data_df.iloc[:, i].unique())

    # Add the colorbar
    if score_axis is not None:
        color_map = matplotlib.colors.LinearSegmentedColormap.from_list(
            "name", ["#450a5c", "#21948b", "#dfe31b"]
        )
        color_bar = figure.colorbar(
            None, ax=axes[-1], aspect=10, cmap=color_map, pad=0.1
        )
        color_bar.set_ticks([])
        color_bar.ax.set_title(score_axis, color="#154C79", pad=9)

    # Setup and format th host axis
    host_axis.set_xlim(0, sampling_df.shape[1] - 1)
    host_axis.set_xticks(range(sampling_df.shape[1]))
    host_axis.set_xticklabels(data_df.keys(), fontsize=12)
    host_axis.tick_params(axis="x", which="major", pad=7, size=0)
    host_axis.spines["right"].set_visible(False)
    host_axis.xaxis.tick_top()
    host_axis.set_title(title, fontsize=18, pad=12, color="#154C79")

    # Highlight score label
    if score_axis is not None:
        host_axis.get_xticklabels()[score_label_index].set_color("#b80434")

    # Plot the data
    for j in range(sampling_df.shape[0]):
        if score_axis is not None:
            host_axis.plot(
                range(sampling_df.shape[1]),
                sampling_df.iloc[j, :],
                color=get_color_by_score(
                    data_df[score_axis][j],
                    data_df[score_axis].min(),
                    data_df[score_axis].max(),
                ),
            )
        else:
            if j == sampling_df.shape[0] - 1:
                host_axis.plot(
                    range(sampling_df.shape[1]), sampling_df.iloc[j, :], color="#b80434"
                )
            else:
                host_axis.plot(
                    range(sampling_df.shape[1]), sampling_df.iloc[j, :], color="#B6B6B6"
                )

    figure.tight_layout()
    return figure


def get_dataframe_from_trials(path):
    """This functions converts a directory of trials to a dataframe for plotting.

    Args:
        score: Path to the parent directory of all trials.

    Return:
        Dataframe for plotting.
    """
    # Search all trail.json files in the directory of path
    trial_files = []
    for subdir, _, files in os.walk(path):
        for file in files:
            filepath = subdir + os.sep + file

            if filepath.endswith("trial.json"):
                trial_files.append(filepath)

    result_dict = []
    for file in trial_files:
        with open(file, encoding="utf8") as json_file:
            trial_dict = json.load(json_file)
            trial = {"trial_id": trial_dict["trial_id"]}
            trial.update(trial_dict["hyperparameters"]["values"])
            trial["score"] = trial_dict["score"]
            result_dict.append(trial)

    dataframe = pandas.DataFrame(data=result_dict)
    # Used to order the trials based on the trial id
    # TODO: Improve based on the proper naming convention or timestamps
    dataframe.sort_values(by="trial_id", axis=0, inplace=True)
    dataframe = dataframe.reset_index(drop=True)

    return dataframe
