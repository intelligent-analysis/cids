# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Bayesian optimization functionality for KadiAI in Kadi Workflows. Part of KadiAI."""
import json
import os
from copy import deepcopy
from pathlib import Path

import keras_tuner
import numpy as np
import scipy.optimize
import tensorflow as tf
from kadi_apy import KadiManager
from kadi_apy.lib.exceptions import KadiAPYInputError
from keras_tuner import HyperParameters
from keras_tuner.src.engine import stateful
from keras_tuner.src.engine import trial as trial_module
from keras_tuner.src.engine.trial import TrialStatus
from keras_tuner.src.tuners.bayesian import BayesianOptimizationOracle
from scipy.stats import norm
from tensorflow.python.framework.errors_impl import NotFoundError

from ..repo.record import KadiAIRecord
from .properties import set_environment_properties
from cids.tensorflow.tuner import CustomTrial as Trial
from kadi_ai.nodes.properties import DEFAULT_KADI_PROPERTY_FILE
from kadi_ai.nodes.properties import read_kadi_properties_file

# import matplotlib.pyplot as plt
# from keras_tuner.src.engine import hyperparameters as hp_module
# from .visualization import get_dataframe_from_trials
# from .visualization import plot_parallel_coordinates


class TrialStatusExtended(TrialStatus):
    # IDLE = "IDLE"             # config. setting just created
    # RUNNING = "RUNNING"       # sim./exp. running with this config.
    EXECUTED = "EXECUTED"  # results available ready to be scored
    # COMPLETED = "COMPLETED"   # results scored, everything done
    # INVALID = "INVALID"
    # STOPPED = "STOPPED"


meta_status = [
    {
        "key": "status",
        "type": "str",
        "validation": {
            "options": [
                "IDLE",
                "RUNNING",
                "EXECUTED",
                "COMPLETED",
                "STOPPED",
                "INVALID",
            ]
        },
        "value": "",
    }
]

meta_score = [
    {
        "key": "score",
        "type": "float",
        "value": 0.0,
    }
]


def _expected_improvement(x, y, gpr, tradeoff, direction="max"):
    """Computes the expected improvements at points x.

    Args:
        x (ArrayLike): Vector of points at which to compute the acquisition function.
        y (ArrayLike): Vector of observations.
        gpr (GaussianPro): Gaussian process regressor fitted to samples.
        tradeoff (float): tradeoff parameter.

    Returns:
        np.ndarray: vector of expected improvements at points x.
    """

    x = np.reshape(x, (-1, 1))
    mu, sigma = gpr.predict(x, return_std=True)
    baseline = np.max(y) if direction == "max" else np.min(y)
    improvement = mu - baseline - tradeoff
    z = improvement / sigma
    return improvement * norm.cdf(z) + sigma * norm.pdf(z)


def _upper_confidence_bound(x, gpr, tradeoff):
    """Computes the upper confidence bound at points x.

    Args:
        x (ArrayLike): Vector of points at which to compute the acquisition function.
        gpr (GaussianPro): Gaussian process regressor fitted to samples.
        tradeoff (float): tradeoff parameter.

    Returns:
        np.ndarray: vector of upper confidence bounds at points x.
    """
    x = np.reshape(x, (-1, 1))
    mu, sigma = gpr.predict(x, return_std=True)
    return mu + tradeoff * sigma


def _lower_confidence_bound(x, gpr, tradeoff):
    """Computes the lower confidence bound at points x.

    Args:
        x (ArrayLike): Vector of points at which to compute the upper confidence bound.
        gpr (GaussianPro): Gaussian process regressor fitted to samples.
        tradeoff (float): tradeoff parameter.

    Returns:
        np.ndarray: vector of lower confidence bounds at points x.
    """
    x = np.reshape(x, (1, -1))
    mu, sigma = gpr.predict(x, return_std=True)
    return mu - tradeoff * sigma


def generate_trial_identifier(trial_id):
    trial_id_nospaces = trial_id.replace(" ", "-")
    root_element = read_kadi_properties_file(Path.cwd() / DEFAULT_KADI_PROPERTY_FILE)
    environment_element = root_element.findall("environment")
    env = {param.get("key"): param.text for param in environment_element}
    trial_identifier = f"{env['umbrella_id']}_trial-{trial_id_nospaces}"
    return trial_identifier


class ModifiedBayesianOracle(BayesianOptimizationOracle):
    def __init__(
        self,
        objective=None,
        max_trials=10,
        num_initial_points=None,
        alpha=1e-4,
        beta=2.6,
        acquisition_function="confidence",
        seed=None,
        hyperparameters=None,
        allow_new_entries=True,
        tune_new_entries=True,
    ):
        self.acquisition_function = acquisition_function
        super().__init__(
            objective=objective,
            max_trials=max_trials,
            num_initial_points=num_initial_points,
            alpha=alpha,
            beta=beta,
            seed=seed,
            hyperparameters=hyperparameters,
            allow_new_entries=allow_new_entries,
            tune_new_entries=tune_new_entries,
        )

    def reload(self):
        # Reload trials from their own files.
        trial_fnames = tf.io.gfile.glob(
            os.path.join(self._project_dir, "trial_*", "trial.json")
        )
        trial_fnames.sort()  # Sort by trial_id!
        for fname in trial_fnames:
            with tf.io.gfile.GFile(fname, "r") as f:
                trial_data = f.read()
            trial_state = json.loads(trial_data)
            trial = trial_module.Trial.from_state(trial_state)
            self.trials[trial.trial_id] = trial
        try:
            stateful.Stateful.reload(self._get_oracle_fname())
        except KeyError as e:
            raise RuntimeError(
                "Error reloading `Oracle` from existing project. "
                "If you did not mean to reload from an existing project, "
                f"change the `project_name` or pass `overwrite=True` "
                "when creating the `Tuner`. Found existing "
                f"project at: {self._project_dir}"
            ) from e
        except NotFoundError:
            # Oracle file does not exist, this is a new project.
            pass

    def get_state(self):
        state = super().get_state()
        state.update(
            {
                "objective_name": (
                    self.objective.name
                    if isinstance(self.objective, keras_tuner.Objective)
                    else self.objective
                )
            }
        )
        return state

    def set_state(self, state):
        self.objective_name = state["objective_name"]
        super().set_state(state)

    def _get_raw_bounds(self, hp):
        """Get real bounds of one hyperparameter."""
        bounds = []
        bounds.append([hp.min_value, hp.max_value])
        return np.array(bounds)

    # def _plot_prediction_model(self):
    #     # FIXME: export plotting from Oracle
    #     hps = [hp[0] for hp in self.hyperparameters._hps.values()]
    #     if len(hps) == 1:
    #         for hp in hps:
    #             # Plot real objective function (normally unknown)
    #             # Grid of (100) equidistant points between boundaries
    #             real_bounds = self._get_raw_bounds(hp).ravel()
    #             x_obj = np.linspace(
    #                 real_bounds[0],
    #                 real_bounds[1],
    #                 num=100,
    #             )
    #             # y_obj = [self.objective_function_options()(xx) for xx in x_obj]

    #             # Transform cumulative prob into values
    #             list_of_dicts_x = [self._vector_to_values(xx) for xx in x]
    #             dict_of_arrays_x = {
    #                 k: np.asarray([dic[k] for dic in list_of_dicts_x])
    #                 for k in list_of_dicts_x[0]
    #             }

    #             # Plot all parameters vs score
    #             for key in dict_of_arrays_x.keys():
    #                 fig, axs = plt.subplots(
    #                     2, 1, gridspec_kw={"wspace": 0, "hspace": 0.5}
    #                 )
    #                 # axs[0].plot(x_obj, y_obj, "y--", lw=1, label="Real objective")
    #                 # Random samples
    #                 axs[0].plot(
    #                     dict_of_arrays_x[key][: self.num_initial_points],
    #                     y[: self.num_initial_points],
    #                     "rx",
    #                     mew=3,
    #                     label="Random samples",
    #                 )
    #                 # Bayesian samples
    #                 axs[0].plot(
    #                     dict_of_arrays_x[key][self.num_initial_points :],
    #                     y[self.num_initial_points :],
    #                     "kx",
    #                     mew=3,
    #                     label="Bayesian samples",
    #                 )
    #                 # values to unitary domain
    #                 prob = hp_module.value_to_cumulative_prob(x_obj, hp)
    #                 prob = prob.reshape(-1, 1)
    #                 ucb = _upper_confidence_bound(prob, self.gpr, self.beta)
    #                 lcb = _lower_confidence_bound(prob, self.gpr, self.beta)
    #                 axs[0].fill_between(x_obj.ravel(), ucb, lcb, alpha=0.1)
    #                 axs[0].set_xlabel(f"{hp.name}")
    #                 axs[0].set_ylabel(f"{self.objective.name}")
    #                 X_next = new_values[key]
    #             if X_next:
    #                 axs[0].axvline(
    #                     x=X_next,
    #                     ls="--",
    #                     c="k",
    #                     lw=1,
    #                     label="Next sampling location",
    #                 )
    #             # Plot acquisition function
    #             if self.direction == "min":
    #                 acq = lcb
    #             else:
    #                 acq = ucb
    #             axs[1].plot(x_obj, acq, "r-", lw=1, label="Acquisition function")
    #             axs[1].axvline(
    #                 x=X_next, ls="--", c="k", lw=1, label="Next sampling location"
    #             )
    #             axs[1].legend()
    #             fig.show()
    #             fig.savefig(f"bayesian_plot_{trial_id}")

    #     else:
    #         # Domain grid
    #         num_points_plot = (
    #             10  # TODO: 100/len(hps)? ...something depending on dimensionality
    #         )
    #         bases = [
    #             np.linspace(hp.min_value, hp.max_value, num_points_plot) for hp in hps
    #         ]
    #         x_grid = np.meshgrid(*bases)
    #         x_grid = np.stack(x_grid, axis=-1)
    #         x_obj = np.reshape(x_grid, [-1, x_grid.shape[-1]])

    #         # multidim_linsp = []
    #         # for i in np.arange(len(hps)):
    #         #     multidim_linsp.append(
    #         #         np.linspace(hps[i].min_value, hps[i].max_value, num_points_plot)
    #         #     )
    #         # grid = np.vstack(np.meshgrid(*multidim_linsp)).reshape(len(hps), -1).T
    #         # x_obj = grid

    #         # Transform trials' cumulative prob (x) into values
    #         list_of_dicts_x = [self._vector_to_values(xx) for xx in x]
    #         dict_of_arrays_x = {
    #             k: np.asarray([dic[k] for dic in list_of_dicts_x])
    #             for k in list_of_dicts_x[0]
    #         }
    #         # Plot all parameters vs score
    #         fig, axs = plt.subplots(
    #             2, len(hps), gridspec_kw={"wspace": 0.3, "hspace": 0.4}
    #         )
    #         prob = np.stack(
    #             [
    #                 hp_module.value_to_cumulative_prob(x_obj[..., i], hp)
    #                 for i, hp in enumerate(hps)
    #             ],
    #             axis=-1,
    #         )
    #         # prob = np.empty((len(x_obj), len(hps)))
    #         # for key in dict_of_arrays_x.keys():
    #         #     key_index = list(dict_of_arrays_x.keys()).index(key)
    #         #     x_obj_key = [item[key_index] for item in x_obj]
    #         #     prob[:, key_index] = hp_module.value_to_cumulative_prob(
    #         #         np.array(x_obj_key), hps[key_index]
    #         #     )

    #         mu, sigma = self.gpr.predict(prob)

    #         mu_grid = mu.reshape(x_grid.shape[:-1])
    #         sigma_grid = sigma.reshape(x_grid.shape[:-1])
    #         lcb_grid = mu_grid - self.beta * sigma_grid
    #         ucb_grid = mu_grid + self.beta * sigma_grid

    #         max_idx = np.unravel_index(
    #             np.argmax(ucb_grid), ucb_grid.shape
    #         )  # TODO: max/min based on direction?

    #         # Plots
    #         for i, (key, value) in enumerate(dict_of_arrays_x.items()):
    #             slices = deepcopy(list(max_idx))
    #             slices[i] = slice(None)
    #             key_index = list(dict_of_arrays_x.keys()).index(key)
    #             x_obj_key = [item[key_index] for item in x_obj]
    #             # Random samples
    #             axs[0, key_index].plot(
    #                 value[: self.num_initial_points],
    #                 y[: self.num_initial_points],
    #                 "rx",
    #                 mew=3,
    #                 label="Random samples",
    #             )
    #             # Bayesian samples
    #             axs[0, key_index].plot(
    #                 value[self.num_initial_points :],
    #                 y[self.num_initial_points :],
    #                 "kx",
    #                 mew=3,
    #                 label="Bayesian samples",
    #             )

    #             axs[0, key_index].plot(
    #                 x_grid[..., i][slices],
    #                 mu_grid[slices],
    #                 "b-",
    #                 lw=1,
    #                 label="Surrogate function",
    #             )
    #             axs[0, key_index].fill_between(
    #                 x_grid[..., i][slices],
    #                 ucb_grid[slices],
    #                 lcb_grid[slices],
    #                 alpha=0.1,
    #             )
    #             axs[0, key_index].set_xlabel(f"{hps[key_index].name}")
    #             axs[0, key_index].set_ylabel(f"{self.objective.name}")

    #             X_next = new_values[key]
    #             if X_next:
    #                 axs[0, key_index].axvline(
    #                     x=X_next,
    #                     ls="--",
    #                     c="k",
    #                     lw=1,
    #                     label="Next sampling location",
    #                 )

    #             # Plot acquisition function
    #             ucb = mu - self.beta * sigma
    #             axs[1, key_index].plot(
    #                 x_obj_key, ucb, "r-", lw=1, label="Acquisition function"
    #             )
    #             axs[1, key_index].axvline(
    #                 x=X_next, ls="--", c="k", lw=1, label="Next sampling location"
    #             )

    #         axs[0, key_index].legend(
    #             bbox_to_anchor=(1.05, 0.5), loc="center left", fancybox=True
    #         )
    #         axs[1, key_index].legend(
    #             bbox_to_anchor=(1.05, 0.5), loc="center left", fancybox=True
    #         )
    #     return fig

    def populate_space(self, trial_id, acquisition_fun=None, plot=False, debug=True):

        # Generate enough samples before training Gaussian process.
        completed_trials = [t for t in self.trials.values() if t.status == "COMPLETED"]

        # Use 3 times the dimensionality of the space as the default number of
        # random points.
        dimensions = len(self.hyperparameters.space)
        num_initial_points = self.num_initial_points or 3 * dimensions
        if len(completed_trials) < num_initial_points:
            print("Random populate space")
            return self._random_populate_space()

        # Fit a GPR to the completed trials and return the predicted optimum
        # values.
        x, y = self._vectorize_trials()

        # Ensure no nan, inf in x, y. GPR cannot process nan or inf.
        x = np.nan_to_num(x, posinf=0, neginf=0)
        y = np.nan_to_num(y, posinf=0, neginf=0)

        self.gpr.fit(x, y)

        def _acquisition_function(x, direction="min"):
            if self.acquisition_function == "confidence":
                if direction == "max":
                    return _upper_confidence_bound(x, self.gpr, self.beta)
                return _lower_confidence_bound(x, self.gpr, self.beta)
            return _expected_improvement(x, y, self.gpr, self.beta, direction=direction)

        if acquisition_fun is None:
            acquisition_fun = _acquisition_function

        # all this part is in [0,1]
        optimal_val = float("inf")
        optimal_x = None
        num_restarts = 50
        bounds = self._get_hp_bounds()
        x_seeds = self._random_state.uniform(
            bounds[:, 0], bounds[:, 1], size=(num_restarts, bounds.shape[0])
        )
        print("Bayesian populate space")
        for x_try in x_seeds:
            # Sign of score is flipped when maximizing!
            result = scipy.optimize.minimize(
                acquisition_fun, x0=x_try, bounds=bounds, method="L-BFGS-B"
            )
            result_fun = result.fun if np.isscalar(result.fun) else result.fun[0]
            if result_fun < optimal_val:
                optimal_val = result_fun
                optimal_x = result.x

        # here it should be transformed back to the real param range(s)
        values = self._vector_to_values(optimal_x)

        # # Plot bayesian optimization process
        # if plot:
        #     data_frame = get_dataframe_from_trials(self._project_dir)
        #     # Current prediction plot
        #     fig_prediction = self._plot_prediction_model()
        #     fig_prediction.savefig(
        #         f"{self._get_trial_dir(trial_id)}/current_pediction_plot_{trial_id}"
        #     )
        #     # Parallel plots (current and overview)
        #     fig_parallel_current = plot_parallel_coordinates(data_frame)
        #     fig_parallel_current.savefig(
        #         f"{self._get_trial_dir(trial_id)}/current_parallel_plot_{trial_id}"
        #     )
        #     fig_parallel_overview = plot_parallel_coordinates(
        #         data_frame, score_axis=self.objective.name
        #     )
        #     fig_parallel_overview.savefig(
        #         f"{self._project_dir}/parallel_plot_overview"
        #     )

        return {"status": trial_module.TrialStatus.RUNNING, "values": values}


def _setup_search_directories(project_dir):
    # TODO: full KadiAIProject instance?
    project_dir = Path(project_dir)
    result_dir = project_dir / "RESULTS"
    model_dir = result_dir / "bayesian_oracle"
    search_name = "bayesian_search_01"
    search_dir = model_dir / search_name
    return search_dir, search_name


def _extract_search_space(umbrella_record):
    # TODO: add handling of fixed values (check hp_module.Fixed)
    # Extract search space from umbrella record
    hyperparameters = HyperParameters()
    extras = umbrella_record.meta["extras"]
    for m in extras:
        key = m["key"]
        value = m["value"]
        # Manual handling of ranges and values
        if m["type"] == "dict":
            # Quantitative key
            min_value = umbrella_record.get_metadatum([key, "min"])["value"]
            max_value = umbrella_record.get_metadatum([key, "max"])["value"]
            try:
                step_value = umbrella_record.get_metadatum([key, "step"])["value"]
            except (KadiAPYInputError, TypeError):
                step_value = None
            dtype = umbrella_record.get_metadatum([key, "min"])["type"]
            try:
                default_value = umbrella_record.get_metadatum([key, "default"])["value"]
            except (KadiAPYInputError, TypeError):
                if step_value is None:
                    default_value = (max_value - min_value) / 2
                else:
                    possible_values = np.arange(min_value, max_value, step_value)
                    default_value = possible_values[len(possible_values) // 2]
            if dtype == "float":
                hyperparameters.Float(
                    key,
                    min_value=min_value,
                    max_value=max_value,
                    step=step_value,
                    default=float(default_value),
                )
            elif dtype == "int":
                hyperparameters.Int(
                    key,
                    min_value=min_value,
                    max_value=max_value,
                    step=step_value,
                    default=int(default_value),
                )
            else:
                raise ValueError("Invalid data type for quantitative parameter.")
        # TODO: Unify usage of validation
        elif m["type"] == "str":
            # Qualitative key
            valid_choices = m["validation"]["options"]
            hyperparameters.Choice(key, valid_choices, default=value)
    return hyperparameters


def _debug_random_value():
    return np.abs(np.random.normal(loc=0.5))


def _read_value_from_results(file, search_str):
    file = Path(file)
    with file.open(encoding="utf-8") as f:
        lines = f.readlines()
        for line in lines:  # if all params are in one line this doesn't work!
            if search_str in line:
                line_split = line.split()
                val = line_split[-1]
                return float(val)
        raise ValueError(f"{search_str} not found.")


def _read_value_from_json_results(file, search_str):
    file = Path(file)
    with file.open(encoding="utf-8") as f:
        try:
            data_dict = json.load(f)
            if isinstance(data_dict, list):
                for item in data_dict:
                    if item[search_str]:
                        return float(item[search_str])
            elif isinstance(data_dict, dict):
                if data_dict[search_str]:
                    return float(data_dict[search_str])
            raise ValueError(f"{search_str} not found.")
        except TypeError as e:
            raise ValueError(f"{file} is not a json file.") from e


def bayesian_next_trial(
    search_space_identifier,
    objective,
    direction="min",
    acquisition_function="lcb",
    exploration_factor=2.6,
    max_index=100,
    num_initial_points=None,
    plot=True,
):
    """KadiAI tool that creates a Trial with the setting of the next
    most informative datapoint based on Bayesian Optimization.

    It uses keras tuner Bayesian optimization with a underlying Gaussian process model.
    The acquisition function used is upper confidence bound (UCB).

    Args:
        search_space_identifier: Umbrella Record identifier holding parameter space for
            Bayesian Optimization.
        objective: A string, `keras_tuner.Objective` instance, or a list of
            `keras_tuner.Objective`s and strings. If a string, the direction of
            the optimization (min or max) will be inferred. If a list of
            `keras_tuner.Objective`, we will minimize the sum of all the
            objectives to minimize subtracting the sum of all the objectives to
            maximize. The `objective` argument is optional when
            `Tuner.run_trial()` or `HyperModel.fit()` returns a single float as
            the objective to minimize.
        exploration_factor (beta): Float, the balancing factor of exploration and
            exploitation. The larger it is, the more explorative it is. Defaults to 2.6.
    """
    # TODO: select acquisition function in function call and command

    # Setup project directory
    project_dir = Path() / "active_search"
    search_dir, search_name = _setup_search_directories(project_dir)

    # Setup Kadi manager and get umbrella record
    manager = KadiManager(instance=None, host=None, token=None)
    umbrella_record = KadiAIRecord(manager, identifier=search_space_identifier)

    # Download all linked records
    linked_ids = umbrella_record.auto_get_link_ids(direction="in", filter="trial for")
    linked_records = [KadiAIRecord(manager, id=i) for i in set(linked_ids.values())]
    linked_trial_records = [r for r in linked_records if r.meta["type"] == "trial"]
    for linked_record in set(linked_trial_records):
        linked_trial_id = linked_record.get_metadatum("trial_id")["value"]
        current_dir = search_dir / f"trial_{linked_trial_id}"
        if not current_dir.exists():
            linked_record.download_files(current_dir)

    # Extract hyperparameters from umbrella Record
    hp = _extract_search_space(umbrella_record)

    # Setup oracle, set search directory and reload trials
    objective = keras_tuner.Objective(objective, direction)
    oracle = ModifiedBayesianOracle(
        objective=objective,
        max_trials=max_index + 1,
        num_initial_points=num_initial_points,  # default = 3*hyp space dim
        hyperparameters=hp,
        beta=exploration_factor,
    )
    oracle._set_project_dir(search_dir.parent, search_name)
    oracle.reload()

    # Create a Trial instance with prediction from the oracle (sets trial to RUNNING)
    # TODO: loop index instead of "tuner1"
    trial = oracle.create_trial("tuner1")
    trial_dir = Path(oracle._get_trial_dir(trial.trial_id))
    trial_file = trial_dir / "trial.json"

    # Save oracle
    oracle.save()

    # Save properties in environment variables
    params = [
        {"key": "trial_id", "value": trial.trial_id, "type": "str"},
        {"key": "trial_dir", "value": os.fspath(trial_dir), "type": "str"},
        {"key": "umbrella_id", "value": umbrella_record.identifier, "type": "str"},
    ]
    for k in trial.hyperparameters.values.keys():
        val = trial.hyperparameters[k]
        if isinstance(val, np.number):
            val = val.item()
        # write xml file
        params.append({"key": k, "value": val})
    set_environment_properties(params)

    # Create a new trial record
    trial_identifier = generate_trial_identifier(trial.trial_id)
    trial_title = f"Trial {trial.trial_id} for {umbrella_record.identifier}"
    trial_record = manager.record(
        identifier=trial_identifier, title=trial_title, create=True
    )

    # Upload trial.json to the trial record
    trial_record.upload_file(file_path=trial_file, file_name=trial_file.name)

    # Status as metadata (mirrors the status in trial.json).
    #   The class TrialStatus of Keras Tuner contains:
    #   IDLE, RUNNING, COMPLETED, INVALID, STOPPED
    #   but I need one more to distinguish the following phases of the process:
    #       - setting configuaration created but not yet used (IDLE)
    #       - the setting configuration has been taken care of and the related
    #           simulation/experiment is running (RUNNING)
    #       - the sim/exp is completed and the results available
    #           (uploaded in kadi?) but they haven't been scored yet (EXECUTED)
    #       - completed = scored (COMPLETED)
    status_extra = deepcopy(meta_status)
    status_extra[0]["value"] = "IDLE"
    trial_record.add_metadata(status_extra)
    trial_record.add_metadatum(
        {"key": "trial_id", "type": "str", "value": trial.trial_id}
    )
    trial_record.set_attribute(attribute="type", value="trial")
    trial_record.set_attribute(
        attribute="description", value="Bayesian Optimizaton trial."
    )

    # Link trial record to the umbrella record
    trial_record.link_record(record_to=umbrella_record.id, name="trial for")
    # TODO: collection link (get all collection_s_ from umbrella record and link)
    umbrella_record.link_record(record_to=trial_record.id, name="new")

    # Visualizations
    # TODO: upload separate from creation of plots
    if plot:
        # Upload the incremental plots to the trial record.
        parallel_plot_file = trial_dir / f"parallel_plot_{trial.trial_id}.png"
        if parallel_plot_file.exists():
            trial_record.upload_file(
                file_path=parallel_plot_file, file_name=parallel_plot_file.name
            )
        confidence_plot_file = trial_dir / f"bayesian_plot_{trial.trial_id}.png"
        if confidence_plot_file.exists():
            trial_record.upload_file(
                file_path=confidence_plot_file, file_name=confidence_plot_file.name
            )
        # Upload overview plot
        overview_plot_path = search_dir / "parallel_plot_overview.png"
        if overview_plot_path.exists():
            # upload new one
            umbrella_record.upload_file(
                file_path=overview_plot_path,
                file_name=overview_plot_path.name,
                force=True,
            )


def finish_trial(trial_id):
    """Execute a dummy simulation / experiment given the setting configuration."""

    # Setup project directory
    project_dir = Path() / "active_search"
    search_dir, search_name = _setup_search_directories(project_dir)

    # Setup oracle, set search directory and reload trials
    oracle = ModifiedBayesianOracle()
    oracle._set_project_dir(search_dir.parent, search_name)

    # Get trial
    trial_dir = Path(oracle._get_trial_dir(trial_id))
    trial_file = trial_dir / "trial.json"
    trial = Trial.load(trial_file)

    # Set trial status to EXECUTED
    trial.status = TrialStatusExtended.EXECUTED
    trial.save(trial_file)
    # TODO: update Kadi record


def score_trial(
    trial_id,
    objective,
    direction="min",
    results_file=None,
    monitor=None,
    target_value=None,
):
    """Compute the score of one executed Trial and update the Trial Record in Kadi."""

    # Setup project directory
    project_dir = Path() / "active_search"
    search_dir, search_name = _setup_search_directories(project_dir)

    # Setup oracle, set search directory and reload trials
    objective = keras_tuner.Objective(objective, direction)
    oracle = ModifiedBayesianOracle(objective=objective)
    oracle._set_project_dir(search_dir.parent, search_name)
    oracle.reload()

    # Get trial
    trial_dir = Path(oracle._get_trial_dir(trial_id))
    trial_json = trial_dir / "trial.json"
    if trial_id is not None:
        trial = Trial.load(trial_json)
    else:
        # Auto select first ongoing trial
        try:
            trial = oracle.ongoing_trials[0]
            trial_id = trial.trial_id
        except IndexError as e:
            raise ValueError("No trial to score.") from e

    # Ensure trial has been EXECUTED
    # TODO: new SynchronizedTrial class to handle this
    status = trial.status
    # Check if trial has been executed remotely
    manager = KadiManager(instance=None, host=None, token=None)
    trial_identifier = generate_trial_identifier(trial.trial_id)
    trial_record = KadiAIRecord(manager, identifier=trial_identifier)
    if status not in [TrialStatusExtended.EXECUTED, TrialStatusExtended.COMPLETED]:
        status = trial_record.get_metadatum("status")
    if status not in [TrialStatusExtended.EXECUTED, TrialStatusExtended.COMPLETED]:
        raise ValueError(f"Cannot score incomplete trial. Status: {status}.")

    # Compute and save score
    # Default scoring method: read value from results file
    if results_file is None:
        raise ValueError("No results file provided.")
    current_watch_value = _read_value_from_json_results(results_file, monitor)
    if target_value:
        score = abs(current_watch_value - target_value)
        # TODO: Normalization.
        # We need a max/min range also for the objective/monitor
        # (e.s. Tortuosity)
    else:
        score = current_watch_value

    # Update trial
    oracle.update_trial(trial_id, {objective.name: score})
    trial.status = "COMPLETED"
    oracle.end_trial(trial)

    # Upload the updated trial.json into kadi
    trial_record.upload_file(
        file_path=trial_json,
        file_name=trial_json.name,
        force=True,
    )

    # Link Umbrella record to the best trial so far
    # TODO: take into account the possibility that the same record will be used
    #       for multiple umbrellas. Consider adding umbrella id to scorer fun.
    umbrella_id = trial_record.auto_get_link_ids(direction="out", filter="trial for")
    umbrella_record = [KadiAIRecord(manager, id=i) for i in set(umbrella_id.values())][
        0
    ]
    desired_best_trials = 1
    best_trial_id = oracle.get_best_trials(desired_best_trials)[0].trial_id
    # Get links to possible best (old one and new added trials)
    old_best = umbrella_record.auto_get_link_ids(direction="out", filter="best trial")
    new = umbrella_record.auto_get_link_ids(direction="out", filter="new")
    to_check = new | old_best
    # Remove old links
    for link in set(to_check):
        umbrella_record.delete_record_link(link)
    # Add new link to best trial
    for i in set(to_check.values()):
        record = KadiAIRecord(manager, id=i)
        for m in record.meta["extras"]:
            if m["key"] == "trial_id" and m["value"] == best_trial_id:
                umbrella_record.link_record(record_to=record.id, name="best trial")

    # Update trial record status into "COMPLETED" and add score
    status_extras = deepcopy(meta_status)
    score_extras = deepcopy(meta_score)
    monitor_extras = deepcopy(meta_score)
    status_extras[0]["value"] = "COMPLETED"
    score_extras[0]["value"] = score
    score_extras[0]["key"] = objective.name
    monitor_extras[0]["value"] = current_watch_value
    monitor_extras[0]["key"] = monitor
    trial_record.add_metadata(status_extras, force=True)
    trial_record.add_metadata(score_extras, force=True)
    trial_record.add_metadata(monitor_extras, force=True)
