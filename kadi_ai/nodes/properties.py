# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import json
import os
from pathlib import Path

import xmljson
from defusedxml.lxml import parse
from lxml import etree


ROOT_ELEMENT_NAME = "properties"
ENVIRONMENT_VARIABLE_NAME = "environment"
DEFAULT_KADI_PROPERTY_FILE = "kadi_properties.json"


class BadgerFish(xmljson.BadgerFish):
    @staticmethod
    def _fromstring(value):
        """Convert XML string value to None, boolean, int or float"""
        # NOTE: Is this even possible ?
        if value is None:
            return None

        # FIXME: In XML, booleans are either 0/false or 1/true (lower-case !)
        if value.lower() == "true":
            return True
        if value.lower() == "false":
            return False

        # FIXME: Using int() or float() is eating whitespaces unintendedly here
        try:
            int_value = int(value)
            if len(str(int_value)) == len(value.strip()):
                return int_value
            return value
        except ValueError:
            pass

        try:
            # Test for infinity and NaN values
            if float("-inf") < float(value) < float("inf"):
                return float(value)
        except ValueError:
            pass

        return value


def format_xml(program_elem, xml_declaration=True):
    xml = etree.tostring(  # pylint: disable=c-extension-no-member
        program_elem,
        pretty_print=True,
        xml_declaration=xml_declaration,
        encoding="UTF-8",
    )
    return xml.decode()[:-1]


def format_json(json_obj):
    json_string = json.dumps(
        json_obj,
        indent=2,
        sort_keys=True,
    )
    return json_string


def read_kadi_properties_file(file=None):
    """Read a Kadi properties from an XML or JSON file into a workflow element.

    Args:
        file (PathLike, optional): A Path to the kadi properties file (xml or json).
            Defaults to "kadi_properties.xml" in current working directory.

    Raises:
        ValueError: For invalid file format.

    Returns:
        root_element: The root element of an XML element tree.
    """
    if file is None:
        file = Path.cwd() / DEFAULT_KADI_PROPERTY_FILE
    filetype = Path(file).suffix
    try:
        if filetype == ".xml":
            root_element = read_xml(file)
        elif filetype == ".json":
            root_element = json_to_xml(file)
        else:
            raise ValueError(
                "Invalid Kadi Property file format. Must in ['.xml', '.json']."
            )
    except (
        FileNotFoundError,
        json.decoder.JSONDecodeError,
        PermissionError,
        OSError,
    ):
        root_element = etree.Element(  # pylint: disable=c-extension-no-member
            ROOT_ELEMENT_NAME
        )
        workflow_element = etree.Element(  # pylint: disable=c-extension-no-member
            "workflow"
        )
        root_element.append(workflow_element)
    return root_element


def write_kadi_properties_file(file, root_element):
    file = Path(file)
    filetype = file.suffix
    if filetype == ".xml":
        write_xml(root_element, file)
    elif filetype == ".json":
        xml_to_json(root_element, out_file=file)
    else:
        raise ValueError(
            "Invalid Kadi Property file format. Must in ['.xml', '.json']."
        )


def set_node_properties(params, program_name, node_id=None, file=None, **kwargs):
    """Set a Kadi property in the current (last) node in the Kadi property file.

    Args:
        name (str): The name or key of the property to set or add.
        value (str, int, float, dict, list): The value of the property.
        dtype (str, optional): The property data type. Defaults to typestring of value.
        description (str, optional): A description for the property. Defaults to None.
        overwrite (bool, optional): Overwrite existing properties? Defaults to False.
        file (os.PathLike, optional): A Path to the kadi properties file (xml or json).
            Defaults to "kadi_properties.xml" in current working directory.

    Raises:
        ValueError: For invalid file format.
        ValueError: If property already exists and overwrite=False.
    """
    if file is None:
        file = Path.cwd() / DEFAULT_KADI_PROPERTY_FILE
    file = Path(file)
    # Read kadi properties
    root_element = read_kadi_properties_file(file)
    # Create new node and program
    workflow_element = root_element.find("workflow")
    node_id = node_id or len(workflow_element) - 1
    node_element = etree.Element("node")  # pylint: disable=c-extension-no-member
    node_element.set("id", node_id)
    # Wrap program inside node
    program_element = etree.Element("program")  # pylint: disable=c-extension-no-member
    program_element.set("name", program_name)
    program_element.set(
        "description",
        "Internal fallback to assign parameters, if workflow has no node yet.",
    )
    # Process parameters
    for p in params:
        # Extract parameter content
        name = p.get("name")
        dtype = p.get("dtype")
        value = p.get("value")
        description = p.get("description")
        # Create param property
        property_element = etree.Element(  # pylint: disable=c-extension-no-member
            "param"
        )
        # Set property values
        if dtype is None:
            dtype = type(value).__name__
        assert dtype in ["str", "int", "float", "dict", "list", "choice"]
        property_element.set("name", name)
        property_element.set("type", dtype)
        property_element.text = str(value)
        if description is not None:
            property_element.set("description", description)
        # Append property to program
        program_element.append(property_element)
    # Write kadi properties to file
    write_kadi_properties_file(file, root_element)


def set_environment_properties(params, file=None, **kwargs):
    """Set multiple Kadi properties for the current workflow environment.

    Args:
        name (str): The name or key of the property to set or add.
        value (str, int, float, dict, list): The value of the property.
        dtype (str, optional): The property data type. Defaults to typestring of value.
        description (str, optional): A description for the property. Defaults to None.
        overwrite (bool, optional): Overwrite existing properties? Defaults to False.
        file (os.PathLike, optional): A Path to the kadi properties file (xml or json).
            Defaults to "kadi_properties.xml" in current working directory.

    Raises:
        ValueError: For invalid file format.
        ValueError: If property already exists and overwrite=False.
    """
    if file is None:
        file = Path.cwd() / DEFAULT_KADI_PROPERTY_FILE
    file = Path(file)
    # Read kadi properties
    root_element = read_kadi_properties_file(file)
    # Process parameters
    for p in params:
        # Extract parameter content
        key = p.get("key")
        dtype = p.get("type")
        value = p.get("value")
        description = p.get("description")
        # Create param property
        property_element = root_element.find(f"environment[@key='{key}']")
        if property_element is None:
            # Create new property and append if not found
            property_element = etree.Element(  # pylint: disable=c-extension-no-member
                ENVIRONMENT_VARIABLE_NAME
            )
            root_element.append(property_element)
        # Set property values
        if dtype is None:
            dtype = type(value).__name__
        assert dtype in [
            "str",
            "int",
            "float",
            "bool",
            "date",
            "dict",
            "list",
            "choice",
        ]
        property_element.set("key", key)
        property_element.set("type", dtype)
        property_element.text = str(value)
        if description is not None:
            property_element.set("description", description)
    # Write kadi properties to file
    write_kadi_properties_file(file, root_element)


def read_xml(file):
    """Read an XML file to an element tree.

    Args:
        file (PathLike): A path to a file

    Returns:
        Element: the root of an XML element tree
    """
    file = Path(file)
    parser = etree.XMLParser(  # pylint: disable=c-extension-no-member
        remove_blank_text=True
    )
    xml_etree = parse(
        os.fspath(file), parser=parser, forbid_dtd=True, forbid_entities=True
    )
    return xml_etree.getroot()


def read_json(file):
    """Read a JSON file.

    Args:
        file (PathLike): A path to a file

    Returns:
        dict: the JSON dictionary
    """
    file = Path(file)
    with file.open("r", encoding="utf-8") as f:
        return json.load(f)


def write_xml(xml_elem, file):
    """Write an XML element tree to a file.

    Args:
        xml_elem (Element): The root of an XML element tree
        file (PathLike): A path to a file
    """
    file = Path(file)
    with file.open("w", encoding="utf-8") as f:
        write_string = format_xml(xml_elem, xml_declaration=True)
        f.write(write_string)
        f.write("\n")


def write_json(json_obj, file):
    """Write a JSON object or dictionary to a file.

    Args:
        json_obj (dict): A dictionary with JSON serializable data.
        file (PathLike): A path to a file
    """
    file = Path(file)
    with file.open("w", encoding="utf-8") as f:
        write_string = format_json(json_obj)
        f.write(write_string)
        f.write("\n")


def _json_badgerfish_to_kadi(obj, obj_name=None, remove_root=True, top_level="dict"):
    """Convert a badgerfish-convention JSON to a Kadi-convention JSON."""

    def _update_key(key):
        if key == "$":
            return "value"
        if key[0] == "@":
            return key[1:]
        return key

    element = {}
    if obj_name is not None:
        element["key"] = _update_key(obj_name)
    # Nested structures
    if isinstance(obj, dict):
        dtype = obj.get("@type")
        if dtype is None:
            # Plain dictionary
            element["type"] = "dict"
            element["value"] = [
                _json_badgerfish_to_kadi(v, obj_name=k, remove_root=False)
                for k, v in obj.items()
            ]
        else:
            # Kadi property wrapped in dictionary
            element["key"] = obj.get("@key")
            element["type"] = dtype
            element["value"] = obj.get("$")
    elif isinstance(obj, list):
        # Plain list
        element["type"] = "list"
        element["value"] = [_json_badgerfish_to_kadi(v, remove_root=False) for v in obj]
    else:
        # Plain metadatum
        element["type"] = obj.__class__.__name__
        element["value"] = obj
        if "unit" not in element and isinstance(obj, (int, float)):
            element["unit"] = None
    # Remove root element and ensure proper output structure
    if remove_root:
        if top_level == "dict":
            return {v["key"]: v["value"] for v in element["value"]}
        return element["value"]
    return element


def _json_kadi_to_badgerfish(element, remove_root=True):
    """Convert a Kadi-convention JSON to a badgerfish-convention JSON."""

    def _update_key(key):
        if key == "value":
            return "$"
        if key[0] != "@":
            return "@" + key
        return key

    # Nested structures
    if isinstance(element, list):
        obj_name = None
        obj = [_json_kadi_to_badgerfish(e, remove_root=True) for e in element]
    elif isinstance(element, dict):
        obj_name = element.get("key", None)
        element_type = element.get("type", "plain_dict")
        if element_type == "plain_dict":
            obj = {
                k: _json_kadi_to_badgerfish(v, remove_root=True)
                for k, v in element.items()
            }
        elif element_type == "dict":
            obj = dict(
                _json_kadi_to_badgerfish(e, remove_root=False) for e in element["value"]
            )
        elif element_type == "list":
            obj = [
                _json_kadi_to_badgerfish(e, remove_root=False)[1]
                for e in element["value"]
            ]
        else:
            # Attributes and values must be named differently
            # Type must be defined by parent element
            if obj_name is not None:
                obj_name = _update_key(obj_name)
            obj = {_update_key(k): v for k, v in element.items()}
    else:
        raise ValueError(
            "Elements must be lists or Kadi Properties with a key, type, and value"
        )
    if remove_root:
        return obj
    return obj_name, obj


def json_to_xml(json_obj, out_file=None, notation="kadi"):
    """Convert a json object or file to an XML etree object and writes it to given file.

    Args:
        json_obj (dict or Path): A JSON compatible file or object.
        out_file (PathLike): An optional output file for the XML element tree.
        notation (str): A JSON to XML conversion notation. Defaults to "kadi".
            One of "kadi", "kadiflow", "abdera", "badgerfish", "cobra", "gdata",
            "parker", "yahoo".

    Raises:
        ValueError: If unknown or invalid JSON notation

    Returns:
        xml_elem: The root of an XML element tree.
    """
    if isinstance(json_obj, os.PathLike):
        json_obj = read_json(json_obj)
    if notation == "kadi":
        json_obj = _json_kadi_to_badgerfish(json_obj)
        xml_etree = BadgerFish().etree(json_obj)
        xml_elem = xml_etree[0]
    else:
        raise ValueError(f"Invalid notation: {notation}")
    # Ensure single root element
    if len(xml_etree) == 1:
        xml_elem = xml_etree[0]
    else:
        xml_elem = etree.Element(  # pylint: disable=c-extension-no-member
            ROOT_ELEMENT_NAME
        )
        xml_elem.extend(xml_etree)
    # Write to file
    if out_file is not None:
        write_xml(xml_elem, out_file)
    return xml_elem


def xml_to_json(xml_elem, out_file=None, notation="kadi"):
    """Convert an XML element tree or file to a JSON object and writes it to given file.

    Args:
        xml_elem (Element or Path): The root of an XML element or XML file.
        out_file (PathLike): An optional output file for the JSON object.
        notation (str): A JSON to XML conversion notation. Defaults to "kadi".
            One of "kadi", "kadiflow", "abdera", "badgerfish", "cobra", "gdata",
            "parker", "yahoo".

    Raises:
        ValueError: If unknown or invalid JSON notation

    Returns:
        json_obj: JSON oject dictionary
    """
    if isinstance(xml_elem, os.PathLike):
        xml_elem = read_xml(xml_elem)
    if notation == "kadi":
        json_obj = BadgerFish().data(xml_elem)
        root_obj = json_obj[ROOT_ELEMENT_NAME]
        json_obj = _json_badgerfish_to_kadi(root_obj)
    else:
        raise ValueError(f"Invalid notation: {notation}")
    if out_file is not None:
        write_json(json_obj, out_file)
    return json_obj
