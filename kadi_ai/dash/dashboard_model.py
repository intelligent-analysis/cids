# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""The KadiAI model and training definition dashboard. Part of KadiAI."""
from pathlib import Path

import dash_bootstrap_components as dbc
import dash_core_components as dcc
from dash import html
from dash_extensions.enrich import DashProxy as Dash
from dash_extensions.enrich import MultiplexerTransform

from ..utility import _setup_model
from ..utility import _setup_project
from .constants import ExternalLinks as Link
from .constants import Messages as Msg
from .constants import WebAppIdentifiers as ID
from .hyperparameter_definitions import ModelDefinition
from .hyperparameter_definitions import TrainingDefinition
from .panels import editor_panel_builder
from .panels import exit_button_builder
from .panels import feature_select_panel_builder
from .panels import find_functions
from .panels import hyperparameter_panel_builder
from .panels import model_figure_panel_builder
from .panels import model_function_selection_panel_builder
from .panels import pull_button_builder
from .panels import push_button_builder
from .panels import titlebar_builder
from cids.tensorflow import model_functions as predefined_model_functions
from cids.tensorflow import training_functions as predefined_training_functions


def layout(
    app,
    project,
    model_definition,
    model_function_choices,
    training_definition,
    training_functions_choices,
):
    # Main content elements and callbacks
    titlebar = titlebar_builder(Msg.model_definition)
    pull_button = pull_button_builder(
        app, project, content=["tfrecords", "data_definition"]
    )
    push_button = push_button_builder(app, project, content=["results"])
    exit_button = exit_button_builder(app, project)
    feature_selection_panel = feature_select_panel_builder(app, model_definition)
    model_function_select_panel = model_function_selection_panel_builder(
        app,
        project,
        model_function_choices,
        model_definition,
        training_functions_choices,
        training_definition,
    )
    model_hyperparameter_panel = hyperparameter_panel_builder(app)
    model_editor_panel = editor_panel_builder(app, type="model")
    training_editor_panel = editor_panel_builder(app, type="training")
    model_figure = model_figure_panel_builder(app, project)

    # Global layout
    elements = [
        # Header
        html.Div(titlebar, className="titlebar"),
        html.Div(
            children=[html.H2()],
            className="p-3",
        ),
        html.Br(),
        html.Hr(),
        # Body
        html.Div(
            className="row p-3 justify-content-between",
            style={"max-height": "calc(100vh - 185px)", "overflow-y": "auto"},
            children=[
                # First column
                html.Div(
                    className="col-3 align-items-center justify-content-center",
                    children=[
                        feature_selection_panel,
                        html.Br(),
                        model_function_select_panel,
                    ],
                ),
                # Second column
                html.Div(
                    className="col-6 align-items-center justify-content-center",
                    children=[
                        dcc.Tabs(
                            children=[
                                dcc.Tab(
                                    label="Hyperparameters",
                                    children=[
                                        model_hyperparameter_panel,
                                    ],
                                ),
                                dcc.Tab(
                                    label="Model code", children=[model_editor_panel]
                                ),
                                dcc.Tab(
                                    label="Training code",
                                    children=[training_editor_panel],
                                ),
                            ],
                        )
                    ],
                ),
                # Third column
                html.Div(
                    className="col-3 align-items-center justify-content-center",
                    children=[model_figure],
                ),
            ],
        ),
        # Footer
        html.Div(
            className="footer",
            children=[
                html.Div(
                    className="row p-3 justify-content-between align-items-center",
                    children=[
                        html.Div(
                            className="col-2 text-start",
                            children=[pull_button],
                        ),
                        html.Div(
                            className="col-4",
                            children=[],
                        ),
                        html.Div(
                            className="col-2 text-end",
                            children=[push_button],
                        ),
                        html.Div(
                            className="col-2 offset-2 text-end",
                            children=[exit_button],
                        ),
                    ],
                )
            ],
        ),
    ]
    return html.Div(elements)


def setup_dash(**kwargs):

    # Setup project
    project = _setup_project(**kwargs)

    # Pull from Kadi
    project.pull(tfrecords=True, data_definition=True, force="update")

    # Get data definition and setup model
    data_definition = project.data_definition

    # # TODO: Something about features???
    feature_keys = data_definition.features.keys()
    if not data_definition.input_features:
        data_definition.input_features = list(feature_keys)[:-1]
    if not data_definition.output_features:
        data_definition.output_features = list(feature_keys)[-1:]

    # TODO: select CIDSModel classmethods
    cids_model = _setup_model(project, data_definition, **kwargs)

    # Get model functions
    model_functions, model_function_choices = find_functions(
        predefined_model_functions,
        file=Path(cids_model.base_model_dir) / "model_function.py",
    )

    # Get training functions
    training_functions, training_function_choices = find_functions(
        predefined_training_functions,
        file=Path(cids_model.base_model_dir) / "training_function.py",
    )

    # Create app
    external_stylesheets = [Link.font_awesome, dbc.themes.FLATLY]
    app = Dash(
        __name__,
        title=Msg.model_definition,
        external_stylesheets=external_stylesheets,
        transforms=[MultiplexerTransform()],
    )

    # Prepare internal variables
    app._hp_definitions = {}
    app._hp_panels = {}
    app._selected_fun = {}

    # Create all possible hyperparameter panels (model)
    app_model_definitions = {}
    app_panels = {}
    for mfc in model_function_choices:
        selected_fun = mfc["value"]
        model_definition = ModelDefinition(
            model_functions[selected_fun],
            selected_fun,
            ID.editor_model_function,
            data_definition,
        )
        hp_panel = model_definition.hyperparameters_to_html(app)
        app_model_definitions[selected_fun] = model_definition
        app_panels[selected_fun] = hp_panel
    app._hp_definitions["model"] = app_model_definitions
    app._hp_panels["model"] = app_panels

    # Create all possible hyperparameter panels (training)
    app_training_definitions = {}
    app_panels = {}
    app_panels_progress = {}
    for tfc in training_function_choices:
        selected_fun = tfc["value"]
        training_definition = TrainingDefinition(
            training_functions[selected_fun], selected_fun, ID.editor_training_function
        )
        hp_panel = training_definition.hyperparameters_to_html(app)
        app_training_definitions[selected_fun] = training_definition
        app_panels[selected_fun] = hp_panel
        app_panels_progress[selected_fun] = training_definition.phases_to_html(app)
    app._hp_definitions["training"] = app_training_definitions
    app._hp_panels["training"] = app_panels
    app._hp_panels["progress"] = app_panels_progress
    # app._my_app_callbacks = app_callbacks

    # Select first model function
    selected_fun = model_function_choices[0]["value"]
    model_definition = app._hp_definitions["model"][selected_fun]
    app._selected_fun["model"] = selected_fun

    # Select first training function
    selected_fun = training_function_choices[0]["value"]
    training_definition = app._hp_definitions["training"][selected_fun]
    app._selected_fun["training"] = selected_fun

    # Create layout
    app.layout = layout(
        app,
        project,
        model_definition,
        model_function_choices,
        training_definition,
        training_function_choices,
    )

    return app
