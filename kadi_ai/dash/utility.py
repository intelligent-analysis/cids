# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Utility functions for KadiAI dashboards. Part of KadiAI."""


def _validate_blank(df):
    styles = []
    for col in df.columns:
        styles.append(
            {
                "if": {"filter_query": f"{{{col}}} is blank", "column_id": col},
                "backgroundColor": "#FF6666",
                "color": "white",
            }
        )
    return styles


def _validate_data_shape(df):
    """
    The shape of the data (use None for unknown)
    """
    styles = []
    unvalid = []
    for data_shape in df["data_shape"]:
        if (
            ";" in data_shape
            or ",," in data_shape
            or data_shape.endswith(",")
            or data_shape.startswith(",")
        ):
            unvalid.append(data_shape)

    styles = []
    for data_shape in unvalid:
        styles.append(
            {
                "if": {
                    "filter_query": f'{{data_shape}} = "{data_shape}"',
                    "column_id": "data_shape",
                },
                "backgroundColor": "#FF6666",
                "color": "white",
            }
        )
    return styles


def _validate_data_format(df):
    """
    Validates a data format string that defines order of the axes:
        N: batch axis. Rule: Data format should always start with N
        F: Feature axis. Rule: Data format should always end with F
        S: Sequence axis
        X,Y,Z: spatial axis
    """
    styles = []
    unvalid = []
    allowed_chars = set("NSXYZF")
    for data_format in df["data_format"]:
        if (
            not data_format.startswith("N")
            or not data_format.endswith("F")
            or not set(data_format).issubset(allowed_chars)
        ):
            unvalid.append(data_format)
    for data_format in unvalid:
        styles.append(
            {
                "if": {
                    "filter_query": f'{{data_format}} = "{data_format}"',
                    "column_id": "data_format",
                },
                "backgroundColor": "#FF6666",
                "color": "white",
            }
        )
    return styles


def _validate_dtype(df):
    styles = []
    valid_types = [
        "string",
        "float",
        "float64",
        "float32",
        "double",
        "single",
        "int",
        "int64",
        "int32",
        "bool",
    ]
    # valid_types_ndarray = ["string"]  # TODO: handle ndarray (string to decode_str)
    for types in df["dtype"][~df["dtype"].isin(valid_types)]:
        styles.append(
            {
                "if": {"filter_query": f'{{dtype}} = "{types}"', "column_id": "dtype"},
                "backgroundColor": "#FF6666",
                "color": "white",
            }
        )
    return styles


def _validate_decode_str_to(df):
    styles = []
    valid_types = [
        "float",
        "float64",
        "float32",
        "double",
        "single",
        "int",
        "int64",
        "int32",
        "bool",
        "None",
    ]
    unvalid = df["decode_str_to"][~df["decode_str_to"].isin(valid_types)]
    for i in unvalid:
        styles.append(
            {
                "if": {
                    "filter_query": f'{{decode_str_to}} = "{i}"',
                    "column_id": "decode_str_to",
                },
                "backgroundColor": "#FF6666",
                "color": "white",
            }
        )
    return styles


def validate(df):
    styles = []
    if len(df) == 0:
        return styles

    styles.extend(_validate_blank(df))
    styles.extend(_validate_dtype(df))
    styles.extend(_validate_decode_str_to(df))
    styles.extend(_validate_data_shape(df))
    styles.extend(_validate_data_format(df))

    return styles
