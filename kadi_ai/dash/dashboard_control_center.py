# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""The KadiAI control center dashboard. Part of KadiAI."""
import dash
import dash_bootstrap_components as dbc
import pandas as pd
from dash import dcc
from dash import html

from ..utility import _setup_project
from .constants import ExternalLinks as Link
from .constants import Messages as Msg
from .panels import exit_button_builder
from .panels import titlebar_builder
from .visualization import _parse_search_data
from .visualization import dashplot_panel_builder
from .visualization import hiplot_panel_builder
from .visualization import tensorboard_panel_builder


def layout(app, project, search_df):
    # Main content elements and callbacks
    titlebar = titlebar_builder(Msg.control_center)
    tensorboard_panel = tensorboard_panel_builder()
    hiplot_panel = hiplot_panel_builder(app, project, search_df)
    dashplot_panel = dashplot_panel_builder(app, project, search_df)
    exit_button = exit_button_builder(app, project)

    # Global layout
    elements = [
        # Header
        html.Div(titlebar, className="titlebar"),
        html.Div(
            children=[html.H2()],
            className="p-3",
        ),
        html.Br(),
        html.Hr(),
        # Body
        dcc.Tabs(
            children=[
                dcc.Tab(label="TensorBoard", children=[tensorboard_panel]),
                dcc.Tab(label="HiPlot", children=[hiplot_panel]),
                dcc.Tab(label="dashPlot", children=[dashplot_panel]),
            ],
        ),
        # Footer
        html.Div(
            className="footer",
            children=[
                html.Div(
                    className="row p-3 justify-content-between align-items-center",
                    children=[
                        html.Div(
                            className="col-5 text-start",
                            # children=execute_panel,
                        ),
                        html.Div(
                            className="col-3 offset-1 text-start",
                            # children=[push_button],
                        ),
                        html.Div(className="col-3 text-end", children=[exit_button]),
                    ],
                )
            ],
        ),
    ]
    return html.Div(elements)


def setup_dash(**kwargs):

    # Create project
    project = _setup_project(**kwargs)

    # Pull from Kadi
    if kwargs.get("result_identifier"):
        project.pull(results=True)
    # TODO: push after training / search if controlled here

    # Get search results
    search_results = _parse_search_data(
        project.result_dir, search_parameters=kwargs.get("search_parameters", "")
    )
    search_df = pd.DataFrame(search_results)

    # Start tensorboard daemon
    project.start_tensorboard_daemon(port="3334")

    # Setup dashboard
    external_stylesheets = [Link.font_awesome, dbc.themes.FLATLY]
    app = dash.Dash(
        __name__,
        title=Msg.control_center,
        external_stylesheets=external_stylesheets,
    )
    app.layout = layout(app, project, search_df)

    return app
