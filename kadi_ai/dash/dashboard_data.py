# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""The KadiAI data definition dashboard. Part of KadiAI."""
import dash_bootstrap_components as dbc
from dash import html
from dash_extensions.enrich import DashProxy as Dash
from dash_extensions.enrich import MultiplexerTransform
from kadi_apy.lib.exceptions import KadiAPYInputError
from kadi_apy.lib.exceptions import KadiAPYRequestError

from ..utility import _setup_project
from .constants import ExternalLinks as Link
from .constants import Messages as Msg
from .panels import execute_panel_builder
from .panels import exit_button_builder
from .panels import file_glob_selection_panel_builder
from .panels import preprocessing_panel_builder
from .panels import pull_button_builder
from .panels import push_button_builder
from .panels import titlebar_builder
from .visualization import after_datatable_panel_builder
from .visualization import before_datatable_panel_builder
from .visualization import visualization_panel_builder
from cids.data.preprocessor import Preprocessor


def layout(
    app,
    project,
    src_dir,
    samples,
    preprocessing_functions,
):
    # Memory
    app._project = project
    app._samples = samples

    # Main content elements and callbacks
    titlebar = titlebar_builder(Msg.data_definition)
    file_glob_selection_panel = file_glob_selection_panel_builder(app, src_dir)
    before_datatable_panel = before_datatable_panel_builder(app)
    after_datatable_panel = after_datatable_panel_builder(app)
    preprocessing_panel = preprocessing_panel_builder(app, preprocessing_functions)
    execute_panel = execute_panel_builder(app, project)
    after_visualization_panel = visualization_panel_builder(app)
    exit_button = exit_button_builder(app, project)
    pull_button = pull_button_builder(app, project, content=["src"])
    push_button = push_button_builder(
        app, project, content=["tfrecords", "data_definition"]
    )

    # Global layout
    elements = [
        # Header
        html.Div(titlebar, className="titlebar"),
        html.Div(
            children=[html.H2()],
            className="p-3",
        ),
        html.Br(),
        html.Hr(),
        # Body
        file_glob_selection_panel,
        html.Div(
            className="row p-3 justify-content-between",
            style={"max-height": "calc(100vh - 185px)", "overflow-y": "auto"},
            children=[
                # First column
                html.Div(
                    className="col-6",
                    children=[
                        html.H3("Features before preprocessing"),
                        before_datatable_panel,
                        html.Br(),
                        preprocessing_panel,
                        html.Div(style={"height": "20vh"}),
                    ],
                ),
                # Second column: after preprocessing
                html.Div(
                    className="col-6",
                    children=[
                        html.H3("Features after preprocessing"),
                        after_datatable_panel,
                        html.Br(),
                        after_visualization_panel,
                        html.Div(style={"height": "20vh"}),
                    ],
                ),
            ],
        ),
        # Footer
        html.Div(
            className="footer",
            children=[
                html.Div(
                    className="row p-3 justify-content-between align-items-center",
                    children=[
                        html.Div(
                            className="col-2 text-start",
                            children=[pull_button],
                        ),
                        html.Div(
                            className="col-4 text-center",
                            children=[execute_panel],
                        ),
                        html.Div(
                            className="col-2 text-end",
                            children=[push_button],
                        ),
                        html.Div(
                            className="col-2 offset-2 text-end",
                            children=[exit_button],
                        ),
                    ],
                )
            ],
        ),
    ]
    return html.Div(children=elements)


def setup_dash(**kwargs):
    # Instantiate project, record and meta data handler
    project = _setup_project(**kwargs)
    # Pull available data
    # TODO: read and preprocess tfrecords anew (derivative projects)
    # try:
    #     project.pull(tfrecords=True, data_definition=True, force="update")
    #     src_dir = project.tfrecord_dir
    # except (KadiAPYInputError, KadiAPYRequestError):
    if project.src_identifier:
        try:
            project.pull(src=True, force="update")
        except (KadiAPYInputError, KadiAPYRequestError) as e2:
            raise ValueError(
                "No valid tfrecord or src identifier found. "
                + "Ensure valid identifiers for the Kadi instance are given."
            ) from e2
    src_dir = project.src_dir

    # Samples will be created later
    samples = []

    # Get preprocessing_functions
    preprocessing_functions = [
        getattr(Preprocessor, f) for f in sorted(Preprocessor.REGISTERED_FUNCTIONS)
    ]

    # Setup dashboard
    external_stylesheets = [Link.font_awesome, dbc.themes.FLATLY]
    app = Dash(
        __name__,
        title=Msg.data_definition,
        external_stylesheets=external_stylesheets,
        transforms=[MultiplexerTransform()],
    )
    app.layout = layout(app, project, src_dir, samples, preprocessing_functions)

    return app
