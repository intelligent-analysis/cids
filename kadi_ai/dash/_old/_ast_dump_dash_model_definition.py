import inspect
from ast import dump as astdump
from ast import parse as astparse
from pickle import dump as pickledump

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input
from dash.dependencies import Output
from flask import request

from cids.tensorflow import model_functions


def _find_model_functions():
    return {
        name: fun
        for name, fun in model_functions.__dict__.items()
        if callable(fun) and name.endswith("function") and not name.startswith("_")
    }


def layout(**kwargs):
    # Dropdown options
    training_functions = _find_model_functions()
    options = [
        {"label": key.replace("_", " ").capitalize(), "value": key}
        for key, value in training_functions.items()
    ]
    # Assemble
    elements = [
        html.H2("Model Definition"),
        dcc.Dropdown(
            id="dropdown-model-definition", options=options, value=options[0]["value"]
        ),
        dcc.Markdown(id="dd-model-definition-container", style={"width": "100%"}),
        html.Button("Save & Exit", id="save-model-definition", n_clicks=0),
    ]
    return html.Div(elements)


def shutdown():
    func = request.environ.get("werkzeug.server.shutdown")
    if func is None:
        raise RuntimeError("Not running with the Werkzeug Server")
    func()


def setup_dash(**kwargs):

    # Read kwargs
    # project_name = kwargs["project_name"]
    # project_dir = kwargs["project_dir"]
    # # kadi_data_identifier = kwargs["kadi_data_identifier"]
    #
    # # Imports
    # from kadi.flow import _setup_project
    # project = _setup_project(**kwargs)

    # TODO: get definition from kadi record?

    app = dash.Dash(__name__)  # external_stylesheets=external_stylesheets)
    app.layout = layout(**kwargs)

    @app.callback(
        Output("dd-model-definition-container", "children"),
        [
            Input("dropdown-model-definition", "value"),
            Input("save-model-definition", "n_clicks"),
        ],
    )
    def update_output(value, save):

        ctx = dash.callback_context
        triggered = ctx.triggered[0]["prop_id"].split(".")[0]

        all_functions = _find_model_functions()
        source_code = inspect.getsource(all_functions[value])
        imports = inspect.getsource(model_functions)
        imports = "\n".join(
            [
                line
                for line in imports.split("\n")
                if line.startswith("import") or line.startswith("from")
            ]
        )

        if triggered == "save-model-definition":

            # TODO: serialize
            print(imports)
            print(source_code)

            project_dir = kwargs.get("project_dir")
            completeCode = imports + "\n" + source_code

            # writing model to .py file
            with open(
                project_dir + r"\custom_model.py", "w", encoding="utf8"
            ) as model_file:
                # model_file.write(imports + '\n')
                # model_file.write(source_code)
                model_file.write(completeCode)

            ast_tree = astparse(completeCode)

            # writing model to .ast file
            with open(
                project_dir + r"\custom_model.ast", "w", encoding="utf8"
            ) as model_file:
                ast_string = astdump(ast_tree)
                model_file.write(ast_string)

            # serializing ast to binary
            with open(
                project_dir + r"\custom_model_ast.binary", "wb", encoding="utf8"
            ) as model_file:
                pickledump(ast_tree, model_file)

            shutdown()

        return f"```py\n{imports}\n\n{source_code}```"

    return app
