# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""(Experimental) Wrap tensorflow layers as kadi workflow nodes for manual assembly."""
import inspect
import os

from jinja2 import Environment
from jinja2 import FileSystemLoader
from jinja2 import select_autoescape
from tensorflow.keras import layers as keras_layers

with open(
    os.path.join(os.path.dirname(__file__), "cids", "_version.py"), encoding="utf8"
) as f:
    exec(f.read())  # pylint: disable=exec-used


def wrap_layers():

    # Jinja2 environment
    env = Environment(
        loader=FileSystemLoader(
            os.path.join(os.path.dirname(__file__), "kadi", "templates")
        ),
        autoescape=select_autoescape(["py"]),
    )
    template = env.get_template("layer.py.tpl")

    # Loop over all layers
    all_layers = []
    all_layers.extend(inspect.getmembers(keras_layers, inspect.isclass))

    # Filter layers
    all_layers = [layer for layer in all_layers if "Cell" not in layer[0]]
    wrapped_layers = []
    for layer in all_layers:

        # Extract command information
        cmd_name = layer[0]
        docstring = inspect.getdoc(layer[1])
        if hasattr(layer[1], "call"):
            call_docstring = inspect.getdoc(layer[1].call)
        else:
            call_docstring = ""
        cmd_description = docstring.split("\n", maxsplit=1)[0]
        if cmd_description[-1] == "," or cmd_description[-1] == ".":
            cmd_description = cmd_description[:-1]

        # Extract parameter information and wrap function reversely
        if hasattr(layer[1], "call"):
            signature = inspect.signature(layer[1].call)
            parameters = list(signature.parameters.items())
            outputs = signature.return_annotation
            if outputs == inspect._empty:
                outputs = []
        else:
            parameters = []
            outputs = []
        if not outputs:
            outputs = ["outputs"]  # Layers always have an output
        options = inspect.signature(layer[1].__init__).parameters
        parameters.extend(options.items())

        cmd_parameters = []
        for parameter_name, parameter in parameters:
            if parameter_name in ["self", "kwargs", "args"]:
                continue
            parameter_default = parameter.default
            if isinstance(parameter_default, (str)):
                parameter_default = '"' + parameter_default + '"'
            parameter_required = parameter_default == inspect._empty
            if parameter_required:
                parameter_default = None
            parameter_is_flag = isinstance(parameter_default, bool)
            # Extract Parameter description
            try:
                parameter_desc = docstring.split(parameter_name + ": ")[1].split("\n")[
                    0
                ]
            except IndexError:
                try:
                    parameter_desc = call_docstring.split(parameter_name + ": ")[
                        1
                    ].split("\n")[0]
                except IndexError:
                    parameter_desc = "empty"
            if parameter_desc:
                if parameter_desc[-1] == "," or parameter_desc[-1] == ".":
                    parameter_desc = parameter_desc[:-1]
                parameter_desc = parameter_desc.replace("/", "")

            # Gather parameters
            cmd_parameters.append(
                (
                    parameter_name,
                    parameter_desc,
                    parameter_default,
                    parameter_required,
                    parameter_is_flag,
                )
            )

        cmd_outputs = []
        for output_name in outputs:
            output_required = False
            output_default = None
            output_desc = "Output"

            # Gather outputs
            cmd_outputs.append(
                (output_name, output_desc, output_default, output_required)
            )

        # Set Jinja2 context
        context = {
            "cmd_name": cmd_name,
            "cmd_description": cmd_description,
            "cmd_parameters": cmd_parameters,
            "cmd_outputs": cmd_outputs,
        }

        # Wrap and save
        output = template.render(context)
        with open(
            os.path.join(
                os.path.dirname(template.filename),
                "..",
                "_generated",
                cmd_name.lower() + ".py",
            ),
            "w",
            encoding="utf8",
        ) as f:
            f.write(output)

        wrapped_layers.append(
            (cmd_name, "kadi._generated." + cmd_name.lower() + ":execute")
        )

    return wrapped_layers


if __name__ == "__main__":
    wrapped_layers = wrap_layers()
