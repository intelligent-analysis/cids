# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Legacy AI project management functionality. Part of KadiAI."""
import inspect
import os
from warnings import warn

from .project import KadiAIProject


# Placeholder for global project
global_project = None


def setup_project(
    project_root=True, return_project_dir=False, verbose=True, external=True
):
    """Returns project and ensures existing directories."""
    warn("projects.setup_project is deprecated. " + "Update to cids.projects.Project")
    assert external, "Keyword argument external=False is deprecated."
    # Get project-relative path, and project name
    stack = inspect.stack()
    frame = stack[1]
    module = inspect.getmodule(frame[0])
    file = module.__file__
    dir = os.path.dirname(file)
    project_abs_path = os.path.normpath(dir)
    project_relative_path = project_abs_path.split("demos" + os.sep)[-1]
    project_relative_path = [d for d in project_relative_path.split(os.sep) if d != ""]
    project_relative_path = os.path.join(*project_relative_path)
    project_name = os.path.basename(project_relative_path)
    # Define project paths
    global global_project  # pylint: disable=global-statement
    if project_root:
        project_dir = os.path.join("..", "DATA", project_relative_path)
        result_dir = os.path.join(project_dir, "RESULTS")
        data_dir = os.path.join(project_dir, "INPUTS")
        # Fallback to global project
        global_project = KadiAIProject(
            project_name, seed=None, log_file=None, root=project_dir
        )
    else:
        result_dir = os.path.join("..", "DATA", "RESULTS", project_relative_path)
        data_dir = os.path.join("..", "DATA", "INPUTS", project_relative_path)
        # Fallback to global project
        global_project = KadiAIProject(
            project_name, seed=None, log_file=None, root=result_dir
        )
        global_project.input_dir = data_dir
        global_project.result_dir = result_dir
    # Create directories
    os.makedirs(result_dir, exist_ok=True)
    os.makedirs(data_dir, exist_ok=True)
    try:
        os.chmod(data_dir, 0o777)
        os.chmod(result_dir, 0o777)
    except PermissionError:
        # Was created previously by other owner
        pass
    # Return directories
    if return_project_dir:
        if verbose:
            print("project name:", project_name)
            print("project dir:", project_dir)
            print("result dir:", result_dir)
            print("data dir:", data_dir)
        return project_name, project_dir, result_dir, data_dir
    if verbose:
        print("project name:", project_name)
        print("result dir:", result_dir)
        print("data dir:", data_dir)
    return project_name, result_dir, data_dir


def log_to(path):
    warn("projects.log_to is deprecated. " + "Update to cids.projects.Project")
    if global_project is not None:
        assert isinstance(global_project, KadiAIProject)
        global_project.log_file = path
        global_project.start_logging()
    else:
        raise ValueError(
            "Project not set-up (project.Project or projects.setup_project)."
        )
