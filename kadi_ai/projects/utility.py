# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Utility functionality for KadiAI projects. Part of KadiAI."""
import os
import random
import sys
from pathlib import Path
from zlib import adler32

import numpy as np
import tensorflow as tf
from requests import get
from requests.exceptions import ConnectionError
from requests.exceptions import Timeout


def get_env():
    """Retrieves filename and folder of caller file, if possible.

    Can be used to determine if the Google Colab python kernel, an ipython
    kernel or a python kernel is running.
    """
    try:
        get_ipython()  # Already imported in jupyter notebooks
    except NameError:
        # If get_ipython is not loaded, we are probably in a python script.
        # Python script name can be read with the following command.
        # If The import occurs from a python shell, filename is stdin.
        env = "python"
        possible_paths = [a for a in sys.argv if os.path.isfile(a)]
        if possible_paths:
            runscript = Path(possible_paths[0]).stem
        else:
            runscript = ""
        return env, runscript
    try:
        # Try for google colab
        response = get("http://172.28.0.2:9000/api/sessions", timeout=0.1)
    except (
        Timeout,
        ConnectionError,
        OSError,
    ):
        # Notebook names of default jupyter notebooks canot be read
        env = "ipython"
        runscript = "jupyter"
        return env, runscript
    # Notebook names of google colab notebooks can be read
    env = "colab"
    runscript = response.json()[0]["name"]
    if "." in runscript:
        runscript = runscript.split(".", maxsplit=1)[0]
    return env, runscript


def set_project_seeds(seed):
    """Sets the random, numpy, and tensorflow seeds."""
    if isinstance(seed, str):
        seed = seed.encode()
        seed = adler32(seed)
    random.seed(seed)
    np.random.seed(seed)
    tf.random.set_seed(seed)
    return seed
