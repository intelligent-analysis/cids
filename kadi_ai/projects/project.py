# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""KadiAI project functionality for linked structured data in Kadi. Part of KadiAI.

    Classes:
        KadiAIProject:  Project that allows synchronization with Kadi repositories.

"""
import copy
import json
import os
import shutil
from datetime import datetime
from datetime import timezone
from pathlib import Path

import dateutil
from kadi_apy import KadiAPYInputError
from kadi_apy import KadiAPYRequestError
from kadi_apy import KadiManager

from ..repo.record import KadiAIRecord
from .base import BaseProject
from .metadata import TrainingResults


class KadiAIProject(BaseProject):
    def __init__(
        self,
        name,
        seed="name",
        log_file="auto",
        root=None,
        runscript="",
        identifier_prefix="cids",
        gitcommit=None,
        _params=None,
        src_identifier=None,
        tfrecord_identifier=None,
        result_identifier=None,
        **kwargs,
    ):
        """KadiAI project allows synchronization with Kadi repositories.

        Args:
            name (str): name of the project
            seed (str, optional): seed for random state. Defaults to "name".
            log_file (PathLike, optional): path to a log file. Defaults to "auto".
            root (PathLike, optional): path to a project directory. Defaults to None.
            runscript (str, optional): name of the runscript. Defaults to "".
            identifier_prefix (str, optional): prefix for Kadi record identifiers.
                Defaults to "cids".
            gitcommit (str, optional): hash of a git commit. Defaults to None.
            src_identifier (str, optional): Kadi record identifier of source/raw data.
                Defaults to None.
            tfrecord_identifier (str, optional): Kadi record identifier of converted
                tfrecord data. Defaults to None.
            result_identifier (str, optional): Kadi record identifier for AI result
                data. Defaults to None.
        """
        # Identifiers
        self.identifier_prefix = identifier_prefix
        self.src_identifier = src_identifier
        self.tfrecord_identifier = tfrecord_identifier
        self.result_identifier = result_identifier
        # Tags
        self.tag_namespace = "kadiai"
        self.project_tag = f"{self.tag_namespace}:project"
        self.cids_model_tag = f"{self.tag_namespace}:cidsmodel"
        self.model_tag = f"{self.tag_namespace}:model"
        self.src_tag = f"{self.tag_namespace}:src"
        self.tfrecord_tag = f"{self.tag_namespace}:dataset"
        self.preprocessor_tag = f"{self.tag_namespace}:preprocessor"
        self.trial_tag = f"{self.tag_namespace}:trial"
        self.evaluation_tag = f"{self.tag_namespace}:evaluation"
        self.results_base_tag = f"{self.tag_namespace}:results"
        self.result_types = ["search", "training"]
        # Links
        self.link_namespace = "cids"
        self.conversion_link = f"{self.link_namespace}:converted-from"
        self.preprocessor_src_link = f"{self.link_namespace}:converts"
        self.preprocessor_tfrecord_link = f"{self.link_namespace}:produces"
        self.training_link = f"{self.link_namespace}:trained-on"
        self.search_link = f"{self.link_namespace}:searched-using"
        self.cidsmodel_result_link = f"{self.link_namespace}:executes"
        self.cidsmodel_model_link = f"{self.link_namespace}:generates"
        self.creates_link = f"{self.link_namespace}:creates"
        self.model_training_link = f"{self.link_namespace}:trains"
        self.trial_model_link = f"{self.link_namespace}:defines"
        self.trial_trial_link = f"{self.link_namespace}:continues"
        self.trial_training_link = f"{self.link_namespace}:requests"
        self.search_trial_link = f"{self.link_namespace}:selects"
        self.evaluation_model_link = f"{self.link_namespace}:evaluates"
        self.project_training_link = (
            f"{self.link_namespace}:defines_training_and_validation_data"
        )
        self.project_evaluation_link = f"{self.link_namespace}:defines_test_data"
        # CIDS metadata
        self.md_namespace = "cids"
        self.md_last_modified = f"{self.md_namespace}:last-modified"
        self.md_subdirectory = f"{self.md_namespace}:subdirectory"
        self.md_hash = f"{self.md_namespace}:hash"
        super().__init__(
            name,
            seed=seed,
            log_file=log_file,
            root=root,
            gitcommit=gitcommit,
            _params=_params,
            **kwargs,
        )

    def __eq__(self, other):
        if not isinstance(other, KadiAIProject):
            return False
        for k, v in self.__dict__.items():
            if (
                any(
                    x in k
                    for x in [
                        "_handler",
                        "log_file",
                        "_timestamp",
                        "_identifier",
                        "_root_dir",
                    ]
                )
                or k[0] == "_"
            ):
                continue
            v2 = other.__dict__.get(k, None)
            if v2 != v:
                return False
        return True

    @property
    def base_identifier(self):
        return self._unify_identifier("-".join([self.identifier_prefix, self.name]))

    def _unify_identifier(self, identifier):
        unified = identifier.replace("_", "-").replace(".", "-").replace(":", "-")
        return "".join([s for s in unified if s.isalnum() or s == "-"])

    @staticmethod
    def _depaginate_response(paginated_response_function, *args, **kwargs):
        response = paginated_response_function(*args, page=1, **kwargs)
        items = response.json()["items"]
        total_pages = response.json()["_pagination"]["total_pages"]
        for p in range(2, total_pages + 1):
            new_response = paginated_response_function(*args, page=p, **kwargs)
            items.extend(new_response.json()["items"])
        return items

    def _assemble_identifier(self, identifier, default_id=None, default_str=""):
        # List of identifiers
        if isinstance(identifier, (list, tuple)) and isinstance(identifier[0], str):
            return identifier
        # Single identifier
        if isinstance(identifier, str):
            return identifier
        # Assemble default identifier instead
        if default_id is not None:
            return default_id
        if default_str:
            return "-".join([self.base_identifier, default_str])
        raise ValueError("No valid identifier combination found.")

    def _get_unique_object_identifier(
        self, object_dir, object_records, object_type, base_identifier=None
    ):
        if base_identifier is None:
            if object_type in self.result_types:
                base_identifier = f"{self.base_identifier}-results"
            else:
                base_identifier = self.base_identifier
        object_record_dict = {}
        for idx, object_record in enumerate(object_records):
            try:
                object_record_dict[
                    object_record.get_metadatum(self.md_subdirectory)["value"]
                ] = object_record
            except TypeError:
                # Record is corrupt and missing some necessary metadata
                # Delete record to clean up
                manager = KadiManager(instance=None, host=None, token=None)
                object_record.delete()
                manager.misc.purge("record", object_record.id)
                object_records.pop(idx)
        local_subdir = self._to_relative_paths(object_dir, root_dir=self.result_dir)
        try:
            # Assume record already exists
            record = object_record_dict[local_subdir.as_posix()]
            identifier = record.identifier
        except KeyError:
            # No record exists. Find new unique name
            # TODO: check if other record of that identifier exists
            #       (they may be not in collection)
            num_records_of_type = len(object_records) + 1
            candidates = {
                f"{base_identifier}-{object_type}{i:03d}"
                for i in range(num_records_of_type)
            }
            occupied = {record.identifier for record in object_records}
            unoccupied = sorted(candidates - occupied)
            identifier = unoccupied[0]
        return identifier, object_records

    def _assemble_type_tag(self, tag_type, base_tag=None):
        if base_tag is None:
            base_tag = self.results_base_tag
        return f"{base_tag}-{tag_type}"

    @staticmethod
    def _extract_type(type_tag):
        return "".join(s for s in type_tag.split("-")[-1] if s.isalpha())

    def _pull_record(
        self,
        record,
        directory,
        default_id=None,
        default_str="",
        force=False,
        project=False,
    ):
        directory = Path(directory)
        if not isinstance(record, KadiAIRecord):
            identifier = self._assemble_identifier(
                record, default_id=default_id, default_str=default_str
            )
            record = KadiAIRecord.use_config(identifier, create=False)
        # Results are stored in separate subdirectories and receive individial records
        if directory.is_relative_to(self.result_dir):
            subdir = record.get_metadatum(self.md_subdirectory)["value"]
            directory = directory / Path(subdir)
        if project:
            if os.path.isfile(self.project_file):
                local_project = KadiAIProject.from_json(self.project_file)
            else:
                self._download_projectjson_from_kadiprojectrecord(record, directory)
                return record
        if directory.exists() and any(directory.iterdir()):
            # Directory exists and is not empty
            self.warn(f"Local directory {os.fspath(directory)} for pull is not empty.")
            if not force:
                # Only force overwrite, if any strategy is defined
                self.warn(
                    "...no files downloaded. Set force=True, force='all', "
                    + "force='update' to overwrite, delete and redownload, or update "
                    + "based on file dates."
                )
                return record
            if force == "update":
                if (
                    project
                    and not self._check_equality_of_localproject_and_kadiproject(
                        record, local_project
                    )
                ):
                    self.warn(
                        "...projects are not the same. Project record "
                        + "was not pulled to prevent overwriting different project."
                    )
                    return record
                # Update strategy to force overwrite
                local_timestamp = self._extract_directory_content_timestamp(directory)
                kadi_timestamp = self._extract_kadi_content_timestamp(record)
                if not project:
                    equal_hash = self._check_equality_of_local_and_kadi_hash(
                        record, directory
                    )
                else:
                    equal_hash = self._check_equality_of_local_and_kadi_hash(
                        record, self.project_file
                    )
                self.warn(
                    "...attempting to update local files based on last content change."
                )
                if local_timestamp < kadi_timestamp and not equal_hash:
                    # Delete all old files
                    shutil.rmtree(directory)
                    self.warn("...local files are outdated. Pulling record files.")
                elif equal_hash or local_timestamp == kadi_timestamp:
                    self.warn("...local files are up-to-date. No changes necessary.")
                    return record
                else:
                    self.warn(
                        "...local files are newer than record. Push to update record."
                    )
                    return record
            elif force == "all":
                # All strategy to force overwrite: delete old files
                self.warn("...deleting local files and pulling record files.")
                shutil.rmtree(directory)
            else:
                # Fallback strategy to force overwrite: only if file exists in record
                self.warn(
                    "...pulling record files and overwriting corresponding local files."
                )
        # Download files
        if not project:
            record.auto_download_dataset(directory)
        else:
            self._download_projectjson_from_kadiprojectrecord(record, directory)
        return record

    def pull(
        self,
        src=None,
        tfrecords=None,
        results=None,
        data_definition=True,
        project=False,
        force=False,
    ):
        """Download project from Kadi."""
        # TODO: read links to find compatible data definition or convert script
        # Extract project name
        try:
            # Project Json
            self._pull_record(
                project, self.root_dir, default_str="project", force=force, project=True
            )
        except KadiAPYInputError:
            self.warn("The project record for this project is not on Kadi.")
        # Source data
        if src:
            # Pull record with identifier
            src_record = self._pull_record(
                src,
                self.src_dir,
                default_id=self.src_identifier,
                default_str="src",
                force=force,
            )
            # Memorize identifier
            self.src_identifier = src_record.identifier
        # Converted tfrecords
        if tfrecords:
            # Pull record with identifier
            tfrecord_record = self._pull_record(
                tfrecords,
                self.tfrecord_dir,
                default_id=self.tfrecord_identifier,
                default_str="tfrecords",
                force=force,
            )
            # Memorize identifier
            self.tfrecord_identifier = tfrecord_record.identifier
            # Data definition
            if data_definition:
                tfrecord_record.datadefinition_to_kadirecord(
                    self.data_definition, upload=True
                )
                data_definition = tfrecord_record.kadirecord_to_datadefinition()
                self.data_definition = data_definition
        # Results
        if results:
            # Find result records in Kadi
            if isinstance(results, str):
                # Variable is a collection identifier
                result_records = self._find_records_by_type(
                    collection_identifier=results, create=False, type="results"
                )
            else:
                result_records = self._find_records_by_type(
                    create=False, type="results"
                )
            # Pull record(s) with identifier
            for _, records in result_records.items():
                for record in records:
                    self._pull_record(
                        record,
                        self.result_dir,
                        force=force,
                    )
            # Memorize identifier
            self.result_identifier = {
                k: [r.identifier] for k, v in result_records.items() for r in v
            }
            # Project
            if project:
                # TODO: get project json?
                pass

    def _push_record(
        self,
        record,
        directory,
        default_id=None,
        default_str="",
        force=False,
        exclude=(".*", "_*"),
        root_dir=None,
        collection=None,
        **kwargs,
    ):
        if root_dir is None:
            root_dir = directory
        # Maybe get kadi record
        if not isinstance(record, KadiAIRecord):
            identifier = self._assemble_identifier(
                record, default_id=default_id, default_str=default_str
            )
            try:
                record = KadiAIRecord.use_config(identifier, create=False)
                # Record already exists
                self.warn(f'Target record "{identifier}" for push already exists.')
                if not force:
                    self.warn(
                        "...no update. Set force=True to overwrite existing files,"
                        + " force='all' to replace with only local files,"
                        + " or force='update' to update only newer files."
                    )
                    return record
            except KadiAPYInputError:
                # Record does not exist yet, can create new
                record = KadiAIRecord.use_config(identifier, create=True)
        # Edit type and tags
        record.edit(**kwargs)
        # Find local paths
        paths, local_subdir = self._get_local_paths_by_object(
            directory, root_dir, exclude, kwargs["type"]
        )
        try:
            kadi_subdir = Path(record.get_metadatum(self.md_subdirectory)["value"])
            assert local_subdir == kadi_subdir, (
                "Kadi and local subdirectory do not match: "
                + f"{local_subdir.as_posix()} != {kadi_subdir.as_posix()}"
            )
        except (KadiAPYInputError, TypeError):
            pass
        # Get record files
        record_files = record.auto_get_files()
        if record_files:
            # Handle pre-existing files
            self.warn(f"Kadi record {record.identifier} for push is not empty.")
            if not force:
                self.warn(
                    "...no files uploaded. Set force=True, force='all', "
                    + "force='update' to overwrite, delete and reupload, or update "
                    + "based on file dates."
                )
                return record
            if force == "all":
                # Delete all old files
                self.warn("...deleting old record files and pushing local files.")
                for rf in record_files:
                    record.delete_file(rf["id"])
            elif force == "update":
                if (
                    default_str == "project"
                    and not self._check_equality_of_localproject_and_kadiproject(
                        record, self
                    )
                ):
                    self.warn(
                        "...projects are not the same. Record "
                        + "was not updated to prevent overwriting different project."
                    )
                    return record
                if default_str != "cidsmodel":
                    local_timestamp = self._extract_directory_content_timestamp(
                        directory
                    )
                else:
                    cidmodel_json_path = Path(directory) / "cidsmodel.json"
                    if os.path.isfile(cidmodel_json_path):
                        local_timestamp = self._extract_file_timestamp(
                            cidmodel_json_path
                        )
                    else:
                        local_timestamp = self._extract_directory_content_timestamp(
                            directory
                        )
                kadi_timestamp = self._extract_kadi_content_timestamp(record)
                if default_str != "project":
                    equal_hash = self._check_equality_of_local_and_kadi_hash(
                        record, directory
                    )
                else:
                    equal_hash = self._check_equality_of_local_and_kadi_hash(
                        record, self.project_file
                    )
                self.warn(
                    "...attempting to update record files based on last content change."
                )
                if kadi_timestamp < local_timestamp and not equal_hash:
                    # Delete all old files
                    self.warn("...record files are outdated. Pushing local files.")
                    for rf in record_files:
                        record.delete_file(rf["id"])
                elif equal_hash or local_timestamp == kadi_timestamp:
                    self.warn("...record files are up-to-date. No changes necessary.")
                    return record
                else:
                    self.warn(
                        "...record files are newer than local files. Pull to update "
                        + "local files."
                    )
                    return record
            else:
                self.warn("...replacing files in record with local files of same name.")
        # Upload
        record.auto_upload_dataset(*paths, force=True)
        # Add cids own metadata
        if default_str == "project":
            timestamp = self._extract_file_timestamp(self.project_file)
            object_hash = KadiAIRecord.calculate_hash(self.project_file)
        elif default_str == "cidsmodel":
            cidmodel_json_path = Path(self.result_dir, directory, "cidsmodel.json")
            if cidmodel_json_path.is_file():
                timestamp = self._extract_file_timestamp(cidmodel_json_path)
                object_hash = KadiAIRecord.calculate_hash(cidmodel_json_path)
            else:
                cidsmodel_dir = Path(os.path.join(self.result_dir, directory))
                timestamp = self._extract_directory_content_timestamp(cidsmodel_dir)
                object_hash = KadiAIRecord.calculate_hash(cidsmodel_dir)
        elif default_str == "preprocessor":
            timestamp = self._extract_file_timestamp(directory)
            object_hash = KadiAIRecord.calculate_hash(directory)
        else:
            timestamp = self._extract_directory_content_timestamp(directory)
            object_hash = KadiAIRecord.calculate_hash(directory)
        if default_str != "cidsmodel":
            subdir = self._to_relative_paths(directory, root_dir=root_dir)
        else:
            subdir = self._to_relative_paths(directory, root_dir=self.result_dir)
        record.add_metadatum(
            {
                "key": self.md_last_modified,
                "value": self._timestamp_to_timestr(timestamp),
                "type": "str",
                "unit": None,
            },
            force=True,
        )
        record.add_metadatum(
            {
                "key": self.md_subdirectory,
                "value": subdir.as_posix(),
                "type": "str",
                "unit": None,
            },
            force=True,
        )
        record.add_metadatum(
            {
                "key": self.md_hash,
                "value": object_hash,
                "type": "str",
                "unit": None,
            },
            force=True,
        )
        # Add record to collection
        if collection:
            record.add_collection_link(collection.id)
        return record

    def _find_local_result_dirs(self, exclude_patterns=()):
        local_dirs = {}
        # Search results
        local_dirs["search"] = sorted(
            {d.parent for d in self.result_dir.glob("**/oracle.json")}
        )
        # Training results
        training_result_dirs = sorted(
            {d.parent for d in self.result_dir.glob("**/train_results_*.json")}
        )
        local_dirs["training"] = [
            d for d in training_result_dirs if not list(d.parent.glob("trial.json"))
        ]
        # Remove excluded paths
        local_dirs = {
            k: self._exclude_paths(v, exclude_patterns=exclude_patterns)
            for k, v in local_dirs.items()
        }
        return local_dirs

    def push(
        self,
        src=None,
        tfrecords=None,
        results=None,
        data_definition=True,
        project=False,
        logs=False,
        link_collection=True,
        link_records=True,
        force=False,
        create=True,
        exclude=(".*", "_*"),
    ):
        """Upload project to Kadi."""
        # Collection
        if link_collection:
            collection_identifier = self._assemble_identifier(
                link_collection, default_id=self.base_identifier
            )
            # Create collection
            manager = KadiManager(instance=None, host=None, token=None)
            collection = manager.collection(
                identifier=collection_identifier, create=create
            )
        else:
            collection = None
        # Source data
        if src:
            # Push record with identifier
            src_record = self._push_record(
                src,
                self.src_dir,
                default_id=self.src_identifier,
                default_str="src",
                force=force,
                exclude=exclude,
                type=None,
                tags=[self.src_tag],
                collection=collection,
            )
            # Memorize identifier
            self.src_identifier = src_record.identifier
        # Converted tfrecords
        if tfrecords:
            # Push record with identifier
            tfrecord_record = self._push_record(
                tfrecords,
                self.tfrecord_dir,
                default_id=self.tfrecord_identifier,
                default_str="tfrecords",
                force=force,
                exclude=exclude,
                type=self.tfrecord_tag,
                tags=[self.tfrecord_tag],
                collection=collection,
            )
            # Memorize identifier
            self.tfrecord_identifier = tfrecord_record.identifier
            # Add meta data
            if logs:
                # TODO: add logs
                pass
            if data_definition:
                tfrecord_record.datadefinition_to_kadirecord(
                    self.data_definition, upload=True
                )
            # Push record for preprocessor
            preprocessor_record = self._push_preprocessor(
                force=force, collection=collection
            )
        # Results
        if results:
            # TODO: add record for model and training function
            # Create or update records of CIDS-Models
            # Find cidsmodels
            cids_model_dirs = sorted(
                [d.parent for d in self.result_dir.glob("**/cidsmodel.json")]
            )
            # Find cidsmodels - Backwards compatibility
            # TODO: remove in the future
            if not cids_model_dirs:
                candidate_dirs = sorted(next(os.walk(self.result_dir))[1])
                cids_model_dirs = []
                for candidate_dir in candidate_dirs:
                    model_json = list(
                        Path(os.path.join(self.result_dir, candidate_dir)).glob(
                            "**/model.json"
                        )
                    )
                    training_json = list(
                        Path(os.path.join(self.result_dir, candidate_dir)).glob(
                            "**/*phase00.json"
                        )
                    )
                    if len(model_json) > 1 or len(training_json) > 1:
                        cids_model_dirs.append(candidate_dir)
            cids_model_records = self._find_records_by_type(
                create=create, type="cidsmodel"
            )[self.cids_model_tag]
            for cids_model in cids_model_dirs:
                identifier, cids_model_records = self._get_unique_object_identifier(
                    cids_model, cids_model_records, "cidsmodel"
                )
                cids_model_record = self._push_record(
                    identifier,
                    cids_model,
                    default_id=None,
                    default_str="cidsmodel",
                    force=force,
                    exclude=exclude,
                    type=self.cids_model_tag,
                    tags=[self.cids_model_tag],
                    collection=collection,
                )
                # Create metadata for cidsmodel
                cidsmodel_json_path = os.path.join(cids_model, "cidsmodel.json")
                if os.path.exists(cidsmodel_json_path):
                    with open(cidsmodel_json_path, encoding="utf-8") as f:
                        cidsmodel_dict = json.load(f)
                    # Add metadata to project record
                    cids_model_record.remove_metadatum("CIDS:Model")
                    cids_model_record.add_metadata([cidsmodel_dict], force=True)
                else:
                    print(
                        "Could not find file 'cidsmodel.json' for current "
                        "cidsmodel.The cidsmodel might have been created "
                        "using an old version of cids. Consider retraining "
                        "of models, if you want to add cidsmodel hyperparameter "
                        "spaces to your model record."
                    )
                if cids_model_record.identifier not in [
                    r.identifier for r in cids_model_records
                ]:
                    cids_model_records.append(cids_model_record)
            # Check collection for current result records
            result_records = self._find_records_by_type(
                create=create, type=["results", "model", "trial", "evaluation"]
            )
            # Find search and training results
            result_dirs = self._find_local_result_dirs(exclude_patterns=exclude)
            # Push record with identifier for each result
            if isinstance(results, str):
                base_identifier = results
            else:
                base_identifier = None
            for result_type in self.result_types:
                result_records = self._push_result_records_by_type(
                    result_dirs[result_type],
                    result_records,
                    result_type,
                    base_identifier=base_identifier,
                    force=force,
                    exclude=exclude,
                    collection=collection,
                )
            # Memorize identifier
            self.result_identifier = {
                k: [r.identifier] for k, v in result_records.items() for r in v
            }
            # Add meta data
            if logs:
                # TODO: add logs
                pass
            if project:
                # TODO: add project json
                pass
        # Project
        if project:
            # Push record with identifier
            project_record = self._push_record(
                project,
                self.root_dir,
                default_id=None,
                default_str="project",
                force=force,
                exclude=(".*", "_*", "INPUTS", "RESULTS", "LOGS", "*.pid"),
                type=self.project_tag,
                tags=[self.project_tag],
                collection=collection,
            )
            # Extract dictionary from project.json file
            json_dict = super().to_json(write_data_definition=False)
            kadi_dict = KadiAIRecord._cids_to_kadi_datastructure(json_dict)
            # Create metadata entry from extracted dictionary
            metadata_summary = [
                {
                    "key": "CIDS:Project",
                    "type": "dict",
                    "value": kadi_dict,
                }
            ]
            # Add metadata to project record
            project_record.remove_metadatum("CIDS:Project")
            project_record.add_metadata(metadata_summary, force=True)
        # Links
        if link_records:
            # Search project collection in Kadi for other present records.
            # For each type all records of this type are searched to enable
            # the linking of new and old records in the following steps.
            if not src:
                src_record = self._find_records_by_type(type="src")[self.src_tag]
                if src_record:
                    src = True
                    src_record = src_record[0]
            if not tfrecords:
                tfrecord_record = self._find_records_by_type(type="tfrecords")[
                    self.tfrecord_tag
                ]
                if tfrecord_record:
                    tfrecords = True
                    tfrecord_record = tfrecord_record[0]
                preprocessor_record = self._find_records_by_type(type="preprocessor")[
                    self.preprocessor_tag
                ]
                if preprocessor_record:
                    preprocessor_record = preprocessor_record[0]
            if not results:
                result_records = self._find_records_by_type(type="results")
                cids_model_records = self._find_records_by_type(type="cidsmodel")[
                    self.cids_model_tag
                ]
                if result_records:
                    results = True
            if not project:
                project_record = self._find_records_by_type(type="project")[
                    self.project_tag
                ][0]
                if project_record:
                    project = True
            model_records = self._find_records_by_type(type="model")[self.model_tag]
            evaluation_records = self._find_records_by_type(type="evaluation")[
                self.evaluation_tag
            ]
            if results:
                # Remove all previous record links and create new links
                for result_type in self.result_types:
                    result_type_tag = self._assemble_type_tag(result_type)
                    for result_record in result_records[result_type_tag]:
                        if tfrecords:
                            if result_type == "search":
                                # Create link for search results
                                self._create_new_record_link(
                                    result_record, self.search_link, tfrecord_record
                                )
                            elif result_type == "training":
                                # Create link for training results
                                self._create_new_record_link(
                                    result_record, self.training_link, tfrecord_record
                                )
                            else:
                                raise ValueError(f"Invalid result type: {result_type}")
                        # Create link from Project to Training
                        if project and result_type == "training":
                            self._create_new_record_link(
                                project_record,
                                self.project_training_link,
                                result_record,
                            )
                        # Create link from CidsModel to result
                        self._link_object_to_cidsmodel(
                            result_record,
                            cids_model_records,
                            self.cidsmodel_result_link,
                        )
                # Link Models to CidsModel
                for model_record in model_records:
                    self._link_object_to_cidsmodel(
                        model_record, cids_model_records, self.cidsmodel_model_link
                    )
                # Link Evaluations to CidsModel and Project
                for evaluation_record in evaluation_records:
                    self._link_object_to_cidsmodel(
                        evaluation_record,
                        cids_model_records,
                        self.cidsmodel_result_link,
                    )
                    self._create_new_record_link(
                        project_record, self.project_evaluation_link, evaluation_record
                    )
                # Create link from CidsModel to Project
                if project:
                    for cids_model_record in cids_model_records:
                        self._create_new_record_link(
                            project_record, self.creates_link, cids_model_record
                        )
            if tfrecords and src:
                self._create_new_record_link(
                    tfrecord_record, self.conversion_link, src_record
                )
                self._create_new_record_link(
                    preprocessor_record, self.preprocessor_src_link, src_record
                )
                self._create_new_record_link(
                    preprocessor_record,
                    self.preprocessor_tfrecord_link,
                    tfrecord_record,
                )

    def _create_new_record_link(self, record, link_name, target_record):
        # Remove all previous record links
        link_ids = record.auto_get_link_ids(direction="out", name=link_name)
        for lid, rid in link_ids.items():
            if rid == target_record.id:
                # TODO: only remove links of same type?
                record.delete_record_link(lid)
        # Create new link
        record.link_record(target_record.id, link_name)

    def _link_object_to_cidsmodel(self, object_record, cids_model_records, link):
        # Find corresponding CidsModel
        try:
            object_record_subdir = object_record.get_metadatum(self.md_subdirectory)[
                "value"
            ].split("/")[0]
        except TypeError:
            # Record is corrupt and missing some necessary metadata
            # Delete record to clean up
            manager = KadiManager(instance=None, host=None, token=None)
            object_record.delete()
            manager.misc.purge("record", object_record.id)
            return
        cids_model_record_dirs = []
        for cids_model_record in cids_model_records:
            try:
                cids_model_record_dirs.append(
                    cids_model_record.get_metadatum(self.md_subdirectory)["value"]
                )
            except TypeError:
                # Record is corrupt and missing some necessary metadata
                # Delete record to clean up
                manager = KadiManager(instance=None, host=None, token=None)
                cids_model_record.delete()
                manager.misc.purge("record", cids_model_record.id)
        if cids_model_record_dirs:
            idx = cids_model_record_dirs.index(object_record_subdir)
            self._create_new_record_link(cids_model_records[idx], link, object_record)

    def push_single_record(
        self,
        identifier=None,
        data_definition=True,
        project=True,
        link_collection=True,
        force=False,
        create=True,
        exclude=(".*", "_*"),
        collection=None,
    ):
        """Upload project root directory to Kadi as a single record for publishing."""
        # Push record with identifier
        base_record = self._push_record(
            identifier,
            self.root_dir,
            default_id=self.base_identifier,
            default_str="base",
            force=force,
            exclude=exclude,
            type=self.project_tag,
            tags=[self.project_tag],
            collection=collection,
        )
        # Add meta data
        if data_definition:
            base_record.datadefinition_to_kadirecord(self.data_definition, upload=True)
        if project:
            # TODO: add project
            pass
        # Collection
        if link_collection:
            collection_identifier = self._assemble_identifier(
                link_collection, default_id=self.base_identifier
            )
            # Add to collection
            manager = KadiManager(instance=None, host=None, token=None)
            collection = manager.collection(
                identifier=collection_identifier, create=create
            )
            base_record.add_collection_link(collection.id)

    def to_json(
        self, file=None, init_params=None, run_params=None, write_data_definition=True
    ):
        """Serialize project to a json file.

        Args:
            file:           a json file to serialize Features into
            init_params:    dictionary of entries to add to init parameters
            run_params:     dictionary of entries to add to runtime parameters
            write_data_definition:  whether to also serialize the data definition
        """
        run_params = {}
        run_params["tfrecord_identifier"] = self.tfrecord_identifier
        run_params["src_identifier"] = self.src_identifier
        run_params["result_identifier"] = self.result_identifier
        return super().to_json(
            file=file,
            run_params=run_params,
            init_params=init_params,
            write_data_definition=write_data_definition,
        )

    def _find_records_by_type(
        self, collection_identifier=None, create=False, type="all"
    ):
        """Get all records of certain type belonging to the project collection.

        Args:
            collection_identifier:  identifier of a kadi collection
            create:                 boolean to state whether new collection
                                    should be created.
            type:                   type of record which should be found

        Output:
            kadi_records:   List with current records in project collection.

        """
        # TODO: always requires common collection
        # TODO: search for linked result records through Kadi
        # TODO: fallback use identifiers already in project?
        if collection_identifier is None:
            collection_identifier = self._assemble_identifier(
                True, default_id=self.base_identifier
            )
        manager = KadiManager(instance=None, host=None, token=None)
        collection = manager.collection(identifier=collection_identifier, create=create)
        # Find all records in project collection
        record_identifiers = [
            record_items["identifier"]
            for record_items in self._depaginate_response(collection.get_records)
        ]
        # Check variable type of argument "type"
        if not isinstance(type, list):
            type = [type]
        # Define tags which should be searched for
        if "all" in type:
            possible_tags = [
                self.project_tag,
                self.src_tag,
                self.tfrecord_tag,
                self.preprocessor_tag,
                self.cids_model_tag,
                self.trial_tag,
                self.model_tag,
            ] + [
                self._assemble_type_tag(result_type)
                for result_type in self.result_types
            ]
        else:
            possible_tags = []
            if "src" in type:
                possible_tags.append(self.src_tag)
            if "tfrecords" in type:
                possible_tags.append(self.tfrecord_tag)
            if "preprocessor" in type:
                possible_tags.append(self.preprocessor_tag)
            if "results" in type:
                result_tags = [
                    self._assemble_type_tag(result_type)
                    for result_type in self.result_types
                ]
                possible_tags += result_tags
            if "project" in type:
                possible_tags.append(self.project_tag)
            if "cidsmodel" in type:
                possible_tags.append(self.cids_model_tag)
            if "trial" in type:
                possible_tags.append(self.trial_tag)
            if "model" in type:
                possible_tags.append(self.model_tag)
            if "evaluation" in type:
                possible_tags.append(self.evaluation_tag)
        # Determine characteristics for each result record
        records_by_type = {k: [] for k in possible_tags}
        for record_identifier in record_identifiers:
            record = KadiAIRecord.use_config(record_identifier, create=False)
            tags = record.get_tags()  # FIXME: pagination???
            for possible_tag in possible_tags:
                if possible_tag in tags:
                    records_by_type[possible_tag].append(record)
        return records_by_type

    @staticmethod
    def _add_training_summary(
        result_dir,
        result_record,
        remove,
    ):
        """Create a training summary from training results
        and save it as metadata.

        Args:
            result_dir:         Path of result folder.
            result_record:      Identifier of record for this result.
            remove:             Boolean whether to remove old metadatum
                                before adding new metadatum.

        """
        json_files = list(Path(result_dir).glob("**/train_results_phase*.json"))
        new_dict = {}
        for json_file in json_files:
            tmp_result = TrainingResults.from_json(json_file)
            new_dict[json_file.stem] = {
                "hyperparameters": tmp_result._hyperparameters,
                "metrics": tmp_result.metrics["best"],
            }
        if new_dict["train_results_phase00"]["hyperparameters"]["start_count"] is None:
            if (
                "train_results_phase01" not in new_dict
                or new_dict["train_results_phase01"]["hyperparameters"]["start_count"]
                is None
            ):
                new_dict["Status"] = "Invalid"
            else:
                new_dict["train_results_phase00"]["hyperparameters"]["start_count"] = 0
                new_dict["train_results_phase00"]["hyperparameters"]["end_count"] = 1
                new_dict["train_results_phase00"]["metrics"] = tmp_result.metrics[
                    "first"
                ]
                new_dict["Status"] = "Successful"
        else:
            new_dict["Status"] = "Successful"
        result_meta = KadiAIRecord._cids_to_kadi_datastructure(
            dict(sorted(new_dict.items()))
        )
        metadata_summary = [
            {
                "key": "CIDS:TrainingSummary",
                "type": "dict",
                "value": result_meta,
            }
        ]
        if remove:
            result_record.remove_metadatum("CIDS:TrainingSummary")
        result_record.add_metadata(metadata_summary, force=True)
        return result_record

    def _extract_directory_content_timestamp(self, result_dir, glob_pattern="**/*.*"):
        timestamps = [t.stat().st_mtime for t in result_dir.glob(glob_pattern)]
        if not timestamps:
            raise FileNotFoundError(
                f"No files matching pattern {glob_pattern} found "
                + f"in {os.fspath(result_dir)}."
            )
        timestamp = datetime.utcfromtimestamp(max(timestamps))
        timestamp = timestamp.replace(tzinfo=timezone.utc)
        return timestamp

    def _extract_file_timestamp(self, filepath):
        if not os.path.isfile(filepath):
            raise FileNotFoundError(f"File {filepath} not found.")
        timestamp = filepath.stat().st_mtime
        timestamp = datetime.utcfromtimestamp(timestamp)
        timestamp = timestamp.replace(tzinfo=timezone.utc)
        return timestamp

    def _extract_kadi_content_timestamp(self, record):
        try:
            kadi_time_str = record.get_metadatum(self.md_last_modified)["value"]
        except (KadiAPYInputError, KadiAPYRequestError):
            # Fallback to Kadi last modified
            kadi_time_str = record.meta["last_modified"]
        return self._timestr_to_timestamp(kadi_time_str)

    @staticmethod
    def _timestamp_to_timestr(timestamp):
        return timestamp.isoformat()

    @staticmethod
    def _timestr_to_timestamp(timestr):
        timestamp = dateutil.parser.isoparse(timestr)
        # Fallback to assume UTC if naive timestamp is given
        if timestamp.tzinfo is None:
            timestamp = timestamp.replace(tzinfo=timezone.utc)
        return timestamp

    def _push_result_records_by_type(
        self,
        result_dirs,
        result_records,
        result_type,
        base_identifier=None,
        force=False,
        exclude=(".*", "_*"),
        collection=None,
    ):
        """Push result records of specific result type if local results are new.

        Args:
            result_dirs:      List with local directories
            result_records:   List of dictionaries for each result record
            result_type:      String of the result type
            base_identifier:  String representing common base identifier
            force:            Boolean whether to force overwrite during push
            exclude:          List with files to exclude from push

        Output:
            new_result_records: Updated list with result records for
                                specific result type.

        """
        type_tag = self._assemble_type_tag(result_type)
        result_records = copy.deepcopy(result_records)
        for result_dir in result_dirs:
            # Identifiers and metadata
            identifier, result_records[type_tag] = self._get_unique_object_identifier(
                result_dir,
                result_records[type_tag],
                result_type,
                base_identifier=base_identifier,
            )
            # No record for this result exists yet. Create new record for results
            if self._extract_type(type_tag) == "training":
                _, _, result_records = self._push_training_results(
                    result_dir, identifier, result_records, force, exclude, collection
                )
            elif self._extract_type(type_tag) == "search":
                result_records = self._push_search_results_by_trial(
                    result_dir,
                    identifier,
                    result_records,
                    force,
                    base_identifier,
                    collection=collection,
                    exclude=exclude,
                )
        return result_records

    def _push_training_results(
        self,
        training_dir,
        training_identifier,
        result_records,
        force,
        exclude,
        collection,
    ):
        # records = self._find_records_by_type(type=["model", "results"])
        training_tag = self._assemble_type_tag("training")
        # Create record for training results
        training_record = self._push_record(
            training_identifier,
            training_dir,
            force=force,
            exclude=exclude,
            type=training_tag,
            tags=[training_tag],
            root_dir=self.result_dir,
            collection=collection,
        )
        # Create record for model
        (
            model_identifier,
            result_records[self.model_tag],
        ) = self._get_unique_object_identifier(
            training_dir, result_records[self.model_tag], "model"
        )
        model_record = self._push_record(
            model_identifier,
            training_dir,
            default_str="model",
            force=force,
            exclude=exclude,
            type=self.model_tag,
            tags=[self.model_tag],
            root_dir=self.result_dir,
            collection=collection,
        )
        # Add metadata to record
        model_json_path = os.path.join(training_dir, "model.json")
        if os.path.isfile(model_json_path):
            with open(model_json_path, encoding="utf-8") as f:
                model_dict = json.load(f)
            model_summary = {
                "Model Hyperparameters": model_dict["model_hyper_parameters"]
            }
            model_record.add_metadata(
                KadiAIRecord._cids_to_kadi_datastructure(model_summary), force=True
            )
        else:
            print(
                "Could not find file 'model.json' for current model. "
                "The model might have been created using an old version "
                "of cids. Consider retraining of model, if you want to "
                "add model hyperparameters to your model record."
            )
        # Create record for model evaluation if present
        if list(Path(training_dir).glob("eval_results.json")):
            (
                evaluation_identifier,
                result_records[self.evaluation_tag],
            ) = self._get_unique_object_identifier(
                training_dir, result_records[self.evaluation_tag], "evaluation"
            )
            evaluation_record = self._push_record(
                evaluation_identifier,
                training_dir,
                default_str="evaluation",
                force=force,
                exclude=exclude,
                type=self.evaluation_tag,
                tags=[self.evaluation_tag],
                root_dir=self.result_dir,
                collection=collection,
            )
            # Add metadata to record
            with Path(training_dir, "eval_results.json").open(encoding="utf-8") as f:
                eval_dict = json.load(f)
            eval_summary = {"Evaluation Summary": eval_dict["eval_results"]}
            evaluation_record.add_metadata(
                KadiAIRecord._cids_to_kadi_datastructure(eval_summary), force=True
            )
            # Add record to list of records
            if evaluation_record.identifier not in [
                r.identifier for r in result_records[self.evaluation_tag]
            ]:
                result_records[self.evaluation_tag].append(evaluation_record)
            # Link training and model
            self._create_new_record_link(
                evaluation_record, self.evaluation_model_link, model_record
            )
        # Add training summary
        self._add_training_summary(training_dir, training_record, False)
        # Link training and model
        self._create_new_record_link(
            training_record, self.model_training_link, model_record
        )
        # Add records to list of records
        if training_record.identifier not in [
            r.identifier for r in result_records[training_tag]
        ]:
            result_records[training_tag].append(training_record)
        if model_record.identifier not in [
            r.identifier for r in result_records[self.model_tag]
        ]:
            result_records[self.model_tag].append(model_record)
        return training_record, model_record, result_records

    def _push_search_results_by_trial(
        self,
        search_dir,
        search_identifier,
        result_records,
        force,
        base_identifier,
        collection,
        exclude=(".*", "_*"),
    ):
        trial_dirs = [
            search_dir / t for t in next(os.walk(search_dir))[1] if "trial" in t
        ]
        trial_dirs.sort()
        # records = self._find_records_by_type(type=["model", "results", "trial"])
        # Create record for search results
        search_tag = self._assemble_type_tag("search")
        search_record = self._push_record(
            search_identifier,
            search_dir,
            force=force,
            exclude=exclude,
            type=search_tag,
            tags=[search_tag],
            root_dir=self.result_dir,
            collection=collection,
        )
        # Add search record to list of records
        if search_record.identifier not in [
            r.identifier for r in result_records[search_tag]
        ]:
            result_records[search_tag].append(search_record)
        # Create a trial overview
        trial_overview_dict = self._create_trial_overview_dictionary(search_dir)
        trial_records = []
        for trial_dir in trial_dirs:
            # Create record for trial
            (
                trial_identifier,
                result_records[self.trial_tag],
            ) = self._get_unique_object_identifier(
                trial_dir, result_records[self.trial_tag], "trial"
            )
            trial_record = self._push_record(
                trial_identifier,
                trial_dir,
                force=force,
                exclude=exclude,
                type=self.trial_tag,
                tags=[self.trial_tag],
                root_dir=self.result_dir,
                collection=collection,
            )
            trial_records.append(trial_record)
            # Check if this trial contains training results
            if len(list(Path(trial_dir).glob("**/train_results_*.json"))) == 0:
                self._link_trial(
                    trial_dir,
                    trial_record,
                    trial_overview_dict,
                    trial_dirs,
                    trial_records,
                )
            else:
                # Create record for training result and model of trial
                training_tag = self._assemble_type_tag("training")
                training_dir = trial_dir / next(os.walk(trial_dir))[1][0]
                (
                    training_identifier,
                    result_records[training_tag],
                ) = self._get_unique_object_identifier(
                    training_dir,
                    result_records[training_tag],
                    "training",
                    base_identifier=base_identifier,
                )
                (
                    training_record,
                    model_record,
                    result_records,
                ) = self._push_training_results(
                    training_dir,
                    training_identifier,
                    result_records,
                    force,
                    exclude,
                    collection=collection,
                )
                # Create record links
                self._create_new_record_link(
                    trial_record, self.trial_model_link, model_record
                )
                self._create_new_record_link(
                    trial_record, self.trial_training_link, training_record
                )
                # Add records to list of records
                if training_record.identifier not in [
                    r.identifier for r in result_records[training_tag]
                ]:
                    result_records[training_tag].append(training_record)
                if model_record.identifier not in [
                    r.identifier for r in result_records[self.model_tag]
                ]:
                    result_records[self.model_tag].append(model_record)
            # Add metadata to records
            trial_summary = self._extract_trial_summary_from_json(trial_dir)
            trial_record.add_metadata(trial_summary, force=True)
            # Link trial to search record
            self._create_new_record_link(
                search_record, self.search_trial_link, trial_record
            )
            # Add trial record to list of records
            if trial_record.identifier not in [
                r.identifier for r in result_records[self.trial_tag]
            ]:
                result_records[self.trial_tag].append(trial_record)

        return result_records

    @staticmethod
    def _check_equality_of_localproject_and_kadiproject(
        kadiprojectrecord, localproject
    ):
        kadi_init_params = KadiAIRecord._kadi_to_cids_datastructure(
            kadiprojectrecord.get_metadatum(["CIDS:Project", "init_params"])["value"]
        )
        kadi_run_params = KadiAIRecord._kadi_to_cids_datastructure(
            kadiprojectrecord.get_metadatum(["CIDS:Project", "run_params"])["value"]
        )
        kadiproject = KadiAIProject(**kadi_init_params, _params=kadi_run_params)
        return kadiproject == localproject

    def _download_projectjson_from_kadiprojectrecord(
        self, kadi_project_record, download_dir
    ):
        project_dict = KadiAIRecord._kadi_to_cids_datastructure(
            kadi_project_record.get_metadatum(["CIDS:Project"])["value"]
        )
        file = Path(download_dir, self.project_file)
        file.parent.mkdir(parents=True, exist_ok=True)
        with file.open("w", encoding="utf8") as f:
            f.write(
                json.dumps(
                    project_dict, sort_keys=True, indent=4, separators=(",", ": ")
                )
            )

    def _check_equality_of_local_and_kadi_hash(self, kadirecord, localobject):
        kadi_hash = kadirecord.get_metadatum(self.md_hash)["value"]
        local_hash = KadiAIRecord.calculate_hash(localobject)
        return kadi_hash == local_hash

    def _get_local_paths_by_object(self, directory, root_dir, exclude, object_type):
        if object_type == self.cids_model_tag:
            funcs = list(Path(directory).glob("*function.py"))
            cidsmodel = [Path(directory) / "cidsmodel.json"]
            paths = funcs + cidsmodel
            local_subdir = self._to_relative_paths(directory, root_dir=self.result_dir)
        elif object_type == self.model_tag:
            plots = list(Path(directory).glob("*.png"))
            model = [Path(directory) / "model.json"]
            paths = list(Path(directory).glob("checkpoint/phase*"))
            paths = paths + plots + model
            local_subdir = self._to_relative_paths(directory, root_dir=root_dir)
        elif object_type == self._assemble_type_tag("training"):
            paths = list(Path(directory).glob("*"))
            exclude = exclude + ("plot",)
            local_subdir = self._to_relative_paths(directory, root_dir=root_dir)
        elif object_type == self._assemble_type_tag("search"):
            oracle = [Path(directory) / "oracle.json"]
            tuner = [Path(directory) / "tuner0.json"]
            search_summary = list(Path(directory).glob("search_summary.txt"))
            paths = oracle + tuner + search_summary
            local_subdir = self._to_relative_paths(directory, root_dir=root_dir)
        elif object_type == self.trial_tag:
            paths = list(Path(directory).glob("trial.json"))
            local_subdir = self._to_relative_paths(directory, root_dir=root_dir)
        elif object_type == self.evaluation_tag:
            paths = [Path(directory) / "eval_results.json"]
            local_subdir = self._to_relative_paths(directory, root_dir=root_dir)
        elif object_type == self.preprocessor_tag:
            paths = [self.input_dir / "preprocessing.json"]
            local_subdir = self._to_relative_paths(directory, root_dir=root_dir)
        else:
            paths = list(Path(directory).glob("*"))
            local_subdir = self._to_relative_paths(directory, root_dir=root_dir)
        paths = self._exclude_paths(paths, exclude)
        return paths, local_subdir

    def _extract_trial_summary_from_json(self, trial_dir):
        jsonpath = list(Path(trial_dir).glob("trial.json"))[0]
        with open(jsonpath, encoding="utf-8") as f:
            trial_dict = json.load(f)

        trial_summary = KadiAIRecord._cids_to_kadi_datastructure(
            {
                "Trial Summary": {
                    "Trial-ID": trial_dict["trial_id"],
                    "Score": trial_dict["score"],
                    "Hyperparameter Values": trial_dict["hyperparameters"]["values"],
                    "Metrics": trial_dict["metrics"]["metrics"],
                }
            }
        )
        return trial_summary

    def _link_trial(
        self, trial_dir, trial_record, trial_overview_dict, trial_dirs, trial_records
    ):
        trial_id = trial_dir.stem.split("trial_")[1]
        if trial_overview_dict["parent"][trial_id]:
            idx_root = next(
                i
                for i, trialpath in enumerate(trial_dirs)
                if trial_overview_dict["root"][trial_id] in str(trialpath)
            )
            idx_parent = next(
                i
                for i, trialpath in enumerate(trial_dirs)
                if trial_overview_dict["parent"][trial_id] in str(trialpath)
            )
            linked_record_ids = trial_records[idx_root].auto_get_link_ids(
                direction="out"
            )
            manager = KadiManager(instance=None, host=None, token=None)
            linked_records = [
                KadiAIRecord(manager, id=linked_record_ids[link_id])
                for link_id in linked_record_ids
            ]
            if linked_records:
                model_record = [
                    linked_record
                    for linked_record in linked_records
                    if linked_record.meta["type"] == self.model_tag
                ][0]
                training_record = [
                    linked_record
                    for linked_record in linked_records
                    if linked_record.meta["type"] == self._assemble_type_tag("training")
                ][0]
                # Link to model record
                self._create_new_record_link(
                    trial_record, self.trial_model_link, model_record
                )
                # Link to training record
                self._create_new_record_link(
                    trial_record, self.trial_training_link, training_record
                )
            # Link to direct parent trial record
            self._create_new_record_link(
                trial_record, self.trial_trial_link, trial_records[idx_parent]
            )

    def _create_trial_overview_dictionary(self, search_dir):
        with open(str(search_dir / "oracle.json"), encoding="utf-8") as f:
            oracle_dict = json.load(f)
        hps = list(oracle_dict["hyperparameters"]["values"].keys())

        trial_dirs = [
            search_dir / t for t in next(os.walk(search_dir))[1] if "trial" in t
        ]
        trial_dirs.sort()
        hp_combs = []
        trial_overview = []
        for trial_dir in trial_dirs:
            trial_number = trial_dir.stem.split("trial_")[1]
            with open(str(trial_dir / "trial.json"), encoding="utf-8") as f:
                trial_dict = json.load(f)
            hp_temp = []
            for hp in hps:
                hp_temp.append(trial_dict["hyperparameters"]["values"][hp])
            idx = None
            for i, hp_comb in enumerate(hp_combs):
                if hp_comb == hp_temp:
                    idx = i
                    break
            if idx is not None:
                trial_overview[idx].append(trial_number)
            else:
                hp_combs.append(hp_temp)
                trial_overview.append([trial_number])
        trial_overview_dict = {}
        trial_overview_dict["root"] = {}
        trial_overview_dict["parent"] = {}
        for major_trial in trial_overview:
            trial_overview_dict["root"][major_trial[0]] = None
            trial_overview_dict["parent"][major_trial[0]] = None
            for i in range(len(major_trial) - 1):
                trial_overview_dict["parent"][major_trial[i + 1]] = major_trial[i]
                trial_overview_dict["root"][major_trial[i + 1]] = major_trial[0]
        return trial_overview_dict

    def _push_preprocessor(self, force, upload=True, collection=None):
        # preprocessor_json = list(Path(self.input_dir).glob("preprocessing*.json"))[0]
        preprocessor_json = list(Path(self.input_dir).glob("preprocessing*.json"))
        if preprocessor_json:
            with open(preprocessor_json[0], encoding="utf-8") as f:
                preprocessor_dict = json.load(f)
            kadi_structure = KadiAIRecord._cids_to_kadi_datastructure(preprocessor_dict)
            meta_data = [
                {
                    "key": "PreprocessingSteps",
                    "type": "dict",
                    "value": kadi_structure,
                }
            ]
            preprocessor_record = self._push_record(
                True,
                preprocessor_json,
                default_str="preprocessor",
                force=force,
                type=self.preprocessor_tag,
                tags=[self.preprocessor_tag],
                collection=collection,
            )
            preprocessor_record.remove_metadatum("PreprocessingSteps")
            if upload:
                preprocessor_record.add_metadata(meta_data, force=False)
        else:
            preprocessor_record = None
        return preprocessor_record
