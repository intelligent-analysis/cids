# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""KadiAI MetaData objects. Part of KadiAI.

    Classes:
        MetricMetadatum:            Metadatum for metric values
        HyperparameterMetadatum:    Metadatum for hyperparameter values and configs
        TrainingResults:            Container for training results

"""
import json
import os
from pathlib import Path


class MetricMetadatum:
    def __init__(
        self,
        name,
        values,
    ):
        """A metric for measuring the training process.

        A metric is a quantity which is evaluated during the training process.
        The metric can be used to measure the training progress but also for evaluating
        the final result.

        Args:
            name:           a unique name that identifies the metric
            values:         a list with all calculated values for this metric
                            during training
        """
        self.name = name
        self.values = values
        self.lastvalue = values[-1]
        if self.name in ["loss", "val_loss"]:
            self.bestvalue = min(values)
        else:
            self.bestvalue = max(values)


class HyperparameterMetadatum:
    def __init__(
        self,
        name,
        value,
    ):
        """A hyperparameter which is used for training the network.

        A hyperparameter is used for controlling the learning process.
        During the learning process the hyperparameter is not changed.

        Args:
            name:           a unique name that identifies the hyperparameter
            value:          value of the hyperparameter
        """
        self.name = name
        self.value = value


class TrainingResults:
    def __init__(self, *metrics):
        """Contains information on training results.

        Contains a list of metrics which are used to assess the training performance.
        For each metric the last value during training is shown. Additonally,
        hyperparameters which were used for training the model are denoted.

        Args:
            *metrics:           Metrics which are assessed during training
        """
        # Metrics maintain order, to ensure tensor indexing remains constant
        self.metrics = {"first": {}, "last": {}, "best": {}}
        for metric in metrics:
            self.metrics["first"][metric.name] = metric.values[0]
            self.metrics["last"][metric.name] = metric.lastvalue
            self.metrics["best"][metric.name] = metric.bestvalue
        # Hyperparameters
        self._hyperparameters = None
        # File
        self._file = None

    @classmethod
    def from_json(cls, src):
        """Instantiates training results by reading a json file.

        Contains a list of metrics which are used to assess the training performance.
        For each metric the last value during training is shown. Additonally,
        hyperparameters which were used for training the model are denoted.

        Args:
            src:                a json file or json dict with training results
        """
        if isinstance(src, dict):
            # File already loaded
            json_dict = src
            path = None
        elif isinstance(src, (str, Path)):
            path = Path(src)
            assert path.exists(), f"File not found: {os.fspath(path)}"
            with path.open(encoding="utf8") as ofile:
                json_dict = json.load(ofile)
        else:
            raise ValueError(f"Invalid json type: {type(src)}")
        # Extract metrics
        if "training_history" in json_dict:
            metrics_dict = dict(json_dict["training_history"])
        else:
            metrics_dict = dict(json_dict["history"])
        metrictypes = list(metrics_dict.keys())
        metrics = []
        for metrictype in metrictypes:
            if metrictype != "epochs":
                metrics.append(
                    MetricMetadatum(metrictype, list(metrics_dict[metrictype]))
                )
            else:
                epochs = list(metrics_dict[metrictype])
        # Initialize training results
        inst = cls(
            *metrics,
        )
        if "training_hyper_parameters" in json_dict:
            hp_dict = dict(json_dict["training_hyper_parameters"])
        else:
            hp_dict = dict(json_dict["hyper_parameters"])
        hps = []
        hps.append(HyperparameterMetadatum("batch_size", hp_dict["batch_size"]))
        if epochs:
            if int(epochs[-1]) == 0:
                hps.append(HyperparameterMetadatum("start_count", int(epochs[-1])))
            else:
                hps.append(
                    HyperparameterMetadatum(
                        "start_count", int(hp_dict["count"]) - int(epochs[-1])
                    )
                )
            hps.append(HyperparameterMetadatum("end_count", int(hp_dict["count"])))
        else:
            hps.append(HyperparameterMetadatum("start_count", None))
            hps.append(HyperparameterMetadatum("end_count", None))
        hps.append(HyperparameterMetadatum("learning_rate", hp_dict["learning_rate"]))
        inst._hyperparameters = {hp.name: hp.value for hp in hps}

        return inst
