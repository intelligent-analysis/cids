# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import copy
import hashlib
import os
import shutil
from pathlib import Path

import numpy as np
import tensorflow as tf
from kadi_apy import KadiManager
from kadi_apy.lib.exceptions import KadiAPYInputError
from kadi_apy.lib.resources.records import Record
from tqdm import tqdm

from cids.base.logging import BaseLoggerMixin
from cids.base.logging import StreamToLogger
from cids.data import DataDefinition
from cids.data import Feature


class KadiAIRecord(Record, BaseLoggerMixin):

    data_definition_entry_name = "KadiAI:DataDefinition"
    local_path_name = "KadiAI:IAM-MMS_LOCAL_PATH"

    def __init__(
        self,
        manager,
        id=None,
        identifier=None,
        skip_request: bool = False,
        create: bool = False,
        **kwargs,
    ):
        super().__init__(
            manager=manager,
            id=id,
            identifier=identifier,
            skip_request=skip_request,
            create=create,
            **kwargs,
        )
        self.identifier = identifier
        self._start_logging()

    @classmethod
    def from_instance_host_identifier(
        cls, instance, host, identifier, token=None, create=False
    ):
        if token == "env":
            token = cls._get_env_token()
        manager = KadiManager(instance=instance, host=host, token=token)
        return cls(manager, identifier=identifier, create=create)

    @classmethod
    def use_config(cls, identifier, create=False, **kwargs):
        manager = KadiManager(instance=None, host=None, token=None)
        return cls(manager, identifier=identifier, create=create, **kwargs)

    @staticmethod
    def _get_env_token():
        env_token = os.getenv("KADI_TOKEN", default=None)
        if env_token:
            return env_token
        raise ValueError("KADI_TOKEN not defined or empty.")

    def kadirecord_to_datadefinition(self):
        metadata = self.meta["extras"]
        top_level_keys = [md["key"] for md in metadata]
        if self.data_definition_entry_name in top_level_keys:
            # Previously defined data definition found
            kadi_serialized = metadata[
                top_level_keys.index(self.data_definition_entry_name)
            ]["value"]
            cids_serialized = self._kadi_to_cids_datastructure(kadi_serialized)
            data_definition = DataDefinition.from_json(cids_serialized)
        else:
            # No previous data definition found
            features = []
            # Extract proposed features from meta data
            for md in metadata:
                name = md["key"]
                type_string = md["type"].lower()
                # Data shape and format
                if type_string == "list":
                    type_string = md["value"][0]["type"].lower()
                    data_shape = [None, len(md["value"])]
                else:
                    data_shape = [None, 1]
                data_format = "NF"
                # Data type
                dtype = self._get_tensorflow_dtype(type_string)
                if dtype is None:
                    continue
                decode_str_to = None  # No encoding for meta data!
                # Create feature
                features.append(
                    Feature(
                        name,
                        data_shape,
                        data_format=data_format,
                        dtype=dtype,
                        decode_str_to=decode_str_to,
                    )
                )
            # TODO: Extract proposed features from data
            # Instantiate data definition
            data_definition = DataDefinition(*features)
        return data_definition

    def datadefinition_to_kadirecord(self, data_definition, upload=True):
        data_definition_dict = data_definition.to_json(
            path=None, serialize_selected_features=False, write=False
        )
        kadi_structure = self._cids_to_kadi_datastructure(data_definition_dict)
        meta_data = [
            {
                "key": self.data_definition_entry_name,
                "type": "dict",
                "value": kadi_structure,
            }
        ]
        self.remove_metadatum(self.data_definition_entry_name)
        if upload:
            self.add_metadata(meta_data, force=False)
        return meta_data

    @staticmethod
    def _get_tensorflow_dtype(type_string):
        """Get value for key "decode_str_to" depending on object.

        Args:
            type_string:          A string of a data type

        Returns:
            tensorflow type:      A tensorflow tensor data type
        """
        if type_string.startswith("str"):
            return tf.string
        if type_string.startswith("int"):
            return tf.int64
        if type_string.startswith("float"):
            return tf.float64
        if type_string.startswith("bool"):
            return tf.bool
        # TODO: handle lists as vectors
        return None

    @staticmethod
    def _cids_to_kadi_datastructure(nested_dict):
        if isinstance(nested_dict, dict):
            kadi_list_of_dicts = []
            for key, value in nested_dict.items():
                value = copy.deepcopy(value)
                ktype = KadiAIRecord._kadi_dtype(value)
                value = KadiAIRecord._cids_to_kadi_datastructure(value)
                item = {"key": key, "type": ktype, "value": value}
                kadi_list_of_dicts.append(item)
            return kadi_list_of_dicts
        if isinstance(nested_dict, list):
            kadi_list_of_dicts = []
            for value in nested_dict:
                ktype = KadiAIRecord._kadi_dtype(value)
                value = KadiAIRecord._cids_to_kadi_datastructure(value)
                item = {"type": ktype, "value": value}
                kadi_list_of_dicts.append(item)
            return kadi_list_of_dicts
        if nested_dict is None:
            return "None"
        return nested_dict

    @staticmethod
    def _kadi_to_cids_datastructure(list_of_dicts):
        if isinstance(list_of_dicts, list) and list_of_dicts:
            if "key" in list_of_dicts[0].keys():
                # List is actually a dictionary
                cids_nested_dict = {}
                for item in list_of_dicts:
                    key = item["key"]
                    # TODO: Type
                    value = item["value"]
                    new_value = copy.deepcopy(value)
                    new_value = KadiAIRecord._kadi_to_cids_datastructure(new_value)
                    cids_nested_dict[key] = new_value
                return cids_nested_dict
            # List is an ordered list
            return [
                KadiAIRecord._replace_null_strings(item["value"])
                for item in list_of_dicts
            ]
        # TODO: asserts
        return KadiAIRecord._replace_null_strings(list_of_dicts)

    @staticmethod
    def _replace_null_strings(string):
        null_strings = ["None", "none", "Null", "null"]
        if string in null_strings:
            return None
        return string

    @staticmethod
    def _kadi_dtype(dtype):
        if dtype is not None:
            return type(dtype).__name__
        return "str"

    def _npy_to_datadefinition(self, file):
        """
        Converts npy file to data definition.

        Args:
            file (str): npy file name
        Returns:
            data definition (dict): dictionary with data definition
        """
        file = np.load(file, allow_pickle=True)
        attributes_dict = self._get_attributes_of_file(file)

        features = []

        for key, _key in attributes_dict.items():
            if key == "image":
                # for npy-file with image data
                data_format = "NXYF"
                data_shape = [None, _key[0], _key[1], _key[2]]
                decode_str_to = _key[3]
            elif _key[0] == 1:
                data_format = "NF"
                data_shape = [None, 1]
                decode_str_to = _key[1]
            else:
                data_format = "NSF"
                data_shape = [None, _key[0], 1]
                decode_str_to = _key[1]

            features.append(
                Feature(
                    key,
                    data_shape,
                    data_format=data_format,
                    dtype=tf.string,
                    decode_str_to=decode_str_to,
                )
            )
        data_definition = DataDefinition(*features)
        # TODO: Write to file
        return data_definition

    def _get_attributes_of_file(self, file):
        """
        Get key, length and type of features in file.

        Args:
            file (ndarray): ndarray representing information of file

        Returns:
            attribues_dict (dict): dictionary with key. Length and type as tuple.
        """
        _file = file.tolist()
        attributes_dict = {}

        if type(_file).__name__ == "dict":
            for key in _file.keys():
                if type(_file[key]).__name__ == "ndarray":
                    length = len(_file[key])
                    _type = self._get_decodestrto_val(type(_file[key][0]).__name__)
                else:
                    length = 1
                    _type = self._get_decodestrto_val(type(_file[key]).__name__)
                attributes_dict[key] = (length, _type)
        elif type(_file).__name__ == "list":
            x_length = len(_file)
            y_length = 0

            if type(_file[0]).__name__ == "list":
                y_length = len(_file[0])
                f_length = len(_file[0][0])
            attributes_dict["image"] = (x_length, y_length, f_length, tf.float64)
        return attributes_dict

    def auto_get_files(self, max_count=None, exclude_files=None):
        """Get paths from
        Args:
            exclude_files (list, optional):
                list of paths which will not be loaded
                Defaults to None.
        Returns:
            dict: list of items
        """
        per_page = 20
        item_count = max_count or self.get_number_files()
        page_count = item_count // per_page
        last_page = int(bool(item_count % per_page))
        items = []
        for p in range(page_count + last_page):
            response = self.get_filelist(page=p, per_page=per_page)
            payload = response.json()
            page_items = payload["items"]
            for page_item in page_items:
                if exclude_files and page_item["name"] in exclude_files:
                    continue
                items.append(page_item)
        return items

    def download_files(self, target_dir, files=None):
        """Download selected paths to a local directory."""
        target_dir = Path(target_dir)
        target_dir.mkdir(parents=True, exist_ok=True)
        files = files or self.auto_get_files()
        local_files = [Path(target_dir).joinpath(f["name"]) for f in files]
        pbar = tqdm(
            list(zip(files, local_files)),
            file=StreamToLogger(self._logger),
            leave=True,
            desc=f"{self.identifier}: Downloading",
            unit="files",
            dynamic_ncols=True,
        )
        for file, local_file in pbar:
            self.download_file(file["id"], local_file)
        return local_files

    def auto_download_dataset(self, target_dir, files=None):
        """Download this data record to a local directory and extract."""
        target_dir = Path(target_dir)
        try:
            assert self.get_metadatum(self.local_path_name) is not None
            src_path = Path(self.get_metadatum(self.local_path_name)["value"])
            assert src_path.exists() and src_path.is_dir()
        except (KadiAPYInputError, ValueError, AssertionError):
            # No valid _LOCAL_PATH key in meta data
            if files is None:
                files = self.auto_get_files()
            local_files = self.download_files(target_dir, files=files)
            archives = {
                f for f in local_files if ".zip" in f.suffix or ".tar" in f.suffixes
            }
            archives = list(archives)
            for archive in archives:
                # Single archive in paths. Assume this is a hierarchical dataset
                #   to unpack
                shutil.unpack_archive(archive, target_dir)
                archive.unlink()
        else:
            # Path already on local file system. No download necessary, link instead
            if target_dir.exists():
                # Try to delete, fails if not empty (as intended)
                os.rmdir(target_dir)
            os.symlink(src_path, target_dir)

    def auto_upload_dataset(self, *paths, force=False):
        paths = [Path(file) for file in paths]
        pbar = tqdm(
            paths,
            file=StreamToLogger(self._logger),
            leave=True,
            desc=f"{self.identifier}: Uploading",
            unit="files",
            dynamic_ncols=True,
        )
        for path in pbar:
            if os.path.exists(path):
                if path.is_dir():
                    # Received directories. Upload as archives to maintain hierarchy
                    base_name = path.parent.joinpath(path.stem)
                    archive = shutil.make_archive(
                        base_name,
                        format="zip",
                        root_dir=path.parent,
                        base_dir=path.parts[-1],
                    )
                    try:
                        self.upload_file(archive, force=force)
                    finally:
                        os.remove(archive)
                else:
                    self.upload_file(os.fspath(path), force=force)
            else:
                print(
                    f"The path {path} does not exist. Cannot upload "
                    f"to record {self.identifier}."
                )

    def auto_get_link_ids(self, filter=None, direction="in", **params):
        """Get links
        Args:
            params (list, optional):
                list of paths which will not be loaded
                Defaults to None.
        Returns:
            list of ids
        """
        if direction not in ["in", "out"]:
            raise ValueError("Link direction must be one of: ['in', 'out']")
        if direction == "in":
            extract_direction = "record_from"
        else:
            extract_direction = "record_to"
        per_page = 100
        item_count = 100
        page_count = item_count // per_page
        last_page = int(bool(item_count % per_page))
        items = {}
        for p in range(page_count + last_page):
            # TODO: direction no longer working
            response = self.get_record_links(
                page=p, per_page=per_page, filter=filter, direction=direction, **params
            )
            payload = response.json()
            page_items = payload["items"]
            for page_item in page_items:
                items[page_item["id"]] = page_item[extract_direction]["id"]
        return items

    @staticmethod
    def calculate_hash(path):
        """Calculation of hash for given path
        Args:
            path (str or Path, required):
                path of object for which hash
                should be calculated.
        Returns:
            hash
        """

        def hash_update_from_file(filename, hash):
            assert Path(filename).is_file()
            with open(str(filename), "rb") as f:
                for chunk in iter(lambda: f.read(4096), b""):
                    hash.update(chunk)
            return hash

        def hash_file(filename):
            return hash_update_from_file(filename, hashlib.sha256()).hexdigest()

        def hash_update_from_dir(directory, hash):
            assert Path(directory).is_dir()
            for path in sorted(Path(directory).iterdir()):
                hash.update(path.name.encode())
                if path.is_file():
                    hash = hash_update_from_file(path, hash)
                elif path.is_dir():
                    hash = hash_update_from_dir(path, hash)
            return hash

        def hash_dir(directory):
            return hash_update_from_dir(directory, hashlib.sha256()).hexdigest()

        if os.path.isfile(path):
            hash = hash_file(path)
        elif os.path.isdir(path):
            hash = hash_dir(path)
        return hash
