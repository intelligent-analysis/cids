import os
from pathlib import Path

import sklearn as sk
import tensorflow as tf
import tqdm
from sklearn.datasets import load_boston

from cids.data import DataDefinition
from cids.data import DataWriter
from cids.data import Feature
from kadi_ai.projects import Project

################################################################################
# Data paths

project_name = "boston"
project_dir = Path.cwd().parent / "DATA" / project_name
project = Project(project_name, root=project_dir)

# Project creates an `input_dir` in the `project_dir`, which stores converted
#   input data as tfrecords in a subdirectory `tfrecord`
tfrecord_dir = Path(project.input_dir) / "tfrecord"


################################################################################
# Data definition

data_definition = DataDefinition(
    Feature(
        "CRIM", [None, 1], data_format="NF", dtype=tf.string, decode_str_to=tf.float64
    ),
    Feature(
        "ZN", [None, 1], data_format="NF", dtype=tf.string, decode_str_to=tf.float64
    ),
    Feature(
        "INDUS", [None, 1], data_format="NF", dtype=tf.string, decode_str_to=tf.float64
    ),
    Feature(
        "CHAS", [None, 1], data_format="NF", dtype=tf.string, decode_str_to=tf.float64
    ),
    Feature(
        "NOX", [None, 1], data_format="NF", dtype=tf.string, decode_str_to=tf.float64
    ),
    Feature(
        "RM", [None, 1], data_format="NF", dtype=tf.string, decode_str_to=tf.float64
    ),
    Feature(
        "AGE", [None, 1], data_format="NF", dtype=tf.string, decode_str_to=tf.float64
    ),
    Feature(
        "DIS", [None, 1], data_format="NF", dtype=tf.string, decode_str_to=tf.float64
    ),
    Feature(
        "RAD", [None, 1], data_format="NF", dtype=tf.string, decode_str_to=tf.float64
    ),
    Feature(
        "TAX", [None, 1], data_format="NF", dtype=tf.string, decode_str_to=tf.float64
    ),
    Feature(
        "PTRATIO",
        [None, 1],
        data_format="NF",
        dtype=tf.string,
        decode_str_to=tf.float64,
    ),
    Feature(
        "B", [None, 1], data_format="NF", dtype=tf.string, decode_str_to=tf.float64
    ),
    Feature(
        "LAST", [None, 1], data_format="NF", dtype=tf.string, decode_str_to=tf.float64
    ),
    Feature(
        "MEDV", [None, 1], data_format="NF", dtype=tf.string, decode_str_to=tf.float64
    ),
    dtype=tf.float64,
)

project.data_definition = data_definition

################################################################################
# Read data

data = load_boston()
X_Train, X_Test, y_train, y_test = sk.model_selection.train_test_split(
    data.data, data.target
)

src_data = list(zip(X_Train, y_train))
################################################################################
# Data processing


def read_and_process(src_sample):
    # Pack into dictionary
    sample = {}
    sample["CRIM"] = src_sample[0][0]
    sample["ZN"] = src_sample[0][1]
    sample["INDUS"] = src_sample[0][2]
    sample["CHAS"] = src_sample[0][3]
    sample["NOX"] = src_sample[0][4]
    sample["RM"] = src_sample[0][5]
    sample["AGE"] = src_sample[0][6]
    sample["DIS"] = src_sample[0][7]
    sample["RAD"] = src_sample[0][8]
    sample["TAX"] = src_sample[0][9]
    sample["PTRATIO"] = src_sample[0][10]
    sample["B"] = src_sample[0][11]
    sample["LAST"] = src_sample[0][12]

    sample["MEDV"] = src_sample[1]
    return sample


################################################################################
# Start processing

# Create a data converter object
data_converter = DataWriter(data_definition)

# Loop over all pairs of source files with a pretty progress bar
n = 0
for src_sample in tqdm.tqdm(
    src_data,
    total=len(src_data),
    file=project.stream_to_logger(),
    leave=True,
    desc="Conversion",
    unit="sources",
    dynamic_ncols=True,
):
    # Process sample
    sample = read_and_process(src_sample)
    out_file = tfrecord_dir / f"sample{n:05d}.tfrecord"
    # Write sample to file
    try:
        data_converter.write_example(out_file, sample)
    except KeyError as e:
        project.warn(f"Missing key {e.args[0]} in: {os.fspath(out_file)}")
        continue
    n += 1
    project.log(f"Done processing: {os.fspath(out_file)}")

# Write the data definition and the features to a human-readable json file
#   The json file can also be loaded directly later-on for training.

project.to_json(write_data_definition=True)

project.log("Done.")
