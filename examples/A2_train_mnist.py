# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
os.environ["TF_FORCE_GPU_ALLOW_GROWTH"] = "true"

import cids

import numpy as np
import tensorflow as tf
import seaborn as sns

from pathlib import Path
from tensorflow.keras import layers as klayers
from cids.tensorflow import layers as clayers
from cids.tensorflow.tuner import SearchResults
from cids.statistics import metrics
from kadi_ai import KadiAIProject
from matplotlib import pyplot as plt
from kerastuner import HyperParameters

# Preamble
plt.style.use("seaborn-v0_8-paper")  # seaborn-talk, seaborn-poster, seaborn-paper
plt.rcParams.update(
    {
        "font.family": "sans-serif",
        "figure.dpi": 300,
        "savefig.format": "png",
    }
)


################################################################################
# Controls

CHECK = True
SEARCH = False
USE_BEST_SEARCH_CONFIG = False
TRAIN = True
EVAL = True
PLOT = True
ANALYZE = False

TRAIN_CONTINUE = False


num_check_samples = 100
num_plot_samples = 20
num_principal_components = 3


################################################################################
# Data paths

project_name = "ex-mnist"
project_dir = Path.cwd().parent / "DATA" / project_name
project = KadiAIProject(project_name, root=project_dir)

# Read paths
train_samples, valid_samples, test_samples = project.get_split_datasets(
    shuffle=True, valid_split=0.15, test_split=0.15
)


################################################################################
# Data definition

data_definition = project.data_definition
data_definition.input_features = ["image"]
data_definition.output_features = ["label"]


################################################################################
# Neural network

# Model
def model_function(hp, data_definition):

    # Hyper parameters
    num_kernels = hp.Choice("num_kernels", [32, 64, 128, 256, 512], default=64)
    dropout_rate = hp.Float("dropout", 0.0, 0.7, default=0.3)

    # Adversarial
    layers = []
    layers.append(klayers.Conv2D(num_kernels, (3, 3), strides=(1, 1), padding="same"))
    layers.append(klayers.LeakyReLU())
    layers.append(klayers.Dropout(dropout_rate))
    layers.append(
        klayers.Conv2D(num_kernels * 2, (3, 3), strides=(1, 1), padding="same")
    )
    layers.append(klayers.LeakyReLU())
    layers.append(klayers.Dropout(dropout_rate))
    layers.append(klayers.Flatten())
    layers.append(klayers.Dense(128))
    layers.append(klayers.Dense(10, activation="softmax"))
    return tf.keras.Sequential(layers)


# Set a model name
model_name = "mnist"
model_name += "--" + "--".join(
    [
        "+".join(list(data_definition.input_features)),
        "+".join(list(data_definition.output_features)),
    ]
)
model_name += "--onehot"


# Training schedule
def schedule_function(hp):
    learning_rate = hp.Choice(
        "learning_rate", [3e-3, 1e-3, 3e-4, 1e-4, 3e-5, 1e-5], default=1e-4
    )
    batch_size = 256  # divided by number of GPUs
    schedule = {
        "count": [1, 201],
        "learning_rate": learning_rate,
        "batch_size": batch_size,
    }
    return schedule


model = cids.CIDSModel.categorical_classification(
    10,
    data_definition,
    model_function,
    name=model_name,
    identifier="",
    result_dir=project.result_dir,
)
model.encode_categorical = "outputs"
model.metrics.append("accuracy")
model.monitor = "val_accuracy"
model.online_normalize = False
model.data_reader.prefetch = "cache"


if CHECK:
    if PLOT:
        check_samples = model.read_tfrecords(
            train_samples[:num_check_samples], disable_feature_merge=True
        )
        project.log("Plotting feature distributions.")
        model.plot_feature_distribution(check_samples, project.input_dir)


if SEARCH:
    project.log(">> Hyperparameters: searching...")
    hp = model.search(
        train_samples,
        valid_samples,
        schedule=schedule_function,
        executions_per_trial=1,
        max_epochs=51,  # hyperband only
        overwrite=False,
        objective="val_mae",
        method="hyperband",
        # method="bayes",
        num_trials=200,
        # callbacks=[tf.keras.callbacks.EarlyStopping(patience=5)],
    )
    if PLOT:
        search_results = SearchResults(model)
        search_results.plot_hyperparameter_search()
    model.identifier = "best"
    project.log(">> Hyperparameters: search complete.")
elif USE_BEST_SEARCH_CONFIG:
    try:
        search_results = SearchResults(model)
        hps = search_results.get_best_hyperparameters(print="best")
        hp = hps[0]
        model.identifier = "best"
        project.log(">> Hyperparameters: loaded from previous search.")
    except (FileNotFoundError, PermissionError) as e:
        project.log(">> Hyperparameters: " + str(e))
        project.log(">> Hyperparameters: No search found. Defaults loaded.")
        hp = HyperParameters()
        model.identifier = "default"
else:
    project.log(">> Hyperparameters: Defaults loaded.")
    hp = HyperParameters()
    model.identifier = "default"


if TRAIN:
    project.log(">> Training...")
    if TRAIN_CONTINUE:
        project.log(">> ...continuing from last...")
        checkpoint = "last"
    else:
        project.log(">> ...starting new...")
        checkpoint = None
    model.VERBOSITY = 2
    model.train(
        train_samples,
        valid_samples,
        schedule=schedule_function,
        checkpoint=checkpoint,
        hp=hp,
    )
    project.log(">> Training complete.")

if EVAL:
    project.log(">> Evaluating...")
    project.log(">>> Metrics...")
    # Compute predictions
    # test_loss = model.eval_data(
    #     test_samples, batch_size=4, checkpoint="last", hp=hp, submodels="generator")
    X, Y, Y_ = model.infer_data(
        test_samples,
        batch_size=4,
        checkpoint="last",
        hp=hp,
    )
    test_result_file = Path(model.base_model_dir, "test_results.npz")
    np.savez(test_result_file, X=X, Y=Y, Y_=Y_)
    # Evaluate metrics
    total_metrics = {}
    total_metrics["mae"] = metrics.mean_absolute_error(Y, Y_)
    total_metrics["mape"] = metrics.mean_absolute_percentage_error(Y, Y_)
    total_metrics["smape"] = metrics.symmetric_mean_absolute_percentage_error(Y, Y_)
    total_metrics["rmse"] = metrics.root_mean_square_error(Y, Y_)
    total_metrics["nrmse"] = metrics.normalized_root_mean_square_error(Y, Y_)
    project.log("Total metrics")
    for k, v in total_metrics.items():
        project.log(f"   {k}: {v}")
    project.log("")

    # Feature metrics
    feature_metrics = {}
    feature_metrics["mae"] = metrics.mean_absolute_error(Y, Y_, reduction_axes=(0,))
    feature_metrics["mape"] = metrics.mean_absolute_percentage_error(
        Y, Y_, reduction_axes=(0,)
    )
    feature_metrics["smape"] = metrics.symmetric_mean_absolute_percentage_error(
        Y, Y_, reduction_axes=(0,)
    )
    feature_metrics["rmse"] = metrics.root_mean_square_error(Y, Y_, reduction_axes=(0,))
    feature_metrics["nrmse"] = metrics.normalized_root_mean_square_error(
        Y, Y_, reduction_axes=(0,)
    )
    project.log("Feature metrics")
    for k, v in feature_metrics.items():
        project.log(f"   {k}: {v}")
    project.log("")

    # Sample metrics
    sample_metrics = {}
    sample_metrics["mae"] = metrics.mean_absolute_error(Y, Y_, reduction_axes=(1,))
    sample_metrics["mape"] = metrics.mean_absolute_percentage_error(
        Y, Y_, reduction_axes=(1,)
    )
    sample_metrics["smape"] = metrics.symmetric_mean_absolute_percentage_error(
        Y, Y_, reduction_axes=(1,)
    )
    sample_metrics["rmse"] = metrics.root_mean_square_error(Y, Y_, reduction_axes=(1,))
    sample_metrics["nrmse"] = metrics.normalized_root_mean_square_error(
        Y, Y_, reduction_axes=(1,)
    )
    project.log("Sample metrics:")
    for k, v in sample_metrics.items():
        project.log(f"   {k}: {v}")
    project.log("")

    # Select worst, best, mean, median samples
    error_metric = "mae"
    sample_errors = sample_metrics[error_metric]
    total_error = total_metrics[error_metric]
    sorted_sample_ids = np.argsort(sample_errors)
    sorted_sample_errors = sample_errors[sorted_sample_ids]
    # nonconvergence_cutoff = 100000.0
    # converged_sorted_sample_errors = sorted_sample_errors[
    #     sorted_sample_errors < nonconvergence_cutoff
    # ]
    # converged_sorted_sample_ids = sorted_sample_ids[
    #     sorted_sample_errors < nonconvergence_cutoff
    # ]
    converged_sorted_sample_ids = sorted_sample_ids
    converged_sorted_sample_errors = sorted_sample_errors
    best_sample_id = converged_sorted_sample_ids[0]
    worst_sample_id = converged_sorted_sample_ids[-1]
    median_sample_id = converged_sorted_sample_ids[
        len(converged_sorted_sample_ids) // 2
    ]
    perc25_sample_id = converged_sorted_sample_ids[
        len(converged_sorted_sample_ids) * 1 // 4
    ]
    perc75_sample_id = converged_sorted_sample_ids[
        len(converged_sorted_sample_ids) * 3 // 4
    ]
    mean_sample_id = np.argmin(
        np.abs(sample_errors - np.mean(converged_sorted_sample_errors))
    )
    project.log(f"{project_name}: Sample quality: sample {error_metric}")
    project.log(
        f"{project_name}:\n"
        + f"    mean: {sample_errors[mean_sample_id]}"
        + f" ({test_samples[mean_sample_id]})\n"
        + f"    best: {sample_errors[best_sample_id]}"
        + f" ({test_samples[best_sample_id]})\n"
        + f"    worst: {sample_errors[worst_sample_id]}"
        + f" ({test_samples[worst_sample_id]})\n"
        + f"    median: {sample_errors[median_sample_id]}"
        + f" ({test_samples[median_sample_id]})\n"
        + f"    perc25: {sample_errors[perc25_sample_id]}"
        + f" ({test_samples[perc25_sample_id]})\n"
        + f"    perc75: {sample_errors[perc75_sample_id]}"
        + f" ({test_samples[perc75_sample_id]})\n"
    )

# Plot
if PLOT:
    project.log(">> Plotting results...")
    test_result_file = Path(model.base_model_dir, "test_results.npz")
    test_results = np.load(test_result_file)
    X = test_results["X"][:num_plot_samples]
    Y = test_results["Y"][:num_plot_samples]
    Y_ = test_results["Y_"][:num_plot_samples]
    # Plot individual samples
    project.log(">>> Plotting individual samples")
    for i, (x, y, y_) in enumerate(zip(X, Y, Y_)):
        image = np.squeeze(x)
        label = np.argmax(y, axis=-1)
        prediction = np.argmax(y_, axis=-1)
        # Draw
        fig = plt.figure(figsize=(4, 3))
        plt.imshow(image, cmap=sns.color_palette("mako_r", as_cmap=True))
        plt.axis("off")
        fig.suptitle(
            f"label = {label:d}, prediction = {prediction:d} ({y_[prediction]})"
        )
        fig.tight_layout()
        # Save and close
        plot_file = os.path.join(
            model.plot_dir, f"best_to_worst_{i:05d}_label={label:d}"
        )
        plt.savefig(plot_file)
        plt.close()

# Explain
if ANALYZE:
    analze_samples = test_samples[:10]
    X, Y = model.read_tfrecords(test_samples[:num_plot_samples])
    # # PCA analysis
    # Y_, C1, C2 = model.predict(X, return_states=True)
    # for si in range(num_plot_samples):
    #     # Compute PCA on single sample      # TODO: pca over multiple samples?
    #     cov = np.dot(C1[si, ...].T, C1[si, ...])
    #     u, s, v = np.linalg.svd(cov, compute_uv=True)
    #     pc_axes = np.dot(C1[si, ...], u[:, :num_principal_components])
    #     # Plot loading and relaxation
    #     fig, axes = plt.subplots(3, 1, sharex=True, figsize=(7, 6),
    #                              gridspec_kw={"height_ratios": [0.5, 1, 1]})
    #     plt.sca(axes[0])
    #     plt.plot(X[si, :, 0], "C0", label="strain")
    #     plt.ylabel("strain [-]")
    #     plt.sca(axes[1])
    #     plt.plot(Y[si, :, 1], "C2", label="strain_plastic_ref")
    #     plt.plot(Y_[si, :, 1], "C2", linestyle="dashed",
    #              label="strain_plastic")
    #     for pc in range(num_principal_components):
    #         plt.plot(pc_axes[:, pc], "C{:d}".format(3 + pc),
    #                  linestyle="dotted", label="state{:d}".format(pc))
    #     plt.ylabel("history [-]")
    #     plt.sca(axes[2])
    #     plt.plot(Y[si, :, 0], "C1", label="stress_ref")
    #     plt.plot(Y_[si, :, 0], "C1", linestyle="dashed", label="stress")
    #     plt.ylabel("stress [-]")
    #     plt.xlabel("increments [-]")
    #     fig.legend(loc="upper center", ncol=4)
    #     plt.tight_layout()
    #     plt.draw()
    #     plt.savefig(os.path.join(model.plot_dir, "pca_{:06d}".format(si)))
    #     plt.close()
    # Local response propagation
    analysis = model.analyze(
        X[:num_plot_samples, ...],
        method="lrp.epsilon",
        neuron_selection_mode="all",
        # neuron_selection=0,
        plot=True,
        checkpoint="last",
    )
    project.log(">> Evaluation complete.")


################################################################################
# Finished

project.log("done")
