import os
from pathlib import Path

import numpy as np
import seaborn as sns
from kerastuner import HyperParameters
from matplotlib import pyplot as plt
from sklearn import metrics as skmetrics
from sklearn import svm

import cids
from kadi_ai.projects import Project

# Preamble
# plt.style.use("seaborn-v0_8-paper")  # seaborn-talk, seaborn-poster, seaborn-paper
plt.rcParams.update(
    {
        "font.family": "sans-serif",
        "figure.dpi": 300,
        "savefig.format": "png",
    }
)


###############################################################################
# Controls

CHECK = False
SEARCH = True
USE_BEST_SEARCH_CONFIG = False
TRAIN = True
EVAL = True
PLOT = True
ANALYZE = False

TRAIN_CONTINUE = False

num_check_samples = 100
num_plot_samples = 20


###############################################################################
# Data paths

project_name = "ex-mnist"
project_dir = Path.cwd().parent / "DATA" / project_name
project = Project(project_name, root=project_dir)

# Read paths
train_samples, valid_samples, test_samples = project.get_split_datasets(
    shuffle=True, valid_split=0.15, test_split=0.15
)


###############################################################################
# Data definition

data_definition = project.data_definition
data_definition.input_features = ["image"]
data_definition.output_features = ["label"]


###############################################################################
# Model

if SEARCH:
    # for hp search, we need set a function which returns a sklearn model with
    # different hp options
    def model_function(hp):
        # we search for different kernel functions and regulization parameter C
        C = hp.Float("C", 1e-4, 1e3, sampling="log")
        kernel = hp.Choice("kernel", ["linear", "poly", "rbf"])
        model = svm.SVC(C=C, kernel=kernel)
        return model

else:
    # SVM classifier -> needed if no hp search
    def model_function(**kwargs):
        return svm.SVC(**kwargs)


# Set a model name
model_name = "mnist"
model_name += "--" + "--".join(
    [
        "+".join(list(data_definition.input_features)),
        "+".join(list(data_definition.output_features)),
    ]
)
model_name += "--onevsrest"


model = cids.CIDSModelSK.categorical_classification(
    10,
    data_definition,
    model_function,
    name=model_name,
    identifier="",
    result_dir=project.result_dir,
)


if SEARCH:
    hp = model.search(
        train_samples, valid_samples, method="bayesian", num_trials=10
    )  # or method can be "random"
    # if no valid_samples provided, will cross-valid on train_samples
    model.identifier = "best"
    project.log(f">> Hyperparameters: Best found: {hp.values}.")
else:
    project.log(">> Hyperparameters: Defaults loaded.")
    hp = HyperParameters()
    model.identifier = "default"

if TRAIN:
    model.VERBOSITY = 2
    model.train(train_samples, valid_samples, hp=hp)
    project.log(">> Training complete.")

if EVAL:
    project.log(">> Evaluating...")
    project.log(">>> Metrics...")
    # Compute predictions
    X, Y, Y_ = model.infer_data(test_samples)
    # save test results
    test_result_file = Path(model.base_model_dir, "test_results.npz")
    np.savez(test_result_file, X=X, Y=Y, Y_=Y_)

    # Evaluate metrics
    metrics_dict = [
        skmetrics.accuracy_score,
        skmetrics.precision_score,
        skmetrics.f1_score,
    ]
    # Can also specify parameters for some metrics
    # create a dict with metric names and their parameters
    metrics_kwargs = {
        "precision_score": {"average": "micro"},
        "f1_score": {"average": "micro"},  # for multi-class we need set average method
    }
    total_metrics = model.eval_data(
        Y, Y_, metrics=metrics_dict, metrics_kwargs=metrics_kwargs
    )
    for k, v in total_metrics.items():
        project.log(f"   {k}: {v}")
    project.log("")


# Plot
if PLOT:
    project.log(">> Plotting results...")
    test_result_file = Path(model.base_model_dir, "test_results.npz")
    test_results = np.load(test_result_file)
    X = test_results["X"][:num_plot_samples]
    Y = test_results["Y"][:num_plot_samples]
    Y_ = test_results["Y_"][:num_plot_samples]
    # Plot individual samples
    project.log(">>> Plotting individual samples")
    for i, (x, y, y_) in enumerate(zip(X, Y, Y_)):
        image = x.reshape(28, 28)
        label = int(y)  # np.argmax(y, axis=-1)
        prediction = int(y_)  # np.argmax(y_, axis=-1)
        # Draw
        fig = plt.figure(figsize=(4, 3))
        plt.imshow(image, cmap=sns.color_palette("mako_r", as_cmap=True))
        plt.axis("off")
        fig.suptitle(f"label = {label:d}, prediction = {prediction:d}")
        fig.tight_layout()
        # Save and close
        plot_file = os.path.join(
            model.base_model_dir, f"best_to_worst_{i:05d}_label={label:d}"
        )
        plt.savefig(plot_file)
        plt.close()

# Explain
if ANALYZE:
    pass

###############################################################################
# Finished

project.log("done")
