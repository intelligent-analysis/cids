# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
from pathlib import Path

import tensorflow as tf
import tqdm

from cids.data import DataDefinition
from cids.data import DataWriter
from cids.data import Feature
from kadi_ai import KadiAIProject


################################################################################
# Data paths

project_name = "ex-mnist"
project_dir = Path.cwd().parent / "DATA" / project_name
project = KadiAIProject(project_name, root=project_dir)

# Project creates an `input_dir` in the `project_dir`, which stores converted
#   input data as tfrecords in a subdirectory `tfrecord`
tfrecord_dir = Path(project.input_dir) / "tfrecord"


################################################################################
# Data definition

data_definition = DataDefinition(
    Feature(
        "image",
        [None, 28, 28, 1],
        data_format="NXYF",
        dtype=tf.string,
        decode_str_to=tf.float32,
    ),
    Feature(
        "label", [None, 1], data_format="NF", dtype=tf.string, decode_str_to=tf.float32
    ),
    dtype=tf.float32,
)

project.data_definition = data_definition


################################################################################
# Read data

(train_images, train_labels), (
    test_images,
    test_labels,
) = tf.keras.datasets.mnist.load_data()

src_data = list(zip(train_images, train_labels))


################################################################################
# Data processing


def read_and_process(src_sample):
    """Read and process source data."""
    # Do some preprocessing
    image = src_sample[0]
    image = (image - 127.5) / 127.5
    # Pack into dictionary
    sample = {}
    sample["image"] = image
    sample["label"] = src_sample[1]
    return sample


################################################################################
# Start processing

# Create a data converter object
data_writer = DataWriter(data_definition)

# Loop over all pairs of source files with a pretty progress bar
n = 0
for src_sample in tqdm.tqdm(
    src_data,
    total=len(src_data),
    file=project.stream_to_logger(),
    leave=True,
    desc="Conversion",
    unit="sources",
    dynamic_ncols=True,
):
    # Process sample
    sample = read_and_process(src_sample)
    out_file = tfrecord_dir / f"sample{n:05d}.tfrecord"
    # Write sample to file
    try:
        data_writer.write_example(out_file, sample)
    except KeyError as e:
        project.warn(f"Missing key {e.args[0]} in: {os.fspath(out_file)}")
        continue
    n += 1
    project.log(f"Done processing: {os.fspath(out_file)}")

# Write the data definition and the features to a human-readable json file
#   The json file can also be loaded directly later-on for training.
project.data_definition = data_definition
project.to_json(write_data_definition=True)

project.log("Done.")
