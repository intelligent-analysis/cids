CIDS and KadiAI readme
===================

[![code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![flake8 status](https://gitlab.com/intelligent-analysis/cids/-/jobs/artifacts/master/raw/flake8/flake8-badge.svg?job=flake8-master)](https://gitlab.com/intelligent-analysis/cids/-/jobs/artifacts/master/raw/flake8/flake8stats.txt)
[![pylint status](https://gitlab.com/intelligent-analysis/cids/-/jobs/artifacts/master/raw/pylint/pylint-badge.svg?job=pylint-master)](https://gitlab.com/intelligent-analysis/cids/-/jobs/artifacts/master/raw/pylint/pylint.log)
[![test](https://gitlab.com/intelligent-analysis/cids/badges/master/pipeline.svg?job=unittests&key_text=tests)](https://gitlab.com/intelligent-analysis/cids/-/commits/master)
[![docs](https://gitlab.com/intelligent-analysis/cids/badges/master/pipeline.svg?job=pages&key_text=docs)](https://intelligent-analysis.gitlab.io/cids/index.html)
[![pipeline](https://gitlab.com/intelligent-analysis/cids/badges/master/pipeline.svg)](https://gitlab.com/intelligent-analysis/cids/-/commits/master)
[![coverage](https://gitlab.com/intelligent-analysis/cids/badges/master/coverage.svg?job=unittests)](https://gitlab.com/intelligent-analysis/cids/-/commits/master)

*CIDS* is a framework for Artificial Intelligence (AI) and Machine Learning (ML) for
applications from engineering, materials, and natural sciences. It combines models,
functions, and pipelines from libraries such as tensorflow/keras, sklearn, scipy, and
pandas to build modular, flexible, and reproducible AI models.

The interface *KadiAI* integrates AI tools, such as CIDS, seamlessly into Kadi workflows
and interacts with Kadi's repositories and data management features.

**The full documentation is available at:**

[https://intelligent-analysis.gitlab.io/cids/](https://intelligent-analysis.gitlab.io/cids/)

**The CIDS and KadiAI source codes are available at:**

[https://gitlab.com/intelligent-analysis/cids](https://gitlab.com/intelligent-analysis/cids)

**Demo scripts of CIDS projects are available at:**

The invite-only community repository `demos` contains scripts for applications ranging
from motion analysis to hybrid finite elements in solid mechanics.

[https://gitlab.com/intelligent-analysis/demos](https://gitlab.com/intelligent-analysis/demos)

## Install

**The reference configuration is a Linux (Ubuntu 20.04) OS.
For any other configuration, the steps below are given as is, but not guaranteed
to work.**

1. Download and install your favorite Python IDE (e.g. PyCharm Professional, VS Code)
2. Download and install Git (Ubuntu: `sudo apt-get install git-all`)
    * Install your favorite Git Client (e.g. GitKraken)

There are two ways to install the required environments.
*Manual installation* with pip requires all operating system components and packages
(in particular Nvidia CUDA, CUDNN, and drivers) to be installed manually with compatible
versions.

### Manual install with conda and pip

3. Install a Python (>3.10) distribution (e.g. Anaconda): [https://www.tensorflow.org/install/pip](https://www.tensorflow.org/install/pip)
4. Set up GPU support and CUDA: [https://www.tensorflow.org/install/gpu](https://www.tensorflow.org/install/gpu)
    * Requires CUDA, CUDNN and Nvidia drivers with compatible versions (may clash
    with requirements by other programs on the host machine)
    * Compatible combinations: [https://www.tensorflow.org/install/source#gpu](https://www.tensorflow.org/install/source#gpu)
    * Anaconda offers a convenient way that sets up CUDA and CUDNN, if the right driver is available.
    Select the CUDA toolkit and CUDNN version compatible with your GPU:

    ```bash
    conda install -c conda-forge cudatoolkit=11.2.2 cudnn=8.1.0
    ```

5. Permanently add the CUDA library path to your environment, e.g., via conda activate:

    ```bash
    conda activate MY_ENVIRONMENT_NAME
    mkdir -p $CONDA_PREFIX/etc/conda/activate.d
    echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CONDA_PREFIX/lib/' > $CONDA_PREFIX/etc/conda/activate.d/env_vars.sh
    ```

6. Go to the CIDS repository directory on your system.
7. Linux (Ubuntu)/Mac shell or Windows command prompt:

    ```bash
    pip install -e .[dev]
    ```

8. Install git pre-commit hooks for development

    ```bash
    pre-commit install
    ```

## First steps

After installing and building the docker images, scripts can be executed from the
project root directory with the following bash scripts (linux only). The scripts under
```demos/00_examples``` can serve as templates.

Clone the repository [https://gitlab.com/intelligent-analysis/demos](https://gitlab.com/intelligent-analysis/demos) besides your `cids`
repository:

```bash
$ git clone git@gitlab.com:intelligent-analysis/demos.git
$ ls
cids/ demos/
```

### Run

```bash
python -u [ path to file in demo folder ]
```

#### Examples

```bash
python -u demos/00_examples/A1_convert_mnist.py
```
