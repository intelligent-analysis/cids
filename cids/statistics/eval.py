# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json
import os
import socket

import numpy as np

from . import metrics
from ..visualize import plot_stats as plot


def eval_model_cids1(
    model,
    samples,
    layers,
    result_path,
    sequence=True,
    num_features=3,
    host=socket.gethostname(),
    checkpoint="last",
):
    """
    Description:
    :param model: # TODO
    :param samples: # TODO
    :param result_path: # TODO
    :param sequence: # TODO
    :param num_features: # TODO
    :param host: # TODO
    :param checkpoint: # TODO
    :return: # TODO
    """

    # TODO: Fix
    print("shape samples:", np.shape(samples))
    num_test_samples = len(samples)

    X, Y, Y_ = model.predict(samples[:num_test_samples], checkpoint=checkpoint)
    # et, mae, eval_time = model.eval(
    #     samples[:num_test_samples], host=host)

    if not sequence:
        # X = np.reshape(X, [-1, 101, 42])
        Y = np.reshape(Y, [-1, 101, num_features])
        Y_ = np.reshape(Y_, [-1, 101, num_features])

    mean_rmse, rmse_vec = metrics.calc_rmse(Y, Y_, reduction_axes=(1,))
    mean_nrmse, nrmse_vec = metrics.calc_nrmse_range(Y, Y_, reduction_axes=(1,))
    mean_corr, corr_vec = metrics.calc_corr(Y, Y_, reduction_axes=(1,))

    if None not in [X, Y, Y_, mean_rmse, mean_nrmse, mean_corr]:
        # Print
        print("  X.shape", X.shape)
        print("  Y.shape", Y.shape)
        assert Y.shape == Y_.shape

        # Write test results
        json_path = result_path + "/test_results.json"
        print("saving to: ", json_path)
        with open(json_path, "w+", encoding="utf8") as file:
            out_dict = {
                "01_mean_RMSE": np.float64(mean_rmse),
                "02_mean_NRMSE_in_%": np.float64(mean_nrmse),
                "03_mean_corr": np.float64(mean_corr),
                "11_RMSE_featurewise": np.mean(rmse_vec, axis=0).tolist(),
                "12_NRMSE_featurewise_in_%": np.mean(nrmse_vec, axis=0).tolist(),
                "13_corr_featurewise": np.mean(corr_vec, axis=0).tolist(),
                "shape": Y.shape,
            }
            file.write(
                json.dumps(out_dict, sort_keys=True, indent=4, separators=(",", ": "))
            )

        np.savetxt(
            result_path + "/test_Y.csv", Y.reshape([Y.shape[0], -1]), delimiter=","
        )
        np.savetxt(
            result_path + "/test_Y_predicted.csv",
            Y_.reshape([Y_.shape[0], -1]),
            delimiter=",",
        )


def eval_model(
    model,
    samples,
    result_subdir,
    num_features,
    sequence=True,
    host=socket.gethostname(),
    batch_size=None,
    checkpoint="last",
    write_all=True,
):
    """
    Description:
    :param model: # TODO
    :param samples: # TODO
    :param result_subdir: # TODO
    :param sequence: # TODO
    :param num_features: # TODO
    :param host: # TODO
    :param checkpoint: # TODO
    :return: # TODO
    """

    # TODO: Fix
    print("shape samples:", np.shape(samples))
    batch_size = batch_size or min(1000, len(samples))

    X, Y, Y_ = model.infer_data(samples, checkpoint=checkpoint, batch_size=batch_size)
    # et, mae, eval_time = model.eval(
    #     samples[:num_test_samples], host=host)

    if not sequence:
        # X = np.reshape(X, [-1, 101, 42])
        Y = np.reshape(Y, [-1, 101, num_features])
        Y_ = np.reshape(Y_, [-1, 101, num_features])
    else:
        # for sequential data with zero-padding, there will be a constant offset on the
        #  predictions Y_ after the net-sequence-length, which will inflate the rmse.
        #  Therefore, we pad Y_ with zeros, too.
        print("Sequential data. Zero-padding predictions...")
        Y_padded = Y_.copy()
        for samp in range(len(samples)):
            # print("sample: {}".format(samp))
            try:
                X_sumofsquares = np.sum(np.power(X[samp], 2), axis=1)
                # print("X_sumofsquares: {}".format(X_sumofsquares))
                length = np.min(np.where(np.array(X_sumofsquares == 0.0)))
                # print("length: {}".format(length))
                Y_padded[samp, length:, :] = 0.0
            except ValueError:
                print(f"Sample {samp} has maximum length. Not padding")
        Y_ = Y_padded.copy()

    # Old calculations
    Y_ = np.squeeze(Y_)
    mean_rmse, rmse_vec = metrics.calc_rmse(Y, Y_, reduction_axes=(1,))
    mean_nrmse, nrmse_vec = metrics.calc_nrmse_range(Y, Y_, reduction_axes=(1,))
    mean_corr, corr_vec = metrics.calc_corr(Y, Y_, reduction_axes=(1,))

    print(f"mean metrics: rmse={mean_rmse:g}, nrmse={mean_nrmse:g}, corr={mean_corr:g}")

    # ROOT MEAN SQUARE ERROR
    mask = metrics.mask(Y, Y.shape, reduction_axes=(-1,))
    masked_rmse = metrics.root_mean_square_error(
        Y, Y_, mask=mask, reduction_axes=(0, 1, 2)
    )
    rmse = metrics.root_mean_square_error(Y, Y_, reduction_axes=(0, 1, 2))
    sample_rmse = metrics.root_mean_square_error(Y, Y_, reduction_axes=(1, 2))
    pointwise_rmse = metrics.root_mean_square_error(Y, Y_, reduction_axes=(1,))
    print(
        "masked_rmse:",
        masked_rmse,
        "rmse:",
        rmse,
        "mean sample_rmse:",
        np.mean(sample_rmse),
        "mean pointwise_rmse:",
        np.mean(pointwise_rmse),
    )

    # NORMALIZED ROOT MEAN SQUARE ERROR
    mask = metrics.mask(Y, Y.shape, reduction_axes=(-1,))
    masked_nrmse = metrics.normalized_root_mean_square_error(
        Y, Y_, mask=mask, reduction_axes=(0, 1, 2), norm_axes=(0, 1), norm_mode="range"
    )
    nrmse = metrics.normalized_root_mean_square_error(
        Y, Y_, reduction_axes=(0, 1, 2), norm_axes=(0, 1), norm_mode="range"
    )
    sample_nrmse = metrics.normalized_root_mean_square_error(
        Y, Y_, reduction_axes=(1, 2), norm_axes=(0, 1), norm_mode="range"
    )
    pointwise_nrmse = metrics.normalized_root_mean_square_error(
        Y, Y_, reduction_axes=(1,), norm_axes=(1,), norm_mode="range"
    )
    print(
        "masked_nrmse:",
        masked_nrmse,
        "%",
        "nrmse:",
        nrmse,
        "%",
        "mean sample_nrmse:",
        np.mean(sample_nrmse),
        "%",
        "mean pointwise_nrmse:",
        np.mean(pointwise_nrmse),
        "%",
    )

    # CORRELATION COEFFICIENTS

    if None not in [X, Y, Y_, mean_rmse, mean_nrmse, mean_corr]:
        # Print
        print("  X.shape", X.shape)
        print("  Y.shape", Y.shape)
        assert Y.shape == Y_.shape

        # Write test results
        result_dir = os.path.join(model.model_dir, result_subdir)
        os.makedirs(result_dir, exist_ok=True)
        json_path = os.path.join(result_dir, "test_results.json")
        print("saving to: ", json_path)
        with open(json_path, "w+", encoding="utf8") as file:
            out_dict = {
                "01_mean_RMSE": np.float64(mean_rmse),
                "02_mean_NRMSE_in_%": np.float64(mean_nrmse),
                "03_mean_corr": np.float64(mean_corr),
                "11_RMSE_featurewise": np.mean(rmse_vec, axis=0).tolist(),
                "12_NRMSE_featurewise_in_%": np.mean(nrmse_vec, axis=0).tolist(),
                "13_corr_featurewise": np.mean(corr_vec, axis=0).tolist(),
                "shape": Y.shape,
            }
            file.write(
                json.dumps(out_dict, sort_keys=True, indent=4, separators=(",", ": "))
            )

        if write_all:
            np.savez_compressed(
                os.path.join(result_dir, "test_Y.npz"), Y.reshape([Y.shape[0], -1])
            )
            np.savez_compressed(
                os.path.join(result_dir, "test_Y_predicted.npz"),
                Y_.reshape([Y_.shape[0], -1]),
            )

    return mean_rmse, mean_nrmse, mean_corr


def eval_results(
    y,
    y_predicted,
    subplot,
    eval_metrics=(
        "RMSE",
        "NRMSE_range",
        "NRMSE_mean",
        "corr",
    ),
    plots=(
        "violin",
        "best_worst",
        "best_worst_featurewise",
        "mean",
    ),
    reduction_axes=(1,),
    save_dir=None,
    file_type="pdf",
):
    """Evaluate the results."""
    # TODO: param: plot with axes?
    # TODO: plots.py -> shaded area (mit alpha) - FF vs LSTM vs real? - val/test/train
    # TODO: Moment plots ... prettier
    # TODO: file_type check possibilities
    # TODO: save with axis?
    # TODO: tikz...
    # TODO: optimize best/ worst plots
    # TODO: check median vals for featurewise best_worst... (why 0-8?)
    # TODO: clean plot.py ... which are needed?
    # TODO: fix try-except structure. if error for plot -> shows error for metric
    # TODO: clean up best_worst and best_worst_featurewise
    # TODO: check all plots / metrics for reduction axes!
    # TODO: subplots in case of different amount of features
    # TODO: mean plot - json? + save_dir...
    # TODO: do mean plot only once, does not depend on eval metrics
    # TODO: TIKZ loop for subplots
    assert np.shape(y) == np.shape(y_predicted)
    assert len(eval_metrics) >= 1
    assert len(plots) >= 1

    for m in eval_metrics:
        try:
            metric_fun = getattr(metrics, "calc_" + m.lower())
            _, mvec = metric_fun(y, y_predicted, reduction_axes=reduction_axes)
            for p in plots:
                try:
                    if save_dir is not None:
                        plot_dir = save_dir + p + "_new/" + m + "." + file_type
                        os.makedirs(save_dir + p + "_new/", exist_ok=True)
                    else:
                        plot_dir = None
                    plot_fun = getattr(plot, "_" + p.lower())
                    plot_fun(
                        mvec,
                        y=y,
                        y_predicted=y_predicted,
                        title=m,
                        save_dir=plot_dir,
                        subplot=subplot,
                    )

                except AttributeError as e:
                    raise AttributeError(p + " not found!") from e
        except AttributeError as e:
            raise AttributeError(m + " not found!") from e


def load_results(path):
    """Load the results from a file."""
    # TODO: save as np
    with open(path + "test_results.json", encoding="utf8") as f:
        data = json.load(f)
    shape = data["shape"]

    y = np.genfromtxt(path + "test_Y.csv", delimiter=",")
    y_pred = np.genfromtxt(path + "test_Y_predicted.csv", delimiter=",")

    y = np.reshape(y, shape)
    y_pred = np.reshape(y_pred, shape)
    assert y.shape == y_pred.shape == tuple(shape)

    return y, y_pred, data


def load_results_npz(path):
    """Load results from numpy npz file."""
    with open(path + "test_results.json", encoding="utf8") as f:
        data = json.load(f)
    shape = data["shape"]

    y = np.load(path + "test_Y.npz")["arr_0"]
    y_pred = np.load(path + "test_Y_predicted.npz")["arr_0"]

    y = np.reshape(y, shape)
    y_pred = np.reshape(y_pred, shape)
    assert y.shape == y_pred.shape == tuple(shape)

    return y, y_pred, data


def load_results_npz_supporting_leg(path):
    """Load results from numpy npz file with leg support."""
    with open(path + "new_test_results.json", encoding="utf8") as f:
        # with open(path + "test_results.json", encoding="utf8") as f:
        data = json.load(f)
    shape = data["shape"]

    y = np.load(path + "new_test_Y.npz")["arr_0"]
    y_pred = np.load(path + "new_test_Y_predicted.npz")["arr_0"]
    # y = np.load(path + "test_Y.npz")["arr_0"]
    # y_pred = np.load(path + "test_Y_predicted.npz")["arr_0"]

    y = np.reshape(y, shape)
    y_pred = np.reshape(y_pred, shape)
    assert y.shape == y_pred.shape == tuple(shape)

    return y, y_pred, data


def load_results_npy(path):
    """Load results from a numpy npy file."""
    with open(path + "test_results.json", encoding="utf8") as f:
        data = json.load(f)
    shape = data["shape"]

    y = np.load(path + "test_Y/arr_0.npy")
    y_pred = np.load(path + "test_Y_predicted/arr_0.npy")

    y = np.reshape(y, shape)
    y_pred = np.reshape(y_pred, shape)
    assert y.shape == y_pred.shape == tuple(shape)

    return y, y_pred, data
