# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""SKlearn models for CIDS. Part of the CIDS toolbox.

    Classes:
        CIDSModelSK:    Wrapper class for SKlearn models.

        This class wraps a sklearn model inside it as self.core_model.
        Bascially, this class provides 4 types of ML models (tasks), namely,
        clustering, decomposition, regression and classification.

        A typical workflow:
            1. initizlize the model according to task (e.g. classification)
            2. build the model before training
                - set input/output preprocessors (default: StandardScaler)
            3. train the model
            4. evaluate performance

        Besides, a hyper-parameter searcher is implemented based on KerasTuner.

        For demo examples please refer to cids/examples.

"""
import glob
import os
from copy import deepcopy

import keras_tuner
import numpy as np
import pandas as pd
import sklearn
import sklearn.preprocessing as sk_pp
from joblib import dump
from joblib import load
from keras_tuner import HyperParameters
from sklearn.model_selection import PredefinedSplit

from ..base.model import BaseModel
from kadi_ai import projects


class CIDSModelSK(BaseModel):  # pylint: disable=abstract-method
    def __init__(self, data_definition, model, **kwargs):
        """A wrapper for statistical machine learning using CIDS and scikit-learn

        This wrapper manages training, evaluation and inference of statistical
        machine learning models implemented in scikit-learn package. It manages
        session creation, initialization and clean up.

        Args:
            data_definition: a DataDefinition object
            model:  an sklearn model or a function returning an sklearn model
        Keyword Args:
            name:            name of the model
            result_dir:      a directory to store the results in
        """
        # BaseModel init
        super().__init__(data_definition, model, **kwargs)
        # Hyperparameters
        self._hp = None
        # Model
        if isinstance(model, sklearn.base.BaseEstimator):  # sklearn model
            # check model is sklearn estimator
            # ref: https://scikit-learn.org/stable/modules/classes.html
            self._core_model = model
            self._core_model_function = None
        elif callable(model):  # a function return an sklearn model
            # Model given as a function # TODO: how to check?
            self._core_model = None
            self._core_model_function = model
        else:
            raise ValueError(
                "The model must be a (function returning an) Sklearn model."
            )
        # Building properties to be set later
        self.input_transformer = None
        self.output_transformer = None
        self.built = False
        self.disable_feature_merge = (
            True  # True for correctly read data for sklearn model
        )
        self.metrics_kwargs = None
        self.metrics = []
        # TODO: training settings... leave in model function, since model dependent?

    @classmethod
    def decomposition(cls, *args, **kwargs):
        """
        Instantiating decomposition tasks, e.g., PCA (sklearn.decomposition)

        Args:
            data_definition: a DataDefinition object
            model:  sklearn model function
        Kwargs:
            kwargs passed to the sklearn function, e.g., n_components of PCA
        """
        inst = cls(*args, **kwargs)
        inst.meta_architecture = "unsupervised"
        inst.num_classes = None
        inst.online_normalize = True
        inst.encode_categorical = False
        return inst

    @classmethod
    def clustering(cls, *args, **kwargs):
        """
        Instantiating clustering tasks, e.g., K-Means (sklearn.cluster)

        Args:
            data_definition: a DataDefinition object
            model_function:  sklearn model function
        Kwargs:
            kwargs passed to the sklearn function, e.g., K of K-Means
        """
        inst = cls(*args, **kwargs)
        inst.meta_architecture = "serial"
        inst.num_classes = None
        return inst

    @classmethod
    def regression(cls, *args, **kwargs):
        """
        Instantiating regression tasks, see sklearn.linear_model

        Args:
            data_definition:    a DataDefinition object
            model:     sklearn model function
        Kwargs:
            kwargs passed to the sklearn function, e.g., penalty, random_state
        """
        inst = cls(*args, **kwargs)
        inst.meta_architecture = "serial"
        inst.num_classes = None
        inst.online_normalize = True
        inst.encode_categorical = False
        return inst

    @classmethod
    def binary_classification(cls, *args, **kwargs):
        """
        Instantiating classification tasks, see sklearn.linear_model

        Args:
            data_definition:    a DataDefinition object
            model_function:     sklearn model function
        Kwargs:
            kwargs passed to the sklearn function, e.g., penalty, random_state
        """
        inst = cls(*args, **kwargs)
        inst.meta_architecture = "serial"
        inst.num_classes = 1
        inst.online_normalize = "inputs"
        inst.encode_categorical = False
        return inst

    @classmethod
    def categorical_classification(cls, *args, **kwargs):
        """
        Instantiating classification tasks, see sklearn.linear_model

        Args:
            data_definition:    a DataDefinition object
            model_function:     sklearn model function
        Kwargs:
            kwargs passed to the sklearn function, e.g., penalty, random_state
        """
        inst = cls(*args[1:], **kwargs)
        inst.meta_architecture = "serial"
        inst.num_classes = args[0]
        inst.online_normalize = "inputs"
        inst.encode_categorical = False  # SKLearn automatically encodes categorically
        # inst.encode_categorical = "outputs"
        return inst

    @property
    def core_model(self):
        """Interface to access sklearn model."""
        return self._core_model

    def build(self, hp=None):
        """Build sklearn model with hyperparameters.

        Prepare for the following cases:
        (1) train/ fit   (2) prediction / evaluation / inference.

        If the model needs rebuilding, use clear() function.

        # TODO: consider a pipeline for multi preprocessors?
        # e.g.:
        # from sklearn.pipeline import make_pipeline
        # self.pipe = make_pipeline(input_transformer, estimator)

        Args:
            hp: hyperparameters for sklearn model.

        Returns:
            None.
        """
        if not self.built:
            # Create preprocessor
            if self.input_transformer is None:
                if self.online_normalize in [True, "input", "inputs", "both"]:
                    self.input_transformer = sk_pp.StandardScaler()
                elif self.encode_categorical in [True, "input", "inputs", "both"]:
                    self.input_transformer = sk_pp.OneHotEncoder()
            if self.output_transformer is None:
                if self.online_normalize in [True, "output", "outputs", "both"]:
                    self.output_transformer = sk_pp.StandardScaler()
                elif self.encode_categorical in [True, "output", "outputs", "both"]:
                    self.output_transformer = sk_pp.OneHotEncoder()
            # Create core model
            if self._core_model is None:
                self._core_model = self._call_model_function(
                    self._core_model_function, hp
                )
        self.built = True  # built flag

    def clear(self, reset_hps=True):
        """Reset model, mostly used for rebuilding with new settings."""
        if reset_hps:
            self._hp = None

        self.input_transformer = None
        self.output_transformer = None
        self._core_model = None

        self.built = False

    def save(self, checkpoint="saved_models"):
        """Save models and inputs/outputs processors with joblib.

        Args:
            checkpoint: (temp. design) name for saving the model.

        Returns:
            None.
        """
        # Get base directory
        base_dir = os.path.join(self.checkpoint_dir, checkpoint)
        # Create base directory
        self.create_dir(base_dir)
        # Write checkpoint files
        # TODO: add check of model
        checkpoint_path = os.path.join(base_dir, "sklearn_model.joblib")
        dump(self._core_model, checkpoint_path)

        # Save inputs/outputs pre&post-processors
        if self.input_transformer:
            checkpoint_path = os.path.join(base_dir, "input_transformer.joblib")
            dump(self.input_transformer, checkpoint_path)
        if self.output_transformer:
            checkpoint_path = os.path.join(base_dir, "output_transformer.joblib")
            dump(self.output_transformer, checkpoint_path)
        # Remove directory if empty (no files were saved)
        if not os.listdir(base_dir):
            os.rmdir(base_dir)
        if self.VERBOSITY > 2:
            self.log(f"Saved models and processors to {base_dir:s}.")

    def load(self, checkpoint="saved_models"):
        """Load saved models and inputs/outputs processors.

        Args:
            checkpoint: (temp. design) directory name of the saved the model.

        Returns:
            None.
        """
        if os.path.exists(self._checkpoint_dir):
            # Get base directory
            base_dir = os.path.join(self._checkpoint_dir, checkpoint)
        else:
            raise ValueError(f"Saved model {checkpoint} directory: Not found.")

        # Load sklearn model
        checkpoint_path = os.path.join(base_dir, "sklearn_model.joblib")
        if os.path.exists(checkpoint_path):
            self._core_model = load(checkpoint_path)
            self.built = True
            if self.VERBOSITY > 2:
                self.log("Sklearn model loaded.")
        # Load inputs&outputs pre&post-processors
        checkpoint_path = os.path.join(base_dir, "input_transformer.joblib")
        if os.path.exists(checkpoint_path):
            self.input_transformer = load(checkpoint_path)
            if self.VERBOSITY > 2:
                self.log("Input transformer loaded.")
        checkpoint_path = os.path.join(base_dir, "output_transformer.joblib")
        if os.path.exists(checkpoint_path):
            self.output_transformer = load(checkpoint_path)
            if self.VERBOSITY > 2:
                self.log("Output transformer loaded.")

    def data_pipeline(self, data):
        """Create the data pipeline for sklearn model.

        Args:
            data: (expected) list of tfrecord paths or list of array-like input&output

        Returns:
            [input, output]: list of array-like parsed data
        """

        def _merge_features_to_2D(dictdata):
            """Merge feature dict into 2d-array."""
            values = []
            for _, v in dictdata.items():
                n_samples = v.shape[0]
                v = v.reshape(n_samples, -1)
                values.append(v)
            data = np.concatenate(values, axis=1)  # sklearn takes 2d input
            return data

        def _get_sub_dict(dictdata, keys=None):
            """Create sub-dict with selected keys."""
            if keys is None:  # if no keys provided, return the origial dict
                return dictdata
            sub_dict = {}
            for k in keys:
                sub_dict[k] = dictdata[k]
            return sub_dict

        if not isinstance(data, (tuple, list)):
            data = [data]
        if isinstance(data[0], str):  # [file paths(str)]
            parsed_data_dict = self.read_tfrecords(
                data, disable_feature_merge=self.disable_feature_merge
            )  # dict of data: {"feature1": value, "feature2": value, ...}
            input_data_dict = _get_sub_dict(
                parsed_data_dict, self.data_definition.input_features
            )
            output_data_dict = _get_sub_dict(
                parsed_data_dict, self.data_definition.output_features
            )

            input_data = _merge_features_to_2D(input_data_dict)
            output_data = _merge_features_to_2D(output_data_dict)
            parsed_data = [input_data, output_data]
            return parsed_data
        if isinstance(data[0], (pd.DataFrame, np.ndarray)):  # [array-like data]
            if len(data) == 1:
                return data[0], []
            if len(data) == 2:
                return data[0], data[1]
            raise ValueError(
                "Can only operate on <=2 arrays (inputs or inputs+targets)."
            )
        raise ValueError("Unsupported data source.")

    def _fit_preprocessors(self, train_inputs, train_targets):
        """Fit the preprocessors based on training inputs and targets.

        Args:
            train_inputs:         array-like data for fitting input preprocessors
            train_targets:        array-like labels for fitting output preprocessors
        """
        if self.input_transformer and len(train_inputs):
            self.input_transformer.fit(train_inputs)
        if self.output_transformer and len(train_targets):
            self.output_transformer.fit(train_targets)

    def train(self, train_data, valid_data=None, write=True, hp=None):
        """Build the model, preprocess the data, and train the model.

        Args:
            train_data:         list of samples/sample function (training)
            valid_data:         list of samples/sample function (validation) # TODO
            write:              opt. write results to json
            hp:                 a keras tuner HyperParameter set # TODO?

        Returns:
            history:  training history of all losses and metrics

        """
        # Build model
        self.build(hp=hp)
        # Data pipeline
        train_inputs, train_targets = self.data_pipeline(train_data)
        self._fit_preprocessors(train_inputs, train_targets)
        # Preprocess inputs
        X_train = self.preprocess_inputs(train_inputs)
        # Train the model
        out_dict = {}
        if self.meta_architecture == "unsupvervised":
            # Train
            self._core_model.fit(X_train)
        elif self.meta_architecture in ["serial"]:
            # Preprocess targets
            Y_train = self.preprocess_outputs(train_targets)
            # Train
            self._core_model.fit(X_train, Y_train)
            # Eval train set
            out_dict["train_score"] = self._core_model.score(X_train, Y_train)
            # Eval validation set
            if valid_data is not None:
                valid_inputs, valid_targets = self.data_pipeline(valid_data)
                X_valid = self.preprocess_inputs(valid_inputs)
                Y_valid = self.preprocess_outputs(valid_targets)
                out_dict["valid_score"] = self._core_model.score(X_valid, Y_valid)
            # TODO: also score self.metrics on both datasets
        else:
            raise NotImplementedError(
                "Task: " + self.meta_architecture + "not implemented"
            )
        # Write training results
        # TODO: collect and serialize train hyperparameters
        if write:
            self._threaded_to_json(out_dict, "train_results.json")
        return out_dict

    def infer_data(
        self,
        test_data,
        postprocess=True,
        hp=None,
    ):
        """Computes and returns inputs, targets and predictions given test data.

        Processes test data, returning inputs, targets and prediction by the
        CIDSModel.

        Args:
            test_data:      the test data, containing inputs and targets
            checkpoint:     which checkpoint to load
            postprocess:    whether to apply postprocessing
            hp:             a keras tuner HyperParameter sets @ depricated?

        Returns:
            X, Y, Y_:       input, target and predictions tensors
        """
        # Hyperparameters
        if hp is None:
            if self._hp is None:
                hp = HyperParameters()
            else:
                hp = self._hp
        self._hp = hp

        # Data pipeline
        test_inputs, test_targets = self.data_pipeline(test_data)
        X = self.preprocess_inputs(test_inputs)
        if not postprocess:
            Y = self.preprocess_outputs(test_targets)
        # Infer
        if self.VERBOSITY:
            self.log("Inferring...")
        try:
            Y_ = self._core_model.predict(X)
        except AttributeError:
            try:
                Y_ = self._core_model.transform(X)
            except AttributeError as e:
                raise AttributeError(
                    "Core model has neither `predict` nor `transform` method."
                ) from e
        # Postprocessing
        if postprocess:
            if self.VERBOSITY:
                self.log("Postprocessing...")
            test_predictions = self.postprocess_outputs(Y_)
        # Concatenate batches
        if self.VERBOSITY:
            self.log("Inference completed.")
        # Output
        if postprocess:
            return test_inputs, test_targets, test_predictions
        return X, Y, Y_

    def predict(self, test_inputs, postprocess=True, hp=None):
        """Computes and returns predictions given input data.

        Processes test data, returning inputs, targets and prediction by the
        CIDSModel.

        Args:
            X:              inputs data
            postprocess:    whether to apply postprocessing
            hp:             a keras tuner HyperParameter sets @ depricated

        Returns:
            Y_:       prediction tensor
        """
        # Hyperparameters
        if hp is None:
            if self._hp is None:
                hp = HyperParameters()
            else:
                hp = self._hp
        self._hp = hp
        # Build
        self.build(hp=hp)
        # Data pipeline
        if self.VERBOSITY:
            self.log("Preprocessing...")
        X = self.preprocess_inputs(test_inputs)
        # Infer
        if self.VERBOSITY:
            self.log("Predicting...")
        try:
            Y_ = self._core_model.predict(X)
        except AttributeError:
            try:
                Y_ = self._core_model.transform(X)
            except AttributeError as e:
                raise AttributeError(
                    "Core model has neither `predict` nor `transform` method."
                ) from e
        # Postprocessing
        if postprocess:
            if self.VERBOSITY:
                self.log("Postprocessing...")
            test_predictions = self.postprocess_outputs(Y_)
        # Concatenate batches
        if self.VERBOSITY:
            self.log("Prediction completed.")
        # Output
        if postprocess:
            return test_predictions
        return Y_

    def eval_data(
        self,
        *args,
        metrics=None,
        metrics_kwargs=None,
        out_dict=None,
        hp=None,
        write=True,
    ):
        """Evaluates losses and metrics of the entire test data.

        Processes test data, returning losses and metrics by the
        CIDSModel.

        Args:
            test_data:      the test data, containing inputs and targets
                OR
            Y:              true output / targets tensor
            Y_:             predicted output / prediction tensor
            metrics:        additional keras/sklearn metrics callbacks
            metrics_kwargs: extra kwargs for metrics callbacks
            out_dict:       dictionary to add to eval results
            hp:             a keras tuner HyperParameter set
            write:          write results to a file?

        Returns:
            results:        losses and evaluated metrics on test batch
        """
        if self.VERBOSITY:
            self.log("Evaluating...")
        if len(args) == 1:  # parse data
            test_data = args[0]
            _, Y, Y_ = self.infer_data(
                test_data,
                postprocess=False,
                hp=hp,
            )
        elif len(args) == 2:  # parse true y and predicted y
            Y, Y_ = args[0], args[1]
        else:
            raise ValueError(
                "Requires files or target and prediction tensors as first arguments."
            )
        # Distinguish between keras, sklearn and own metrics
        metrics = metrics or self.metrics or []
        metrics_kwargs = metrics_kwargs or self.metrics_kwargs or {}
        keras_metrics, functional_metrics = self._filter_metrics(
            metrics, prioritize="sklearn"
        )

        # Evaluate Keras metrics
        if keras_metrics:
            raise NotImplementedError(
                f"Cannot compute keras metrics metrics: {keras_metrics}"
            )
        results = out_dict or {}
        # Evaluate Scikit-learn and functional metrics
        if functional_metrics:
            functional_results = {
                m.__name__: m(
                    np.squeeze(Y),
                    np.squeeze(Y_),
                    **metrics_kwargs.get(m.__name__, {}),
                )
                for m in functional_metrics
            }
            results.update(functional_results)
        if self.VERBOSITY:
            self.log("Evaluating completed.")
        # Write results to human readable format
        if write:
            # Model and test results
            out_dict = {"eval_results": results, "hyper_parameters": hp}
            # Write
            self._threaded_to_json(out_dict, "eval_results.json")
        return results

    def search(
        self,
        train_data,
        valid_data=None,
        method="bayesian",
        num_trials=10,
        scoring=None,
        cv=None,
    ):
        """Searches for the best possible hyper parameters using keras tuner.

        Keras tuner provides several methods for hyperparameter search. To
        add a hyperparameter to the search space, the schedule in *train_args
        or the sklearn model must be defined as a function that processes a hp
        argument. This hp argument allows the definition of a range or choice of
        values for each hyper parameters. Outside of the search function, the
        default value is used.

        Args:
        train_data: training data, containing inputs and targets
        valid_data: validation data, if not provided (None), training data will
            be 5-fold cross-validated to serve as validation data (see cv)
        method: `bayesian` or `random` are supported (no "hyperband")
        num_trials: number of trials (Random/Bayesian search)
        scoring: An sklearn `scoring` function (e.g. `metrics.make_scorer(
            metrics.accuracy_score)`). If not provided, the Model's default
            scoring will be used via `model.score`
        cv: An `sklearn.model_selection` Splitter class. Used to
            determine how samples are split up into groups for
            cross-validation. If not provided, default is
            `sklearn.model_selection.KFold(5, shuffle=True, random_state=1`)

        ATTENTION:
        1. if no valid_data, cross-validation on train_data is used.
            However, currect design will fit preprocessors on whole train_data,
            then split train_data and perform cv, which is inappropriate.

        Returns:
            hp: a HyperParameters() with best hyperparameters.
        """
        if self.VERBOSITY:
            self.log(f"Starting {method:s} search...")

        # Add schedule hyperparameters
        hp = self._hp or HyperParameters()
        self._hp = hp

        # some saving settings
        # Find previous searches
        previous_searches = glob.glob(os.path.join(self.base_model_dir, "*search*"))
        previous_searches = [
            s
            for s in previous_searches
            if os.path.isdir(s) and os.path.exists(os.path.join(s, "oracle.json"))
        ]
        num_previous_searches = len(previous_searches)
        # Update seeds if previous search,
        # to ensure diff. hyperparameters are sampled
        if num_previous_searches > 0:  # is this needed?
            for _ in previous_searches:
                seed = np.random.get_state()[1][0] + len(previous_searches) + 1  # ?
                seed = int(
                    projects.set_project_seeds(seed)  # ?
                )  # TODO: not find in class projects
        else:
            seed = int(projects.set_project_seeds(np.random.get_state()[1][0]))  # ?

        # Set search directories
        # Create a new search directory
        search_identifier = f"{num_previous_searches:02d}"
        self.meta_folder = "_".join([method, "search", search_identifier])

        # Set oracle and tuner
        if "bayesian" in method:
            oracle = keras_tuner.oracles.BayesianOptimizationOracle(
                objective=keras_tuner.Objective("score", "max"),
                max_trials=num_trials,
                seed=seed,
            )
        elif "random" in method:
            oracle = keras_tuner.oracles.RandomSearchOracle(
                objective=keras_tuner.Objective("score", "max"),
                max_trials=num_trials,
                seed=seed,
            )
        else:
            raise ValueError(
                "Unsupported hyperparameter optimization method: " + method
            )

        # Data pipeline
        self.build(hp=hp)  # build() for making model ready to train
        train_inputs, train_targets = self.data_pipeline(train_data)
        self._fit_preprocessors(train_inputs, train_targets)
        # Preprocess inputs
        X_train = self.preprocess_inputs(train_inputs)
        Y_train = self.preprocess_outputs(train_targets)

        if valid_data is not None:
            valid_inputs, valid_targets = self.data_pipeline(valid_data)
            X_valid = self.preprocess_inputs(valid_inputs)
            Y_valid = self.preprocess_outputs(valid_targets)

            # Trick to deal with cross-validation(cv) in SklearnTuner
            # ref: https://stackoverflow.com/questions/71740662/
            # how-to-use-an-explicit-validation-set-with-predefined-split-fold
            # step 1. concatenate training data and validation data
            # step 2. then use PredefinedSplit to split it back
            n_train = X_train.shape[0]
            n_valid = X_valid.shape[0]
            X_train = np.concatenate((X_train, X_valid))
            Y_train = np.concatenate((Y_train, Y_valid))
            cv = PredefinedSplit(
                np.concatenate((np.zeros(n_train) - 1, np.ones(n_valid)))
            )  # for checking use: for xx in cv.split(): print("xx", xx)

        # Set keras sklearn-tuner
        tuner = keras_tuner.tuners.SklearnTuner(
            oracle=oracle,
            hypermodel=self._core_model_function,
            scoring=scoring,
            cv=cv,
            directory=self.base_model_dir,
            project_name=self.meta_folder,
        )
        tuner.search(X_train, Y_train)
        best_hyperparameters = tuner.get_best_hyperparameters()[0]

        self.clear()  # reset model for later train
        return best_hyperparameters

    def preprocess_inputs(self, inputs):
        """Preprocess inputs, such as scaling/normalizing.

        Args:
            inputs: to be processed input data, usually in array-like format.

        Returns:
            data: processed data in np-array format.
        """
        X = inputs
        # Apply transformer
        if self.input_transformer:
            X = self.input_transformer.transform(X)
        # Squeeze
        X = np.squeeze(X)
        return X

    def preprocess_outputs(self, outputs):
        """Preprocess outputs, such as scaling/normalizing.

        Args:
            ouputs: to be processed output data, usually in array-like format.

        Returns:
            data: processed data in np-array format.
        """
        Y = outputs
        # Apply transformer
        if self.output_transformer:
            Y = self.output_transformer.transform(Y)
        # Squeeze
        Y = np.squeeze(Y)
        return Y

    def postprocess_inputs(self, inputs):
        """Postprocess inputs by inverting scaling/normalizing.

        Args:
            inputs: to be processed input data, usually in array-like format.

        Returns:
            data: processed data in np-array format.
        """
        X = inputs
        # Apply transformer
        if self.input_transformer:
            X = self.input_transformer.inverse_transform(X)
        return X

    def postprocess_outputs(self, outputs):
        """Postprocess outputs by inverting scaling/normalizing.

        Args:
            outputs: to be processed output data, usually in array-like format.

        Returns:
            data: processed data in np-array format.
        """
        Y = outputs
        # Apply transformer
        if self.output_transformer:
            Y = self.output_transformer.inverse_transform(Y)
        return Y

    def to_json(self, file, **kwargs):
        """Serialize model to human-readable json file.

        Args:
            file:       a json file
            **kwargs:   a dictionary of additional data to store in the json
        """
        out_dict = deepcopy(kwargs)
        # Add model information
        model_dict = {
            "settings": {k: v for k, v in self.__dict__.items() if not callable(v)}
        }
        # TODO(@Yinghan): serialize sklearn models (compare CIDSModelTF)
        # Collect, clean, and output
        out_dict["model"] = model_dict
        # Call BaseModel
        super().to_json(file, **out_dict)
