# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Logging functionality for all CIDS classes. Part of the CIDS toolbox.

    Classes:
        BaseLoggerMixin:    A Mixin class to add logging functionality to child class
        StreamToLogger:     File-like interface to stream file / output to BaseLogger

"""
import logging
import os
import sys
import traceback
from io import TextIOBase

from colorlog import ColoredFormatter
from tqdm.auto import tqdm


class StreamToLogger(TextIOBase):
    """File-like stream object that redirects writes to a logger instance.

    Args:
        logger:     An instanc of a class inheriting from BaseLoggerMixin
        level:      The level of logging for text data streamed
    """

    def __init__(
        self, logger, level=logging.INFO
    ):  # pylint: disable=super-init-not-called
        self.logger = logger
        self.level = level
        self.linebuf = ""

    def write(self, buf):
        lines = [line for line in buf.rstrip().splitlines() if line]
        for line in lines:
            self.logger.log(self.level, line.rstrip())

    def flush(self):
        pass


class _TqdmHandler(logging.StreamHandler):
    """Hack to avoid tqdm progress bar interruption by logger's output to console."""

    def emit(self, record):
        try:
            msg = self.format(record)
            tqdm.write(msg, end=self.terminator)
            self.flush()
        except (RecursionError, KeyboardInterrupt, SystemExit):
            raise
        except Exception:
            self.handleError(record)


class BaseLoggerMixin:
    """Mixin class to add logging functionality to inheriting class."""

    DEBUG = False
    log_file = None

    def _start_logging(self, set_root=False):
        if self.DEBUG:
            level = logging.DEBUG
        else:
            level = logging.INFO
        if self.log_file is not None:
            filename = os.fspath(self.log_file)
            self.log_file.parent.mkdir(parents=True, exist_ok=True)
        else:
            filename = None
        if set_root:
            # Create root logger
            self._root_logger = logging.getLogger()
            self._root_logger.setLevel(level)
            # Handlers
            if not any(isinstance(h, _TqdmHandler) for h in self._root_logger.handlers):
                self._tqdm_handler = _TqdmHandler()
                self._tqdm_handler.setFormatter(
                    ColoredFormatter(
                        "%(bold_white)s%(name)s:%(reset)s "
                        + "%(log_color)s%(message)s%(reset)s",
                        log_colors={
                            "DEBUG": "thin_white",
                            "INFO": "white",
                            "WARNING": "yellow",
                            "ERROR": "red",
                            "CRITICAL": "bold_red",
                        },
                    )
                )
                self._root_logger.addHandler(self._tqdm_handler)
            self._file_handler = logging.FileHandler(filename)
            self._file_handler.setFormatter(
                logging.Formatter("[%(asctime)s)](%(levelname)s)%(name)s: %(message)s")
            )
            self._root_logger.addHandler(self._file_handler)
            # Capture exceptions too
            sys.excepthook = log_exceptions
        # Class logger
        self._logger = logging.getLogger(self.__class__.__name__)
        self._logger.setLevel(level)

    def stream_to_logger(self, level=logging.INFO):
        return StreamToLogger(self._logger, level=level)

    def log(self, msg, **kwargs):
        self._logger.info(msg, **kwargs)

    def debug(self, msg, **kwargs):
        self._logger.debug(msg, **kwargs)

    def info(self, msg, **kwargs):
        self._logger.info(msg, **kwargs)

    def warn(self, msg, **kwargs):
        # warnings.warn(msg)
        self._logger.warning(msg, **kwargs)

    def error(self, msg, **kwargs):
        self._logger.error(msg, **kwargs)

    def critical(self, msg, **kwargs):
        self._logger.critical(msg, **kwargs)

    # https://stackoverflow.com/questions/23113494/double-progress-bar-in-python
    # @staticmethod
    # def pit(it, *pargs, **nargs):
    #     """Easily create enlighten progress bar using a global manager instance.

    #     Args:
    #         it (iteratable): an iterable

    #     Yields:
    #         iterable: iterable with progress bar
    #     """
    #     import enlighten

    #     global __pit_man__
    #     try:
    #         __pit_man__
    #     except NameError:
    #         __pit_man__ = enlighten.get_manager()
    #     man = __pit_man__
    #     try:
    #         it_len = len(it)
    #     except Exception:
    #         it_len = None
    #     try:
    #         ctr = None
    #         for i, e in enumerate(it):
    #             if i == 0:
    #                 ctr = man.counter(
    #                     *pargs, **{**dict(leave=False, total=it_len), **nargs}
    #                 )
    #             yield e
    #             ctr.update()
    #     finally:
    #         if ctr is not None:
    #             ctr.close()


# log uncaught exceptions
def log_exceptions(type, value, tb):
    logger = logging.getLogger()
    name = logger.name
    logger.name = "ERROR"
    for line in traceback.TracebackException(type, value, tb).format(chain=True):
        logger.error(line.strip())
    logger.error(value)
    logger.name = name
    # sys.__excepthook__(type, value, tb)  # calls default excepthook
