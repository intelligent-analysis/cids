# Get Python six functionality:
###############################################################################
###############################################################################
###############################################################################
import pytest

from ...analyzer import AugmentReduceBase
from ...analyzer import GaussianSmoother
from ...analyzer import Gradient
from ...analyzer import PathIntegrator
from ...analyzer import WrapperBase
from ...utils.tests import cases
from ...utils.tests import dryrun


###############################################################################
###############################################################################
###############################################################################


@pytest.mark.fast
@pytest.mark.precommit
@pytest.mark.parametrize("case_id", cases.FAST)
def test_fast__WrapperBase(case_id):
    def create_analyzer_f(model):
        return WrapperBase(Gradient(model))

    dryrun.test_analyzer(case_id, create_analyzer_f)


@pytest.mark.precommit
@pytest.mark.parametrize("case_id", cases.PRECOMMIT)
def test_precommit__WrapperBase(case_id):
    def create_analyzer_f(model):
        return WrapperBase(Gradient(model))

    dryrun.test_analyzer(case_id, create_analyzer_f)


###############################################################################
###############################################################################
###############################################################################


@pytest.mark.fast
@pytest.mark.precommit
@pytest.mark.parametrize("case_id", cases.FAST)
def test_fast__AugmentReduceBase(case_id):
    def create_analyzer_f(model):
        return AugmentReduceBase(Gradient(model))

    dryrun.test_analyzer(case_id, create_analyzer_f)


@pytest.mark.precommit
@pytest.mark.parametrize("case_id", cases.PRECOMMIT)
def test_precommit__AugmentReduceBase(case_id):
    def create_analyzer_f(model):
        return AugmentReduceBase(Gradient(model))

    dryrun.test_analyzer(case_id, create_analyzer_f)


###############################################################################
###############################################################################
###############################################################################


@pytest.mark.fast
@pytest.mark.precommit
@pytest.mark.parametrize("case_id", cases.FAST)
def test_fast__GaussianSmoother(case_id):
    def create_analyzer_f(model):
        return GaussianSmoother(Gradient(model))

    dryrun.test_analyzer(case_id, create_analyzer_f)


@pytest.mark.precommit
@pytest.mark.parametrize("case_id", cases.PRECOMMIT)
def test_precommit__GaussianSmoother(case_id):
    def create_analyzer_f(model):
        return GaussianSmoother(Gradient(model))

    dryrun.test_analyzer(case_id, create_analyzer_f)


###############################################################################
###############################################################################
###############################################################################


@pytest.mark.fast
@pytest.mark.precommit
@pytest.mark.parametrize("case_id", cases.FAST)
def test_fast__PathIntegrator(case_id):
    def create_analyzer_f(model):
        return PathIntegrator(Gradient(model))

    dryrun.test_analyzer(case_id, create_analyzer_f)


@pytest.mark.precommit
@pytest.mark.parametrize("case_id", cases.PRECOMMIT)
def test_precommit__PathIntegrator(case_id):
    def create_analyzer_f(model):
        return PathIntegrator(Gradient(model))

    dryrun.test_analyzer(case_id, create_analyzer_f)
