# Get Python six functionality:
###############################################################################
###############################################################################
###############################################################################
import pytest

from ...analyzer import Input
from ...analyzer import Random
from ...utils.tests import cases
from ...utils.tests import dryrun


###############################################################################
###############################################################################
###############################################################################


@pytest.mark.fast
@pytest.mark.precommit
@pytest.mark.parametrize("case_id", cases.FAST + cases.PRECOMMIT)
def test_fast__Input(case_id):
    def create_analyzer_f(model):
        return Input(model)

    dryrun.test_analyzer(case_id, create_analyzer_f)


@pytest.mark.fast
@pytest.mark.precommit
@pytest.mark.parametrize("case_id", cases.FAST + cases.PRECOMMIT)
def test_fast__Random(case_id):
    def create_analyzer_f(model):
        return Random(model)

    dryrun.test_analyzer(case_id, create_analyzer_f)
