# Get Python six functionality:
###############################################################################
###############################################################################
###############################################################################
from .. import layers as ilayers
from .. import utils as iutils
from .base import AnalyzerNetworkBase

__all__ = ["Random", "Input"]


###############################################################################
###############################################################################
###############################################################################


class Input(AnalyzerNetworkBase):
    """Returns the input.

    Returns the input as analysis.

    :param model: A Keras model.
    """

    def _create_analysis(self, model, stop_analysis_at_tensors=[]):
        tensors_to_analyze = [
            x for x in iutils.to_list(model.inputs) if x not in stop_analysis_at_tensors
        ]
        return [ilayers.Identity()(x) for x in tensors_to_analyze]


class Random(AnalyzerNetworkBase):
    """Returns noise.

    Returns the Gaussian noise as analysis.

    :param model: A Keras model.
    :param stddev: The standard deviation of the noise.
    """

    def __init__(self, model, stddev=1, **kwargs):
        self._stddev = stddev

        super().__init__(model, **kwargs)

    def _create_analysis(self, model, stop_analysis_at_tensors=[]):
        noise = ilayers.TestPhaseGaussianNoise(stddev=self._stddev)
        tensors_to_analyze = [
            x for x in iutils.to_list(model.inputs) if x not in stop_analysis_at_tensors
        ]
        return [noise(x) for x in tensors_to_analyze]
