# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Tensorflow/Keras model for CIDS. Part of the CIDS toolbox.
"""
import os

import numpy as np
import tensorflow as tf
from tensorflow.python.keras.engine import data_adapter

# NOTE: Do not assign core model components to variables inside the compiled
# @tf.function train and test steps! The assignment will be hard coded at compile time
# and the metrics will not update properly.


def get_gan_mode(ordered_keys):
    if not isinstance(ordered_keys, list):
        ordered_keys = ordered_keys.core_model.keys()
    if "conditional" in "".join(ordered_keys):
        return "conditional"
    if "auxiliary" in "".join(ordered_keys):
        return "auxiliary"
    return None


def _shorten_name(name, prefix=""):
    if "Subcore" in name:
        return prefix + name.split("_", maxsplit=1)[1]
    return prefix + name


class MeanBalance(tf.keras.metrics.Mean):
    def __init__(self, name="mean_balance", optimal_balance=0.7, dtype=None):
        super().__init__(name=name, dtype=dtype)
        self.optimal_balance = optimal_balance

    def update_state(self, values, sample_weight=None):
        return super().update_state(
            tf.abs(values - self.optimal_balance), sample_weight=sample_weight
        )


class GANControl(tf.keras.callbacks.Callback):
    def __init__(self, gan_model, decay_factor=0.99):
        self.gan_model = gan_model
        self.decay_factor = decay_factor
        super().__init__()

    def _decay(self, variable, factor):
        if variable is not None:
            value = tf.keras.backend.get_value(variable)
            value *= factor
            tf.keras.backend.set_value(variable, value)

    def _decay_python(self, variable, factor):
        if variable is not None:
            return variable * factor

    def _flip(self, variable, probability):
        if variable is not None:
            if np.random.binomial(1, probability):
                value = tf.keras.backend.get_value(variable)
                value = not value
                tf.keras.backend.set_value(variable, value)

    def on_epoch_begin(self, epoch, logs=None):
        self._decay(self.gan_model.noisy_image_var, self.decay_factor)
        self.gan_model.noisy_labels = self._decay_python(
            self.gan_model.noisy_labels, self.decay_factor
        )
        self._flip(self.gan_model.noisy_labels_var, self.gan_model.noisy_labels)


class GANModel(tf.keras.Model):
    def __init__(
        self,
        generator,
        adversary,
        smooth_labels=0.2,
        noisy_image=0.05,
        noisy_labels=0.001,
        decay_factor=0.99,
        optimal_balance=0.7,
    ):
        """A Generative Adversarial Network (GAN) model for keras

        Args:
            generator (keras.Model): generator model
            discriminator (keras.Model): discriminator model
            smooth_labels (float): noise standard deviation to apply to each label
            noisy_labels (float): probability for flipping the labels each batch
            optimal_balance (float): desired target accuracy for binary discrimination
        """
        super().__init__()
        self.generator = generator
        self.adversary = adversary
        self.smooth_labels = smooth_labels
        self.smooth_labels_var = None
        self.noisy_image = noisy_image
        self.noisy_image_var = None
        self.noisy_labels = noisy_labels
        self.noisy_labels_var = None
        self.decay_factor = decay_factor
        if len(adversary.inputs) > 1:
            self.mode = "conditional"
        elif len(adversary.outputs) > 1:
            self.mode = "auxiliary"
        else:
            self.mode = None
        # Average the distance of the accuracy of the binary discrimination from 0.7
        # over the entire training to find models with stable learning
        self.train_balance_tracker = MeanBalance(
            "mean_balance", optimal_balance=optimal_balance
        )
        self.val_balance_tracker = MeanBalance(
            "mean_balance", optimal_balance=optimal_balance
        )

    def build(self, input_shape):
        if self.smooth_labels:
            self.smooth_labels_var = self.add_weight(
                name="smooth_labels", shape=None, dtype=tf.float32, trainable=False
            )
            tf.keras.backend.set_value(self.smooth_labels_var, self.smooth_labels)
        if self.noisy_image:
            self.noisy_image_var = self.add_weight(
                name="noisy_image", shape=None, dtype=tf.float32, trainable=False
            )
            tf.keras.backend.set_value(self.noisy_image_var, self.noisy_image)
        if self.noisy_labels:
            self.noisy_labels_var = self.add_weight(
                name="noisy_labels", shape=None, dtype=tf.bool, trainable=False
            )
            tf.keras.backend.set_value(self.noisy_labels_var, False)
            # self.noisy_labels_var = self.add_weight(
            #     name="noisy_labels", shape=None, dtype=tf.float32, trainable=False
            # )
            # tf.keras.backend.set_value(self.noisy_labels_var, self.noisy_labels)

    def call(self, inputs, training=None):
        if training is None:
            training = tf.keras.backend.learning_phase()
        return self.generator(inputs, training=training)

    def fit(self, *args, **kwargs):
        callbacks = kwargs.get("callbacks", [])
        callbacks.append(GANControl(self, decay_factor=self.decay_factor))
        kwargs["callbacks"] = callbacks
        return super().fit(*args, **kwargs)

    def compile(
        self,
        optimizer_gen,
        optimizer_adv,
        loss_gen=None,
        loss_adv=None,
        metrics_gen=None,
        metrics_adv=None,
    ):
        self.generator.compile(optimizer_gen, loss=loss_gen, metrics=metrics_gen)
        self.adversary.compile(optimizer_adv, loss=loss_adv, metrics=metrics_adv)
        super().compile()

    def train_step(self, data):
        # Get data
        data = data_adapter.expand_1d(data)
        x, y, sample_weight = data_adapter.unpack_x_y_sample_weight(data)
        # Meta architecture assembly
        with tf.GradientTape() as forward_tape, tf.GradientTape() as adversarial_tape:
            # Forward model prediction
            y_ = self.generator(x, training=True)
            # Apply noise to auxiliary labels
            if self.noisy_image:
                y = y + tf.random.normal(tf.shape(y), stddev=self.noisy_image_var)
                y_ = y_ + tf.random.normal(tf.shape(y), stddev=self.noisy_image_var)
            # Adversarial model prediction fake and real input
            if self.mode == "conditional":
                e_ = self.adversary((x, y_), training=True)
                e = self.adversary((x, y), training=True)
            elif self.mode == "auxiliary":
                e_, x_y_ = self.adversary(y_, training=True)
                e, x_y = self.adversary(y, training=True)
            else:
                e_ = self.adversary(y_, training=True)
                e = self.adversary(y, training=True)
            # Compute Masks
            if sample_weight is None:
                mask_e = None
                mask_x_y = None
                double_mask_e = None
            else:
                mask_e = tf.concat([sample_weight] * e_.shape[-1], -1)
                mask_x_y = tf.concat([sample_weight] * x_y_.shape[-1], -1)
                double_mask_e = tf.concat([mask_e] * 2, 0)
            # Losses
            e_true = tf.ones_like(e_)
            e_fake = tf.zeros_like(e_)
            # Apply noise to binary labels
            if self.smooth_labels:
                e_true = e_true - self.smooth_labels_var * tf.random.uniform(
                    tf.shape(e_true)
                )
                e_fake = e_fake + self.smooth_labels_var * tf.random.uniform(
                    tf.shape(e_fake)
                )
            # Flip labels
            if self.noisy_labels:
                # flip = tf.random.uniform([], minval=0.0, maxval=1.0)
                # e_true = tf.cond(
                #     flip > self.noisy_labels_var,
                #     lambda: tf.identity(e_true),
                #     lambda: tf.identity(e_fake),
                # )
                # e_fake = tf.cond(
                #     flip > self.noisy_labels_var,
                #     lambda: tf.identity(e_fake),
                #     lambda: tf.identity(e_true),
                # )
                # self.noisy_labels_var.assign(flip < self.noisy_labels)
                e_true = tf.cond(
                    self.noisy_labels_var,
                    lambda: tf.identity(e_fake),
                    lambda: tf.identity(e_true),
                )
                e_fake = tf.cond(
                    self.noisy_labels_var,
                    lambda: tf.identity(e_true),
                    lambda: tf.identity(e_fake),
                )
            # Forward losses
            if self.mode == "auxiliary":
                l_forward = self.adversary.compiled_loss(
                    (e_true, x),
                    (e_, x_y_),
                    (mask_e, mask_x_y),
                    regularization_losses=self.generator.losses,
                )
            else:
                l_forward = self.adversary.compiled_loss(
                    e_true,
                    e_,
                    mask_e,
                    regularization_losses=self.generator.losses,
                )
            # Adversarial losses
            if self.mode == "auxiliary":
                # # Apply noise to auxiliary labels
                # if self.noisy_labels:
                #     x = x + tf.random.normal(tf.shape(x))
                l_adversarial1 = self.adversary.compiled_loss(
                    (e_fake, x),
                    (e_, x_y_),
                    (mask_e, mask_x_y),
                    regularization_losses=self.adversary.losses,
                )
                l_adversarial2 = self.adversary.compiled_loss(
                    (e_true, x),
                    (e, x_y),
                    (mask_e, mask_x_y),
                    regularization_losses=self.adversary.losses,
                )
            else:
                l_adversarial1 = self.adversary.compiled_loss(
                    e_fake,
                    e_,
                    mask_e,
                    regularization_losses=self.adversary.losses,
                )
                l_adversarial2 = self.adversary.compiled_loss(
                    e_true,
                    e,
                    mask_e,
                    regularization_losses=self.adversary.losses,
                )
            # Combine losses
            l_adversarial = l_adversarial1 + l_adversarial2
        # Compute gradients
        # clipnorms working????
        gradients_forward = forward_tape.gradient(
            l_forward, self.generator.trainable_variables
        )
        gradients_adversarial = adversarial_tape.gradient(
            l_adversarial, self.adversary.trainable_variables
        )
        # Apply gradients
        self.generator.optimizer.apply_gradients(
            zip(gradients_forward, self.generator.trainable_variables)
        )
        self.adversary.optimizer.apply_gradients(
            zip(
                gradients_adversarial,
                self.adversary.trainable_variables,
            )
        )
        # Update metrics
        self.generator.compiled_metrics.update_state(y, y_, sample_weight)
        if self.mode == "auxiliary":
            # Auxiliary metrics for test only measure predictions from true inputs with
            # true outputs
            self.adversary.compiled_metrics.update_state(
                (tf.concat([tf.ones_like(e), tf.zeros_like(e_)], 0), x),
                (tf.concat([e, e_], 0), x_y),
                (double_mask_e, mask_x_y),
            )
        else:
            self.adversary.compiled_metrics.update_state(
                tf.concat([e_true, e_fake], 0), tf.concat([e, e_], 0), double_mask_e
            )
        # Merge metrics
        metrics = {_shorten_name(m.name): m.result() for m in self.generator.metrics}
        metrics.update(
            {
                _shorten_name(m.name, prefix="adv_"): m.result()
                for m in self.adversary.metrics
            }
        )
        # Compute balance metric
        adv_accuracy = metrics.get("adv_accuracy")
        if adv_accuracy is not None:
            self.train_balance_tracker.update_state(  # pylint: disable=not-callable
                adv_accuracy
            )
            short_name = _shorten_name(self.train_balance_tracker.name)
            metrics[
                short_name
            ] = self.train_balance_tracker.result()  # pylint: disable=not-callable
        # Overwrite loss metrics
        metrics["loss"] = l_forward
        metrics["adv_loss"] = l_adversarial
        return metrics

    def test_step(self, data):
        data = data_adapter.expand_1d(data)
        x, y, sample_weight = data_adapter.unpack_x_y_sample_weight(data)
        # Meta architecture assembly
        y_ = self.generator(x, training=False)
        # Adversarial model prediction fake and real input
        if self.mode == "conditional":
            e_ = self.adversary((x, y_), training=False)
            e = self.adversary((x, y), training=False)
        elif self.mode == "auxiliary":
            e_, x_y_ = self.adversary(y_, training=False)
            e, x_y = self.adversary(y, training=False)
        else:
            e_ = self.adversary(y_, training=False)
            e = self.adversary(y, training=False)
        # Compute Masks
        if sample_weight is None:
            mask_e = None
            mask_x_y = None
            double_mask_e = None
        else:
            mask_e = tf.concat([sample_weight] * e_.shape[-1], -1)
            mask_x_y = tf.concat([sample_weight] * x_y_.shape[-1], -1)
            double_mask_e = tf.concat([mask_e] * 2, 0)
        # Losses
        e_true = tf.ones_like(e_)
        e_fake = tf.zeros_like(e_)
        if self.mode == "auxiliary":
            l_forward = self.adversary.compiled_loss(
                (e_true, x),
                (e_, x_y_),
                (mask_e, mask_x_y),
                regularization_losses=self.generator.losses,
            )
            l_adversarial1 = self.adversary.compiled_loss(
                (e_fake, x),
                (e_, x_y_),
                (mask_e, mask_x_y),
                regularization_losses=self.adversary.losses,
            )
            l_adversarial2 = self.adversary.compiled_loss(
                (e_true, x),
                (e, x_y),
                (mask_e, mask_x_y),
                regularization_losses=self.adversary.losses,
            )
        else:
            l_forward = self.adversary.compiled_loss(
                e_true, e_, mask_e, regularization_losses=self.generator.losses
            )
            l_adversarial1 = self.adversary.compiled_loss(
                e_fake, e_, mask_e, regularization_losses=self.adversary.losses
            )
            l_adversarial2 = self.adversary.compiled_loss(
                e_true, e, mask_e, regularization_losses=self.adversary.losses
            )
        # Combine losses
        l_adversarial = l_adversarial1 + l_adversarial2
        # Update metrics
        self.generator.compiled_metrics.update_state(y, y_, sample_weight)
        if self.mode == "auxiliary":
            # Auxiliary metrics for test only measure predictions from true inputs with
            # true outputs
            self.adversary.compiled_metrics.update_state(
                (tf.concat([e_true, e_fake], 0), x),
                (tf.concat([e, e_], 0), x_y),
                (double_mask_e, mask_x_y),
            )
        else:
            self.adversary.compiled_metrics.update_state(
                tf.concat([e_true, e_fake], 0), tf.concat([e, e_], 0), double_mask_e
            )
        # Merge metrics
        metrics = {_shorten_name(m.name): m.result() for m in self.generator.metrics}
        metrics.update(
            {
                _shorten_name(m.name, prefix="adv_"): m.result()
                for m in self.adversary.metrics
            }
        )
        # Compute balance metric
        adv_accuracy = metrics.get("adv_accuracy")
        if adv_accuracy is not None:
            self.val_balance_tracker.update_state(  # pylint: disable=not-callable
                adv_accuracy
            )
            short_name = _shorten_name(self.val_balance_tracker.name)
            metrics[
                short_name
            ] = self.val_balance_tracker.result()  # pylint: disable=not-callable
        # Overwrite loss metrics
        metrics["loss"] = l_forward
        metrics["adv_loss"] = l_adversarial
        return metrics

    def predict_step(self, data):
        data = data_adapter.expand_1d(data)
        x, y, _ = data_adapter.unpack_x_y_sample_weight(data)
        y_ = self.generator(x, training=False)
        if self.mode == "auxiliary":
            return y_, self.adversary(y, training=False)
        return y_

    def reset_metrics(self):
        self.generator.reset_metrics()
        self.adversary.reset_metrics()
        super().reset_metrics()

    def save(
        self,
        filepath,
        overwrite=True,
        include_optimizer=True,
        save_format=None,
        signatures=None,
        options=None,
        save_traces=True,
    ):
        filepath_gen = os.path.join(filepath, "gen")
        filepath_adv = os.path.join(filepath, "adv")
        self.generator.save(
            filepath_gen,
            overwrite=overwrite,
            include_optimizer=include_optimizer,
            save_format=save_format,
            signatures=signatures,
            options=options,
            save_traces=save_traces,
        )
        self.adversary.save(
            filepath_adv,
            overwrite=overwrite,
            include_optimizer=include_optimizer,
            save_format=save_format,
            signatures=signatures,
            options=options,
            save_traces=save_traces,
        )
