# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Stateful and accurate online processing layers for CIDS. Part of the CIDS toolbox.

    Classes:
        ExpandMergeFeatures:    Expands and merge multiple features
        OnlineNormalize:        Standardizes data with online statistics
        OnlinePCA:              experimental) standardizes with online PCA
        SaltAndPepper:          Salt and Pepper noise layer
        SequenceMasking:        Masks the sequence to sequence length

"""
import tensorflow as tf
from tensorflow.keras import backend as K


def _read_axes(data_format):
    """Returns the axe indices from the data format string."""
    # Handle dictionary type data format
    if isinstance(data_format, dict):
        data_axes = {k: _read_axes(v) for k, v in data_format.items()}
        return [dict(zip(data_axes, t)) for t in zip(*data_axes.values())]
    assert len(data_format) > 1, "data_format string must be longer than 1"
    # Batch
    assert "N" in data_format, "Requires at least batch axis: N"
    batch_axis = data_format.index("N")
    # Feature or Channel
    if "F" in data_format:
        feature_axis = data_format.index("F") - len(
            data_format
        )  # Use negative index here!
    elif "C" in data_format:
        feature_axis = data_format.index("C") - len(
            data_format
        )  # Use negative index here!
    else:
        feature_axis = None
    # Sequence
    if "S" in data_format:
        sequence_axis = data_format.index("S")
    else:
        sequence_axis = None
    # Spatial axes (height, width, x, y, ...)
    spatial_axes = [i for i, a in enumerate(data_format) if a in "DWHXYZ"]
    # Iteration axis
    if "I" in data_format:
        iter_axis = data_format.index("I")
    else:
        iter_axis = None
    return batch_axis, feature_axis, sequence_axis, spatial_axes, iter_axis


class ExpandMergeFeatures(tf.keras.layers.Layer):
    def __init__(self, **kwargs):  # pylint: disable=useless-super-delegation
        """Layer to merge two compatible tensors along the feature (last) axis."""
        super().__init__(**kwargs)

    # def get_config(self):
    #     """Get configuration."""
    #     return super().get_config()

    def compute_output_shape(self, input_shape):
        """Compute output shape of feature merge."""
        x_shape = input_shape[0]
        y_shape = input_shape[1]
        if len(y_shape) > len(x_shape):
            for i in range(len(x_shape) - 1):
                assert (
                    x_shape[i] == y_shape[i]
                ), f"Inconsistent dimension ({i}): {x_shape[i]} != {y_shape[i]}"
            output_shape = list(y_shape[:-1]) + [y_shape[-1] + x_shape[-1]]
        else:
            for i in range(len(y_shape) - 1):
                assert (
                    x_shape[i] == y_shape[i]
                ), f"Inconsistent dimension ({i}): {x_shape[i]} != {y_shape[i]}"
            output_shape = list(x_shape[:-1]) + [y_shape[-1] + x_shape[-1]]
        return output_shape

    def call(self, inputs):
        """Merge two compatible tensors along the feature (last) axis."""
        x = inputs[0]
        y = inputs[1]
        x_shape = x.shape
        y_shape = y.shape
        output_shape = self.compute_output_shape([x_shape, y_shape])
        if len(x_shape) != len(y_shape):
            if len(y_shape) > len(x_shape):
                missing_dims = len(y_shape) - len(x_shape)
                # Expand
                xb = x
                for _ in range(missing_dims):
                    xb = tf.expand_dims(xb, -2)
                # Tile
                multiples = list(output_shape)
                for i in range(len(x_shape) - 1):
                    multiples[i] = 1
                multiples[-1] = 1
                xb = tf.tile(xb, multiples)
                yb = y
            else:
                missing_dims = len(x_shape) - len(y_shape)
                # Expand
                yb = y
                for _ in range(abs(missing_dims)):
                    yb = tf.expand_dims(yb, -2)
                # Tile
                multiples = list(output_shape)
                for i in range(len(y_shape) - 1):
                    multiples[i] = 1
                multiples[-1] = 1
                yb = tf.tile(yb, multiples)
                xb = x
        else:
            xb = x
            yb = y
        return tf.keras.layers.Concatenate(axis=-1)([xb, yb])


class OnlineNormalize(tf.keras.layers.Layer):
    def __init__(
        self,
        data_shape,
        data_format="NF",
        dtype=tf.float64,
        reduction_axes=None,
        normalize_mode="stddev",
        center=True,
        scale=True,
        merge_features=None,
        transform_fun=None,
        init_stats=None,
        **kwargs,
    ):
        """An normalizer layer that computes standardization statistics online.

        Args:
            data_shape (list): shape of the input tensor
            data_format (str, optional): format of the input tensor. Defaults to "NF".
            dtype (tf.Dtype, optional): data type. Defaults to tf.float64.
            reduction_axes (tuple, optional): tuple of axis indices. Defaults to None.
            normalize_mode (str, optional): normalization mode ("stddev" or "minmax").
                Defaults to "stddev".
            center (bool, optional): center incoming tensor? Defaults to True.
            scale (bool, optional): scale incoming data tensor? Defaults to True.
            merge_features (list, optional): treat multiple features as one variable.
                Defaults to None.
            transform_fun (callable, optional): function to transform tensor.
                Defaults to None.
            init_stats (dict, optional): initial values of statistics. Defaults to None.
        """
        assert normalize_mode in ["stddev", "minmax"]
        assert len(data_shape) > 1, "Data_shape must be larger than 1"
        assert len(data_shape) == len(
            data_format
        ), f"Invalid data_format {data_format:s} for data_shape."
        self.data_shape = data_shape
        self.data_format = data_format
        axes = _read_axes(data_format)
        self.batch_axis = axes[0]
        self.feature_axis = axes[1]
        self.sequence_axis = axes[2]
        self.spatial_axes = axes[3]
        self.reduction_axes = reduction_axes or tuple(
            i for i, a in enumerate(self.data_format) if not a == "F"
        )
        self.smoothing_const = tf.constant(1e-12, dtype=dtype)
        self.normalize_mode = normalize_mode
        self.center = center
        self.scale = scale
        self.merge_features = merge_features
        self.transform_fun = transform_fun
        # init stats
        # TODO: move statistics shape to build based in input_shape
        self.stat_shape = tuple(
            data_shape[i] if i not in self.reduction_axes else 1
            for i in range(len(data_shape))
        )
        self.init_stats = init_stats
        self.count = None
        self.mean = None
        self.std = None
        self.m2 = None
        self.min = None
        self.max = None
        # Flags
        self._freeze = tf.Variable(False, trainable=False, name="freeze")
        super().__init__(dtype=dtype, **kwargs)

    @property
    def freeze(self):
        return self._freeze

    @freeze.setter
    def freeze(self, val):
        self._freeze.assign(val)

    def get_config(self):
        config = super().get_config()
        config.update(
            {
                "data_shape": self.data_shape,
                "data_format": self.data_format,
                "dtype": self.dtype,
                "reduction_axes": self.reduction_axes,
                "normalize_mode": self.normalize_mode,
                "center": self.center,
                "scale": self.scale,
                # "transform_fun": self.transform_fun,  # TODO: transform fun not saved
                "merge_features": self.merge_features,
                "init_stats": self.init_stats,
            }
        )
        return config

    def build(self, input_shape):
        count_init = tf.constant_initializer(0)
        count_shape = []
        mean_init = tf.constant_initializer(0.0)
        mean_shape = self.stat_shape
        std_init = tf.constant_initializer(0.0)
        std_shape = self.stat_shape
        m2_init = tf.constant_initializer(0.0)
        m2_shape = self.stat_shape
        min_init = tf.constant_initializer(tf.as_dtype(self.dtype).max)
        min_shape = self.stat_shape
        max_init = tf.constant_initializer(tf.as_dtype(self.dtype).min)
        max_shape = self.stat_shape
        if self.init_stats is not None:
            if "count" in self.init_stats.keys():
                count_init = tf.constant(self.init_stats["count"], dtype=tf.int32)
                count_shape = None  # get from constant
            if "mean" in self.init_stats.keys():
                mean_init = tf.constant(self.init_stats["mean"], dtype=self.dtype)
                mean_shape = None  # get from constant
            if "std" in self.init_stats.keys():
                std_init = tf.constant(self.init_stats["std"], dtype=self.dtype)
                std_shape = None  # get from constant
            if "m2" in self.init_stats.keys():
                m2_init = tf.constant(self.init_stats["m2"], dtype=self.dtype)
                m2_shape = None  # get from constant
            if "min" in self.init_stats.keys():
                min_init = tf.constant(self.init_stats["min"], dtype=self.dtype)
                min_shape = None  # get from constant
            if "max" in self.init_stats.keys():
                max_init = tf.constant(self.init_stats["max"], dtype=self.dtype)
                max_shape = None  # get from constant
        # Define tensorflow population statistics
        self.count = self.add_weight(
            "count",
            shape=count_shape,
            initializer=count_init,
            dtype=tf.int32,
            trainable=False,
        )
        self.mean = self.add_weight(
            "mean",
            shape=mean_shape,
            initializer=mean_init,
            dtype=self.dtype,
            trainable=False,
        )
        self.std = self.add_weight(
            "std",
            shape=std_shape,
            initializer=std_init,
            dtype=self.dtype,
            trainable=False,
        )
        self.m2 = self.add_weight(
            "m2", shape=m2_shape, initializer=m2_init, dtype=self.dtype, trainable=False
        )
        self.min = self.add_weight(
            "min",
            shape=min_shape,
            initializer=min_init,
            dtype=self.dtype,
            trainable=False,
        )
        self.max = self.add_weight(
            "max",
            shape=max_shape,
            initializer=max_init,
            dtype=self.dtype,
            trainable=False,
        )
        super().build(input_shape)

    def sequence_mask(self, x, axis=None):
        """Calculate the sequence mask of tensor batch.

        The sequence mask has the same shape as batch, with ones at each value
        that is part of sequence length and zeros otherwise.

        Args:
            x:      batch tensor
            axis:   sequence axis (defaults to self.sequence_axis

        Returns:
            sequence_mask:  batch-sized tensor with ones for each sequence entry
                            of batch and zero at other indices

        """
        x = tf.cast(x, self.dtype)
        if axis is None:
            axis = self.sequence_axis
        axes = [i for i in range(x.shape.ndims) if i > axis]
        # Find frames with non-zero features, reduce all axes larger than axis
        sequence_mask = tf.sign(tf.reduce_max(tf.abs(x), axis=axes, keepdims=True))
        # Convert to boolean
        sequence_mask = tf.cast(sequence_mask, tf.bool)
        # Blow sequence mask up
        multiples = [1] * len(x.shape)
        current_shape = tf.shape(x)
        for i in axes:
            multiples[i] = current_shape[i]
        multiples = tf.stack(multiples)
        sequence_mask = tf.tile(sequence_mask, multiples=multiples)
        return sequence_mask

    def sequence_lengths(self, x, axis=None):
        """Calculate the lengths of each sequence in a batch.

        Args:
            x:      batch tensor
            axis:   sequence axis (defaults to self.sequence_axis

        Returns:
            sequence_length:    1D-tensor of the number of frames for each
                                sample in the batch.

        Raises:
            AssertionError:     if shape of batch or self.batch shape does not
                                have enough dimensions for reduction

        """
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        if axis is None:
            axis = self.sequence_axis
        axes = tuple(i for i in range(x.shape.ndims) if i > axis)
        # Find frames with non-zero features
        sequence_mask = tf.sign(tf.reduce_max(tf.abs(x), axis=axes))
        # count numbers of non-zero frames, sequence axis is now last axis
        sequence_length = tf.reduce_sum(sequence_mask, axis=axis)
        sequence_length = tf.cast(
            sequence_length, tf.int32
        )  # ensure we return an int32
        return sequence_length

    @tf.function
    def _calc_online_moments(self, x):
        """Successively calculate and store moments of the batch tensors.

        The moments are calculated along the batch (0) axis only and sequences
        are masked.

        The used online one-pass algorithm is based on:
            Chan, Tony F.; Golub, Gene H.; LeVeque, Randall J. (1979),
            "Updating Formulae and a Pairwise Algorithm for Computing sample
             Variances."

        Args:
            x:  batch tensor (batch_size, ..., num_features)

        Returns:
            Tuple of
                pop_count:  Number of samples in pupulation so far
                pop_mean:   Estimated population mean tensor
                pop_std:    Estimated population standard deviation tensor
                pop_m2:     second moment

        """
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        # Recall memory variables (perform deep copy)
        #  TODO: tensorflow 1.2: identity op is not threadsafe!
        #   The following workaround enforces a deep copy for reliability
        pop_count = tf.add(self.count, 0)
        pop_mean = tf.add(self.mean, 0.0)
        if self.sequence_axis is not None:
            # Calculate sequence mask
            sequence_mask = self.sequence_mask(x)
            # Calculate input moments
            x_mean, x_var = tf.nn.weighted_moments(
                x,
                axes=self.reduction_axes,
                frequency_weights=sequence_mask,
                keepdims=True,
            )
            # Sequence steps that are zero for all samples (e.g. when padding globally)
            # divide zero by zero in tf.nn.weighted_moments(). We overwrite these nans.
            stat_sequence_mask = tf.reduce_any(
                sequence_mask, axis=self.reduction_axes, keepdims=True
            )
            x_mean = tf.where(stat_sequence_mask, x_mean, pop_mean)
            x_var = tf.where(stat_sequence_mask, x_var, tf.zeros_like(x_var))
        else:
            # Calculate input moments
            x_mean, x_var = tf.nn.moments(x, axes=self.reduction_axes, keepdims=True)
        x_count = tf.shape(x)[0]
        x_m2 = x_var * tf.cast(x_count - 1, self.dtype)
        # Update mean value
        delta_mean = x_mean - pop_mean
        # Update second moment
        if pop_count + x_count < 2:
            delta_m2 = tf.zeros(self.stat_shape, dtype=self.dtype)
        else:
            delta_m2 = x_m2 + (
                delta_mean**2
                * tf.cast(pop_count, self.dtype)
                * tf.cast(x_count, self.dtype)
                / tf.cast(pop_count + x_count, self.dtype)
            )
        delta_mean *= tf.cast(x_count, self.dtype) / tf.cast(
            pop_count + x_count, self.dtype
        )
        delta_count, delta_mean, delta_m2 = tf.tuple([x_count, delta_mean, delta_m2])
        return delta_count, delta_mean, delta_m2

    @tf.function
    def _calc_online_minmax(self, x):
        """Successively calculate and store the mean, min, max of batch tensors.

        The statistics are calculated along the batch (0) axis only and the
        sequences are masked.

        Args:
            x:  batch tensor (batch_size, ..., num_features)

        Returns:
            pop_mean:   Estimated population mean tensor (..., num_features)
            pop_min:    Estimated population min tensor (..., num_features)
            pop_max:    Estimated population max tensor (..., num_features)

        """
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        # Recall memory variables
        old_pop_count = tf.add(self.count, 0)
        pop_mean = tf.add(self.mean, 0.0)
        pop_min = tf.add(self.min, 0.0)
        pop_max = tf.add(self.max, 0.0)
        if self.sequence_axis is not None:
            # Calculate sequence mask
            sequence_mask = self.sequence_mask(x)
            # Mask and reduce mean
            x_mean, _ = tf.nn.weighted_moments(
                x,
                axes=self.reduction_axes,
                frequency_weights=sequence_mask,
                keepdims=True,
            )
            # Sequence steps that are zero for all samples (e.g. when padding globally)
            # divide zero by zero in tf.nn.weighted_moments(). We overwrite these nans.
            stat_sequence_mask = tf.reduce_any(
                sequence_mask, axis=self.reduction_axes, keepdims=True
            )
            x_mean = tf.where(stat_sequence_mask, x_mean, pop_mean)
            # Mask and reduce min and max
            x_min = tf.reduce_min(
                tf.where(
                    sequence_mask, x, tf.fill(tf.shape(x), tf.as_dtype(self.dtype).max)
                ),
                axis=self.reduction_axes,
                keepdims=True,
            )
            x_max = tf.reduce_max(
                tf.where(
                    sequence_mask, x, tf.fill(tf.shape(x), tf.as_dtype(self.dtype).min)
                ),
                axis=self.reduction_axes,
                keepdims=True,
            )
        else:
            # Calculate shift and variance
            x_mean = tf.reduce_mean(x, axis=self.reduction_axes, keepdims=True)
            x_min = tf.reduce_min(x, axis=self.reduction_axes, keepdims=True)
            x_max = tf.reduce_max(x, axis=self.reduction_axes, keepdims=True)
        x_count = tf.shape(x)[self.batch_axis]
        # Update counter
        pop_count = old_pop_count + x_count
        # Update mean value
        delta = x_mean - pop_mean
        # TODO: Numerically unstable if counts and self._pop_count close and large
        pop_mean += delta * (
            tf.cast(x_count, self.dtype) / tf.cast(pop_count, self.dtype)
        )
        # Update min and max
        pop_min = tf.minimum(pop_min, x_min)
        pop_max = tf.maximum(pop_max, x_max)
        return (pop_count, pop_mean, pop_min, pop_max)

    def _get_online_moments(self):
        """Recall the online mean and standard deviation.

        Returns:
            pop_count:  Number of samples in pupulation so far
            pop_mean:   Estimated population mean tensor
            pop_std:    Estimated population standard deviation tensor
            pop_m2:     second moment

        """
        # Recall memory variables
        pop_count = tf.add(self.count, 0)
        pop_mean = tf.add(self.mean, 0.0)
        pop_m2 = tf.add(self.m2, 0.0)
        pop_std = tf.add(self.std, 0.0)
        return pop_count, pop_mean, pop_std, pop_m2

    def _get_online_minmax(self):
        """Recall the online mean, min and max tensors.

        Returns:
            pop_count:  Number of samples in pupulation so far
            pop_mean:   Estimated population mean tensor
            pop_min:    Estimated population minimum tensors
            pop_max:    Estimated population maximum tensors

        """
        # Recall memory variables
        pop_count = tf.add(self.count, 0)
        pop_mean = tf.add(self.mean, 0.0)
        pop_min = tf.add(self.min, 0.0)
        pop_max = tf.add(self.max, 0.0)
        return pop_count, pop_mean, pop_min, pop_max

    @tf.function
    def _assign_online_moments(self, delta_count, delta_mean, delta_m2):
        """Set online population count, mean std and m2.

        Args:
            delta_count:  population count update
            delta_mean:   population mean update
            delta_m2:     population second moment update

        Returns:
            pop_count:  assigned population count
            pop_mean:   assigned population mean
            pop_m2:     assigned population second moment
            pop_std:    assigned standard deviation
        """
        self.count.assign_add(delta_count)
        self.mean.assign_add(delta_mean)
        self.m2.assign_add(delta_m2)
        if self.count < 1:
            div = tf.constant(1.0, dtype=self.dtype)
        else:
            div = tf.cast(self.count - 1, self.dtype)
        pop_std = tf.sqrt(self.m2 / div)
        self.std.assign(pop_std)
        return self.count, self.mean, self.m2, self.std

    @tf.function
    def _assign_online_minmax(self, pop_count, pop_mean, pop_min, pop_max):
        """Set online population count, mean, min and max.

        Args:
            pop_count:  population count
            pop_mean:   population mean value
            pop_min:    population min value
            pop_max:    population max value

        Returns:
            pop_count:  assigned population count
            pop_mean:   assigned population mean value
            pop_min:    assigned population min value
            pop_max:    assigned population max value

        """
        assign_count = self.count.assign(pop_count)
        assign_mean = self.mean.assign(pop_mean)
        assign_min = self.min.assign(pop_min)
        assign_max = self.max.assign(pop_max)
        # group all together
        pop_count, pop_mean, pop_min, pop_max = tf.tuple(
            [assign_count, assign_mean, assign_min, assign_max]
        )
        return pop_count, pop_mean, pop_min, pop_max

    def _reduce_online_moments(self, shift, width):
        """Reduce axes other than the batch axis.

        This function combines shift (e.g. mean) and width (e.g. std_dev)
        entries of arrays that have been calculated along the batch axis.
        Shifting and normalizing a batch or dataset with common shift and width
        values maintains relative sizes of the values along that axis. For
        sequences the batch is masked.

        Args:
            shift:  A tensor of shift values calculated only along batch axis
            width:  A tensor of width values calculated only along batch axis

        Returns:
            shift:  A tensor of shift values calculated along multiple axes
            width:  A tensor of width values calculated along multiple axes

        Example:
            A sequence dataset tensor of shape (20, 7, 5) can be normalized
            using shift and width tensors of shape (7, 5), normalizing each
            entry along the sequence (1) axis and feature (2) axis
            independently. Normalizing with reduced shift and width tensors of
            shapes (5,) ensures that the relative sizes along the sequence axis
            are maintained.

        """
        # TODO: Is this step still necessary?
        # TODO: use variance/M2 instead
        # TODO: Use stored values instead?
        shift = tf.cast(shift, self.dtype)
        width = tf.cast(width, self.dtype)
        if len(self.reduction_axes) > 1:
            nonbatch_axes = tuple(
                i - 1 for i in self.reduction_axes if i > self.batch_axis
            )
            if self.sequence_axis is not None:
                # Calculate sequence mask
                sequence_mask = self.sequence_mask(shift)
                # Calculate average shift value and variance
                shift, shift_var = tf.nn.weighted_moments(
                    shift,
                    axes=nonbatch_axes,
                    frequency_weights=sequence_mask,
                    keepdims=True,
                )
            else:
                # Calculate average shift value and calculate shift variance
                shift, shift_var = tf.nn.moments(
                    shift, axes=nonbatch_axes, keepdims=True
                )
            # Calculate squared width (variance)
            square_width = tf.square(width)
            # Add all variances together
            #  https://stats.stackexchange.com/questions/280667/higher-dimensional-version-of-variance  # pylint: disable=line-too-long
            square_width = (
                tf.reduce_sum(square_width, axis=nonbatch_axes, keepdims=True)
                + shift_var
            )
            width = tf.sqrt(square_width)
        return shift, width

    def _reduce_online_minmax(self, mean, min, max):
        """Reduce mean, min, and max for axes other than the batch axis.

        This function reduces mean, min and max entries of arrays that have been
        calculated along the batch axis. Shifting and normalizing a batch or
        dataset with common mean and min/max values maintains relative sizes of
        the values along that axis. For sequences the batch is masked.

        Args:
            mean:  A tensor of mean values calculated only along the batch axis
            min:   A tensor of min values calculated only along the batch axis)
            min:   A tensor of max values calculated only along the batch axis)

        Returns:
            mean:  A tensor of mean values calculated along multiple axes
            min:   A tensor of min values calculated along multiple axes
            max:   A tensor of max values calculated along multiple axes

        Example:
            A sequence dataset tensor of shape (20, 7, 5) can be normalized
            using shift and width tensors of shape (7, 5), normalizing each
            entry along the sequence (1) axis and feature (2) axis
            independently. Normalizing with reduced mean and width tensors of
            shapes (5,) ensures that the relative sizes along the sequence axis
            are maintained.

        """
        # TODO: store reduced values instead?
        mean = tf.cast(mean, self.dtype)
        min = tf.cast(min, self.dtype)
        max = tf.cast(max, self.dtype)
        if len(self.reduction_axes) > 1:
            nonbatch_axes = tuple(
                i - 1 for i in self.reduction_axes if i > self.batch_axis
            )
            if self.sequence_axis is not None:
                # Calculate sequence mask
                sequence_mask = self.sequence_mask(mean)
                # Mask and reduce mean
                mean, _ = tf.nn.weighted_moments(
                    mean,
                    axes=nonbatch_axes,
                    frequency_weights=sequence_mask,
                    keepdims=True,
                )
                # Mask and reduce min, max with sequence mask
                min = tf.reduce_min(
                    tf.where(
                        sequence_mask,
                        min,
                        tf.fill(tf.shape(min), tf.as_dtype(self.dtype).max),
                    ),
                    axis=nonbatch_axes,
                    keepdims=True,
                )
                max = tf.reduce_max(
                    tf.where(
                        sequence_mask,
                        max,
                        tf.fill(tf.shape(max), tf.as_dtype(self.dtype).min),
                    ),
                    axis=nonbatch_axes,
                    keepdims=True,
                )
            else:
                # Calculate average mean, min and max value
                mean = tf.reduce_mean(mean, axis=nonbatch_axes, keepdims=True)
                min = tf.reduce_mean(min, axis=nonbatch_axes, keepdims=True)
                max = tf.reduce_mean(max, axis=nonbatch_axes, keepdims=True)
        return mean, min, max

    @tf.function
    def _update_online_stats_stddev(self, x):
        """Updates and returns stored shift and width values for normalization.

        Args:
            x:              batch tensor

        Returns:
            shift:  tensor of size self.stat_shape to shift input
            width:  tensor of size self.stat_shape to normalize shifted input

        Raises:
            ValueError: if self.normalize_mode is not "stddev" or "minmax"

        """
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        if self.freeze:
            delta_count = tf.zeros([], dtype=tf.int32)
            delta_mean = tf.zeros(self.stat_shape, dtype=self.dtype)
            delta_m2 = tf.zeros(self.stat_shape, dtype=self.dtype)
        else:
            delta_count, delta_mean, delta_m2 = self._calc_online_moments(x)
        # Transform function applied on centered data
        if self.transform_fun is not None:
            assert callable(self.transform_fun)
            x = self.transform_fun(x - self.mean - delta_mean)
            if self.freeze:
                delta_m2 = tf.zeros(self.stat_shape, dtype=self.dtype)
            else:
                _, _, delta_m2 = self._calc_online_moments(x)
        # Ensure that all deltas are calculated monolithically
        with tf.control_dependencies([delta_count, delta_mean, delta_m2]):
            pop_count, pop_mean, pop_m2, pop_std = self._assign_online_moments(
                delta_count, delta_mean, delta_m2
            )
        # Ensure that all assignments are completed
        with tf.control_dependencies([pop_count, pop_mean, pop_m2, pop_std]):
            shift = pop_mean
            width = pop_std
        return shift, width

    @tf.function
    def _update_online_stats_minmax(self, x):
        """Updates and returns stored shift and width values for normalization.

        Args:
            x:              batch tensor

        Returns:
            shift:  tensor of size self.stat_shape to shift input
            width:  tensor of size self.stat_shape to normalize shifted input

        Raises:
            ValueError: if self.normalize_mode is not "stddev" or "minmax"

        """
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        pop_count, pop_mean, pop_min, pop_max = tf.cond(
            self.freeze, self._get_online_minmax, lambda: self._calc_online_minmax(x)
        )
        # Transform function applied on centered data
        if self.transform_fun is not None:
            assert callable(self.transform_fun)
            x = self.transform_fun(x - pop_mean)
            if self.freeze:
                _, _, pop_min, pop_max = self._get_online_minmax()
            else:
                _, _, pop_min, pop_max = self._calc_online_minmax(x)
        # Ensure that all calculations are performed monolithically
        with tf.control_dependencies([pop_count, pop_mean, pop_min, pop_max]):
            pop_count, pop_mean, pop_min, pop_max = self._assign_online_minmax(
                pop_count, pop_mean, pop_min, pop_max
            )
        # Ensure that all assignments are completed
        with tf.control_dependencies([pop_count, pop_mean, pop_min, pop_max]):
            shift = pop_mean
            if self.transform_fun is None:
                width = tf.maximum(tf.abs(pop_max - shift), tf.abs(pop_min - shift))
            else:
                # width calculated on centered data if data is transformed
                width = tf.maximum(tf.abs(pop_max), tf.abs(pop_min))
        return shift, width

    def _update_online_stats(self, x):
        if self.normalize_mode == "stddev":
            return self._update_online_stats_stddev(x)
        if self.normalize_mode == "minmax":
            return self._update_online_stats_minmax(x)
        raise ValueError("Invalid self.normalize_mode: stddev or minmax expected!")

    def _get_online_stats(self, x):
        """Returns stored shift and width values for normalization.

        Args:
            x:      batch or sample tensor

        Returns:
            shift:  tensor of size self.stat_shape to shift input
            width:  tensor of size self.stat_shape to normalize shifted input

        Raises:
            ValueError: if self.normalize_mode is not "stddev" or "minmax"

        """
        # Ensure all previous steps in graph are executed (assignments!)
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        with tf.control_dependencies([x]):
            if self.normalize_mode == "stddev":
                _, pop_mean, pop_std, _ = self._get_online_moments()
                shift = pop_mean
                width = pop_std
            elif self.normalize_mode == "minmax":
                _, pop_mean, pop_min, pop_max = self._get_online_minmax()
                shift = pop_mean
                width = tf.maximum(tf.abs(pop_max - shift), tf.abs(pop_min - shift))
            else:
                raise ValueError(
                    "Invalid self.normalize_mode: stddev or minmax expected!"
                )
            return shift, width

    def online_normalize(self, x):
        """Successively normalize batches using online statistics.

        Shift (e.g. Mean) and width (e.g. standard deviation) tensors are
        calculated using an online algorithm along the batch (0) axis. Then both
        tensors are further reduced along other axes"specified in
        self.reduction_axes. Finally, shift and normalization are calculated.
        Sequences are masked.

        Args:
            x:              sample or batch tensor

        Returns:
            x:      normalized sample or batch tensor

        """
        with tf.device("/device:cpu:0"):
            x = tf.cast(x, self.dtype)
            if len(x.shape) < len(self.data_format):
                x = tf.expand_dims(x, 0)
            # Calculate sequence mask
            if self.sequence_axis is not None:
                sequence_mask = self.sequence_mask(x)
            shift, width = self._update_online_stats(x)
            # Shift
            if self.center:
                x = x - shift
            # Transform
            if self.transform_fun is not None:
                assert callable(self.transform_fun)
                x = self.transform_fun(x)
            # Merge feature widths
            if self.merge_features:
                width_list = tf.unstack(width, axis=self.feature_axis)
                for merge_indices in self.merge_features:
                    merged_width = tf.reduce_max(
                        tf.gather(width, merge_indices, axis=self.feature_axis)
                    )
                    for i in merge_indices:
                        width_list[i] = merged_width
                width = tf.stack(width_list)
            # Scale
            if self.scale:
                x = x / tf.maximum(width, self.smoothing_const)
            # Mask x to undo shift for empty frames
            if self.sequence_axis is not None:
                x = tf.where(sequence_mask, x, tf.zeros_like(x))
            return x

    def online_rescale(self, x):
        """Rescales and unshifts a normalized batch with online statistics.

        Args:
            x:              normalized sample or batch

        Returns:
            sample:  sample rescaled to original size

        """
        with tf.device("/device:cpu:0"):
            initial_dtype = x.dtype
            x = tf.cast(x, self.dtype)
            if len(x.shape) < len(self.data_format):
                x = tf.expand_dims(x, 0)
            if self.sequence_axis is not None:
                # Calculate sequence mask
                sequence_mask = self.sequence_mask(x)
            shift, width = self._get_online_stats(x)
            # Merge feature widths
            if self.merge_features:
                width_list = tf.unstack(width, axis=self.feature_axis)
                for merge_indices in self.merge_features:
                    merged_width = tf.reduce_max(
                        tf.gather(width, merge_indices, axis=self.feature_axis)
                    )
                    for i in merge_indices:
                        width_list[i] = merged_width
                width = tf.stack(width_list)
            # Scale
            if self.scale:
                x = x * tf.maximum(width, self.smoothing_const)
            # Transform
            if self.transform_fun is not None:
                assert callable(self.transform_fun)
                x = self.transform_fun(x)
            # Shift
            if self.center:
                x = x + shift
            if self.sequence_axis is not None:
                # Mask x to undo shift for empty frames
                x = tf.where(sequence_mask, x=x, y=tf.zeros_like(x))
            x = tf.cast(x, initial_dtype)
            return x

    def compute_mask(self, inputs, mask=None):
        return mask

    def compute_output_shape(self, input_shape):
        return input_shape

    def call(self, inputs, invert=False, **kwargs):
        if not invert:
            return self.online_normalize(inputs)
        return self.online_rescale(inputs)


class OnlinePCA(tf.keras.layers.Layer):
    def __init__(
        self,
        data_shape,
        data_format="NF",
        dtype=tf.float64,
        reduction_axes=None,
        normalize_mode="stddev",
        init_stats=None,
        **kwargs,
    ):
        assert len(data_shape) > 1, "Data_shape must be larger than 1"
        assert len(data_shape) == len(
            data_format
        ), f"Invalid data_format {data_format:s} for data_shape."
        self.data_shape = data_shape
        self.data_format = data_format
        axes = _read_axes(data_format)
        self.batch_axis = axes[0]
        self.feature_axis = axes[1]
        self.sequence_axis = axes[2]
        self.spatial_axes = axes[3]
        self.reduction_axes = reduction_axes or tuple(
            i for i, a in enumerate(self.data_format) if not a == "F"
        )
        self.smoothing_const = tf.constant(1e-12, dtype=dtype)
        self.normalize_mode = normalize_mode
        # init stats
        # TODO: move statistics shape to build based in input_shape
        self.stat_shape = tuple(
            data_shape[i] if i not in self.reduction_axes else 1
            for i in range(len(data_shape))
        )
        self.init_stats = init_stats
        self.count = None
        self.mean = None
        self.std = None
        self.m2 = None
        self.min = None
        self.max = None
        # Flags
        self._freeze = tf.Variable(False, trainable=False, name="freeze")
        super().__init__(dtype=dtype, **kwargs)

    @property
    def freeze(self):
        return self._freeze

    @freeze.setter
    def freeze(self, val):
        self._freeze.assign(val)

    def get_config(self):
        config = super().get_config()
        config.update(
            {
                "data_shape": self.data_shape,
                "data_format": self.data_format,
                "dtype": self.dtype,
                "reduction_axes": self.reduction_axes,
                "normalize_mode": self.normalize_mode,
                "init_stats": self.init_stats,
            }
        )
        return config

    def build(self, input_shape):
        count_init = tf.constant_initializer(0)
        count_shape = []
        mean_init = tf.constant_initializer(0.0)
        mean_shape = self.stat_shape
        std_init = tf.constant_initializer(0.0)
        std_shape = self.stat_shape
        m2_init = tf.constant_initializer(0.0)
        m2_shape = self.stat_shape
        min_init = tf.constant_initializer(tf.as_dtype(self.dtype).max)
        min_shape = self.stat_shape
        max_init = tf.constant_initializer(tf.as_dtype(self.dtype).min)
        max_shape = self.stat_shape
        if self.init_stats is not None:
            if "count" in self.init_stats.keys():
                count_init = tf.constant(self.init_stats["count"], dtype=tf.int32)
                count_shape = None  # get from constant
            if "mean" in self.init_stats.keys():
                mean_init = tf.constant(self.init_stats["mean"], dtype=self.dtype)
                mean_shape = None  # get from constant
            if "std" in self.init_stats.keys():
                std_init = tf.constant(self.init_stats["std"], dtype=self.dtype)
                std_shape = None  # get from constant
            if "m2" in self.init_stats.keys():
                m2_init = tf.constant(self.init_stats["m2"], dtype=self.dtype)
                m2_shape = None  # get from constant
            if "min" in self.init_stats.keys():
                min_init = tf.constant(self.init_stats["min"], dtype=self.dtype)
                min_shape = None  # get from constant
            if "max" in self.init_stats.keys():
                max_init = tf.constant(self.init_stats["max"], dtype=self.dtype)
                max_shape = None  # get from constant
        # Define tensorflow population statistics
        self.count = self.add_weight(
            "count",
            shape=count_shape,
            initializer=count_init,
            dtype=tf.int32,
            trainable=False,
        )
        self.mean = self.add_weight(
            "mean",
            shape=mean_shape,
            initializer=mean_init,
            dtype=self.dtype,
            trainable=False,
        )
        self.std = self.add_weight(
            "std",
            shape=std_shape,
            initializer=std_init,
            dtype=self.dtype,
            trainable=False,
        )
        self.m2 = self.add_weight(
            "m2", shape=m2_shape, initializer=m2_init, dtype=self.dtype, trainable=False
        )
        self.min = self.add_weight(
            "min",
            shape=min_shape,
            initializer=min_init,
            dtype=self.dtype,
            trainable=False,
        )
        self.max = self.add_weight(
            "max",
            shape=max_shape,
            initializer=max_init,
            dtype=self.dtype,
            trainable=False,
        )
        super().build(input_shape)

    def sequence_mask(self, x, axis=None):
        """Calculate the sequence mask of tensor batch.

        The sequence mask has the same shape as batch, with ones at each value
        that is part of sequence length and zeros otherwise.

        Args:
            x:      batch tensor
            axis:   sequence axis (defaults to self.sequence_axis

        Returns:
            sequence_mask:  batch-sized tensor with ones for each sequence entry
                            of batch and zero at other indices

        """
        x = tf.cast(x, self.dtype)
        if axis is None:
            axis = self.sequence_axis
        axes = [i for i in range(x.shape.ndims) if i > axis]
        # Find frames with non-zero features, reduce all axes larger than axis
        sequence_mask = tf.sign(tf.reduce_max(tf.abs(x), axis=axes, keepdims=True))
        # Convert to boolean
        sequence_mask = tf.cast(sequence_mask, tf.bool)
        # Blow sequence mask up
        multiples = [1] * len(x.shape)
        for i in axes:
            multiples[i] = tf.shape(x)[i]
        multiples = tf.stack(multiples)
        sequence_mask = tf.tile(sequence_mask, multiples=multiples)
        return sequence_mask

    def sequence_lengths(self, x, axis=None):
        """Calculate the lengths of each sequence in a batch.

        Args:
            x:      batch tensor
            axis:   sequence axis (defaults to self.sequence_axis

        Returns:
            sequence_length:    1D-tensor of the number of frames for each
                                sample in the batch.

        Raises:
            AssertionError:     if shape of batch or self.batch shape does not
                                have enough dimensions for reduction

        """
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        if axis is None:
            axis = self.sequence_axis
        axes = tuple(i for i in range(x.shape.ndims) if i > axis)
        # Find frames with non-zero features
        sequence_mask = tf.sign(tf.reduce_max(tf.abs(x), axis=axes))
        # count numbers of non-zero frames, sequence axis is now last axis
        sequence_length = tf.reduce_sum(sequence_mask, axis=axis)
        sequence_length = tf.cast(
            sequence_length, tf.int32
        )  # ensure we return an int32
        return sequence_length

    @tf.function
    def _sketch_matrix(self, A, ll):
        """Implements \"Edo Liberty: Simple and Deterministic Matrix Sketching\"."""
        nn = A.shape[0]
        mm = A.shape[-1]
        B = tf.Variable(tf.zeros([ll, mm], dtype=A.dtype))  # TODO: move to init
        for i in range(nn):
            if i < ll:
                # Insert into zero valued row
                B[i, ...] = A[i, ...]
            else:
                s, _, v = tf.linalg.svd(B)
                # C = tf.linalg.diag(S) @ V.T
                delta = tf.square(s[ll // 2])
                s_ = tf.sqrt(tf.maximum(tf.square(s) - delta, 0.0))
                B = s_ @ v.T
        return B

    @tf.function
    def _online_pca(self, X, k, eps, w0=0.0):  # TODO: move some to init
        """Implements "Edo Liberty: Simple and Deterministic Matrix Sketching"."""
        raise NotImplementedError
        d, n = X.shape[0], X.shape[-1]  # pylint:disable=unreachable
        U = tf.Variable(
            tf.zeros([d, k // eps**3], dtype=X.dtype)
        )  # TODO: move to init
        Z = tf.Variable(
            tf.zeros([d, k // eps**3], dtype=X.dtype)
        )  # TODO: move to init
        wu = tf.Variable(tf.zeros([k // eps**3], dtype=X.dtype))  # TODO: move to init
        w = tf.Variable(0.0, dtype=X.dtype)
        for t in range(n):
            xt = X[t, ...]
            w += tf.norm(xt) ** 2
            UU = U @ U.T
            ZZ = Z @ Z.T
            rt = xt - UU @ xt
            C = (1.0 - UU) @ ZZ @ (1.0 - UU)
            while tf.norm(C + rt @ rt.T) >= tf.maximum(w0, w) * k // eps**3:
                s, u, _ = tf.linalg.svd(C)
                s = s[0]
                u = u[0, ...]  # TODO: row or column?
                wu[t] = s
                # TODO: continue implementation
                raise NotImplementedError
                UU = U @ U.T  # pylint:disable=unreachable
                C = (1.0 - UU) @ ZZ @ (1.0 - UU)
                rt = xt - UU @ xt
            self._sketch_matrix()
        # TODO: finish!
        return C

    @tf.function
    def _calc_online_mean(self, x):
        """Successively calculate and store means of the batch tensors.

        The moments are calculated along the batch (0) axis only and sequences
        are masked.

        The used online one-pass algorithm is based on:
            Chan, Tony F.; Golub, Gene H.; LeVeque, Randall J. (1979),
            "Updating Formulae and a Pairwise Algorithm for Computing sample
             Variances."

        Args:
            x:  batch tensor (batch_size, ..., num_features)

        Returns:
            Tuple of
                pop_count:  Number of samples in pupulation so far
                pop_mean:   Estimated population mean tensor
                pop_std:    Estimated population standard deviation tensor
                pop_m2:     second moment

        """
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        # Recall memory variables (perform deep copy)
        #  TODO: tensorflow 1.2: identity op is not threadsafe!
        #   The following workaround enforces a deep copy for reliability
        pop_mean = tf.add(self.mean, 0.0)
        if self.sequence_axis is not None:
            # Calculate sequence mask
            sequence_mask = self.sequence_mask(x)
            # Calculate input moments
            x_mean, _ = tf.nn.weighted_moments(
                x,
                axes=self.reduction_axes,
                frequency_weights=sequence_mask,
                keepdims=True,
            )
            # Sequence steps that are zero for all samples (e.g. when padding globally)
            # divide zero by zero in tf.nn.weighted_moments(). We overwrite these nans.
            stat_sequence_mask = tf.reduce_any(
                sequence_mask, axis=self.reduction_axes, keepdims=True
            )
            x_mean = tf.where(stat_sequence_mask, x_mean, pop_mean)
        else:
            # Calculate input moments
            x_mean, _ = tf.nn.moments(x, axes=self.reduction_axes, keepdims=True)
        x_count = tf.shape(x)[0]
        # Update mean value
        delta_mean = x_mean - pop_mean
        delta_count, delta_mean = tf.tuple([x_count, delta_mean])
        return delta_count, delta_mean

    def _get_online_mean(self):
        """Recall the online mean and standard deviation.

        Returns:
            pop_count:  Number of samples in pupulation so far
            pop_mean:   Estimated population mean tensor
            pop_std:    Estimated population standard deviation tensor
            pop_m2:     second moment

        """
        # Recall memory variables
        pop_count = tf.add(self.count, 0)
        pop_mean = tf.add(self.mean, 0.0)
        return pop_count, pop_mean

    @tf.function
    def _assign_online_moments(self, delta_count, delta_mean):
        """Set online population count, mean std and m2.

        Args:
            delta_count:  population count update
            delta_mean:   population mean update

        Returns:
            pop_count:  assigned population count
            pop_mean:   assigned population mean
        """
        raise NotImplementedError
        self.count.assign_add(delta_count)  # pylint:disable=unreachable
        self.mean.assign_add(delta_mean)
        if self.count < 1:
            div = tf.constant(1.0, dtype=self.dtype)
        else:
            div = tf.cast(self.count - 1, self.dtype)
        div = div + 1  # TODO: placeholder
        # TODO: continue!
        return self.count, self.mean

    @tf.function
    def _update_online_stats(self, x):
        """Updates and returns stored shift and width values for normalization.

        Args:
            x:              batch tensor

        Returns:
            shift:  tensor of size self.stat_shape to shift input
            width:  tensor of size self.stat_shape to normalize shifted input

        Raises:
            ValueError: if self.normalize_mode is not "stddev" or "minmax"

        """
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        if self.freeze:
            delta_count = tf.zeros([], dtype=tf.int32)
            delta_mean = tf.zeros(self.stat_shape, dtype=self.dtype)
        else:
            delta_count, delta_mean = self._calc_online_mean(x)
        # Ensure that all deltas are calculated monolithically
        with tf.control_dependencies([delta_count, delta_mean]):
            pop_count, pop_mean = self._assign_online_moments(delta_count, delta_mean)
        # Ensure that all assignments are completed
        with tf.control_dependencies([pop_count, pop_mean]):
            shift = pop_mean
        return shift

    def _get_online_stats(self, x):
        """Returns stored shift and width values for normalization.

        Args:
            x:      batch or sample tensor

        Returns:
            shift:  tensor of size self.stat_shape to shift input
            width:  tensor of size self.stat_shape to normalize shifted input

        Raises:
            ValueError: if self.normalize_mode is not "stddev" or "minmax"

        """
        # Ensure all previous steps in graph are executed (assignments!)
        x = tf.cast(x, self.dtype)
        if len(x.shape) < len(self.data_format):
            x = tf.expand_dims(x, 0)
        with tf.control_dependencies([x]):
            (
                _,
                pop_mean,
            ) = self._get_online_mean()
            shift = pop_mean
        return shift

    def online_rotate(self, x):
        """Successively performs a principal component analysis on the batch.

        Shift (e.g. Mean) and width (e.g. standard deviation) tensors are
        calculated using an online algorithm along the batch (0) axis. Then both
        tensors are further reduced along other axes"specified in
        self.reduction_axes. Finally, shift and normalization are calculated.
        Sequences are masked.

        Args:
            x:              sample or batch tensor

        Returns:
            x:      normalized sample or batch tensor

        """
        with tf.device("/device:cpu:0"):
            x = tf.cast(x, self.dtype)
            if len(x.shape) < len(self.data_format):
                x = tf.expand_dims(x, 0)
            # Calculate sequence mask
            if self.sequence_axis is not None:
                sequence_mask = self.sequence_mask(x)
            shift = self._update_online_stats(x)
            # Shift
            if self.center:
                x = x - shift
            # Transform
            if self.transform_fun is not None:
                assert callable(self.transform_fun)
                x = self.transform_fun(x)
            # Rotate
            # TODO:
            raise NotImplementedError
            width = None  # pylint:disable=unreachable
            # Scale
            if self.scale:
                x = x / tf.maximum(width, self.smoothing_const)
            # Mask x to undo shift for empty frames
            if self.sequence_axis is not None:
                x = tf.where(sequence_mask, x, tf.zeros_like(x))
            return x

    def online_rescale(self, x):
        """Rescales and unshifts a normalized batch with online statistics.

        Args:
            x:              normalized sample or batch

        Returns:
            sample:  sample rescaled to original size

        """
        with tf.device("/device:cpu:0"):
            initial_dtype = x.dtype
            x = tf.cast(x, self.dtype)
            if len(x.shape) < len(self.data_format):
                x = tf.expand_dims(x, 0)
            if self.sequence_axis is not None:
                # Calculate sequence mask
                sequence_mask = self.sequence_mask(x)
            shift, width = self._get_online_stats(x)
            # Merge feature widths
            if self.merge_features:
                width_list = tf.unstack(width, axis=self.feature_axis)
                for merge_indices in self.merge_features:
                    merged_width = tf.reduce_max(
                        tf.gather(width, merge_indices, axis=self.feature_axis)
                    )
                    for i in merge_indices:
                        width_list[i] = merged_width
                width = tf.stack(width_list)
            # Scale
            if self.scale:
                x = x * tf.maximum(width, self.smoothing_const)
            # Transform
            if self.transform_fun is not None:
                assert callable(self.transform_fun)
                x = self.transform_fun(x)
            # Shift
            if self.center:
                x = x + shift
            if self.sequence_axis is not None:
                # Mask x to undo shift for empty frames
                x = tf.where(sequence_mask, x=x, y=tf.zeros_like(x))
            x = tf.cast(x, initial_dtype)
            return x

    def compute_mask(self, inputs, mask=None):
        return mask

    def compute_output_shape(self, input_shape):
        return input_shape

    def call(self, inputs, invert=False, **kwargs):
        if not invert:
            return self.online_normalize(inputs)
        return self.online_rescale(inputs)


class SaltAndPepper(tf.keras.layers.Layer):
    """Apply salt and pepper noise.

    Arguments:
        noise:  Float, amount of noise that should be added.
        ratio:  Float, ratio between salt and pepper pixel.
                defined in the interval [0,1] in which 0 equals to
                pepper pixels only and 1 to salt pixels only

        ToDo: grain_size: Float, defines the diameter of the added grains

    Call arguments:
        inputs:     Input tensor (of any rank)
        diameter:   Diameter of the added noise
        training:   Python boolean indicating whether the layer should behave in
                    training mode (adding noise) or in inference mode (doing nothing)

    Input shape:
        Arbitrary. Use the keyword argument `input_shape`
        (tuple of integers, does not include the sample axis)
        when using this layer as the first laye in a model.

    Output shape:
        Same as input.
    """

    def __init__(self, noise, ratio, **kwargs):
        super().__init__(**kwargs)
        self.supports_masking = True
        self.ratio = ratio
        self.noise = noise

    def call(self, inputs, diameter=0, training=None):
        @tf.function
        def noised():
            shape = K.shape(inputs)  # [1:]
            mask_noise = K.random_binomial(shape=shape, p=self.noise)
            mask_select = K.random_binomial(shape=shape, p=self.ratio)
            mask_struc = mask_select * mask_noise
            if diameter:
                mask_noise = tf.nn.max_pool3d(
                    mask_noise,
                    ksize=(diameter, diameter, diameter),
                    strides=1,
                    padding="SAME",
                    name="dilation2d",
                )
                mask_struc = tf.nn.max_pool3d(
                    mask_struc,
                    ksize=(diameter, diameter, diameter),
                    strides=1,
                    padding="SAME",
                    name="dilation2d2",
                )

            return inputs * (1 - mask_noise) + mask_struc

        return K.in_train_phase(noised(), inputs, training=training)

    def get_config(self):
        config = {"ratio": self.ratio, "noise": self.noise}
        base_config = super().get_config()
        return dict(list(base_config.items()) + list(config.items()))


class SequenceMasking(tf.keras.layers.Layer):
    """CUDNN compatible version of Masking for sequential data.

    Ensures only continuous masking from the right. Values at the start and
    within the sequence are not masked. Continuous values at the end are
    masked.

    ```python
        model = Sequential()
        model.add(SequenceMasking(mask_value=0, data_format="NSF"))
        model.add(LSTM(32))
    ```
    """

    def __init__(self, mask_value=0.0, data_format="NSF", **kwargs):
        super().__init__(**kwargs)
        self.data_format = data_format
        self.supports_masking = True
        self.mask_value = mask_value
        self._compute_output_and_mask_jointly = True

    def compute_mask(self, inputs, mask=None):
        # Nested unpack
        if isinstance(inputs, (list, tuple)):
            return [self.compute_mask(input) for input in inputs]
        sequence_axis = self.data_format.index("S")
        max_sequence_length = inputs.shape[sequence_axis]
        mask = tf.not_equal(inputs, self.mask_value)
        sequence_range = tf.range(1, max_sequence_length + 1)
        sequence_range = tf.tile(
            tf.expand_dims(sequence_range, 0), [tf.shape(inputs)[0], 1]
        )
        # Reduce away non-sequential and non-batch axes
        for i, d in reversed(list(enumerate(self.data_format))):
            if d not in "NS":
                mask = tf.keras.backend.any(mask, axis=i)
        # Mask only from the right
        sequence_lengths = tf.reduce_max(
            sequence_range * tf.cast(mask, tf.int32), axis=sequence_axis
        )
        mask = tf.sequence_mask(sequence_lengths, max_sequence_length)
        # Expand non-sequential axis back
        for i, d in enumerate(self.data_format):
            if d not in "NS":
                mask = tf.expand_dims(mask, i)
        return mask

    def call(self, inputs):
        # Nested unpack
        if isinstance(inputs, (list, tuple)):
            return [self.call(input) for input in inputs]
        mask = self.compute_mask(inputs)
        outputs = inputs * tf.cast(mask, inputs.dtype)
        outputs._keras_mask = tf.squeeze(mask, axis=-1)
        return outputs

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        config = {"mask_value": self.mask_value, "data_format": self.data_format}
        base_config = super().get_config()
        return dict(list(base_config.items()) + list(config.items()))


class MaskedShuffling(tf.keras.layers.Layer):
    """Shuffle the order of an axis while taking masking into account."""

    def __init__(self, axis=1, **kwargs):
        super().__init__(**kwargs)
        self.axis = axis
        self.supports_masking = True
        self._compute_output_and_mask_jointly = True

    def _shuffle_axis(self, x, axis):
        transpose_order = [axis] + [
            i if i != axis else 0 for i in range(1, len(x.shape))
        ]
        x = tf.transpose(x, transpose_order)
        x = tf.random.shuffle(x)
        x = tf.transpose(x, transpose_order)
        return x

    def call(self, inputs, mask=None, training=None):
        if training is None:
            training = tf.keras.backend.learning_phase()
        # Disable during validation and testing
        if not training:
            return inputs
        # Just shuffle if no mask is given
        if mask is None:
            return self._shuffle_axis(inputs, axis=self.axis)
        # Replace masked values with reverse of sequence
        mask = tf.expand_dims(mask, axis=-1)
        reverse = tf.reverse(inputs, axis=[self.axis])
        only_valids = tf.where(mask, inputs, reverse)
        # Apply shuffle order to both inputs and mask
        outputs = self._shuffle_axis(only_valids, axis=self.axis)
        # Apply mask again
        outputs = outputs * tf.cast(mask, outputs.dtype)
        outputs._keras_mask = tf.squeeze(mask, axis=-1)
        return outputs

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        config = {"axis": self.axis}
        base_config = super().get_config()
        return dict(list(base_config.items()) + list(config.items()))


class RandomFlip3D(tf.keras.layers.Layer):
    """A preprocessing layer which randomly flips 3D data during training.

    This layer will flip the data along its x-, y- or z-axis based on the
    `direction` option. During inference time, the output will be identical to
    input. Call the layer with `training=True` to flip the input.

    Input pixel values can be of any range (e.g. `[0., 1.)` or `[0, 255]`) and
    of interger or floating point dtype. By default, the layer will output
    floats.

    Input shape:
        4D (unbatched) or 5D (batched) tensor with shape:
        `(..., height, width, depth, channels)`, in `"channels_last"` format.

    Output shape:
        4D (unbatched) or 5D (batched) tensor with shape:
        `(..., height, width, depth, channels)`, in `"channels_last"` format.

    Arguments:
        direction: String indicating which flip direction to use. Can be `"X"`,
        `"Y"`, or `"Z"` and all of their combinations.
        Defaults to `"x"`.
        seed: Integer. Used to create a random seed.
    """

    def __init__(self, data_format, directions="XYZ", seed=None, **kwargs):
        super().__init__(**kwargs)

        self.data_format = data_format
        self.directions = directions
        self.seed = seed
        # Initialise a random generator for consistent flipping in all layers
        if self.seed is None:
            self.g = tf.random.get_global_generator()
        else:
            self.g = tf.random.Generator.from_seed(seed)
        # Check whether invalid arguments have been passed as direction.
        if directions.translate({ord(i): None for i in "XYZ"}):
            print(
                f"RandomFlip3D layer {self.name} received unknown direction "
                f"arguments in {directions}. Note that the invalid directions were "
                "removed and no flipping is performed along them."
            )
            invalid_args = directions.translate({ord(i): None for i in "XYZ"})
            self.directions = directions.translate({ord(i): None for i in invalid_args})

    def call(self, inputs):
        # Check if the input matches the possible dimensions.
        if len(self.data_format) not in (4, 5):
            raise ValueError(
                f"RandomFlip3D layer {self.name} received an input with "
                f"{len(self.data_format)} dimensions. Note that the input must be a "
                "4D (unbachted) or 5D (batched) tensor."
            )
        # Flip the data along each specified axis.
        x = inputs
        for direction in self.directions:
            # Some flips are omitted to integrate some form of randomness
            if tf.math.less(self.g.uniform([]), 0.5):
                continue
            axis = [self.data_format.index(direction)]
            # The data is finally flipped along the set axis and returned.
            x = tf.reverse(x, axis)
        return x

    def get_config(self):
        config = {
            "data_format": self.data_format,
            "directions": self.directions,
            "seed": self.seed,
        }
        base_config = super().get_config()
        return dict(list(base_config.items()) + list(config.items()))
