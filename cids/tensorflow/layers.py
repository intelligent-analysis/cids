# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Tensorflow layer implementations for CIDS. Part of the CIDS toolbox.

    Classes:
        StateWrapper:           Wraps recurrent neural network state into output
        UNet:                   U-Net implementation
        TimeUNet:               Time-dependent U-Net implementation
        LayerNormalization:     Layer-normalization layer
        MaskedTimeDistributed:  Time distributed layer that passes the mask on
        Sampling:               Sampling layer for Autoencoders1
        NonlinearRegression:    Nonlinear regression layer with exponential kernel
"""
import copy

import numpy as np
import tensorflow as tf
from tensorflow.python.keras import constraints
from tensorflow.python.keras import initializers
from tensorflow.python.keras import regularizers
from tensorflow.python.keras.utils import generic_utils
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import nn


class StateWrapper(tf.keras.layers.Layer):
    def __init__(self, wrapped, *args, **kwargs):
        assert isinstance(wrapped, tf.keras.layers.RNN)
        self.wrapped = wrapped
        self.state_fetch0 = None
        self.state_fetch1 = None
        self.state_placeholder0 = None
        self.state_placeholder1 = None
        super().__init__(*args, **kwargs)

    # TODO activate again
    # @property
    # def units(self):
    #     return self.wrapped.units

    def compute_output_shape(self, input_shape):
        return self.wrapped.compute_output_shape(self, input_shape)

    def compute_mask(self, inputs, mask=None):
        return mask

    def build(self, input_shape):
        self.state_placeholder0 = tf.compat.v1.placeholder_with_default(
            tf.zeros([1, self.wrapped.units], dtype=self.dtype),
            [None, self.wrapped.units],
            name="state_placeholder0",
        )
        self.state_placeholder1 = tf.compat.v1.placeholder_with_default(
            tf.zeros([1, self.wrapped.units], dtype=self.dtype),
            [None, self.wrapped.units],
            name="state_placeholder1",
        )
        if not self.wrapped.built:
            self.wrapped.build(input_shape)
        shape = self.wrapped.compute_output_shape(input_shape)
        super().build(shape)
        self.built = True

    def call(self, x, *args, **kwargs):
        results = self.wrapped.call(
            x,
            *args,
            initial_state=[
                tf.tile(
                    self.state_placeholder0,
                    [tf.shape(x)[0] // tf.shape(self.state_placeholder0)[0], 1],
                ),
                tf.tile(
                    self.state_placeholder1,
                    [tf.shape(x)[0] // tf.shape(self.state_placeholder1)[0], 1],
                ),
            ],
            **kwargs,
        )
        y_, state0, state1 = results[0], results[1], results[2]
        self.state_fetch0 = state0
        self.state_fetch1 = state1
        return y_


class UNet(tf.keras.layers.Layer):
    def __init__(
        self,
        num_kernels,
        kernel_size=3,
        num_levels=3,
        conv_per_level=3,
        bypass_mode="residual",
        data_format="NXYF",
        keep_prob=1.0,
        normalize=False,
        pool_size=2,
        pool_type="max",
        feature_base=2,
        activation="relu",
        floor_size=None,
        skip_down=False,
        skip_up=False,
        **kwargs,
    ):
        """A U-Net building block."""
        self.num_kernels = num_kernels
        self.kernel_size = kernel_size
        self.num_levels = num_levels
        self.conv_per_level = conv_per_level
        self.bypass_mode = bypass_mode
        self.data_format = data_format
        self.keep_prob = keep_prob
        self.normalize = normalize
        self.pool_size = pool_size
        self.pool_type = pool_type
        self.skip_down = skip_down
        self.skip_up = skip_up
        self.floor_size = floor_size or int(num_kernels * (feature_base**num_levels))
        self.feature_base = feature_base
        self.num_spatial_dims = (
            data_format.count("X") + data_format.count("Y") + data_format.count("Z")
        )
        self.activation = activation
        # Prepare core
        self.down_levels = []
        self.floor_layers = []
        self.up_levels = []
        self.bypasses = []

        # Down: level 0 to num_levels - 1
        for il in range(self.num_levels):
            level = []

            # Pooling
            if il > 0:
                # Reduce spatial size by 2
                level.append(self.add_pool(self.pool_size, name=f"Udown{il:d}-pool"))
            # Convolutions
            for ic in range(self.conv_per_level):
                level.append(
                    self.add_conv(
                        int(self.num_kernels * (self.feature_base**il)),
                        self.kernel_size,
                        padding="same",
                        activation="linear",
                        use_bias=not self.normalize,
                        name=f"Udown{il:d}-conv{ic:d}",
                    )
                )
                if self.normalize:
                    level.append(
                        self.add_batch_norm(
                            # fused=True,
                            name=f"Udown{il:d}-bn{ic:d}"
                        )
                    )
                # TODO: batch norm before or after relu? Single batch norm per block
                level.append(self.add_activation())

            # Bypass out
            if il < self.num_levels - 1:
                level.append("bypass-out")

            # Append level
            self.down_levels.append(level)

        # Floor: no spatiality
        self.floor_layers.extend(
            self.add_floor(
                self.floor_size,
                activation=self.activation,
                use_bias=not self.normalize,
                name="Ufloor",
            )
        )

        # Up: level num_levels - 1 to 0
        for il in reversed(range(self.num_levels - 1)):

            level = []

            # Unpool
            level.append(
                self.add_upconv(
                    int(self.num_kernels * (self.feature_base**il)),
                    self.pool_size,
                    strides=self.pool_size,
                    name=f"Uup{il:d}-upconv",
                )
            )

            # Bypass in
            level.append("bypass-in")

            # Convolutions
            for ic in range(self.conv_per_level):
                level.append(
                    self.add_conv(
                        int(self.num_kernels * (self.feature_base**il)),
                        self.kernel_size,
                        padding="same",
                        activation="linear",
                        use_bias=not self.normalize,
                        name=f"Uup{il:d}-conv{ic:d}",
                    )
                )
                if self.normalize:
                    level.append(
                        self.add_batch_norm(
                            name=f"Uup{il:d}-bn{ic:d}",
                            # fused=True
                        )
                    )
                # TODO: batch norm before or after relu? Single batch norm per block
                level.append(self.add_activation())

            # Append level
            self.up_levels.append(level)

        super().__init__(**kwargs)

    def get_config(self):
        config = super().get_config()
        config.update(
            {
                "num_kernels": self.num_kernels,
                "kernel_size": self.kernel_size,
                "num_levels": self.num_levels,
                "conv_per_level": self.conv_per_level,
                "bypass_mode": self.bypass_mode,
                "data_format": self.data_format,
                "keep_prob": self.keep_prob,
                "normalize": self.normalize,
            }
        )
        return config

    @property
    def units(self):
        return self.num_kernels

    @property
    def layers(self):
        layers = [
            layer
            for level in self.down_levels
            for layer in level
            if isinstance(layer, tf.keras.layers.Layer)
        ]
        layers += [
            layer
            for layer in self.floor_layers
            if isinstance(layer, tf.keras.layers.Layer)
        ]
        layers += [
            layer
            for level in self.up_levels
            for layer in level
            if isinstance(layer, tf.keras.layers.Layer)
        ]
        return layers

    def add_conv(self, *args, **kwargs):
        if self.num_spatial_dims == 2:
            return tf.keras.layers.Conv2D(*args, **kwargs)
        if self.num_spatial_dims == 3:
            return tf.keras.layers.Conv3D(*args, **kwargs)
        raise ValueError(
            f"Invalid number of spatial dimensions: {self.num_spatial_dims:d}"
        )

    def add_pool(self, *args, **kwargs):
        if self.num_spatial_dims == 2:
            if self.pool_type == "max":
                return tf.keras.layers.MaxPooling2D(*args, **kwargs)
            if self.pool_type == "average":
                return tf.keras.layers.AveragePooling2D(*args, **kwargs)
            if self.pool_type == "sum":
                raise NotImplementedError("Arnd: Implement this?")
            if "conv" in self.pool_type:
                kkwargs = copy.deepcopy(kwargs)
                kkwargs["strides"] = args[0]
                kkwargs["padding"] = "valid"
                if "-" in self.pool_type:
                    kkwargs["activation"] = self.pool_type.split("-")[-1]
                else:
                    kkwargs["activation"] = None
                return tf.keras.layers.Conv2D(self.num_kernels, *args, **kkwargs)
            raise ValueError(f"Invalid self.pool_type: {self.pool_type}")
        if self.num_spatial_dims == 3:
            if self.pool_type == "max":
                return tf.keras.layers.MaxPooling3D(*args, **kwargs)
            if self.pool_type == "average":
                return tf.keras.layers.AveragePooling3D(*args, **kwargs)
            if self.pool_type == "sum":
                raise NotImplementedError("Arnd: Implement this?")
            if "conv" in self.pool_type:
                kkwargs = copy.deepcopy(kwargs)
                kkwargs["strides"] = args[0]
                kkwargs["padding"] = "valid"
                if "-" in self.pool_type:
                    kkwargs["activation"] = self.pool_type.split("-")[-1]
                else:
                    kkwargs["activation"] = None
                return tf.keras.layers.Conv3D(self.num_kernels, *args, **kkwargs)
            raise ValueError(f"Invalid self.pool_type: {self.pool_type}")
        raise ValueError(
            f"Invalid number of spatial dimensions: {self.num_spatial_dims:d}"
        )

    def add_upconv(self, *args, **kwargs):
        if self.num_spatial_dims == 2:
            return tf.keras.layers.Conv2DTranspose(*args, **kwargs)
        if self.num_spatial_dims == 3:
            return tf.keras.layers.Conv3DTranspose(*args, **kwargs)
        raise ValueError(
            f"Invalid number of spatial dimensions: {self.num_spatial_dims:d}"
        )

    def add_floor(self, num_units, activation="relu", **kwargs):
        floor = [
            self.add_conv(num_units, 1, padding="same", activation="linear", **kwargs)
        ]
        if self.normalize:
            floor.append(
                self.add_batch_norm(
                    # fused=True,
                    name="Ufloor-bn"
                )
            )  # TODO: batch norm before or after relu? Single batch norm per block
        floor.append(tf.keras.layers.Activation(activation))
        if self.keep_prob < 1.0:
            floor.append(self.add_dropout(rate=1.0 - self.keep_prob))
        return floor

    def add_batch_norm(self, *args, **kwargs):
        if self.normalize == "batch":
            return tf.keras.layers.BatchNormalization(*args, **kwargs)
        if self.normalize == "layer":
            if "fused" in kwargs:
                del kwargs["fused"]
            return LayerNormalization(*args, **kwargs)
        raise ValueError("Invalid normalize value: ", self.normalize)

    def add_dropout(self, *args, **kwargs):
        return tf.keras.layers.Dropout(*args, **kwargs)

    def add_activation(self, *args, **kwargs):
        return tf.keras.layers.Activation(self.activation)

    def _bypass_merge(self, y_, bypass):
        # Pad to spatial size of downward branch
        paddings = []
        for di, dd in enumerate(self.data_format):
            if dd in "XYZ":
                # spatial axes XYZ
                if bypass.shape[di] - y_.shape[di] == 1:
                    paddings += [[0, 1]]
                elif bypass.shape[di] - y_.shape[di] == 0:
                    paddings += [[0, 0]]
                else:
                    raise ValueError("U-net bypass shape not recovered.")
            else:
                # non spatial axes NSF
                paddings += [[0, 0]]
        y_ = tf.pad(y_, paddings, "SYMMETRIC")
        # Merge with bypass
        if self.bypass_mode == "residual":
            y_ = tf.add(y_, bypass)
        elif self.bypass_mode == "concat":
            y_ = tf.concat([y_, bypass], -1)
        return y_

    def _bypass_merge_shape(self, y_shape, bypass_shape):
        if self.bypass_mode == "residual":
            return bypass_shape
        new_shape = list(y_shape)
        new_shape[-1] += bypass_shape[-1]
        return new_shape

    def _merge_skip_down(self, y_, skip):
        y_ = tf.keras.layers.Flatten()(y_)
        skip = tf.keras.layers.Flatten()(skip)
        y_ = tf.concat([y_, skip], -1)
        return y_

    def _merge_skip_down_shape(self, y_shape, skip_shape):
        new_shape = [y_shape[0], np.prod(y_shape[1:])]
        new_shape[-1] += np.prod(skip_shape[1:])
        return new_shape

    def _merge_skip_up(self, y_, skip):
        skip_shape = skip.shape.as_list()
        y_shape = y_.shape.as_list()
        target_shape = list(y_shape[2:])
        target_shape[-1] = int(np.prod(skip_shape[2:])) // int(np.prod(y_shape[2:-1]))
        skip = MaskedTimeDistributed(tf.keras.layers.Reshape(target_shape))(skip)
        y_ = tf.concat([y_, skip], -1)
        return y_

    def _merge_skip_up_shape(self, y_shape, skip_shape):
        new_shape = list(y_shape)
        new_shape[-1] += int(np.prod(skip_shape[2:])) // int(np.prod(y_shape[2:-1]))
        return new_shape

    def call(self, inputs, training=None, mask=None):
        y_ = inputs
        for level in self.down_levels:
            for layer in level:
                if layer == "bypass-out":
                    self.bypasses.append(y_)
                else:
                    kwargs = {}
                    if generic_utils.has_arg(layer.call, "training"):
                        kwargs["training"] = training
                    if generic_utils.has_arg(layer.call, "mask"):
                        kwargs["mask"] = mask
                    y_ = layer.call(y_, **kwargs)
        if self.skip_down:
            y_ = self._merge_skip_down(y_, inputs)
        for layer in self.floor_layers:
            kwargs = {}
            if generic_utils.has_arg(layer.call, "training"):
                kwargs["training"] = training
            if generic_utils.has_arg(layer.call, "mask"):
                kwargs["mask"] = mask
            if isinstance(layer, tf.keras.layers.RNN):
                # TODO: only supports one stateful layer
                results = layer.call(
                    y_,
                    initial_state=[
                        tf.tile(
                            self.state_placeholder0,
                            [
                                tf.shape(y_)[0] // tf.shape(self.state_placeholder0)[0],
                                1,
                            ],
                        ),
                        tf.tile(
                            self.state_placeholder1,
                            [
                                tf.shape(y_)[0] // tf.shape(self.state_placeholder1)[0],
                                1,
                            ],
                        ),
                    ],
                    **kwargs,
                )
                y_, state0, state1 = results[0], results[1], results[2]
                self.state_fetch0 = state0
                self.state_fetch1 = state1
            else:
                y_ = layer.call(y_, **kwargs)
        floor = y_
        for level, bypass in zip(self.up_levels, reversed(self.bypasses)):
            for layer in level:
                if layer == "bypass-in":
                    y_ = self._bypass_merge(y_, bypass)
                else:
                    kwargs = {}
                    if generic_utils.has_arg(layer.call, "training"):
                        kwargs["training"] = training
                    if generic_utils.has_arg(layer.call, "mask"):
                        kwargs["mask"] = mask
                    y_ = layer.call(y_, **kwargs)
        if self.skip_up:
            y_ = self._merge_skip_up(y_, floor)
        return y_

    def compute_output_shape(self, input_shape):
        shape = input_shape
        bypass_shapes = []
        for level in self.down_levels:
            for layer in level:
                if layer == "bypass-out":
                    bypass_shapes.append(shape)
                else:
                    shape = layer.compute_output_shape(shape)
        if self.skip_down:
            shape = self._merge_skip_down_shape(shape, input_shape)
        for layer in self.floor_layers:
            shape = layer.compute_output_shape(shape)
            if isinstance(layer, tf.keras.layers.RNN):
                shape = shape[0]
        floor_shape = shape
        for level, bypass_shape in zip(self.up_levels, reversed(bypass_shapes)):
            for layer in level:
                if layer == "bypass-in":
                    shape = self._bypass_merge_shape(shape, bypass_shape)
                else:
                    shape = layer.compute_output_shape(shape)
        if self.skip_up:
            shape = self._merge_skip_up_shape(shape, floor_shape)
        return shape

    def build(self, input_shape):
        shape = input_shape
        bypass_shapes = []
        for level in self.down_levels:
            for layer in level:
                if layer == "bypass-out":
                    bypass_shapes.append(shape)
                else:
                    if not layer.built:
                        with tf.name_scope(layer.name):
                            layer.build(shape)
                    shape = layer.compute_output_shape(shape)
        if self.skip_down:
            shape = self._merge_skip_down_shape(shape, input_shape)
        for layer in self.floor_layers:
            if isinstance(layer, tf.keras.layers.RNN):
                # TODO: only supports one stateful layer
                self.state_placeholder0 = tf.compat.v1.placeholder_with_default(
                    tf.zeros([1, layer.units], dtype=self.dtype),
                    [None, layer.units],
                    name="state_placeholder0",
                )
                self.state_placeholder1 = tf.compat.v1.placeholder_with_default(
                    tf.zeros([1, layer.units], dtype=self.dtype),
                    [None, layer.units],
                    name="state_placeholder1",
                )
            if not layer.built:
                with tf.name_scope(layer.name):
                    layer.build(shape)
            shape = layer.compute_output_shape(shape)
            if isinstance(layer, tf.keras.layers.RNN):
                shape = shape[0]
        floor_shape = shape
        for level, bypass_shape in zip(self.up_levels, reversed(bypass_shapes)):
            for layer in level:
                if layer == "bypass-in":
                    shape = self._bypass_merge_shape(shape, bypass_shape)
                else:
                    if not layer.built:
                        with tf.name_scope(layer.name):
                            layer.build(shape)
                    shape = layer.compute_output_shape(shape)
        if self.skip_up:
            shape = self._merge_skip_up_shape(shape, floor_shape)
        super().build(shape)
        self.built = True

    def compute_mask(self, inputs, mask=None):
        return mask


class TimeUNet(UNet):
    def __init__(
        self,
        sequence_length,
        num_kernels,
        kernel_size=3,
        num_levels=3,
        conv_per_level=3,
        bypass_mode="residual",
        data_format="NSXYF",
        activation="relu",
        keep_prob=1.0,
        normalize=False,
        time_mode="distribute",
        **kwargs,
    ):
        """A Recurrent U-Net building block."""
        assert time_mode in ["distribute", "recurrent"]
        self.time_mode = time_mode
        self.sequence_length = sequence_length
        self.state_fetch0 = None
        self.state_fetch1 = None
        self.state_placeholder0 = None
        self.state_placeholder1 = None
        super().__init__(
            num_kernels,
            kernel_size=kernel_size,
            num_levels=num_levels,
            conv_per_level=conv_per_level,
            bypass_mode=bypass_mode,
            data_format=data_format,
            keep_prob=keep_prob,
            activation=activation,
            normalize=normalize,
            **kwargs,
        )

    def add_conv(self, *args, **kwargs):
        if self.num_spatial_dims == 2:
            if self.time_mode == "recurrent":
                return tf.keras.layers.ConvLSTM2D(
                    *args, **kwargs, return_sequences=True
                )
            if self.time_mode == "distribute":
                return tf.keras.layers.TimeDistributed(
                    tf.keras.layers.Conv2D(*args, **kwargs)
                )
            return tf.keras.layers.Conv2D(*args, **kwargs)
        if self.num_spatial_dims == 3:
            return tf.keras.layers.Conv3D(*args, **kwargs)
        raise ValueError(
            f"Invalid number of spatial dimensions: {self.num_spatial_dims:d}"
        )

    def add_pool(self, *args, **kwargs):
        return tf.keras.layers.TimeDistributed(super().add_pool(*args, **kwargs))

    def add_upconv(self, *args, **kwargs):
        return tf.keras.layers.TimeDistributed(super().add_upconv(*args, **kwargs))

    def add_batch_norm(self, *args, **kwargs):
        return tf.keras.layers.TimeDistributed(super().add_batch_norm(*args, **kwargs))

    def _merge_skip_down(self, y_, skip):
        y_ = MaskedTimeDistributed(tf.keras.layers.Flatten())(y_)
        skip = MaskedTimeDistributed(tf.keras.layers.Flatten())(skip)
        y_ = tf.concat([y_, skip], -1)
        return y_

    def _merge_skip_down_shape(self, y_shape, skip_shape):
        new_shape = [y_shape[0], y_shape[1], np.prod(y_shape[2:])]
        new_shape[-1] += np.prod(skip_shape[2:])
        return new_shape

    def add_floor(self, num_units, **kwargs):
        floor = [
            tf.keras.layers.Reshape([self.sequence_length, -1]),
            tf.keras.layers.LSTM(num_units, return_sequences=True, return_state=True),
            tf.keras.layers.Reshape(
                [self.sequence_length] + [1] * self.num_spatial_dims + [num_units]
            ),
        ]
        if self.normalize:
            floor.append(
                self.add_batch_norm(
                    # fused=True,
                    name="Ufloor-bn"
                )
            )  # TODO: batch norm before or after relu? Single batch norm per block
        if self.keep_prob < 1.0:
            floor.append(self.add_dropout(rate=1.0 - self.keep_prob))
        return floor

    def add_dropout(self, *args, **kwargs):
        return tf.keras.layers.TimeDistributed(super().add_dropout(*args, **kwargs))


class LayerNormalization(tf.keras.layers.Layer):
    """Layer normalization layer (Ba et al., 2016).
    Normalize the activations of the previous layer for each given example in a
    batch independently, rather than across a batch like Batch Normalization.
    i.e. applies a transformation that maintains the mean activation within each
    example close to 0 and the activation standard deviation close to 1.
    Arguments:
      axis: Integer or List/Tuple. The axis that should be normalized
        (typically the features axis).
      epsilon: Small float added to variance to avoid dividing by zero.
      center: If True, add offset of `beta` to normalized tensor.
          If False, `beta` is ignored.
      scale: If True, multiply by `gamma`.
        If False, `gamma` is not used.
        When the next layer is linear (also e.g. `nn.relu`),
        this can be disabled since the scaling
        will be done by the next layer.
      beta_initializer: Initializer for the beta weight.
      gamma_initializer: Initializer for the gamma weight.
      beta_regularizer: Optional regularizer for the beta weight.
      gamma_regularizer: Optional regularizer for the gamma weight.
      beta_constraint: Optional constraint for the beta weight.
      gamma_constraint: Optional constraint for the gamma weight.
      trainable: Boolean, if `True` the variables will be marked as trainable.
    Input shape:
      Arbitrary. Use the keyword argument `input_shape`
      (tuple of integers, does not include the samples axis)
      when using this layer as the first layer in a model.
    Output shape:
      Same shape as input.
    References:
      - [Layer Normalization](https://arxiv.org/abs/1607.06450)
    """

    def __init__(
        self,
        axis=-1,
        epsilon=1e-3,
        center=True,
        scale=True,
        beta_initializer="zeros",
        gamma_initializer="ones",
        beta_regularizer=None,
        gamma_regularizer=None,
        beta_constraint=None,
        gamma_constraint=None,
        trainable=True,
        name=None,
        **kwargs,
    ):
        super().__init__(name=name, trainable=trainable, **kwargs)
        if isinstance(axis, (list, tuple)):
            self.axis = axis[:]
        elif isinstance(axis, int):
            self.axis = axis
        else:
            raise ValueError(
                "Expected an int or a list/tuple of ints for the argument 'axis',"
                + f" but received instead: {axis:s}"
            )

        self.epsilon = epsilon
        self.center = center
        self.scale = scale
        self.beta_initializer = initializers.get(beta_initializer)
        self.gamma_initializer = initializers.get(gamma_initializer)
        self.beta_regularizer = regularizers.get(beta_regularizer)
        self.gamma_regularizer = regularizers.get(gamma_regularizer)
        self.beta_constraint = constraints.get(beta_constraint)
        self.gamma_constraint = constraints.get(gamma_constraint)

        self.supports_masking = True

    def build(self, input_shape):
        ndims = len(input_shape)
        if ndims is None:
            raise ValueError(f"Input shape {input_shape:s} has undefined rank.")

        # Convert axis to list and resolve negatives
        if isinstance(self.axis, int):
            self.axis = [self.axis]
        for idx, x in enumerate(self.axis):
            if x < 0:
                self.axis[idx] = ndims + x

        # Validate axes
        for x in self.axis:
            if x < 0 or x >= ndims:
                raise ValueError(f"Invalid axis: {x:d}")
        if len(self.axis) != len(set(self.axis)):
            raise ValueError(f"Duplicate axis: {tuple(self.axis)}")

        param_shape = [input_shape[dim] for dim in self.axis]
        if self.scale:
            self.gamma = self.add_weight(
                name="gamma",
                shape=param_shape,
                initializer=self.gamma_initializer,
                regularizer=self.gamma_regularizer,
                constraint=self.gamma_constraint,
                trainable=True,
            )
        else:
            self.gamma = None

        if self.center:
            self.beta = self.add_weight(
                name="beta",
                shape=param_shape,
                initializer=self.beta_initializer,
                regularizer=self.beta_regularizer,
                constraint=self.beta_constraint,
                trainable=True,
            )
        else:
            self.beta = None

    def call(self, inputs):
        # Compute the axes along which to reduce the mean / variance
        input_shape = inputs.shape
        ndims = len(input_shape)

        # Calculate the moments on the last axis (layer activations).
        mean, variance = nn.moments(inputs, self.axis, keep_dims=True)

        # Broadcasting only necessary for norm where the axis is not just
        # the last dimension
        broadcast_shape = [1] * ndims
        for dim in self.axis:
            broadcast_shape[dim] = input_shape.dims[dim].value

        def _broadcast(v):
            if v is not None and len(v.shape) != ndims and self.axis != [ndims - 1]:
                return array_ops.reshape(v, broadcast_shape)
            return v

        scale, offset = _broadcast(self.gamma), _broadcast(self.beta)

        # Compute layer normalization using the batch_normalization function.
        outputs = nn.batch_normalization(
            inputs,
            mean,
            variance,
            offset=offset,
            scale=scale,
            variance_epsilon=self.epsilon,
        )

        # If some components of the shape got lost due to adjustments, fix that.
        outputs.set_shape(input_shape)

        return outputs

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        config = {
            "axis": self.axis,
            "epsilon": self.epsilon,
            "center": self.center,
            "scale": self.scale,
            "beta_initializer": initializers.serialize(self.beta_initializer),
            "gamma_initializer": initializers.serialize(self.gamma_initializer),
            "beta_regularizer": regularizers.serialize(self.beta_regularizer),
            "gamma_regularizer": regularizers.serialize(self.gamma_regularizer),
            "beta_constraint": constraints.serialize(self.beta_constraint),
            "gamma_constraint": constraints.serialize(self.gamma_constraint),
        }
        base_config = super().get_config()
        return dict(list(base_config.items()) + list(config.items()))


class MaskedTimeDistributed(tf.keras.layers.TimeDistributed):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.supports_masking = True
        self._compute_output_and_mask_jointly = True

    def compute_mask(self, inputs, mask=None):
        if mask is None:
            return None
        # output_shape = self.compute_output_shape(inputs.shape)
        # while len(output_shape) < len(mask.shape):
        mask = tf.expand_dims(mask, axis=-1)
        return mask

    def call(self, inputs, mask=None):
        mask = self.compute_mask(inputs, mask=mask)
        outputs = super().call(inputs, mask=mask)
        if mask is None:
            return outputs
        outputs = outputs * tf.cast(mask, outputs.dtype)
        outputs._keras_mask = tf.squeeze(mask, axis=-1)
        return outputs


class Sampling(tf.keras.layers.Layer):
    def __init__(
        self,
        add_sampling_loss=False,
        sampling_model=None,
        mode="repar",
        latent_dim=None,
        beta=1.0,
        analytic_kl=True,
        use_input_as_seed=False,
        **kwargs,
    ):
        """Sample from normal distribution of given mean and logvar tensors."""
        super().__init__(**kwargs)
        self.add_sampling_loss = add_sampling_loss
        assert sampling_model is None or isinstance(sampling_model, tf.keras.Model)
        self.sampling_model = sampling_model
        assert mode in ["repar", "concat", "reparaugment", "drop"]
        self.mode = mode
        if mode == "concat":
            assert (
                latent_dim is not None
            ), "Need to provide latent_dim to concat noise to input tensor."
        self.latent_dim = latent_dim
        self.use_input_as_seed = use_input_as_seed
        self.weight = beta
        self.analytic_kl = analytic_kl

    def get_config(self):
        if self.sampling_model is None:
            sampling_model_config = None
        else:
            sampling_model_config = self.sampling_model.get_config()
        config = {
            "add_sampling_loss": self.add_sampling_loss,
            "sampling_model": sampling_model_config,
            "mode": self.mode,
            "latent_dim": self.latent_dim,
        }
        base_config = super().get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def compute_output_shape(self, input_shape):
        output_shape = list(input_shape)
        if self.mode in "repar":
            output_shape[-1] = int(output_shape[-1] // 2)
        elif self.mode == "reparaugment":
            output_shape[-1] = int(output_shape[-1] // 2 + input_shape[-1])
        elif self.mode == "concat":
            output_shape[-1] = output_shape[-1] + self.latent_dim
        return output_shape

    def _log_normal_pdf(self, sample, mean, logvar, axis=-1):
        log2pi = tf.math.log(2.0 * np.pi)
        return tf.reduce_sum(
            -0.5 * ((sample - mean) ** 2.0 * tf.exp(-logvar) + logvar + log2pi),
            axis=axis,
        )

    def _logpz(self, z):
        return self._log_normal_pdf(z, 0.0, 0.0)

    def _logqz_x(self, z, mean, logvar):
        return self._log_normal_pdf(z, mean, logvar)

    def build(self, input_shape):
        self.beta = tf.Variable(
            self.weight, name="beta", dtype="float32", trainable=False
        )

    def call(self, inputs, eps=None):
        x = inputs
        if self.mode in ["repar", "reparaugment"]:
            if self.sampling_model is not None:
                mean = self.sampling_model(x)
            else:
                mean = x
            # Get mean and variance
            mean, logvar = tf.split(mean, 2, -1)
            # Generate noise
            if eps is None:
                shape = tf.shape(mean)
                if self.use_input_as_seed:
                    seed = tf.stack(
                        [tf.reduce_sum(x) * 10000.0, tf.reduce_max(tf.abs(x)) * 1000.0]
                    )
                    seed = tf.cast(seed, tf.int32)
                    eps = tf.random.stateless_normal(shape=shape, seed=seed)
                else:
                    eps = tf.random.normal(shape=shape)
            # Apply noise
            z = eps * tf.exp(0.5 * logvar) + mean
            if self.add_sampling_loss:
                if self.analytic_kl:
                    kl = tf.reduce_mean(
                        -0.5
                        * tf.reduce_sum(
                            1 + logvar - tf.square(mean) - tf.exp(logvar), axis=1
                        )
                    )
                    kl_loss = tf.math.multiply(self.beta, kl)

                else:
                    kl = -tf.reduce_mean(
                        self._logpz(z) - self._logqz_x(z, mean, logvar)
                    )
                    kl_loss = kl_loss = tf.math.multiply(self.beta, kl)
                self.add_loss(kl_loss)
                # TODO(deepa): is this correct? this seems quite similar to part of the
                # original implementation, but with some missing terms.
                # TODO: check and derive this
            if "augment" in self.mode:
                z = tf.concat([x, z], -1)
        elif self.mode in ["concat", "drop"]:
            # Generate noise
            if eps is None:
                shape = tf.concat([tf.shape(x)[:-1], [self.latent_dim]], -1)
                if self.use_input_as_seed:
                    seed = tf.stack(
                        [tf.reduce_sum(x) * 10000.0, tf.reduce_max(tf.abs(x)) * 1000.0]
                    )
                    seed = tf.cast(seed, tf.int32)
                    eps = tf.random.stateless_normal(shape=shape, seed=seed)
                else:
                    eps = tf.random.normal(shape=shape)
            # Apply noise
            if self.mode == "concat":
                z = tf.concat([x, eps], -1)
            elif self.mode == "drop":
                # Apply noise
                z = eps
            else:
                raise RuntimeError("This should not happen.")
        else:
            raise NotImplementedError
        return z


class NonlinearRegression(tf.keras.layers.Layer):
    def __init__(
        self,
        bias_initializer=None,
        frequency_initializer=None,
        factor_initializer=None,
        **kwargs,
    ):
        """Layer that implements nonlinear regression."""
        super().__init__(**kwargs)
        if bias_initializer is None:
            self.bias_initializer = tf.keras.initializers.Zeros()
        else:
            self.bias_initializer = bias_initializer
        if frequency_initializer is None:
            self.frequency_initializer = tf.keras.initializers.RandomNormal()
        else:
            self.frequency_initializer = frequency_initializer
        if factor_initializer is None:
            self.factor_initializer = tf.keras.initializers.RandomNormal()
        else:
            self.factor_initializer = factor_initializer
        self.factor = None
        self.frequency = None
        self.bias = None

    def get_config(self):
        config = super().get_config()
        config["bias_initializer"] = self.bias_initializer
        config["frequency_initializer"] = self.frequency_initializer
        config["factor_initializer"] = self.factor_initializer
        return config

    def build(self, input_shape):
        self.factor = self.add_weight(
            "factor", (1,), initializer=self.factor_initializer, trainable=True
        )
        self.frequency = self.add_weight(
            "frequency",
            (input_shape[-1], 1),
            initializer=self.frequency_initializer,
            trainable=True,
        )
        self.bias = self.add_weight(
            "bias", (1,), initializer=self.bias_initializer, trainable=True
        )

    def call(self, inputs):
        return self.factor * tf.exp(inputs @ self.frequency) - self.bias
