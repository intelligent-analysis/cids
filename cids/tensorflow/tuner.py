# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Kerastuner interface for CIDS with Tensorflow. Part of the CIDS toolbox.

    Classes:
        CIDSTuner:      Tuner for CIDS compatible with Kerastuner
        CustomTrial:    Custom trial class that allows reading directly from json file.
        SearchResults:  Allows loading of kerastuner search results

"""
import collections
import copy
import glob
import json
import os

import colorama
import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf
from colorama import Fore
from colorama import Style
from keras_tuner.src.engine import base_tuner
from keras_tuner.src.engine import hyperparameters as hp_module
from keras_tuner.src.engine import trial as trial_module
from keras_tuner.src.engine import tuner_utils
from keras_tuner.src.engine.metrics_tracking import infer_metric_direction
from keras_tuner.src.engine.oracle import Objective
from matplotlib import pyplot as plt

colorama.init()


class CIDSTuner(base_tuner.BaseTuner):  # pylint: disable=abstract-method

    DEBUG = False

    def __init__(
        self,
        oracle,
        cids_model,
        executions_per_trial=1,
        distribution_strategy=None,
        directory=None,
        project_name=None,
        logger=None,
        tuner_id=None,
        overwrite=False,
        **kwargs,
    ):
        """A Tuner for hyperparameters in CIDS models.

        Args:
            oracle:     Instance of an Oracle class.
            cids_model: Instance of CIDSModel class.
            **kwargs: Keyword arguments relevant to all `Tuner` subclasses.
                Please see the docstring for `Tuner`.
        """
        super().__init__(
            oracle=oracle,
            hypermodel=cids_model,
            directory=directory,
            project_name=project_name,
            logger=logger,
            overwrite=overwrite,
        )

        # TODO: distribution strategy
        # self.distribution_strategy = distribution_strategy

        # Save only the last N checkpoints.
        if isinstance(oracle.objective, list):
            raise ValueError(
                f"Multi-objective is not supported, found: {oracle.objective}"
            )
        self.executions_per_trial = executions_per_trial
        # This is the `step` that will be reported to the Oracle at the end
        # of the Trial. Since intermediate results are not used, this is set
        # to 0.
        self._reported_step = 0

    def _populate_initial_space(self):
        old_verbosity = self.hypermodel.VERBOSITY
        self.hypermodel.VERBOSITY = 0
        if self.hypermodel is None:
            return
        hp = self.oracle.get_space()
        # Lists of stacks of conditions used during `explore_space()`.
        scopes_never_active = []
        scopes_once_active = []
        while True:
            self.hypermodel.build(hp, checkpoint=None)
            # Update the recored scopes.
            for conditions in hp.active_scopes:
                if conditions not in scopes_once_active:
                    scopes_once_active.append(copy.deepcopy(conditions))
                if conditions in scopes_never_active:
                    scopes_never_active.remove(conditions)
            for conditions in hp.inactive_scopes:
                if conditions not in scopes_once_active:
                    scopes_never_active.append(copy.deepcopy(conditions))
            # All conditional scopes are activated.
            if len(scopes_never_active) == 0:
                break
            # Generate new values to activate new conditions.
            conditions = scopes_never_active[0]
            for condition in conditions:
                hp.values[condition.name] = condition.values[0]
        self.oracle.update_space(hp)
        self.hypermodel.VERBOSITY = old_verbosity
        self.hypermodel.clear()  # Must clear model again!

    def _find_parent_trial_id(self, trial, recursive=True):
        hp = trial.hyperparameters
        if "tuner/trial_id" in hp.values:
            trial_id = hp.values["tuner/trial_id"]
            parent_trial = self.oracle.get_trial(trial_id)
            if recursive:
                parent_trial_id = self._find_parent_trial_id(
                    parent_trial, recursive=recursive
                )
                return parent_trial_id
            return parent_trial.trial_id
        return trial.trial_id

    def run_trial(self, trial, *fit_args, **fit_kwargs):
        hp = trial.hyperparameters
        ancestor_id = self._find_parent_trial_id(trial, recursive=True)
        print(
            Fore.YELLOW
            + f"[Trial {trial.trial_id:s} ({ancestor_id:s}) started]"
            + Style.RESET_ALL
        )
        if "tuner/epochs" in hp.values:
            fit_kwargs["limit_epochs"] = hp.values["tuner/epochs"]
            fit_kwargs["initial_epoch"] = hp.values["tuner/initial_epoch"]
        original_callbacks = fit_kwargs.pop("callbacks", [])
        # Run the training process multiple times.
        metrics = collections.defaultdict(list)
        old_hypermodel_identifier = self.hypermodel.identifier
        old_meta_folder = self.hypermodel.meta_folder
        old_save_freq = self.hypermodel.save_freq
        if not self.DEBUG:
            old_verbosity = self.hypermodel.VERBOSITY
            old_data_reader_verbosity = self.hypermodel.data_reader.VERBOSITY
            self.hypermodel.VERBOSITY = 1
            self.hypermodel.data_reader.VERBOSITY = 0
        try:
            stopped_execs = 0
            for execution in range(self.executions_per_trial):
                print(f" Execution {execution:d}")
                self.hypermodel.INDENT = "   "
                self.hypermodel.identifier = f"execution{execution:02d}"
                self.hypermodel.meta_folder = os.path.join(
                    old_meta_folder, f"trial_{ancestor_id:s}"
                )
                self.hypermodel.save_freq = 0
                copied_fit_kwargs = copy.copy(fit_kwargs)
                copied_fit_kwargs["save_after_phases"] = copied_fit_kwargs.get(
                    "save_after_phases", True
                )
                # Update callbacks
                callbacks = self._deepcopy_callbacks(original_callbacks)
                # self._configure_tensorboard_dir(callbacks, trial_id, execution)
                callbacks.append(tuner_utils.TunerCallback(self, trial))
                copied_fit_kwargs["callbacks"] = callbacks
                # Handle checkpoint loading
                initial_epoch = copied_fit_kwargs.get("initial_epoch")
                if initial_epoch:
                    copied_fit_kwargs["checkpoint"] = initial_epoch
                else:
                    copied_fit_kwargs["checkpoint"] = None
                # Catch if checkpoint does not exist or run diverged
                try:
                    # Run
                    history = self.hypermodel.train(
                        *fit_args, hp=hp, **copied_fit_kwargs
                    )
                    # Check if stopped
                    if self.hypermodel.forward_model.stop_training:
                        self.hypermodel.log("Completed by early stopping.")
                    # Save
                    self.hypermodel.save(self.hypermodel.count)
                    # Evaluate run
                    for metric, epoch_values in history.items():
                        if self.oracle.objective.direction == "min":
                            best_value = np.min(epoch_values)
                        else:
                            best_value = np.max(epoch_values)
                        metrics[metric].append(best_value)
                except OSError:
                    # Checkpoint does not exists: early stopping detected
                    stopped_execs += 1
                    self.hypermodel.log(
                        f"Execution {execution:d} was completed by early stopping."
                    )
                    # Extract metrics from parent
                    parent_id = self._find_parent_trial_id(trial, recursive=False)
                    parent_trial = self.oracle.get_trial(parent_id)
                    for metric in parent_trial.metrics.metrics.keys():
                        metric_value = parent_trial.metrics.get_best_value(metric)
                        metrics[metric].append(metric_value)
                except (
                    tf.errors.InvalidArgumentError,
                    tf.errors.ResourceExhaustedError,
                    ValueError,
                ) as e:
                    # NaN in summary history: diverged
                    # or Invalid configuration
                    # or not enough memory for configuration
                    stopped_execs += 1
                    self.hypermodel.log(f"Execution {execution:d} failed ({str(e):s}).")
                    # Extract metrics from parent
                    parent_id = self._find_parent_trial_id(trial, recursive=False)
                    parent_trial = self.oracle.get_trial(parent_id)
                    for metric in parent_trial.metrics.metrics.keys():
                        metric_value = parent_trial.metrics.get_best_value(metric)
                        metrics[metric].append(metric_value)
                finally:
                    # Clear model
                    self.hypermodel.clear()
        finally:
            self.hypermodel.INDENT = ""
            self.hypermodel.identifier = old_hypermodel_identifier
            self.hypermodel.meta_folder = old_meta_folder
            self.hypermodel.save_freq = old_save_freq
            if not self.DEBUG:
                self.hypermodel.VERBOSITY = old_verbosity
                self.hypermodel.data_reader.VERBOSITY = old_data_reader_verbosity

        # Average the results across executions and send to the Oracle.
        averaged_metrics = {}
        for metric, execution_values in metrics.items():
            averaged_metrics[metric] = np.mean(execution_values)
        # Ensure all metrics exist
        if self.oracle.objective.name not in averaged_metrics:
            if self.oracle.objective.direction == "min":
                averaged_metrics[self.oracle.objective.name] = np.finfo(np.float32).max
            else:
                averaged_metrics[self.oracle.objective.name] = np.finfo(np.float32).min
        self.oracle.update_trial(
            trial.trial_id, metrics=averaged_metrics, step=self._reported_step
        )

        if stopped_execs >= self.executions_per_trial:
            # End trial early if all executions stopped
            self.hypermodel.log("Trial finished early.")
            try:
                trial.status = "COMPLETED"
                self.oracle.end_trial(trial)
            except ValueError:
                # Probably already completed
                pass

    def on_trial_end(self, trial):
        """A hook called after each trial is run.

        # Arguments:
            trial: A `Trial` instance.
        """
        # Send status to Logger
        if self.logger:
            self.logger.report_trial_state(trial.trial_id, trial.get_state())

        try:
            self.oracle.end_trial(trial)
        except ValueError:
            # Probably already completed
            pass
        self.oracle.update_space(trial.hyperparameters)
        # Display needs the updated trial scored by the Oracle.
        self._display.on_trial_end(self.oracle.get_trial(trial.trial_id))
        self.save()

    def on_epoch_begin(self, trial, model, epoch, logs=None):
        """A hook called at the start of every epoch.

        # Arguments:
            trial: A `Trial` instance.
            model: A Keras `Model`.
            epoch: The current epoch number.
            logs: Additional metrics.
        """
        return

    def on_batch_begin(self, trial, model, batch, logs):
        """A hook called at the start of every batch.

        # Arguments:
            trial: A `Trial` instance.
            model: A Keras `Model`.
            batch: The current batch number within the
              curent epoch.
            logs: Additional metrics.
        """
        return

    def on_batch_end(self, trial, model, batch, logs=None):
        """A hook called at the end of every batch.

        # Arguments:
            trial: A `Trial` instance.
            model: A Keras `Model`.
            batch: The current batch number within the
              curent epoch.
            logs: Additional metrics.
        """
        return

    def on_epoch_end(self, trial, model, epoch, logs=None):
        # Intermediate results are not passed to the Oracle, and
        # checkpointing is handled via a `ModelCheckpoint` callback.
        return

    def get_best_models(self, num_models=1):  # pylint: disable=useless-super-delegation
        """Returns the best model(s), as determined by the tuner"s objective.

        The models are loaded with the weights corresponding to
        their best checkpoint (at the end of the best epoch of best trial).

        This method is only a convenience shortcut. For best performance, It is
        recommended to retrain your Model on the full dataset using the best
        hyperparameters found during `search`.

        Args:
            num_models (int, optional): Number of best models to return.
                Models will be returned in sorted order. Defaults to 1.

        Returns:
            List of trained model instances.
        """
        # Method only exists in this class for the docstring override.
        return super().get_best_models(num_models)

    def _deepcopy_callbacks(self, callbacks):
        try:
            callbacks = copy.deepcopy(callbacks)
        except Exception as e:
            raise ValueError(
                "All callbacks used during a search should be deep-copyable (since "
                + "they are reused across trials). It is not possible to do"
                + f"`copy.deepcopy({callbacks:s})`"
            ) from e
        return callbacks


class CustomTrial(trial_module.Trial):
    # overrides load of Kerastuner
    # json.load(string) returns a dict
    # the implementation of load in kerastuner uses tf.io.GFile and returns a string,
    # which results in a type error
    @classmethod
    def load(cls, fname):
        with open(fname, encoding="utf8") as f:
            state_data = json.load(f)
        return cls.from_state(state_data)


class SearchResults:
    def __init__(self, model, search_name="*search*", objective=None):
        self.model = model
        self.project_name = search_name
        self.objective = objective or model.monitor or "val_loss"
        self.trials = []
        self.hyperparameters = []
        self.metrics = None

    @property
    def search_dir(self):
        return os.path.join(self.model.base_model_dir, self.project_name)

    def get_best_hyperparameters(self, num_hyperparameters=None, print="best"):
        """
        returns the best hp as HyperParameters object
        """
        self.reload()
        best_trials = self.get_best_trials(num_trials=num_hyperparameters)
        if print:
            if print == "best":
                self.print_hyperparameters(best_trials[0])
            else:
                for t in best_trials:
                    self.print_hyperparameters(t)
        best_hp = [i.hyperparameters for i in best_trials]
        return best_hp

    def reload(self):
        """Populate `self.trials` and `self.oracle` state."""
        search_dirs = glob.glob(self.search_dir)
        search_dirs = [s for s in search_dirs if os.path.isdir(s)]
        if not search_dirs:
            raise FileNotFoundError(
                f"No search directory found that matches pattern {self.search_dir:s}"
            )
        for search_dir in search_dirs:
            fname = os.path.join(search_dir, "oracle.json")
            if os.path.exists(fname):
                # This was a proper hyperparameter search
                with open(fname, encoding="utf8") as f:
                    state = json.load(f)
                # Read hyperparameters
                # TODO: what happens if different hyperparameters each search?
                self.hyperparameters.append(
                    hp_module.HyperParameters.from_config(state["hyperparameters"])
                )
                # Parse trials
                trial_folders = glob.glob(
                    os.path.join(search_dir, "trial_*", "trial.json")
                )
                new_trials = [
                    CustomTrial.load(trial)
                    for trial in trial_folders
                    if os.path.exists(trial)
                ]
                self.trials.extend(new_trials)
                # Set consistent metrics
                # self.metrics = metrics_tracking.MetricsTracker.from_config(
                #      self.trials[0]["metrics"])
                if new_trials:
                    if self.metrics is None:
                        self.metrics = self.trials[0].metrics
                    else:
                        assert (
                            self.metrics == self.trials[0].metrics
                        ), f"Search {search_dir:s} did not use consistent metrics."

            else:
                # This was no real hyperparameter search
                # TODO: also load manual tuning results
                #       (as custom trials? parse cids jsons?)
                print(f"Search directory {fname:s} empty.")

    def get_best_trials(self, num_trials=None):
        # Read objective and direaction
        if isinstance(self.objective, Objective):
            objective = self.objective.name
            direction = self.objective.direction
        else:
            objective = self.objective
            direction = infer_metric_direction(objective)
        # Existence
        if not self.metrics.exists(objective):
            return []
        # Extract values
        trials = []
        for x in self.trials:
            if x.score is not None:
                trials.append(x)
        if not trials:
            return []
        sorted_trials = sorted(
            trials,
            key=lambda x: x.metrics.get_best_value(objective),
            reverse=direction == "max",
        )
        if num_trials is None:
            num_trials = len(sorted_trials)
        return sorted_trials[:num_trials]

    def print_hyperparameters(self, trial):
        print("----Best Trials:-------")
        print(f"Trial id: {trial.get_state()['trial_id']}:")
        print(f"Hyperparameters {trial.get_state()['hyperparameters']['values']}")
        print(f"Score: {trial.get_state()['score']}")

    def plot_hyperparameter_search(self):
        self.reload()
        # Find all trials
        best_trials = self.get_best_trials(num_trials=None)
        # Extract hyperparameters
        hyperparameters = [t.hyperparameters.values for t in best_trials]
        # Append score
        for i, t in enumerate(best_trials):
            hyperparameters[i][self.objective] = t.score
        hyperparameter_df = pd.DataFrame.from_records(hyperparameters)
        # Filter out tuner specific keys
        vars = [k for k in hyperparameter_df.keys() if "tuner/" not in k]
        # Plot
        # TODO: matplotlib for continuous coloring instead of histogram/kde
        # TODO: mask unknown areas
        g = sns.PairGrid(
            hyperparameter_df, vars=vars, diag_sharey=False, hue=self.objective
        )
        g.map_upper(sns.scatterplot)
        g.map_diag(
            sns.lineplot, data=hyperparameter_df, ci="sd", y=self.objective, hue=None
        )
        # g.map_lower(sns.kdeplot, legend=False)
        g.add_legend()
        plt.draw()
        search_coverage_file = os.path.join(
            self.model.base_model_dir, "search_coverage.png"
        )
        plt.savefig(search_coverage_file)
        os.chmod(search_coverage_file, 0o666)
