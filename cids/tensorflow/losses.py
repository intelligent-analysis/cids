# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Tensorflow loss implementations for CIDS. Part of the CIDS toolbox.

    Classes:
        ScaledLoss: scales a given loss by a fixed factor

"""
import tensorflow as tf
from tensorflow.keras.losses import Loss
from tensorflow.python.keras.losses import LossFunctionWrapper


class ScaledLoss(LossFunctionWrapper):
    def __init__(
        self,
        base_loss,
        factor,
        reduction=tf.keras.losses.Reduction.AUTO,
        name=None,
        **kwargs,
    ):
        """Loss that scales the given loss by a fixed factor.

        Args:
            base_loss (Loss): A tensorflow loss object
            factor (float): A fixed factor
            reduction (Reduction, optional): A reduction strategy to use. Defaults
                to tf.keras.losses.Reduction.AUTO.
            name (str, optional): A name for the scaled loss. Defaults to None.

        Returns:
            Loss: A tensorflow loss object
        """
        # Get function
        if isinstance(base_loss, Loss):
            fn = base_loss.fn
            fn_kwargs = base_loss._fn_kwargs
            fn_name = base_loss.name
        elif callable(base_loss):
            fn = base_loss
            fn_kwargs = {}
            fn_name = "loss"
        if name is None:
            name = f"scaled_{fn_name}"
        # Get kwargs and settings
        fn_kwargs.update(kwargs)
        # Apply scale
        self.factor = factor

        def scaled_fn(y_true, y_pred, **kwargs):
            return factor * fn(y_true, y_pred, **kwargs)

        super().__init__(scaled_fn, reduction=reduction, name=name, **fn_kwargs)

    # def call(self, y_true, y_pred):
    #     return self.factor * super().call(y_true, y_pred)
