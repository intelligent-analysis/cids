# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Tensorflow/Keras model for CIDS. Part of the CIDS toolbox.
"""
import tensorflow as tf
from tensorflow.python.keras.engine import data_adapter

# NOTE: Do not assign core model components to variables inside the compiled
# @tf.function train and test steps! The assignment will be hard coded at compile time
# and the metrics will not update properly.


def make_parallel_steps(self):
    def parallel_train_step(data):
        # Get data
        data = data_adapter.expand_1d(data)
        x, y, sample_weight = data_adapter.unpack_x_y_sample_weight(data)
        with tf.GradientTape() as forward_tape, tf.GradientTape() as parallel_tape:
            # Forward_model prediction
            y_ = self.core_model["forward"](x, training=True)
            y1 = y[self.output_format[0]]
            # TODO: improve assignment
            # Parallel model prediction
            yp_ = self.core_model["forward_parallel"](x, training=True)
            y2 = y[self.output_format[1]]
            # TODO: improve assignment
            # Loss computation
            l_forward = self.core_model["forward"].compiled_loss(
                y1,
                y_,
                sample_weight,
                regularization_losses=self.core_model["forward"].losses,
            )
            l_parallel = self.core_model["forward_parallel"].compiled_loss(
                y2,
                yp_,
                sample_weight,
                regularization_losses=self.core_model["forward_parallel"].losses,
            )
            total_loss = l_forward + l_parallel
        gradients_forward = forward_tape.gradient(
            total_loss, self.core_model["forward"].trainable_variables
        )
        gradients_parallel = parallel_tape.gradient(
            total_loss, self.core_model["forward_parallel"].trainable_variables
        )
        # Apply gradients
        self.core_model["forward"].optimizer.apply_gradients(
            zip(gradients_forward, self.core_model["forward"].trainable_variables)
        )
        self.core_model["forward_parallel"].optimizer.apply_gradients(
            zip(
                gradients_parallel,
                self.core_model["forward_parallel"].trainable_variables,
            )
        )
        # Update metrics
        self.train_loss_tracker.update_state(total_loss)
        self.core_model["forward"].compiled_metrics.update_state(y1, y_, sample_weight)
        self.core_model["forward_parallel"].compiled_metrics.update_state(
            y2, yp_, sample_weight
        )
        # Merge metrics
        metrics = {"loss": self.train_loss_tracker.result()}
        metrics.update(
            {
                m.name + "_forward": m.result()
                for m in self.core_model["forward"].metrics
            }
        )
        metrics.update(
            {
                m.name + "_parallel": m.result()
                for m in self.core_model["forward_parallel"].metrics
            }
        )
        return metrics

    def parallel_test_step(data):
        # Get data
        data = data_adapter.expand_1d(data)
        x, y, sample_weight = data_adapter.unpack_x_y_sample_weight(data)
        # Forward_model prediction
        y_ = self.core_model["forward"](x, training=False)
        y1 = y[self.output_format[0]]
        # Parallel model prediction
        yp_ = self.core_model["forward_parallel"](x, training=False)
        y2 = y[self.output_format[1]]
        # Loss computation
        l_forward = self.core_model["forward"].compiled_loss(
            y1,
            y_,
            sample_weight,
            regularization_losses=self.core_model["forward"].losses,
        )
        l_parallel = self.core_model["forward_parallel"].compiled_loss(
            y2,
            yp_,
            sample_weight,
            regularization_losses=self.core_model["forward_parallel"].losses,
        )
        total_loss = l_forward + l_parallel
        # Update metrics
        self.validation_loss_tracker.update_state(total_loss)
        self.core_model["forward"].compiled_metrics.update_state(y1, y_, sample_weight)
        self.core_model["forward_parallel"].compiled_metrics.update_state(
            y2, yp_, sample_weight
        )
        # Merge metrics
        metrics = {"loss": self.validation_loss_tracker.result()}
        metrics.update(
            {
                m.name + "_forward": m.result()
                for m in self.core_model["forward"].metrics
            }
        )
        metrics.update(
            {
                m.name + "_parallel": m.result()
                for m in self.core_model["forward_parallel"].metrics
            }
        )
        return metrics

    return parallel_train_step, parallel_test_step


def make_hessian_steps(self):
    def hessian_train_step(data):
        # Get data
        data = data_adapter.expand_1d(data)
        x, y, sample_weight = data_adapter.unpack_x_y_sample_weight(data)
        # Model logic
        with tf.GradientTape():
            # Select models
            main_model = self.core_model
            # Forward model prediction
            y_ = main_model(x, training=True)
            # Loss forward model
            l_forward = main_model.compiled_loss(
                y,
                y_,
                sample_weight,
                regularization_losses=main_model.losses,
            )
        # Compute gradients
        # clipnorms working????
        gradients, hessians = main_model.optimizer.get_gradients_hessian(
            l_forward, main_model.trainable_variables
        )
        # Apply gradients
        main_model.optimizer.apply_gradients_hessian(
            zip(gradients, hessians, main_model.trainable_variables)
        )
        # Update metrics
        main_model.compiled_metrics.update_state(y, y_, sample_weight)
        # Merge metrics
        metrics = {m.name: m.result() for m in main_model.metrics}
        return metrics

    # TODO: implement hessian test step

    return hessian_train_step
