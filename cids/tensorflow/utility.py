# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Utility functions for CIDS with Tensorflow. Part of the CIDS toolbox.

    Functions:
        get_available_cpus:                 list available CPUs on the local machine
        get_available_gpus:                 list available GPUs on the local machine
        disable_tensorflow_memory_greed:    disables tensorflow preallocating all memory
"""
import tensorflow as tf
from tensorflow.python.client import device_lib


def get_available_cpus():
    """Returns a list of available CPUs"""
    devices = device_lib.list_local_devices()
    return [d.name for d in devices if d.device_type == "CPU"]


def get_available_gpus():
    """Returns a list of available GPUs"""
    devices = device_lib.list_local_devices()
    return [d.name for d in devices if d.device_type != "CPU"]


def disable_tensorflow_memory_greed():
    physical_devices = tf.config.list_physical_devices("GPU")
    for device in physical_devices:
        try:
            tf.config.experimental.set_memory_growth(device, True)
        except Exception:
            # Invalid device or cannot modify virtual devices once initialized.
            pass
