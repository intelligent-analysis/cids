"""CIDS computational intelligence library"""
# TODO: Multiple threads?
# TODO: DataReader.image_from_paths
# TODO: use read_batch_features (convenience function) implemented very similar to the functions in DataReader
# TODO: sharding?
# TODO: Combine functions further
from copy import deepcopy
from warnings import warn

import numpy as np
import tensorflow as tf

import cids
from cids.legacy.decorators import *
from cids.tensorflow.utility import read_axes


class DataReader:
    def __init__(
        self,
        data_shape,
        data_format,
        batch_size,
        input_features=None,
        output_features=None,
        src_type="file",
        sample_dtype=tf.float64,
        cast_dtype=tf.float64,
        slice_tensors=False,
        file_type="tfr",
        tfr_features=None,
        csv_skip=0,
        csv_comment="#",
        num_epochs=None,
        shuffle=True,
        prefetch=False,
        buffer_size=10000,
        chunk_size=None,
        iter_probability=0.0,
        seed=None,
    ):
        """A class that loads and batches data into tensorflow tensors.

        DataReader uses the new tensorflow dataset API to load files into
        tf.Dataset objects. The class currently supports reading of csv and
        tfrecord files. When processing tfrecords and no tfr_features is present
        it defaults to the cids default tfrecord format (see below).

        Args:
            data_shape:     Shape of the input data
            data_format:    Data format used ("NF", NSF", or "NCHW")
                                 N:             batch axis,
                                 S:             sequence axis
                                 H/W/D/X/Y/Z:   spatial axes
                                 F/C:           feature or channel axis
            batch_size:     The desired batch size (accepts dicts)
            src_type:       The source of data ("file" or "placeholder")
            sample_dtype:   Data type in the data file
            cast_dtype:     Data type to cast to
            slice_tensors:  Boolean: Slice dataset into multiple samples?
            tfr_features:   Dictionary of protobuf keys and tf.Features()
                            (Defaults to cids default format) for tfrecords
            csv_skip:       Number of lines to skip for csv
            csv_comment:    Lines to skip starting with this string for csv
            num_epochs:     Number of epochs to repeat data (None for infinite)
            shuffle:        Boolean: Shuffle dataset using buffer_size?
            prefetch:       Boolean: Preload dataset of buffer_size?
            buffer_size:    Number of samples to collect for shuffling
            chunk_size:     Size of chunks along sequence axis (None: disabled)
            seed:           Set a seed for shuffling. (Default None: disabled)
        """
        if tfr_features is None:
            # Use default cids tfrecord format
            self.tfr_features = {
                "data": tf.FixedLenFeature((), tf.string, default_value=""),
                "data_shape": tf.FixedLenFeature((len(data_shape),), tf.int64),
            }
        else:
            self.tfr_features = tfr_features
        self.input_features = input_features
        self.output_features = output_features
        self.data_shape = data_shape
        if isinstance(batch_size, int):
            self.batch_size = batch_size
            self.valid_batch_size = batch_size
            self.test_batch_size = batch_size
        elif hasattr(batch_size, "keys"):
            self.batch_size = batch_size["train"]
            self.valid_batch_size = batch_size["valid"]
            try:
                self.test_batch_size = batch_size["test"]
            except KeyError:
                self.test_batch_size = batch_size["valid"]
        else:
            raise ValueError("Variable batch_size must be type int or dict.")
        self.src_type = src_type
        self.data_format = data_format
        axes = read_axes(data_format)
        self.batch_axis = axes[0]
        self.feature_axis = axes[1]
        self.sequence_axis = axes[2]
        self.spatial_axes = axes[3]
        self.iter_axis = axes[4]
        self.sample_dtype = sample_dtype
        self.cast_dtype = cast_dtype
        self.slice_tensors = slice_tensors
        self.skip = csv_skip
        self.comment = csv_comment
        self.file_type = file_type
        self.num_epochs = num_epochs
        self.shuffle = shuffle
        self.seed = seed
        self.prefetch = prefetch
        self.buffer_size = buffer_size
        self.chunk_size = chunk_size
        self.iter_probability = iter_probability
        self.VERBOSITY = 2

    def _parse_tfrecord(self, tfr):
        parsed = tf.parse_single_example(tfr, self.tfr_features)
        data = parsed["data"]
        dynamic_data_shape = parsed["data_shape"]
        # Decode and cast
        data = tf.decode_raw(data, self.sample_dtype)
        data = tf.cast(data, self.cast_dtype)
        dynamic_data_shape = tf.cast(dynamic_data_shape, tf.int32)
        # Restore data
        data = tf.reshape(data, dynamic_data_shape)
        data = tf.cond(
            tf.equal(dynamic_data_shape[self.batch_axis], 1),
            lambda: tf.squeeze(data, axis=0),
            lambda: data,
        )
        # Tile parameter data
        param_keys = sorted(k for k in self.tfr_features.keys() if "data" not in k)
        if self.VERBOSITY > 2:
            if param_keys:
                print(
                    "  Expanding data with parameter features ",
                    "(sorted: alphanumerically):",
                )
                print("    ", param_keys)
        param_data = [parsed[k] for k in param_keys]
        # Cast
        param_data = [tf.cast(d, self.cast_dtype) for d in param_data]
        # Expand and tile
        param_data = [tf.squeeze(d) for d in param_data]
        for p in param_data:
            assert (
                len(p.shape) == 0
            ), 'All parameters without "data" in their key must be scalars.'
        active_feature_axis = self.feature_axis
        active_dims = list(range(len(self.data_shape)))
        if not self.slice_tensors:
            active_dims.remove(self.batch_axis)
        for _ in active_dims:
            param_data = [tf.expand_dims(d, 0) for d in param_data]
        # Tile (replace feature size with 1)
        tile_shape = tf.gather(dynamic_data_shape, active_dims)
        tile_shape = tf.unstack(tile_shape)
        tile_shape[active_feature_axis] = tf.ones([], dtype=tf.int32)
        tile_shape = tf.stack(tile_shape)
        param_data = [tf.tile(d, tile_shape) for d in param_data]
        # Concat
        data = tf.concat([data] + param_data, active_feature_axis)
        return data

    def _parse_csv_line(self, line):
        if self.cast_dtype == tf.float32 or self.cast_dtype == tf.float64:
            defaults = [[0.0]] * self.data_shape[-1]
        elif self.cast_dtype == tf.int64 or self.cast_dtype == tf.int32:
            defaults = [[0]] * self.data_shape[-1]
        else:
            raise ValueError("Unknown self.cast_dtype = ", self.cast_dtype)
        parsed = tf.decode_csv(line, record_defaults=defaults)
        parsed = tf.stack(parsed, axis=0)
        return parsed

    def _parse_image_from_tfr(self, example):
        # TODO: features in class
        features = tf.parse_single_example(
            example,
            features={
                "image_raw": tf.FixedLenFeature([], tf.string),
                "label": tf.FixedLenFeature([], tf.int64),
            },
        )
        label = tf.cast(features["label"], tf.int32)
        image = tf.image.decode_image(features["image_raw"])
        return image, label

    def _parse_image_and_target_from_tfr(self, example):
        # TODO: melt with other functions?!
        features = tf.parse_single_example(
            example,
            features={
                "image_raw": tf.FixedLenFeature([], tf.string),
                "target_raw": tf.FixedLenFeature([], tf.string),
                "n_target": tf.FixedLenFeature([], tf.int64),
            },
        )
        image = tf.image.decode_image(features["image_raw"])
        target = tf.decode_raw(features["target_raw"], self.sample_dtype)
        target = tf.cast(target, self.cast_dtype)
        n_target = tf.cast(features["n_target"], tf.int32)
        # TODO: reshape!!!
        # target = tf.reshape(target, [n_target, -1])
        return image, target

    def _parse_h5_from_tfr(self, example):
        # TODO: melt with other functions?!
        features = tf.parse_single_example(example, features=self.tfr_features)
        # FIXME: Make flexible
        num_features = tf.cast(features["num_features"], tf.int32)
        velocity = features["velocity"]
        weight = features["weight"]
        height = features["height"]
        length_thigh = features["length_thigh"]
        length_shank = features["length_shank"]
        length_foot = features["length_foot"]
        data = tf.decode_raw(features["data"], self.sample_dtype)
        data = tf.cast(data, self.cast_dtype)
        data = tf.reshape(data, [-1, num_features])
        # Concat with context
        context = tf.stack(
            [velocity, weight, height, length_thigh, length_shank, length_foot]
        )
        context_data = tf.expand_dims(context, axis=0)
        context_tiled = tf.tile(context_data, [tf.shape(data)[0], 1])
        data = tf.concat([data, context_tiled], -1)
        return data

    def generate_batch_dataset(self, data, mode="train"):
        """Read data as specified by src_type during object initialisation.

        Args:
            data:       Data source tensor
            mode:       Which mode to use (defines batch sized used)

        Returns:
            dataset:    A tensorflow Dataset (for iterator)

        """
        if self.src_type[:5].lower() == "file":
            dataset = self.generate_dataset_from_files(data)
        elif self.src_type[:5].lower() == "place":
            dataset = self.generate_dataset_from_placeholders(data)
        else:
            raise ValueError(
                'Unknown src_type: must be either "file" or "placeholder".'
            )
        # Define final shape of sample
        sample_shape = list(deepcopy(self.data_shape))
        del sample_shape[self.batch_axis]
        # Chunk sequence dataset
        if self.sequence_axis is not None and self.chunk_size:
            assert self.data_shape[self.sequence_axis] is not None, (
                "Data shape along sequence axis must be defined when "
                "chunk_size is set."
            )
            sample_sequence_axis = self.sequence_axis - 1
            sequence_length = sample_shape[sample_sequence_axis]
            # Shuffle dataset before synthesizing more samples
            if self.shuffle:
                dataset = dataset.shuffle(buffer_size=self.buffer_size, seed=self.seed)
            # Insure that all sequences are padded with zeros to max length
            dataset = dataset.padded_batch(batch_size=1, padded_shapes=sample_shape)
            dataset = dataset.map(tf.squeeze)  # remove added axis
            # Create multiple sequence chunks from chunkable sequences
            if sequence_length > self.chunk_size:
                sample_shape[sample_sequence_axis] = self.chunk_size
                num_chunks = sequence_length - self.chunk_size
                chunk_idx = [
                    list(range(i, i + self.chunk_size)) for i in range(num_chunks)
                ]
                dataset = dataset.flat_map(
                    lambda sample: tf.data.Dataset.from_tensor_slices(
                        [
                            tf.gather(sample, c, axis=sample_sequence_axis)
                            for c in chunk_idx
                        ]
                    )
                )
                # remove blank samples
                dataset = dataset.filter(
                    lambda sample: tf.reduce_any(
                        tf.cast(tf.sign(tf.abs(sample)), dtype=tf.bool)
                    )
                )
        # Select random iteration from iteration axis
        if self.iter_axis is not None:
            sample_iter_axis = self.iter_axis - 1
            if self.iter_probability > 0.0:
                # Insure that all sequences are padded with zeros to max length
                dataset = dataset.padded_batch(batch_size=1, padded_shapes=sample_shape)
                dataset = dataset.map(tf.squeeze)  # remove added axis
                # Take random iteration sample (index > 0)
                dataset = dataset.map(
                    self._extract_random_iterations,
                    num_parallel_calls=tf.data.experimental.AUTOTUNE,
                )
                del sample_shape[sample_iter_axis]
            else:
                # Take only converged sample (index 0)
                dataset = dataset.map(
                    lambda sample: tf.squeeze(
                        tf.gather(sample, [0], axis=sample_iter_axis),
                        axis=sample_iter_axis,
                    )
                )
                del sample_shape[sample_iter_axis]
        # Shuffle dataset after synthesizing samples
        if self.shuffle:
            dataset = dataset.shuffle(buffer_size=self.buffer_size, seed=self.seed)
        # Repeat dataset
        if self.num_epochs is not None:
            dataset = dataset.repeat(self.num_epochs)
        elif self.num_epochs != 0:
            dataset = dataset.repeat()
        # Create batches
        if mode == "valid":
            batch_size = self.valid_batch_size
        else:
            batch_size = self.batch_size
        dataset = dataset.padded_batch(
            batch_size=batch_size, padded_shapes=sample_shape
        )
        if self.input_features is not None:
            dataset = dataset.map(
                lambda batch: (
                    tf.gather(batch, self.input_features, axis=self.feature_axis),
                    tf.gather(batch, self.output_features, axis=self.feature_axis),
                )
            )
        # Prefetch
        if self.prefetch:
            dataset = dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
        return dataset

    def _get_random_iter(self, frame):
        # TODO: hard coded iter axis
        # Get all axes but sequence and iteration
        axes = tuple(i for i, d in enumerate(self.data_format[2:]) if d not in "I")
        # Find mask of iterations with non-zero features for all frames
        iter_mask = tf.sign(tf.reduce_max(tf.abs(frame), axis=axes))
        # Count numbers of non-zero iterations for each frame
        iter_length = tf.reduce_sum(iter_mask)
        iter_length = tf.cast(iter_length, tf.int32)
        iter_length = tf.maximum(iter_length, 1)
        # Which iteration to choose
        iter_index = tf.random.uniform([], 0, iter_length, dtype=tf.int32)
        iter_index *= tf.cast(
            tf.keras.backend.random_binomial([], self.iter_probability), dtype=tf.int32
        )
        return frame[..., iter_index, :]

    def _extract_random_iterations(self, sample):
        # Get selected iteration for each frame in sequence
        sample_sequence_axis = self.sequence_axis - 1
        frames = tf.unstack(
            sample, num=self.data_shape[self.sequence_axis], axis=sample_sequence_axis
        )
        frames = [self._get_random_iter(frame) for frame in frames]
        sample = tf.stack(frames, axis=sample_sequence_axis)
        return sample

    @variable_scoped_method
    def generate_test_dataset(self, data, batch_size=None):
        """Create a dataset of tensors from various data sources.

        Args:
            data:          List of data
        Returns:
            dataset:        A dataset of tensors
        """
        if batch_size is None:
            batch_size = self.test_batch_size
        if self.src_type[:5].lower() == "file":
            dataset = self.generate_dataset_from_files(data)
        elif self.src_type[:5].lower() == "place":
            dataset = self.generate_dataset_from_placeholders(data)
        else:
            raise ValueError(
                'Unknown src_type: must be either "file" or "placeholder".'
            )
        # Define final shape of sample
        sample_shape = list(deepcopy(self.data_shape))
        del sample_shape[self.batch_axis]
        # Select converged sample if iteration axis is present
        if self.iter_axis is not None:
            sample_iter_axis = self.iter_axis - 1
            # Take only converged sample (index 0)
            dataset = dataset.map(
                lambda sample: tf.squeeze(
                    tf.gather(sample, [0], axis=sample_iter_axis), axis=sample_iter_axis
                )
            )
            del sample_shape[sample_iter_axis]
        # Create batch of the entire dataset
        dataset = dataset.padded_batch(
            batch_size=batch_size, padded_shapes=sample_shape
        )
        if self.input_features is not None:
            dataset = dataset.map(
                lambda batch: (
                    tf.gather(batch, self.input_features, axis=self.feature_axis),
                    tf.gather(batch, self.output_features, axis=self.feature_axis),
                )
            )
        return dataset

    @variable_scoped_method
    def generate_dataset_from_files(self, files):
        """Create a dataset of tensors from files.

        Args:
            files:          List of filenames
        Returns:
            dataset:    A dataset of tensors
        """
        # Parse file
        if self.file_type[:3] == "tfr":
            dataset = tf.data.TFRecordDataset(files)
            dataset = dataset.map(self._parse_tfrecord)
            if self.slice_tensors:
                dataset = dataset.flat_map(
                    lambda d: tf.data.Dataset.from_tensor_slices(d)
                )
        elif self.file_type[:3] == "csv":
            dataset = tf.data.Dataset.from_tensor_slices(files)
            # Skip line and filter out comments
            dataset = dataset.flat_map(
                lambda filename: (
                    tf.data.TextLineDataset(filename)
                    .skip(self.skip)
                    .filter(
                        lambda line: tf.not_equal(tf.substr(line, 0, 1), self.comment)
                    )
                )
            )
            dataset = dataset.map(self._parse_csv_line)
            if not self.slice_tensors:
                # Stack sequence
                if self.sequence_axis:
                    dataset = dataset.batch(
                        batch_size=self.data_shape[self.sequence_axis]
                    )
        else:
            raise ValueError(f'file_type "{self.file_type:s}" unknown.')
        return dataset

    @variable_scoped_method
    def generate_dataset_from_placeholders(self, placeholders):
        """Create a dataset of tensors from placeholders.

        Args:
            placeholders:   A tensorflow placeholder for feeding
        Returns:
            dataset:        A dataset of tensors
        """
        dataset = tf.data.Dataset.from_tensors(placeholders)
        if self.slice_tensors:
            dataset = dataset.flat_map(lambda d: tf.data.Dataset.from_tensor_slices(d))
        dataset = dataset.map(lambda sample: tf.cast(sample, self.cast_dtype))
        return dataset

    def read_tfrecords(self, tfr_files):
        """Reads a single tfrecord file to check its content."""
        if isinstance(tfr_files, str):
            num_samples = 1
        else:
            num_samples = len(tfr_files)
        dataset = self.generate_test_dataset(tfr_files, batch_size=num_samples)
        iterator = dataset.make_one_shot_iterator()
        with tf.Session() as sess:
            samples = sess.run(iterator.get_next())
        if len(tfr_files) == 1:
            samples = np.squeeze(samples, axis=self.batch_axis)
        return samples
