"""CIDS computational intelligence library"""
# TODO: Constructor
# TODO: Combine functions
# TODO: Rename functions appropriatly
import os

import numpy as np
import tensorflow as tf


# from natsort import natsorted

natsorted = sorted

from cids.legacy.decorators import variable_scoped_method


class DataConverter:
    def __init__(self):
        """A class that loads and batches data into tensorflow tensors.

        Args:


        """

    # @staticmethod
    #
    # def _bytes_feature(value):
    #     return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))
    #
    # @staticmethod
    #
    # def _int64_feature(value):
    #     return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))
    #
    # @staticmethod
    #
    # def _int64_features(values):
    #     return tf.train.Feature(int64_list=tf.train.Int64List(value=values))

    @staticmethod
    def convert_ndarray_to_proto(
        ndarray, data_format="NSF", dtype=np.float64, param_dict=None
    ):
        """Creates a Proto from ndarray"""
        assert np.all(np.isfinite(ndarray))
        assert ndarray.dtype == dtype
        # Ensure a batch axis is present (default to index 0)
        if not "N" in data_format:
            data_format = "N" + data_format
        # Ensure shapes match length of data_format
        if len(ndarray.shape) < len(data_format):
            ndarray = np.expand_dims(ndarray, 0)
        assert len(ndarray.shape) == len(
            data_format
        ), f"Not enough dimensions in ndarray for dataformat {data_format:s}"
        # Init example
        ex = tf.train.Example()
        # Fill in data features
        ndarray = ndarray.astype(dtype)
        ex.features.feature["data"].bytes_list.value.append(ndarray.tostring(order="C"))
        ex.features.feature["data_format"].bytes_list.value.append(data_format.encode())
        ex.features.feature["data_shape"].int64_list.value.extend(  # for lists
            ndarray.shape
        )
        # Append parameters
        # TODO: Variable parameter type (now only float)
        if param_dict:
            for k, v in param_dict.items():
                if isinstance(v, (list, tuple)):
                    # Append entire iterable
                    ex.features.feature[k].float_list.value.extend(v)
                elif isinstance(v, (float, int, np.ndarray, np.number)):
                    try:
                        # append single entry
                        ex.features.feature[k].float_list.value.append(np.float32(v))
                    except ValueError:
                        # Try to convert to list
                        ex.features.feature[k].float_list.value.extend(list(v))
                else:
                    raise ValueError(
                        "Unknown type in param dict: " + k + ": " + str(type(v))
                    )
        return ex

    def read_images_and_targets_from_direc(self, folder_directory):
        """gets all image and target file directories at folder_directory
        Args:
            folder_directory:      path to project files.
                            structure should be: [directory] -> [images] / [targets]
                            -> image files or target csv files

        Returns:
            image files:    list of all image files of project
            target:         list of all target csv files (same name as image files)
        """
        image_files = []
        target_files = []
        images = [image for image in os.listdir(folder_directory + "/images")]
        targets = [i.replace("jpg", "csv") for i in images]
        targets = [i.replace("png", "csv") for i in targets]
        # TODO: assert
        for image in images:
            image_files.append(
                os.path.realpath(os.path.join(folder_directory + "/images", image))
            )
        for target in targets:
            target_files.append(
                os.path.realpath(os.path.join(folder_directory + "/targets", target))
            )
        assert len(target_files) == len(
            image_files
        ), "number of target files not the same as number of image files!!"
        return image_files, target_files

    def get_hdf5_files(self, directory):
        filenames = []
        files = [filename for filename in os.listdir(directory)]
        for file in files:
            filenames.append(os.path.realpath(os.path.join(directory, file)))
        return filenames

    def get_labels_and_imagefiles(self, directory):
        """gets all labels and imagefiles at directory
        Args:
            directory:      path to project image files.
                            structure should be: directory -> label_i -> imagefile_ij(with label i)
                            i=1...n , n = number of labels,
                            j=1...m , m = number of images with label i

        Returns:
            labels:         natsorted list of all label names according to directory
            imagefiles:     list of all image files of project
        """
        folders = [
            folder for folder in os.listdir(directory)
        ]  # if os.path.isdir(os.path.join(dir, folder))
        imagefiles = []
        for folder in folders:
            folderdir = os.path.join(directory, folder)
            files = [filename for filename in os.listdir(folderdir)]
            for file in files:
                imagefiles.append(os.path.realpath(os.path.join(folderdir, file)))
        labels = natsorted(folders)
        return imagefiles, labels

    def _get_label_number(self, path, label_list):
        """gets number of label
        Args:
            path:       path of image file
            label_list: complete natsorted list of all labels

        Returns:
            label:    int value for label, retransfereable to label name
        """
        part_path, _ = os.path.split(path)
        _, label_name = os.path.split(part_path)
        # search list for label_name return index
        label = label_list.index(label_name)
        return label

    def save_labels(self, sorted_label_list, save_to_path):
        # saves list of sorted labels in text file at given path
        with open(save_to_path, "w") as text_file:
            for label in sorted_label_list:
                text_file.write("%s\n" % label)

    @staticmethod
    def _create_example_images(image_raw, label):
        return tf.train.Example(
            features=tf.train.Features(
                feature={
                    "image_raw": tf.train.Feature(
                        bytes_list=tf.train.BytesList(value=[image_raw])
                    ),
                    "label": tf.train.Feature(
                        int64_list=tf.train.Int64List(value=[label])
                    ),
                }
            )
        )

    @staticmethod
    def _create_image_target_examples(image_raw, target_raw, n_target):
        return tf.train.Example(
            features=tf.train.Features(
                feature={
                    "image_raw": tf.train.Feature(
                        bytes_list=tf.train.BytesList(value=[image_raw])
                    ),
                    "target_raw": tf.train.Feature(
                        bytes_list=tf.train.BytesList(value=[target_raw])
                    ),
                    "n_target": tf.train.Feature(
                        int64_list=tf.train.Int64List(value=[n_target])
                    ),
                }
            )
        )

    @staticmethod
    def _create_h5_examples(data, context):
        features = dict()
        for key, val in context.items():
            if val.dtype == "int64":
                features[key] = tf.train.Feature(
                    int64_list=tf.train.Int64List(value=val.as_matrix())
                )
            elif val.dtype == "float32" or val.dtype == "float64":
                features[key] = tf.train.Feature(
                    float_list=tf.train.FloatList(value=val.as_matrix())
                )
            else:
                raise ValueError("Unknown datatype: ", val, val.dtype)
        features["data"] = tf.train.Feature(
            bytes_list=tf.train.BytesList(value=[data.tostring()])
        )
        return tf.train.Example(features=tf.train.Features(feature=features))

    def write_tfrecords_image_file(self, image_filenames, label_list, outfile):
        """
        reads images from project at infile,
        writes tfrecord image file at outfile,
        saves natsorted list of labels at label_path
            Args:
                image_filenames:        filenames of all images of example
                label_list:             list of all labels
                outfile:                path of where to save tfrecords file with images and labels

        """
        writer = tf.python_io.TFRecordWriter(outfile)
        for file in image_filenames:
            raw_image = tf.gfile.FastGFile(file, "rb").read()
            label = self._get_label_number(file, label_list)
            example = self._create_example_images(raw_image, label)
            writer.write(example.SerializeToString())
        writer.close()

    def write_tfrecords_image_target_file(
        self, image_filenames, target_filenames, outfile
    ):
        writer = tf.python_io.TFRecordWriter(outfile)
        for img, trgt in zip(image_filenames, target_filenames):
            raw_image = tf.gfile.FastGFile(img, "rb").read()
            target = np.genfromtxt(
                trgt, dtype="float32", delimiter=";", skip_header=0, comments="#"
            )
            n_target = np.size(target, 0)
            raw_target = target.tostring()
            example = self._create_image_target_examples(
                raw_image, raw_target, n_target
            )
            writer.write(example.SerializeToString())
        writer.close()

    def convert_h5_file(self, h5_file, tfr_file, context):
        writer = tf.python_io.TFRecordWriter(tfr_file)
        data = self.h5_to_data_target_tensors(h5_file)
        num_features = np.size(data, axis=1)
        context["num_features"] = num_features
        example = self._create_h5_examples(data, context)
        writer.write(example.SerializeToString())
        writer.close()
        """for file in file_list:
            writer = tf.python_io.TFRecordWriter(outfile)
            raw_data, raw_target = self.h5_to_data_target_tensors(file, data_input)
            n_time = np.size(raw_data, 0)
            data = raw_data.tostring()
            target = raw_target.tostring()
            example = self._create_h5_examples(n_time, data, target)
            writer.write(example.SerializeToString())
        writer.close()"""

    def h5_to_data_target_tensors(self, h5file):
        # TODO: array -> tensor

        data = []
        force_plates = list(h5file["/Output/EnvironmentModel"].keys())
        # filter
        force_plates = [fp for fp in force_plates if "ForcePlate" in fp]

        for fp in force_plates:

            # print("Force plate: ", fp)

            # ##################### GRF, GRM & COP #########################################
            # TODO : check at every time step? differentiate fp1 / fp2 ?? treppe
            grf_base_path = "/Output/EnvironmentModel/{:s}/Force/Fout"
            cop_base_path = "/Output/EnvironmentModel/{:s}/CenterOfPressure/GlobalRef/COP_ball/Position"
            grm_base_path = "/Output/EnvironmentModel/{:s}/Moment/Mlocal"
            grf = np.array(h5file[grf_base_path.format(fp)])
            cop = np.array(h5file[cop_base_path.format(fp)])
            grm = np.array(h5file[grm_base_path.format(fp)])
            # TODO : assert
            # cop - grf - grm
            reaction = [cop, grf, grm]
            # print("reaction: ", np.shape(reaction))
            reaction = np.hstack(reaction)
            # print(np.shape(reaction))

            # ##################### JOINT MOMENTS #############################################
            # print(grf.shape)
            max_grf_idx = np.argmax(np.abs(grf[:, 2]))
            # print("max_grf_idx: ", max_grf_idx)
            left_abs_joint_moment = np.abs(
                h5file[
                    "/Output/JointAnglesAndMoments/Left/AnkleDorsiFlexion/M_Projected"
                ][max_grf_idx]
            )
            right_abs_joint_moment = np.abs(
                h5file[
                    "/Output/JointAnglesAndMoments/Right/AnkleDorsiFlexion/M_Projected"
                ][max_grf_idx]
            )
            # print("left_abs_joint_moment: ", left_abs_joint_moment)
            # print("right_abs_joint_moment: ", right_abs_joint_moment)
            if left_abs_joint_moment >= right_abs_joint_moment:
                # print("left foot")
                foot = "Left"
                short_foot = "L"
            else:
                # print("right foot")
                foot = "Right"
                short_foot = "R"

            # print(list(h5file["/Output/JointAnglesAndMoments/Left"].keys()))
            # print(list(h5file["/Output/JointAnglesAndMoments/Right"].keys()))
            # print(list(h5file["/Output/JointAnglesAndMoments/Left/KneeAdduction"].keys()))
            # print(list(h5file["/Output/JointAnglesAndMoments/Right/KneeAdduction"].keys()))
            knee_flex = np.array(
                h5file[
                    "/Output/JointAnglesAndMoments/{:s}/KneeFlexion/M_Projected".format(
                        foot
                    )
                ]
            )
            knee_add = np.array(
                h5file[
                    "/Output/JointAnglesAndMoments/{:s}/KneeAdduction/M_Projected".format(
                        foot
                    )
                ]
            )
            knee_rot = np.array(
                h5file[
                    "/Output/JointAnglesAndMoments/{:s}/KneeInternalRotation/M_Projected".format(
                        foot
                    )
                ]
            )

            hip_flex = np.array(
                h5file[
                    "/Output/JointAnglesAndMoments/{:s}/HipFlexion/M_Projected".format(
                        foot
                    )
                ]
            )
            hip_add = np.array(
                h5file[
                    "/Output/JointAnglesAndMoments/{:s}/HipAdduction/M_Projected".format(
                        foot
                    )
                ]
            )
            hip_rot = np.array(
                h5file[
                    "/Output/JointAnglesAndMoments/{:s}/HipInternalRotation/M_Projected".format(
                        foot
                    )
                ]
            )

            ankle_flex = np.array(
                h5file[
                    "/Output/JointAnglesAndMoments/{:s}/AnkleDorsiFlexion/M_Projected".format(
                        foot
                    )
                ]
            )
            ankle_add = np.array(
                h5file[
                    "/Output/JointAnglesAndMoments/{:s}/AnkleInversion/M_Projected".format(
                        foot
                    )
                ]
            )
            ankle_rot = np.array(
                h5file[
                    "/Output/JointAnglesAndMoments/{:s}/AnkleInternalRotation/M_Projected".format(
                        foot
                    )
                ]
            )

            # TODO: check joint_moments!
            joint_moments = np.transpose(
                [
                    hip_flex,
                    hip_add,
                    hip_rot,
                    knee_flex,
                    knee_add,
                    knee_rot,
                    ankle_flex,
                    ankle_add,
                    ankle_rot,
                ]
            )
            # print("joint_moments: ", np.shape(joint_moments))

            # ####################### MARKERS ##############################
            # thigh
            tro = np.array(
                h5file[
                    "/Output/OptKinModel/{:s}/Seg/Thigh/{:s}ThighSuperior/r".format(
                        foot, short_foot
                    )
                ]
            )
            knem_t = np.array(
                h5file[
                    "/Output/OptKinModel/{:s}/Seg/Thigh/{:s}KneeMedial/r".format(
                        foot, short_foot
                    )
                ]
            )
            kne_t = np.array(
                h5file[
                    "/Output/OptKinModel/{:s}/Seg/Thigh/{:s}KneeLateral/r".format(
                        foot, short_foot
                    )
                ]
            )

            # shank
            knem_s = np.array(
                h5file[
                    "/Output/OptKinModel/{:s}/Seg/Shank/{:s}KneeMedial/r".format(
                        foot, short_foot
                    )
                ]
            )
            kne_s = np.array(
                h5file[
                    "/Output/OptKinModel/{:s}/Seg/Shank/{:s}KneeLateral/r".format(
                        foot, short_foot
                    )
                ]
            )
            tib = np.array(
                h5file[
                    "/Output/OptKinModel/{:s}/Seg/Shank/{:s}TUB/r".format(
                        foot, short_foot
                    )
                ]
            )  # ???
            ank_s = np.array(
                h5file[
                    "/Output/OptKinModel/{:s}/Seg/Shank/{:s}AnkleLateral/r".format(
                        foot, short_foot
                    )
                ]
            )
            ankm_s = np.array(
                h5file[
                    "/Output/OptKinModel/{:s}/Seg/Shank/{:s}AnkleMedial/r".format(
                        foot, short_foot
                    )
                ]
            )

            # foot
            toe = np.array(
                h5file[
                    "/Output/OptKinModel/{:s}/Seg/Foot/{:s}Toe/r".format(
                        foot, short_foot
                    )
                ]
            )
            mt5 = np.array(
                h5file[
                    "/Output/OptKinModel/{:s}/Seg/Foot/{:s}ToeLateral/r".format(
                        foot, short_foot
                    )
                ]
            )
            cal = np.array(
                h5file[
                    "/Output/OptKinModel/{:s}/Seg/Foot/{:s}MidfootLateral/r".format(
                        foot, short_foot
                    )
                ]
            )
            calm = np.array(
                h5file[
                    "/Output/OptKinModel/{:s}/Seg/Foot/{:s}MidfootMedial/r".format(
                        foot, short_foot
                    )
                ]
            )
            heel = np.array(
                h5file[
                    "/Output/OptKinModel/{:s}/Seg/Foot/{:s}Heel/r".format(
                        foot, short_foot
                    )
                ]
            )
            ank_f = np.array(
                h5file[
                    "/Output/OptKinModel/{:s}/Seg/Foot/{:s}AnkleLateral/r".format(
                        foot, short_foot
                    )
                ]
            )
            ankm_f = np.array(
                h5file[
                    "/Output/OptKinModel/{:s}/Seg/Foot/{:s}AnkleMedial/r".format(
                        foot, short_foot
                    )
                ]
            )

            markers = [
                tro,
                knem_t,
                kne_t,
                knem_s,
                kne_s,
                tib,
                ank_s,
                ankm_s,
                toe,
                mt5,
                cal,
                calm,
                heel,
                ank_f,
                ankm_f,
            ]
            # print("markers: ", np.shape(markers))
            markers = np.hstack(markers)
            # print(np.shape(markers))

            # #################### ANGLES #################################

            # hip
            hip_flex = np.array(
                h5file[f"/Output/JointAnglesAndMoments/{foot:s}/HipFlexion/Pos"]
            )
            hip_add = np.array(
                h5file[f"/Output/JointAnglesAndMoments/{foot:s}/HipAdduction/Pos"]
            )
            hip_rot = np.array(
                h5file[
                    "/Output/JointAnglesAndMoments/{:s}/HipInternalRotation/Pos".format(
                        foot
                    )
                ]
            )

            # knee
            knee_flex = np.array(
                h5file[f"/Output/JointAnglesAndMoments/{foot:s}2/KneeFlexion/Pos"]
            )
            knee_add = np.array(
                h5file[f"/Output/JointAnglesAndMoments/{foot:s}2/KneeAdduction/Pos"]
            )
            knee_rot = np.array(
                h5file[
                    "/Output/JointAnglesAndMoments/{:s}2/KneeInternalRotation/Pos".format(
                        foot
                    )
                ]
            )

            # ankle
            ankle_flex = np.array(
                h5file[
                    "/Output/JointAnglesAndMoments/{:s}/AnkleDorsiFlexion/Pos".format(
                        foot
                    )
                ]
            )
            ankle_add = np.array(
                h5file[f"/Output/JointAnglesAndMoments/{foot:s}/AnkleInversion/Pos"]
            )
            ankle_rot = np.array(
                h5file[
                    "/Output/JointAnglesAndMoments/{:s}/AnkleInternalRotation/Pos".format(
                        foot
                    )
                ]
            )

            angles = [
                hip_flex,
                hip_add,
                hip_rot,
                knee_flex,
                knee_add,
                knee_rot,
                ankle_flex,
                ankle_add,
                ankle_rot,
            ]

            # print("angles: ", np.shape(angles))
            angles = np.hstack(angles)
            # print(np.shape(angles))

            # #################################### JOINT CENTERS #############################

            hip_center = np.array(
                h5file[f"/Output/OptKinModel/{foot:s}/Seg/Thigh/HipJoint/r"]
            )
            knee_center = np.array(
                h5file[f"/Output/OptKinModel/{foot:s}/Seg/Shank/KneeJoint/r"]
            )
            ankle_center = np.array(
                h5file[f"/Output/OptKinModel/{foot:s}/Seg/Shank/AnkleJoint/r"]
            )

            joint_centers = [hip_center, knee_center, ankle_center]
            # print("joint_centers: ", np.shape(joint_centers))
            joint_centers = np.hstack(joint_centers)
            # print(np.shape(joint_centers))

            ##########################################
            # Assemble
            ndarray = [markers, angles, joint_centers, reaction, joint_moments]
            # Relevant time period
            idx = np.abs(grf[:, 2]) > 5.0
            # Cutoff
            ndarray = [d[idx, :] for d in ndarray]
            # Stack
            ndarray = np.hstack(ndarray)
            print("input: ", np.shape(ndarray))

            data.append(ndarray)

        return data
