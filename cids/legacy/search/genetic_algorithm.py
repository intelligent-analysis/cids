"""CIDS computational intelligence library"""
import random

from deap import base
from deap import creator
from deap import tools


class GeneticAlgorithm:
    """A basic genetic algorithm."""

    def __init__(
        self,
        num_attributes,
        attribute_range,
        num_individuals,
        num_generations,
        eval_fun,
        mate_prob,
        mutation_prob,
    ):

        # Define hyper parameters
        self.num_attributes = num_attributes
        self.attribute_range = attribute_range
        self.num_individuals = num_individuals
        self.num_generations = num_generations
        self.eval_funct = eval_fun
        self.mate_prob = mate_prob
        self.mutation_prob = mutation_prob

        # Define types
        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMin)

        # Setup individual
        self.toolbox = base.Toolbox()
        self.toolbox.register(
            "attribute",
            random.randint,
            self.attribute_range[0],
            self.attribute_range[1],
        )
        self.toolbox.register(
            "individual",
            tools.initRepeat,
            creator.Individual,
            self.toolbox.attribute,
            n=self.num_attributes,
        )

        # Setup population
        self.toolbox.register(
            "population", tools.initRepeat, list, self.toolbox.individual
        )

        # setup evaluation
        self.toolbox.register("evaluate", self.evaluate)

        # setup operators
        # self.toolbox.register("mate", tools.cxTwoPoint)
        self.toolbox.register("mate", tools.cxBlend, alpha=0.5)
        self.toolbox.register("mutate", tools.mutGaussian, mu=0, sigma=1, indpb=0.1)
        self.toolbox.register("select", tools.selTournament, tournsize=3)

    def evaluate(self, individual):
        return self.eval_funct(individual)

    def run(self):
        # create population
        pop = self.toolbox.population(n=self.num_individuals)

        print("- Initial population")

        fitnesses = list(map(self.toolbox.evaluate, pop))

        for ind, fit in zip(pop, fitnesses):
            ind.fitness.values = fit

        # evolution
        for gen in range(self.num_generations):

            print("- Generation: " + str(gen) + "/" + str(self.num_generations))

            # select
            offspring = self.toolbox.select(pop, len(pop))
            offspring = list(
                map(self.toolbox.clone, offspring)
            )  # clone to remove references

            # mate
            for child1, child2 in zip(offspring[::2], offspring[1::2]):
                if random.random() < self.mate_prob:
                    self.toolbox.mate(child1, child2)
                    del child1.fitness.values  # values are outdated!
                    del child2.fitness.values  # values are outdated!

            # mutate
            for mutant in offspring:
                if random.random() < self.mutation_prob:
                    self.toolbox.mutate(mutant)
                    del mutant.fitness.values

            # re-evaluate individuals without fitness
            invalid_pop = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = self.toolbox.map(self.toolbox.evaluate, invalid_pop)
            for ind, fit in zip(invalid_pop, fitnesses):
                ind.fitness.values = fit

            # update population
            pop[:] = offspring

            # simple statistics
            fits = [ind.fitness.values[0] for ind in pop]

            length = len(pop)
            mean = sum(fits) / length
            sum2 = sum(x * x for x in fits)
            std = abs(sum2 / length - mean**2) ** 0.5

            print("-----------------------------------------")
            print("     Min %s" % min(fits))
            print("     Max %s" % max(fits))
            print("     Avg %s" % mean)
            print("     Std %s" % std)
            print("-----------------------------------------")
            print("-----------------------------------------")
