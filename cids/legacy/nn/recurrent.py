"""CIDS computational intelligence library"""
# TODO: Layer Norm: https://danijar.com/layer-norm-and-gru-for-state-of-the-art-language-modeling/
# TODO: Recurrent dropout or zoneout (code self)
# TODO: Variable sequence length and padding?
# ENHANCEMENT: CUDNN LSTMs? Problems: Only GradientDescentOptimizer, Saving and Restoring doesn"t work
# ENHANCEMENT: Store state across executions: https://stackoverflow.com/questions/38441589/rnn-initial-state
# ENHANCEMENT: bypass connections
# ENHANCEMENT: Multi-step loss
import copy

from tensorflow.python.keras.utils import generic_utils

from cids.legacy.data.datareader import DataReader
from cids.legacy.nn.fully_connected import *
from cids.legacy.nn.modules import *


class SingleGPURecNet(SingleGPUNeuralNetwork):
    def __init__(
        self,
        data_shape,
        input_idx,
        output_idx,
        layers,
        batch_size=1000,
        init_learning_rate=0.01,
        num_epochs=None,
        decay_rate=1.0,
        decay_steps=0,
        keep_prob=1.0,
        grad_clip=None,
        use_batch_norm=False,
        data_format="NSF",
        dtype=tf.float32,
        dr=None,
        idp=None,
        odp=None,
        optimizer=None,
        prep_device=None,
        comp_device=None,
        name=None,
        host=None,
        writer=None,
        result_dir="RESULTS/",
        sessrun_kwargs=None,
    ):
        """A recurrent neural network for up to 1 GPU.

        Args:
            data_shape:     Shape of the data tensor
            input_idx:      Indices of data for the input
            output_idx:     Indices of data for the output
            layers:         A tuple of layer sizes
            batch_size:     Number of samples in batch
            init_learning_rate:  The initial learning rate
            num_epochs:     The maximum number of epochs
            decay_rate:     The learning rate reduction factor over decay_steps
            decay_steps:    The number of steps over which decay_rate is applied
            keep_prob:      The dropout keep probability during training
            grad_clip:      The value used for L2 gradient clipping
            use_batch_norm: Boolean: Use batch norm in layers if available?
            data_format:    Data format used ("NF", NSF", or "NCHW")
                                 N:         batch axis,
                                 S/D:       sequence axis / depth axis
                                 H/W/X/Y:   spatial axes (height, width, x, y)
                                 F/C:       feature or channel axis
            dtype:          Data type (defaults to tf.float32)
            dr:             A Datareader object (None uses default)
            idp:            The input DataProcessor (None uses default)
            odp:            The output DataProcessor (None uses default)
            optimizer:      The optimizer used for training (None uses default)
            prep_device:    The comp_devices used for preprocessing
                            (default: cpu[0])
            comp_device:    The comp_devices used for training (default: gpu[0])
            name:          The shared name scope (None defaults to class name)
            host:           Host name (None defaults to environment hostname)
            result_dir:    Directory to store results, summaries and
                            checkpoints
            sessrun_kwargs: Keyword arguments for tensorflow session.run()

        """
        dr = dr or DataReader(data_shape, data_format, batch_size, slice_tensors=False)
        super().__init__(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=init_learning_rate,
            num_epochs=num_epochs,
            decay_rate=decay_rate,
            decay_steps=decay_steps,
            keep_prob=keep_prob,
            grad_clip=grad_clip,
            use_batch_norm=use_batch_norm,
            data_format=data_format,
            dtype=dtype,
            dr=dr,
            idp=idp,
            odp=odp,
            optimizer=optimizer,
            prep_device=prep_device,
            comp_device=comp_device,
            name=name,
            host=host,
            writer=writer,
            result_dir=result_dir,
            sessrun_kwargs=sessrun_kwargs,
        )

    def build(self):
        self.preprocess
        with tf.device(self.comp_device):
            with tf.variable_scope(self.name):
                self.sequence_lengths
                self.output_sequence_mask
                self.forward_pass
                self.loss
                self.gradients
                self.train
                with tf.variable_scope("metrics"):
                    self.mean_absolute_error
        self.postprocess
        self.infer
        # Setup saver
        self.saver = tf.train.Saver()

    @name_scoped_lazy_property
    def sequence_lengths(self):
        return self.idp.sequence_lengths(self.x)

    @name_scoped_lazy_property
    def output_sequence_mask(self):
        return self.odp.sequence_mask(self.y)

    def _forward_pass(self, x, y):
        y_ = x
        # Build forward pass graph
        prev_layer = ""
        for i, layer in enumerate(self.layers):
            if isinstance(layer, tf.keras.layers.Layer):
                layer.built = False
                if generic_utils.has_arg(layer, "training"):
                    y_ = layer(y_, training=self.TRAIN)
                else:
                    y_ = layer(y_)
                continue
            layer_name = layer[0]
            layer_size = layer[1]
            if layer_size == -1:
                layer_size = self.output_shape[self.feature_axis]
            # Select cell or layer
            if layer_name == "fc_relu":
                if prev_layer in RNN_LAYERS:
                    # Transpose back to NSF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, self.feature_axis]
                    )
                y_ = fc_relu(
                    y_,
                    layer_size,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                )
            elif layer_name == "legacy_fc_relu":
                if prev_layer in RNN_LAYERS:
                    # Transpose back to NSF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, self.feature_axis]
                    )
                y_ = legacy_fc_relu(
                    y_,
                    layer_size,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                )
            elif layer_name == "fc_tanh":
                if prev_layer in RNN_LAYERS:
                    # Transpose back to NSF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, self.feature_axis]
                    )
                y_ = fc_tanh(
                    y_,
                    layer_size,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                )
            elif layer_name == "lstm":
                if prev_layer not in RNN_LAYERS:
                    # Transpose to SNF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, self.feature_axis]
                    )
                y_ = lstm_cell(
                    y_,
                    layer_size,
                    self.sequence_lengths,
                    i,
                    is_training=self.TRAIN,
                    dtype=self.dtype,
                    keep_prob=self.keep_prob,
                )
            elif layer_name == "cudnn_lstm":
                if prev_layer not in RNN_LAYERS:
                    # Transpose to SNF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, self.feature_axis]
                    )
                y_ = cudnn_lstm_cell(
                    y_, layer_size, self.sequence_lengths, i, dtype=self.dtype
                )
            elif layer_name == "gru":
                if prev_layer not in RNN_LAYERS:
                    # Transpose to SNF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, self.feature_axis]
                    )
                y_ = gru_cell(
                    y_, layer_size, self.sequence_lengths, i, dtype=self.dtype
                )
            elif layer_name == "cudnn_gru":
                if prev_layer not in RNN_LAYERS:
                    # Transpose to SNF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, self.feature_axis]
                    )
                y_ = cudnn_gru_cell(
                    y_, layer_size, self.sequence_lengths, i, dtype=self.dtype
                )
            elif layer_name == "fc_out":
                if prev_layer in RNN_LAYERS:
                    # Transpose back to NSF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, self.feature_axis]
                    )
                y_ = fc_out(y_, layer_size)
            elif layer_name == "legacy_fc_out":
                if prev_layer in RNN_LAYERS:
                    # Transpose back to NSF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, self.feature_axis]
                    )
                y_ = legacy_fc_out(y_, layer_size)
            elif callable(layer[0]):  # is function
                if prev_layer in RNN_LAYERS:
                    # Transpose back to NSF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, self.feature_axis]
                    )
                args = copy.deepcopy(layer[1])
                kwargs = copy.deepcopy(layer[2])
                # If layer size is unknown asssume output shape
                if len(args) > 0:
                    if args[0] == -1:
                        args[0] = self.output_shape[self.feature_axis]
                if "keep_prob" not in kwargs.keys() and i == len(self.layers) - 1:
                    kwargs["keep_prob"] = self.keep_prob
                if "is_training" not in kwargs.keys():
                    kwargs["is_training"] = self.TRAIN
                # Create layer
                y_ = layer_name(y_, *args, **kwargs)
                if isinstance(y_, list):
                    y_, self.state[y_] = y_[0], y_[1:]
            else:
                raise ValueError("Unknown layer name:", layer_name)
            # Add summaries
            with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                tf.contrib.summary.histogram("y_", y_, step=self.global_step)
            # Update prev_layer
            prev_layer = layer_name
            # Print layer shape
            if self.VERBOSITY >= 2:
                print(self.INDENT + f"Layer {i:03d}: output_shape:", y_.shape)
        # Add output summary
        with self.writer.as_default(), self._record_summary_if(self.TRAIN):
            for fi in range(self.output_shape[self.feature_axis]):
                tf.contrib.summary.histogram(
                    f"y__feature_{fi:d}",
                    tf.gather(y_, [fi], axis=-1),
                    step=self.global_step,
                )
        # Ensure we go back to NSF
        if prev_layer in RNN_LAYERS:
            # Transpose back to NSF
            y_ = tf.transpose(
                y_, [self.sequence_axis, self.batch_axis, self.feature_axis]
            )
        return x, y, y_


class RecNetTower(NeuralNetworkTower):
    def __init__(self, foundation, index, comp_device, name=None):
        """A tower for a MultiGPU Neural network.

        Args:
            foundation:     A Multi-GPU neural network
            index:          The index of this tower in foundation"s tower list
            comp_device:    The comp_devices used for computation
            name:          The shared name scope (None defaults to class
                             name plus device name)
        """
        self.sequence_lengths = foundation.preprocessors[index][2]
        self.output_sequence_mask = foundation.preprocessors[index][3]
        super().__init__(foundation, index, comp_device, name=name)

    def _forward_pass(self, x, y, prediction_only=False):
        y_ = x
        # Build forward pass graph
        prev_layer = ""
        feature_axis = self.feature_axis
        if feature_axis < 0:
            feature_axis += len(self.data_format)
        for i, layer in enumerate(self.layers):
            if isinstance(layer, tf.keras.layers.Layer):
                layer.built = False
                if generic_utils.has_arg(layer, "training"):
                    y_ = layer(y_, training=self.TRAIN)
                else:
                    y_ = layer(y_)
                continue
            layer_name = layer[0]
            layer_size = layer[1]
            if layer_size == -1:
                layer_size = self.output_shape[feature_axis]
            # Select cell or layer
            if layer_name == "fc_relu":
                if prev_layer in RNN_LAYERS:
                    # Transpose back to NSF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, feature_axis]
                    )
                y_ = fc_relu(
                    y_,
                    layer_size,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                )
            elif layer_name == "fc_tanh":
                if prev_layer in RNN_LAYERS:
                    # Transpose back to NSF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, feature_axis]
                    )
                y_ = fc_tanh(
                    y_,
                    layer_size,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                )
            elif layer_name == "legacy_fc_relu":
                if prev_layer in RNN_LAYERS:
                    # Transpose back to NSF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, feature_axis]
                    )
                y_ = legacy_fc_relu(
                    y_,
                    layer_size,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                )
            elif layer_name == "lstm":
                if prev_layer not in RNN_LAYERS:
                    # Transpose to SNF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, feature_axis]
                    )
                y_ = lstm_cell(
                    y_,
                    layer_size,
                    self.sequence_lengths,
                    i,
                    is_training=self.TRAIN,
                    dtype=self.dtype,
                    keep_prob=self.keep_prob,
                )
            elif layer_name == "cudnn_lstm":
                if prev_layer not in RNN_LAYERS:
                    # Transpose to SNF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, feature_axis]
                    )
                y_ = cudnn_lstm_cell(
                    y_, layer_size, self.sequence_lengths, i, dtype=self.dtype
                )
            elif layer_name == "gru":
                if prev_layer not in RNN_LAYERS:
                    # Transpose to SNF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, feature_axis]
                    )
                y_ = gru_cell(
                    y_, layer_size, self.sequence_lengths, i, dtype=self.dtype
                )
            elif layer_name == "cudnn_gru":
                if prev_layer not in RNN_LAYERS:
                    # Transpose to SNF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, feature_axis]
                    )
                y_ = cudnn_gru_cell(
                    y_, layer_size, self.sequence_lengths, i, dtype=self.dtype
                )
            elif layer_name == "fc_out":
                if prev_layer in RNN_LAYERS:
                    # Transpose back to NSF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, feature_axis]
                    )
                y_ = fc_out(y_, layer_size)
            elif layer_name == "legacy_fc_out":
                if prev_layer in RNN_LAYERS:
                    # Transpose back to NSF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, feature_axis]
                    )
                y_ = legacy_fc_out(y_, layer_size)
            elif callable(layer[0]):  # is function
                if prev_layer in RNN_LAYERS:
                    # Transpose back to NSF
                    y_ = tf.transpose(
                        y_, [self.sequence_axis, self.batch_axis, feature_axis]
                    )
                args = copy.deepcopy(layer[1])
                kwargs = copy.deepcopy(layer[2])
                # If layer size is unknown asssume output shape
                if len(args) > 0:
                    if args[0] == -1:
                        args[0] = self.output_shape[feature_axis]
                if "keep_prob" not in kwargs.keys() and i == len(self.layers) - 1:
                    kwargs["keep_prob"] = self.keep_prob
                if "is_training" not in kwargs.keys():
                    kwargs["is_training"] = self.TRAIN
                # Create layer
                y_ = layer_name(y_, *args, **kwargs)
                if isinstance(y_, list):
                    y_, self.state[y_] = y_[0], y_[1:]
            else:
                raise ValueError("Unknown layer name:", layer_name)
            # Add summaries
            with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                tf.contrib.summary.histogram("y_", y_, step=self.global_step)
            # Update prev_layer
            prev_layer = layer_name
            # Print layer shape
            if self.VERBOSITY >= 2:
                print(self.INDENT + f"Layer {i:03d}: output_shape:", y_.shape)
        # Add output summary
        with self.writer.as_default(), self._record_summary_if(self.TRAIN):
            try:
                num_features = self.output_shape[feature_axis]
            except IndexError:
                num_features = self.output_shape[-1]
            if num_features:
                for fi in range(num_features):
                    tf.contrib.summary.histogram(
                        f"y__feature_{fi:d}",
                        tf.gather(y_, [fi], axis=-1),
                        step=self.global_step,
                    )
        # Ensure we go back to NSF
        if prev_layer in RNN_LAYERS:
            # Transpose back to NSF
            y_ = tf.transpose(y_, [self.sequence_axis, self.batch_axis, feature_axis])
        if prediction_only:
            return y_
        else:
            return x, y, y_


class RecNet(NeuralNetwork):

    Tower = RecNetTower

    def __init__(
        self,
        data_shape,
        input_idx,
        output_idx,
        layers,
        batch_size=1000,
        init_learning_rate=0.01,
        num_epochs=None,
        decay_rate=1.0,
        decay_steps=0,
        keep_prob=1.0,
        grad_clip=None,
        use_batch_norm=False,
        data_format="NSF",
        dtype=tf.float32,
        dr=None,
        idp=None,
        odp=None,
        optimizer=None,
        prep_devices=None,
        comp_devices=None,
        name=None,
        host=None,
        writer=None,
        result_dir="RESULTS/",
        sessrun_kwargs=None,
    ):
        """A recurrent neural network for multiple GPUs.

        Args:
            data_shape:     Shape of the data tensor
            input_idx:      Indices of data for the input
            output_idx:     Indices of data for the output
            layers:         A tuple of layer sizes
            batch_size:     Number of samples in batch
            init_learning_rate:  The initial learning rate
            num_epochs:     Epochs iterating over dataset (None for infinite)
            decay_rate:     The learning rate reduction factor over decay_steps
            decay_steps:    The number of steps over which decay_rate is applied
            keep_prob:      Dropout keep probability
            grad_clip:      The L2-Norm value used for gradient clipping
            use_batch_norm: Boolean: use batch norm if available?
            data_format:    Data format used ("NF", NSF", or "NCHW")
                                 N:         batch axis,
                                 S/D:       sequence axis / depth axis
                                 H/W/X/Y:   spatial axes (height, width, x, y)
                                 F/C:       feature or channel axis
            dtype:          Data type (defaults to tf.float32)
            dr:             A Datareader object (None uses default)
            idp:            The input DataProcessor (None uses default)
            odp:            The output DataProcessor (None uses default)
            optimizer:      The optimizer (class) used (None uses default)
            prep_devices:   The comp_devices used for preprocessing
                            (default: cpus[0:1])
            comp_devices:   The comp_devices used for training (default: gpus)
            name:          The shared name scope (None defaults to class name)
            host:           Host name (None defaults to environment hostname)
            result_dir:    Directory to store results, summaries and
                            checkpoints
            sessrun_kwargs: Keyword arguments for tensorflow session.run()

        """
        if dr is None:
            dr = DataReader(data_shape, data_format, batch_size, slice_tensors=False)
        super().__init__(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=init_learning_rate,
            num_epochs=num_epochs,
            decay_rate=decay_rate,
            decay_steps=decay_steps,
            keep_prob=keep_prob,
            grad_clip=grad_clip,
            use_batch_norm=use_batch_norm,
            data_format=data_format,
            dtype=dtype,
            dr=dr,
            idp=idp,
            odp=odp,
            optimizer=optimizer,
            prep_devices=prep_devices,
            comp_devices=comp_devices,
            name=name,
            host=host,
            writer=writer,
            result_dir=result_dir,
            sessrun_kwargs=sessrun_kwargs,
        )

    def _preprocess(self, iterator):
        """Preprocess the input data before a forward pass.

        Args:
            train_iterator:   An iterator across the training dataset
            valid_iterator:   An iterator across the validation dataset

        Returns:
            x:                      The input batch Tensor
            y:                      The target batch Tensor
            sequence_lengths:       The sequence lengths of the batch Tensor
            output_sequence_mask:   The sequence mask for the output Tensor

        """
        x, y = super()._preprocess(iterator)
        sequence_lengths = self.idp.sequence_lengths(x)
        output_sequence_mask = self.odp.sequence_mask(y)
        return x, y, sequence_lengths, output_sequence_mask
