"""CIDS computational intelligence library"""
import copy

from tensorflow.python.keras.utils import generic_utils

from cids.legacy.data.datareader import DataReader
from cids.legacy.nn.fully_connected import *
from cids.legacy.nn.modules import *


class SingleGPUConvNet(SingleGPUNeuralNetwork):
    def __init__(
        self,
        data_shape,
        input_idx,
        output_idx,
        layers,
        batch_size=1000,
        init_learning_rate=0.01,
        num_epochs=None,
        decay_rate=1.0,
        decay_steps=0,
        keep_prob=1.0,
        grad_clip=None,
        use_batch_norm=False,
        data_format="NHWC",
        dtype=tf.float32,
        dr=None,
        idp=None,
        odp=None,
        optimizer=None,
        prep_device=None,
        comp_device=None,
        name=None,
        host=None,
        writer=None,
        result_dir="RESULTS/",
        sessrun_kwargs=None,
    ):
        """A convolutional neural network for up to 1 GPU.

        Args:
            data_shape:     Shape of the data tensor
            input_idx:      Indices of data for the input
            output_idx:     Indices of data for the output
            layers:         A tuple of layer sizes
            batch_size:     Number of samples in batch
            init_learning_rate:  The initial learning rate
            num_epochs:     The maximum number of epochs
            decay_rate:     The learning rate reduction factor over decay_steps
            decay_steps:    The number of steps over which decay_rate is applied
            keep_prob:      The dropout keep probability during training
            grad_clip:      The value used for L2 gradient clipping
            use_batch_norm: Boolean: use batch norm if available
            data_format:    Data format used ("NF", NSF", or "NHWC")
                                 N:         batch axis,
                                 S/D:       sequence axis / depth axis
                                 H/W/X/Y:   spatial axes (height, width, x, y)
                                 F/C:       feature or channel axis
            dtype:          Data type (defaults to tf.float32)
            dr:             A Datareader object (None uses default)
            idp:            The input DataProcessor (None uses default)
            odp:            The output DataProcessor (None uses default)
            optimizer:      The optimizer (class) used (None uses default)
            prep_device:    The comp_devices used for preprocessing
                            (default: cpu[0])
            comp_device:    The comp_devices used for training (default: gpu[0])
            name:          The shared name scope (None defaults to class name)
            host:           Host name (None defaults to environment hostname)
            result_dir:    Directory to store results, summaries and
                            checkpoints
            sessrun_kwargs: Keyword arguments for tensorflow session.run()

        """
        if dr is None:
            dr = DataReader(data_shape, data_format, batch_size, slice_tensors=False)
        super().__init__(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=init_learning_rate,
            num_epochs=num_epochs,
            decay_rate=decay_rate,
            decay_steps=decay_steps,
            keep_prob=keep_prob,
            grad_clip=grad_clip,
            use_batch_norm=use_batch_norm,
            data_format=data_format,
            dtype=dtype,
            dr=dr,
            idp=idp,
            odp=odp,
            optimizer=optimizer,
            prep_device=prep_device,
            comp_device=comp_device,
            name=name,
            host=host,
            writer=writer,
            result_dir=result_dir,
            sessrun_kwargs=sessrun_kwargs,
        )

    def _forward_pass(self, x, y):
        y_ = x
        for i, layer in enumerate(self.layers):
            if isinstance(layer, tf.keras.layers.Layer):
                layer.built = False
                if generic_utils.has_arg(layer, "training"):
                    y_ = layer(y_, training=self.TRAIN)
                else:
                    y_ = layer(y_)
                continue
            layer_name = layer[0]
            # Select cell or layer
            if layer_name == "fc_relu":
                layer_size = layer[1]
                if layer_size == -1:
                    layer_size = self.output_shape[self.feature_axis]
                y_ = fc_relu(
                    y_,
                    layer_size,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                )
            elif layer_name == "fc_out":
                layer_size = layer[1]
                if layer_size == -1:
                    layer_size = self.output_shape[self.feature_axis]
                y_ = fc_out(y_, layer_size)
            elif layer_name == "conv2":
                num_kernels = layer[1]
                kernel_shape = layer[2]
                if len(layer) > 3:
                    kwargs = layer[3]
                else:
                    kwargs = {}
                y_ = conv2(
                    y_,
                    num_kernels,
                    kernel_shape,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                    **kwargs,
                )
            elif layer_name == "conv2T":
                num_kernels = layer[1]
                kernel_shape = layer[2]
                if len(layer) > 3:
                    kwargs = layer[3]
                else:
                    kwargs = {}
                y_ = conv2T(
                    y_,
                    num_kernels,
                    kernel_shape,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                    **kwargs,
                )
            elif layer_name == "conv3":
                num_kernels = layer[1]
                kernel_shape = layer[2]
                if len(layer) > 3:
                    kwargs = layer[3]
                else:
                    kwargs = {}
                y_ = conv3(
                    y_,
                    num_kernels,
                    kernel_shape,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                    **kwargs,
                )
            elif layer_name == "conv3T":
                num_kernels = layer[1]
                kernel_shape = layer[2]
                if len(layer) > 3:
                    kwargs = layer[3]
                else:
                    kwargs = {}
                y_ = conv3T(
                    y_,
                    num_kernels,
                    kernel_shape,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                    **kwargs,
                )
            elif layer_name == "mpool2":
                pool_shape = layer[1]
                if len(layer) > 2:
                    kwargs = copy.deepcopy(layer[2])
                else:
                    kwargs = {}
                y_ = mpool2(y_, pool_shape, **kwargs)
            elif layer_name == "mpool3":
                pool_shape = layer[1]
                if len(layer) > 2:
                    kwargs = copy.deepcopy(layer[2])
                else:
                    kwargs = {}
                y_ = mpool3(y_, pool_shape, **kwargs)
            elif callable(layer[0]):
                args = copy.deepcopy(layer[1])
                kwargs = copy.deepcopy(layer[2])
                # If layer size is unknown asssume output shape
                if len(args) > 0:
                    if args[0] == -1:
                        args[0] = self.output_shape[self.feature_axis]
                if "keep_prob" not in kwargs.keys() and i == len(self.layers) - 1:
                    kwargs["keep_prob"] = self.keep_prob
                if "is_training" not in kwargs.keys():
                    kwargs["is_training"] = self.TRAIN
                # Create layer
                y_ = layer_name(y_, *args, **kwargs)
                if isinstance(y_, list):
                    y_, self.state[y_] = y_[0], y_[1:]
            else:
                raise ValueError("Unknown layer name:", layer_name)
            # Add summaries
            with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                tf.contrib.summary.histogram("y_", y_, step=self.global_step)
            # Print layer shape
            if self.VERBOSITY >= 2:
                print(self.INDENT + f"Layer {i:03d}: output_shape:", y_.shape)
        with self.writer.as_default(), self._record_summary_if(self.TRAIN):
            for fi in range(self.output_shape[self.feature_axis]):
                tf.contrib.summary.histogram(
                    f"y__feature_{fi:d}", y_[..., fi], step=self.global_step
                )
        return x, y, y_


class ConvNetTower(NeuralNetworkTower):
    def _forward_pass(self, x, y, prediction_only=False):
        y_ = x
        for i, layer in enumerate(self.layers):
            if isinstance(layer, tf.keras.layers.Layer):
                layer.built = False
                if generic_utils.has_arg(layer, "training"):
                    y_ = layer(y_, training=self.TRAIN)
                else:
                    y_ = layer(y_)
                if self.VERBOSITY:
                    print("Layer", i, layer.__class__.__name__, y_.shape)
                continue
            layer_name = layer[0]
            # Select cell or layer
            if layer_name == "fc_relu":
                layer_size = layer[1]
                if layer_size == -1:
                    layer_size = self.output_shape[self.feature_axis]
                y_ = fc_relu(
                    y_,
                    layer_size,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                )
            elif layer_name == "fc_out":
                layer_size = layer[1]
                if layer_size == -1:
                    layer_size = self.output_shape[self.feature_axis]
                y_ = fc_out(y_, layer_size)
            elif layer_name == "conv2":
                num_kernels = layer[1]
                kernel_shape = layer[2]
                if len(layer) > 3:
                    kwargs = layer[3]
                else:
                    kwargs = {}
                y_ = conv2(
                    y_,
                    num_kernels,
                    kernel_shape,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                    **kwargs,
                )
            elif layer_name == "conv2T":
                num_kernels = layer[1]
                kernel_shape = layer[2]
                if len(layer) > 3:
                    kwargs = layer[3]
                else:
                    kwargs = {}
                y_ = conv2T(
                    y_,
                    num_kernels,
                    kernel_shape,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                    **kwargs,
                )
            elif layer_name == "conv3":
                num_kernels = layer[1]
                kernel_shape = layer[2]
                if len(layer) > 3:
                    kwargs = layer[3]
                else:
                    kwargs = {}
                y_ = conv3(
                    y_,
                    num_kernels,
                    kernel_shape,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                    **kwargs,
                )
            elif layer_name == "conv3T":
                num_kernels = layer[1]
                kernel_shape = layer[2]
                if len(layer) > 3:
                    kwargs = layer[3]
                else:
                    kwargs = {}
                y_ = conv3T(
                    y_,
                    num_kernels,
                    kernel_shape,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                    **kwargs,
                )
            elif layer_name == "mpool2":
                pool_shape = layer[1]
                if len(layer) > 2:
                    kwargs = copy.deepcopy(layer[2])
                else:
                    kwargs = {}
                y_ = mpool2(y_, pool_shape, **kwargs)
            elif layer_name == "mpool3":
                pool_shape = layer[1]
                if len(layer) > 2:
                    kwargs = copy.deepcopy(layer[2])
                else:
                    kwargs = {}
                y_ = mpool3(y_, pool_shape, **kwargs)
            elif callable(layer[0]):  # is function
                args = copy.deepcopy(layer[1])
                kwargs = copy.deepcopy(layer[2])
                # If layer size is unknown asssume output shape
                if len(args) > 0:
                    if args[0] == -1:
                        args[0] = self.output_shape[self.feature_axis]
                if not isinstance(layer[0], tf.keras.layers.Layer):
                    if "keep_prob" not in kwargs.keys() and i == len(self.layers) - 1:
                        kwargs["keep_prob"] = self.keep_prob
                    if "is_training" not in kwargs.keys():
                        kwargs["is_training"] = self.TRAIN
                # Create layer
                y_ = layer_name(y_, *args, **kwargs)
                if isinstance(y_, list):
                    y_, self.state[y_] = y_[0], y_[1:]
            else:
                raise ValueError("Unknown layer name:", layer_name)
            # Add summaries
            with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                tf.contrib.summary.histogram("y_", y_, step=self.global_step)
            # Print layer shape
            if self.VERBOSITY >= 2:
                print(self.INDENT + f"Layer {i:03d}: output_shape:", y_.shape)
        with self.writer.as_default(), self._record_summary_if(self.TRAIN):
            for fi in range(self.output_shape[self.feature_axis]):
                tf.contrib.summary.histogram(
                    f"y__feature_{fi:d}", y_[..., fi], step=self.global_step
                )
        if prediction_only:
            return y_
        else:
            return x, y, y_


class ConvNet(NeuralNetwork):

    Tower = ConvNetTower

    def __init__(
        self,
        data_shape,
        input_idx,
        output_idx,
        layers,
        batch_size=1000,
        init_learning_rate=0.01,
        num_epochs=None,
        decay_rate=1.0,
        decay_steps=0,
        keep_prob=1.0,
        grad_clip=None,
        use_batch_norm=False,
        data_format="NHWC",
        dtype=tf.float32,
        dr=None,
        idp=None,
        odp=None,
        optimizer=None,
        prep_devices=None,
        comp_devices=None,
        name=None,
        host=None,
        writer=None,
        result_dir="RESULTS/",
        sessrun_kwargs=None,
    ):
        """A SingleGPUConvNet neural network for multiple GPUs.

        Args:
            data_shape:     Shape of the data tensor
            input_idx:      Indices of data for the input
            output_idx:     Indices of data for the output
            layers:         A tuple of layer sizes
            batch_size:     Number of samples in batch
            init_learning_rate:  The initial learning rate
            num_epochs:     Epochs iterating over dataset (None for infinite)
            decay_rate:     The learning rate reduction factor over decay_steps
            decay_steps:    The number of steps over which decay_rate is applied
            keep_prob:      Dropout keep probability
            grad_clip:      The L2-Norm value used for gradient clipping
            use_batch_norm: Boolean: use batch norm if available
            data_format:    Data format used ("NF", NSF", or "NHWC")
                                 N:         batch axis,
                                 S/D:       sequence axis / depth axis
                                 H/W/X/Y:   spatial axes (height, width, x, y)
                                 F/C:       feature or channel axis
            dtype:          Data type (defaults to tf.float32)
            dr:             A Datareader object (None uses default)
            idp:            The input DataProcessor (None uses default)
            odp:            The output DataProcessor (None uses default)
            optimizer:      The optimizer used for training (None uses default)
            prep_devices:   The comp_devices used for preprocessing
                            (default: cpus[0:1])
            comp_devices:   The comp_devices used for training (default: gpus)
            name:          The shared name scope (None defaults to class name)
            host:           Host name (None defaults to environment hostname)
            result_dir:    Directory to store results, summaries and
                            checkpoints
            sessrun_kwargs: Keyword arguments for tensorflow session.run()

        """
        dr = dr or DataReader(data_shape, data_format, batch_size, slice_tensors=False)
        super().__init__(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=init_learning_rate,
            num_epochs=num_epochs,
            decay_rate=decay_rate,
            decay_steps=decay_steps,
            keep_prob=keep_prob,
            grad_clip=grad_clip,
            use_batch_norm=use_batch_norm,
            data_format=data_format,
            dtype=dtype,
            dr=dr,
            idp=idp,
            odp=odp,
            optimizer=optimizer,
            prep_devices=prep_devices,
            comp_devices=comp_devices,
            name=name,
            writer=writer,
            host=host,
            result_dir=result_dir,
            sessrun_kwargs=sessrun_kwargs,
        )
