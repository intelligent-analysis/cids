"""CIDS computational intelligence library"""
# TODO: Add data processor kwargs
# TODO: Avoid feeding data tensor
import glob
import json
import sys
import threading
from threading import Thread

import pandas as pd
from sortedcontainers import SortedDict
from tqdm.autonotebook import tqdm
from tqdm.autonotebook import trange

from cids.legacy.data.dataprocessor import *
from cids.legacy.data.datareader import *
from cids.legacy.nn.convolutional import *


# CONDITIONAL IMPORT
if sys.version_info[0] > 2:
    from queue import Queue
else:
    from Queue import Queue


class ModelWrapper:

    DEBUG = False
    INDENT = ""

    def __init__(
        self,
        NN,
        layers,
        data_shape,
        data_format,
        input_idx,
        output_idx,
        nn_kwargs=None,
        dr_kwargs=None,
        idp_kwargs=None,
        odp_kwargs=None,
        config=None,
        feed_threads=0,
        report_steps=250,
        save_steps=1000,
        name=None,
        global_seed=None,
    ):
        """A wrapper for neural network models.

        This wrapper manages training, evaluation and inference of cids neural
        network models. It manages session creation, initialization and
        clean up. Must be used in combination with the "with" statement.

        Example:

            with ModelWrapper(NeuralNetwork,  [("fc_relu", 10), ("fc_out", 5)],
                              [10, 4], "NF", [0, 1, 2], [3]) as model:
                 e, ev, ev_min = model.training_schedule(...)
                 ...

        Args:
            NN:             class of neural network model
            layers:         architecture of the neural networks
            data_shape:     data shape
            data_format:    data format (e.g. "NF" or "NSF" or "NHWC")
            input_idx:      feature indices for the inputs
            output_idx:     feature indices for the output
            nn_kwargs:      neural network keyword arguments
            dr_kwargs:      data reader keyword arguments
            idp_kwargs:     input data processor keyword arguments
            odp_kwargs:     output data processor keyword arguments
            config:         specify a tensorflow config
            feed_threads:   number of threads used for feeding with pyfunction
            report_steps:   number of steps inbetween reports
            save_steps:     number of steps inbetween model saving
            name:           name of the model
            global_seed:    sets a global seed for all tf operations. Default:None

        """
        self.VERBOSITY = 2
        # Data
        self._data_format = data_format
        self._data_shape = data_shape
        self.input_idx = input_idx
        self.input_shape = list(self.data_shape)
        self.input_shape[-1] = len(self.input_idx)
        self.output_idx = output_idx
        self.output_shape = list(self.data_shape)
        self.output_shape[-1] = len(self.output_idx)
        self.global_seed = global_seed
        # Set task
        self.task = "regression"
        self.num_classes = None
        # Data reader
        if dr_kwargs is not None:
            self.dr_kwargs = dr_kwargs
        else:
            self.dr_kwargs = dict()
        # Input data processor
        if idp_kwargs is None:
            self.idp_kwargs = dict()
        else:
            self.idp_kwargs = idp_kwargs
        if "data_format" not in self.idp_kwargs.keys():
            self.idp_kwargs["data_format"] = self.data_format
        if "scope" not in self.idp_kwargs.keys():
            self.idp_kwargs["scope"] = "InputDataProcessor"
        # Output data processor
        if odp_kwargs is None:
            self.odp_kwargs = dict()
        else:
            self.odp_kwargs = odp_kwargs
        if "data_format" not in self.odp_kwargs.keys():
            self.odp_kwargs["data_format"] = self.data_format
        if "scope" not in self.odp_kwargs.keys():
            self.odp_kwargs["scope"] = "OutputDataProcessor"
        # Neural network
        self.NN = NN
        self.layers = layers
        if nn_kwargs is not None:
            self.nn_kwargs = nn_kwargs
        else:
            self.nn_kwargs = dict()
        # Checkpoint
        self.checkpoint = "last"
        # Tensorflow settings
        if config:
            self.config = config
        else:
            self.config = tf.ConfigProto(
                allow_soft_placement=True,
                intra_op_parallelism_threads=8,
                inter_op_parallelism_threads=8,
            )
            self.config.gpu_options.allow_growth = True
            self.config.gpu_options.per_process_gpu_memory_fraction = 1.0
        # Model wrapper settings
        self.feed_threads = feed_threads
        self.train_queue = None
        self.stop_event = threading.Event()
        self.threads = []
        self.report_steps = report_steps
        self.report_samples_above_threshold = False
        self.save_steps = save_steps
        if name:
            self.name = name
        else:
            self.name = NN.__name__
        self.base_name = self.name
        # Create empty neural network and session instances
        self.mode = ""
        self.nn = None
        self.train_sess = None
        self.train_graph = None
        self.test_sess = None
        self.test_graph = None
        self.writer = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        # Ensure all threads are closed
        self._close_all_threads()
        # Ensure session is closed
        self._clear_train()
        self._clear_test()

    def __del__(self):
        # Ensure all threads are closed
        self._close_all_threads()
        # Ensure session is closed
        self._clear_train()
        self._clear_test()

    @property
    def data_format(self):
        return self._data_format.replace("I", "")

    @property
    def data_shape(self):
        if "I" in self._data_format:
            # Shape of iteration axis is only passed to data reader
            return [
                d
                for i, d in enumerate(self._data_shape)
                if i != self._data_format.index("I")
            ]
        else:
            return self._data_shape

    @classmethod
    def regression(
        cls, data_shape, data_format, input_idx, output_idx, model, **kwargs
    ):
        inst = cls(data_shape, data_format, input_idx, output_idx, model, **kwargs)
        inst.task = "regression"
        inst.num_classes = None
        inst.loss = tf.keras.losses.MeanSquaredError()
        return inst

    @classmethod
    def binary_classification(
        cls, data_shape, data_format, inpud_idx, output_idx, model, **kwargs
    ):
        inst = cls(data_shape, data_format, inpud_idx, output_idx, model, **kwargs)
        inst.task = "classification"
        inst.num_classes = 1
        inst.output_shape = [
            inst.output_shape[data_format.index("N")],
            inst.output_shape[data_format.index("F")],
        ]
        inst.loss = tf.keras.losses.BinaryCrossentropy()
        return inst

    @classmethod
    def categorical_classification(
        cls,
        num_classes,
        data_shape,
        data_format,
        input_idx,
        output_idx,
        model,
        **kwargs,
    ):
        inst = cls(data_shape, data_format, input_idx, output_idx, model, **kwargs)
        inst.task = "classification"
        inst.num_classes = num_classes
        inst.output_shape = [
            inst.output_shape[data_format.index("N")],
            inst.output_shape[data_format.index("F")],
        ]
        inst.loss = tf.keras.losses.CategoricalCrossentropy()
        return inst

    @staticmethod
    def _create_dir(dir):
        os.makedirs(dir, exist_ok=True)
        try:
            os.chmod(dir, 0o777)
        except PermissionError:
            pass

    @property
    def result_dir(self):
        if "result_dir" in self.nn_kwargs.keys():
            result_dir = self.nn_kwargs["result_dir"]
        else:
            result_dir = "RESULTS"
        self._create_dir(result_dir)
        return result_dir

    @property
    def model_dir(self):
        if "host" in self.nn_kwargs.keys():
            host = self.nn_kwargs["host"]
        else:
            host = None
        model_name = self.NN.assemble_model_name(self.name, host, self.layers)
        model_dir = os.path.join(self.result_dir, self.name, model_name)
        self._create_dir(model_dir)
        return model_dir

    @property
    def summary_dir(self):
        summary_dir = os.path.join(self.model_dir, "summary")
        self._create_dir(summary_dir)
        return summary_dir

    @property
    def checkpoint_dir(self):
        checkpoint_dir = os.path.join(self.model_dir, "checkpoint")
        self._create_dir(checkpoint_dir)
        return checkpoint_dir

    @property
    def log_dir(self):
        log_dir = os.path.join(self.model_dir, "log")
        self._create_dir(log_dir)
        return log_dir

    @property
    def profile_dir(self):
        profile_dir = os.path.join(self.model_dir, "profile")
        self._create_dir(profile_dir)
        return profile_dir

    @property
    def tmp_dir(self):
        tmp_dir = os.path.join(self.model_dir, "tmp")
        self._create_dir(tmp_dir)
        return os.path.join(self.model_dir, "tmp")

    @property
    def plot_dir(self):
        plot_dir = os.path.join(self.model_dir, "plot")
        self._create_dir(plot_dir)
        return plot_dir

    def build_and_initialize(self, src, batch_size, mode="test", **kwargs):
        if mode == "test":
            if self.mode != mode:
                # Clear away train graph
                self._clear_train()
                self.mode = mode
                # (Re)initialize if previous mode was not inference
                return self._build_and_initialize_test(src, batch_size, **kwargs)
            else:
                with self.test_graph.as_default():
                    # Do not reinitialize if previous mode was test
                    test_feed_dict = self._generate_feed_test(
                        batch_size, test_src=src, use_feed=kwargs["use_feed"]
                    )
                    self.nn.sess_update_data_pipeline(
                        self.test_sess, mode="test", feed_dict=test_feed_dict
                    )
                return test_feed_dict
        elif mode == "train":
            # Clear away both graphs
            self._clear_train()
            self._clear_test()
            self.mode = mode
            # Initialize train graph
            train_src = src["train"]
            valid_src = src["valid"]
            return self._build_and_initialize_train(
                train_src, valid_src, batch_size, **kwargs
            )
        else:
            raise ValueError(f"Invalid mode: {mode:s}.")

    def _clear_train(self):
        if self.nn is not None:
            self.nn = None
        if self.writer is not None:
            self.writer = None
        if self.train_sess is not None:
            self.train_sess.close()
        self.train_sess = None
        self.train_graph = None

    def _clear_test(self):
        if self.nn is not None:
            self.nn = None
        if self.test_sess is not None:
            self.test_sess.close()
        self.test_sess = None
        self.test_graph = None

    def _build_and_initialize_test(self, test_src, test_batch_size, **kwargs):
        # Read keyword arguments or set defaults
        # TODO: Can this be more elegant? (e.g. global function that sets from kwargs or global defaults)
        if "host" in kwargs.keys():
            host = kwargs["host"]
        else:
            host = None
        if host is None and "host" in self.nn_kwargs.keys():
            host = self.nn_kwargs["host"]
        if "checkpoint" in kwargs.keys():
            checkpoint = kwargs["checkpoint"]
        else:
            checkpoint = "last"
        if "use_feed" in kwargs.keys():
            use_feed = kwargs["use_feed"]
        else:
            use_feed = False
        # Define graph
        self.test_graph = tf.Graph()
        with self.test_graph.as_default():
            self.nn = self._define_test_graph(test_batch_size, host)
            # Generate test feed
            test_feed_dict = self._generate_feed_test(
                test_batch_size, test_src=test_src, use_feed=use_feed
            )
            # Reset session
            if self.test_sess is not None:
                self.test_sess.close()
            self.test_sess = tf.Session(graph=self.test_graph, config=self.config)
            # Initialize and run graph
            if self.VERBOSITY:
                print(self.INDENT + self.name + ": Initializing test graph.")
            self.nn.sess_initialize(
                self.test_sess,
                feed_dict=test_feed_dict,
                mode="test",
                checkpoint=checkpoint,
            )
        return test_feed_dict

    def _build_and_initialize_train(self, train_src, valid_src, batch_size, **kwargs):
        # Read keyword arguments or set defaults
        # TODO: Can this be more elegant? (e.g. global function that sets from kwargs or global defaults)
        if "num_steps" in kwargs.keys():
            num_steps = kwargs["num_steps"]
        else:
            raise AttributeError("num_steps must be defined.")
        if "freeze" in kwargs.keys():
            freeze = kwargs["freeze"]
        else:
            freeze = True
        if "chunk_size" in kwargs.keys():
            chunk_size = kwargs["chunk_size"]
        else:
            chunk_size = None
        if "use_feed" in kwargs.keys():
            use_feed = kwargs["use_feed"]
        else:
            use_feed = False
        # Define graph
        self.train_graph = tf.Graph()
        with self.train_graph.as_default():
            if self.writer is None:
                summary_dir = self.summary_dir
                # Create summary writer
                self.writer = tf.contrib.summary.create_file_writer(
                    summary_dir, flush_millis=10000
                )
            self.nn = self._define_train_graph(batch_size, chunk_size)
            with self.writer.as_default():
                tf.contrib.summary.always_record_summaries()
        # Generate feed dictionary
        num_devices = self._get_num_devices()
        num_train_samples = self._get_num_train_samples(num_steps, batch_size)
        if self.feed_threads and callable(train_src):
            assert use_feed, "Multi thread feeding requires use_feed = True"
            # Start threads that fill a sample queue
            self._start_generating_train(
                num_train_samples, train_src, num_threads=self.feed_threads
            )
            feed_batch_size = batch_size * num_devices
            current_train_samples = self._deque_train(feed_batch_size)
            current_valid_samples = self._deque_train(feed_batch_size)
            train_feed_dict = self._generate_feed_train(
                train_src=current_train_samples,
                valid_src=current_valid_samples,
                freeze=freeze,
                use_feed=use_feed,
            )
        else:
            # Generate entire feed dictionary a-priori
            train_feed_dict = self._generate_feed_train(
                num_train_samples=num_train_samples,
                train_src=train_src,
                valid_src=valid_src,
                freeze=freeze,
                use_feed=use_feed,
            )
        # Reset session
        with self.train_graph.as_default(), self.writer.as_default():
            if self.train_sess is not None:
                self.train_sess.close()
            self.train_sess = tf.Session(graph=self.train_graph, config=self.config)
            # Initialize graph
            if self.VERBOSITY:
                print(self.INDENT + self.name + ": Initializing training graph.")
            self.nn.sess_initialize(
                self.train_sess,
                feed_dict=train_feed_dict,
                mode="train",
                checkpoint="last",
            )
        return train_feed_dict

    def training_schedule(
        self,
        train_src,
        valid_src,
        batch_size_schedule,
        num_steps_schedule,
        freeze_schedule=None,
        chunk_size_schedule=None,
        loss_threshold=1.0,
        eval_fun=None,
        write=True,
    ):
        """Train the model following a training schedule.

        Args:
            train_src:              list of samples/sample function (training)
            valid_src:              list of samples/sample function (validation)
            batch_size_schedule:    batch sizes during schedule phases
            num_steps_schedule:     number of training steps for each phase
            freeze_schedule:        opt.: freeze booleans during schedule phases
            chunk_size_schedule:    opt.: chunk sizes during schedule phases
            loss_threshold:         threshold loss checked after first phase
            eval_fun:               opt.: evaluation function at each save
            write:                  opt.: write results to json

        Returns:
            e:      final loss
            ev:     final validation loss

        """
        assert len(batch_size_schedule) == len(num_steps_schedule)
        if chunk_size_schedule:
            assert len(batch_size_schedule) == len(chunk_size_schedule)
        if freeze_schedule:
            assert len(freeze_schedule) == len(batch_size_schedule)
        else:
            # Freeze data_processor variables after first phase
            freeze_schedule = [
                False if i == 0 else True for i, b in enumerate(batch_size_schedule)
            ]
        if self.VERBOSITY:
            print(self.INDENT + self.name + ": Starting training schedule.")

        e = 0.0
        ev = 0.0
        ev_min = np.finfo(np.float64).max

        try:
            # Training schedule
            batch_size = -1  # Initialize
            chunk_size = None  # Initialize
            for i in range(len(batch_size_schedule)):
                # Get current parameter set
                batch_size_changed = batch_size != batch_size_schedule[i]
                batch_size = batch_size_schedule[i]
                if chunk_size_schedule:
                    chunk_size_changed = chunk_size != chunk_size_schedule[i]
                    chunk_size = chunk_size_schedule[i]
                else:
                    chunk_size_changed = False
                freeze = freeze_schedule[i]
                num_steps = num_steps_schedule[i]
                # Did the model change?
                model_changed = batch_size_changed or chunk_size_changed
                if model_changed:
                    train_feed_dict = self.build_and_initialize(
                        {"train": train_src, "valid": valid_src},
                        batch_size,
                        mode="train",
                        num_steps=num_steps,
                        freeze=freeze,
                        chunk_size=chunk_size,
                    )
                if self.VERBOSITY:
                    print("")
                    print(self.INDENT + self.name + f": Starting training phase {i:d}.")
                # Training loop
                if self.VERBOSITY:
                    print(self.INDENT + self.name + ": Starting training.")
                num_devices = self._get_num_devices()
                try:
                    if self.VERBOSITY:
                        tqdm_target = sys.stdout
                    else:
                        tqdm_target = open(os.devnull, "w")
                    with trange(
                        num_steps,
                        miniters=self.report_steps,
                        file=tqdm_target,
                        dynamic_ncols=True,
                    ) as pbar:
                        pbar.set_description(
                            self.INDENT + self.name + f": Training phase {i:d}"
                        )
                        for j in pbar:
                            # When using queue, dequeue samples from queue
                            if self.train_queue is not None:
                                feed_batch_size = batch_size * num_devices
                                current_train_samples = self._deque_train(
                                    feed_batch_size
                                )
                                current_valid_samples = self._deque_train(
                                    feed_batch_size
                                )
                                train_feed_dict = self._generate_feed_train(
                                    train_src=current_train_samples,
                                    valid_src=current_valid_samples,
                                    freeze=freeze,
                                    use_feed=True,
                                )
                            # Train
                            summarize = j % self.report_steps == 0 or j == num_steps - 1
                            e, ev = self.nn.sess_train(
                                self.train_sess,
                                summarize=summarize,
                                pbar=pbar,
                                feed_dict=train_feed_dict,
                            )
                            # Report bad samples
                            if (
                                summarize
                                and self.VERBOSITY
                                and self.report_samples_above_threshold
                            ):
                                if e > self.report_samples_above_threshold:
                                    pbar.write(
                                        "Training batch with high loss: "
                                        + str(train_feed_dict[self.nn.train_data])
                                    )
                                if ev > self.report_samples_above_threshold:
                                    pbar.write(
                                        "Validation batch with high loss: "
                                        + str(train_feed_dict[self.nn.valid_data])
                                    )
                            # Save
                            if j > 1 and j % self.save_steps == 0:
                                # Store last checkpoint
                                last_index = sum(num_steps_schedule[:i])
                                next_index = sum(num_steps_schedule[: (i + 1)])
                                self.nn.sess_save(
                                    self.train_sess, global_step=next_index, pbar=pbar
                                )
                                # Store best checkpoint
                                if ev < ev_min:
                                    ev_min = ev
                                    self.nn.sess_save(
                                        self.train_sess, global_step="best", pbar=pbar
                                    )
                                # Run eval function
                                if eval_fun:
                                    self.nn.sess_update_data_pipeline(
                                        self.train_sess, mode="valid"
                                    )
                                    xx, yy, yy_ = self.nn.sess_infer(
                                        self.train_sess, feed_dict=train_feed_dict
                                    )
                                    self.nn.sess_update_data_pipeline(
                                        self.train_sess, mode="train"
                                    )
                                    try:
                                        eval_fun(xx, yy, yy_, step=(last_index + j))
                                    except TypeError:
                                        eval_fun(xx, yy, yy_)

                    if self.VERBOSITY:
                        print(
                            self.INDENT
                            + self.name
                            + ": Training phase finished successfully."
                        )
                    if e > loss_threshold and ev > loss_threshold:
                        if self.VERBOSITY:
                            print(
                                self.INDENT
                                + self.name
                                + ": Loss above threshold. Aborting."
                            )
                        break
                except KeyboardInterrupt:
                    if self.VERBOSITY:
                        print(
                            self.INDENT
                            + self.name
                            + ": Training phase interrupted by user."
                        )
                    try:
                        if self.VERBOSITY:
                            print(
                                self.INDENT + "... waiting for interrupt of",
                                " training schedule.",
                                end="\r",
                            )
                        time.sleep(3.0)
                    except KeyboardInterrupt:
                        raise
                finally:
                    # # Do a run for a full trace
                    # # self.sess.run([self.nn.iterator.initializer],
                    # #               feed_dict=train_feed_dict)
                    # e, ev = self.nn.sess_train(
                    #     self.train_sess, summarize=True, full_trace=True)
                    # Save
                    last_index = sum(num_steps_schedule[:i])
                    next_index = sum(num_steps_schedule[: (i + 1)])
                    self.nn.sess_save(self.train_sess, global_step=next_index)
                    # Write results
                    if write:
                        # Model and test results
                        # Model and test results
                        out_dict = {
                            "batch_size": batch_size,
                            "train_loss": np.float64(e),
                            "valid_loss": np.float64(ev),
                            "valid_loss_best": np.float64(ev_min),
                        }
                        # Write
                        thread = Thread(
                            target=self.write_model_and_kwargs,
                            args=("train_results.json",),
                            kwargs=out_dict,
                        )
                        thread.start()
                        thread.join()
                    # Evaluate on the test and training set
                    if eval_fun:
                        # Run eval function
                        self.nn.sess_update_data_pipeline(self.train_sess, mode="valid")
                        xx, yy, yy_ = self.nn.sess_infer(self.train_sess)
                        self.nn.sess_update_data_pipeline(self.train_sess, mode="train")
                        try:
                            eval_fun(xx, yy, yy_, step=(last_index + j))
                        except TypeError:
                            eval_fun(xx, yy, yy_)
                    # Join all feeding threads
                    self._close_all_threads()
                    # Ensure open /os/devnull is closed
                    if tqdm_target is not sys.stdout:
                        tqdm_target.close()

            if self.VERBOSITY:
                print(self.INDENT + self.name + ": Training schedule finished.")
        except KeyboardInterrupt:
            if self.VERBOSITY:
                print(
                    self.INDENT + self.name + ": Training schedule interrupted by user."
                )
            try:
                if self.VERBOSITY:
                    print(self.INDENT + "... waiting for further interrupts.", end="\r")
                time.sleep(3.0)
            except KeyboardInterrupt:
                raise
        except tf.errors.OutOfRangeError:
            if self.VERBOSITY:
                print(
                    self.INDENT
                    + self.name
                    + ": Training finished: Maximum number of epochs."
                )
        return np.float64(e), np.float64(ev), np.float64(ev_min)

    def infer(
        self,
        test_src,
        test_batch_size=None,
        host=None,
        checkpoint="last",
        use_feed=False,
        prediction_only=False,
        fetches=None,
        feeds=None,
    ):
        """Perform a forward pass for inference.

        Args:
            test_src:           list of samples or sample generator function
            test_batch_size:    batch size during inference
                                  (required for sample generator function)
            host:               overwrite name for host (defaults to local host)
            checkpoint:         checkpoint file to load ("last" loads last)
            use_feed:           boolean: use direct feed instead of data reader

        returns:
            X:              input batch
            Y:              target batch
            Y_:             prediction batch

        """
        if self.VERBOSITY:
            print(self.INDENT + self.name + ": Starting inference." + 20 * " ")
        if not callable(test_src) and not test_batch_size:
            test_batch_size = len(test_src)
        try:
            test_feed_dict = self.build_and_initialize(
                test_src,
                test_batch_size,
                mode="test",
                host=host,
                checkpoint=checkpoint,
                use_feed=use_feed,
            )
            if feeds:
                assert hasattr(feeds, "keys")
                test_feed_dict.update(feeds)
            results = self.nn.sess_infer(
                self.test_sess, feed_dict=test_feed_dict, fetches=fetches
            )
            if prediction_only:
                if fetches:
                    Y_ = results[0][2]
                else:
                    Y_ = results[2]
                # Ensure float64
                Y_ = np.asarray(Y_, dtype=np.float64)
            else:
                if fetches:
                    X, Y, Y_ = results[0]
                else:
                    X, Y, Y_ = results
                # Ensure float64
                X = np.asarray(X, dtype=np.float64)
                Y = np.asarray(Y, dtype=np.float64)
                Y_ = np.asarray(Y_, dtype=np.float64)
            if self.VERBOSITY:
                print(self.INDENT + self.name + ": Inference finished.")
        except KeyboardInterrupt:
            if self.VERBOSITY:
                print(self.INDENT + self.name + ": Inference interrupted by user.")
            try:
                if self.VERBOSITY:
                    print(self.INDENT + "... waiting for further interrupts.", end="\r")
                time.sleep(3.0)
            except KeyboardInterrupt:
                raise
            X = None
            Y = None
            Y_ = None
        if prediction_only:
            if fetches:
                return Y_, results[1]
            else:
                return Y_
        else:
            if fetches:
                return X, Y, Y_, results[1]
            else:
                return X, Y, Y_

    def forward_pass(
        self,
        test_src,
        test_batch_size=None,
        host=None,
        checkpoint="last",
        use_feed=False,
    ):
        """Perform a forward pass for debugging without postprocessing.

        Args:
            test_src:           list of samples or sample generator function
            test_batch_size:    batch size during inference
                                  (required for sample generator function)
            host:               overwrite name for host (defaults to local host)
            checkpoint:         checkpoint file to load ("last" loads last)
            use_feed:           boolean: use direct feed instead of data reader

        returns:
            X:              input batch
            Y:              target batch
            Y_:             prediction batch

        """
        if self.VERBOSITY:
            print(self.INDENT + self.name + ": Starting forward pass." + 20 * " ")
        if not callable(test_src) and not test_batch_size:
            test_batch_size = len(test_src)
        try:
            test_feed_dict = self.build_and_initialize(
                test_src,
                test_batch_size,
                mode="test",
                host=host,
                checkpoint=checkpoint,
                use_feed=use_feed,
            )
            X, Y, Y_ = self.nn.sess_forward_pass(
                self.test_sess, feed_dict=test_feed_dict
            )
            # Ensure float64
            X = np.asarray(X, dtype=np.float64)
            Y = np.asarray(Y, dtype=np.float64)
            Y_ = np.asarray(Y_, dtype=np.float64)
            if self.VERBOSITY:
                print(self.INDENT + self.name + ": Forward pass finished.")
        except KeyboardInterrupt:
            if self.VERBOSITY:
                print(self.INDENT + self.name + ": Forward pass interrupted by user.")
            try:
                if self.VERBOSITY:
                    print(self.INDENT + "... waiting for further interrupts.", end="\r")
                time.sleep(3.0)
            except KeyboardInterrupt:
                raise
            X = None
            Y = None
            Y_ = None
        return X, Y, Y_

    def eval(
        self,
        test_src,
        test_batch_size=None,
        metrics=tuple(),
        timer=True,
        host=None,
        checkpoint="last",
        use_feed=False,
        write=True,
    ):
        """Perform a forward pass for evaluation.

        Args:
            test_src:           list of samples or sample generator function
            test_batch_size:    batch size during inference
                                  (required for sample generator function)
            metrics:            names of neural network methods to evaluate
            timer:              also evaluate time of forward pass
            host:               overwrite name for host (defaults to local host)
            checkpoint:         checkpoint file to load ("last" loads last)
            use_feed:           boolean: use direct feed instead of data reader
            write:              boolean: write evaluation results to json

        returns:
            e:              loss for testing
            mae:            mean absolute error metric
            test_time:      evaluation time

        """
        if self.VERBOSITY:
            print(self.INDENT + self.name + ": Starting evaluation.")
        if not callable(test_src) and not test_batch_size:
            test_batch_size = len(test_src)
        try:
            test_feed_dict = self.build_and_initialize(
                test_src,
                test_batch_size,
                mode="test",
                host=host,
                checkpoint=checkpoint,
                use_feed=use_feed,
            )
            metric_methods = [getattr(self.nn, m) for m in metrics]
            results = self.test_sess.run(
                [self.nn.loss] + metric_methods, feed_dict=test_feed_dict
            )
            e = results[0]
            if len(results) > 1:
                metric_results = results[1:]
                metrics_dict = dict(zip(metrics, metric_results))
            else:
                metrics_dict = {}
            if timer:
                self.nn.sess_update_data_pipeline(
                    self.test_sess, mode="test", feed_dict=test_feed_dict
                )  # TODO: Move?
                tic = timeit.default_timer()
                fetches = [self.nn.odp.mean, self.nn.odp.std]
                results = self.nn.sess_infer(
                    self.test_sess, fetches=fetches, feed_dict=test_feed_dict
                )
                if fetches:
                    Y, Y, Y_ = results[0]
                else:
                    Y, Y, Y_ = results
                toc = timeit.default_timer()
                test_time = toc - tic
            else:
                test_time = -1.0
            if self.VERBOSITY:
                # Print
                print(
                    self.INDENT + self.name + ":",
                    "e:",
                    e,
                    ", ".join(f"{k!s}: {v!r}" for k, v in metrics_dict.items()),
                    "time:",
                    test_time,
                )
            if write:
                # Model and test results
                out_dict = {
                    "test_batch_size": test_batch_size,
                    "test_loss": np.float64(e),
                    "test_time": np.float64(test_time),
                }
                # Append metric results
                out_dict.update(
                    {"test_" + k: np.float64(v) for k, v in metrics_dict.items()}
                )
                # Write
                thread = Thread(
                    target=self.write_model_and_kwargs,
                    args=("eval_results.json",),
                    kwargs=out_dict,
                )
                thread.start()
                thread.join()

            if self.VERBOSITY:
                print(self.INDENT + self.name + ": Evaluation finished.")
        except KeyboardInterrupt:
            if self.VERBOSITY:
                print(self.INDENT + self.name + ": Evaluation interrupted by user.")
            try:
                if self.VERBOSITY:
                    print(self.INDENT + "... waiting for further interrupts.", end="\r")
                time.sleep(3.0)
            except KeyboardInterrupt:
                raise
            e = None
            metrics_dict = ({},)
            test_time = None
        return np.float64(e), metrics_dict, test_time

    def write_model_and_kwargs(self, file, **kwargs):
        # Model and test results json
        if "/" in file or "\\" in file:
            json_path = file
        else:
            json_path = os.path.join(self.model_dir, file)
        with open(json_path, "w+") as file:
            out_dict = dict(**kwargs)
            # Add model information
            model_dict = {k: v for k, v in self.__dict__.items() if not callable(v)}
            out_dict["model"] = model_dict
            out_dict = self._clean_model_dict(out_dict)
            file.write(
                json.dumps(out_dict, sort_keys=True, indent=4, separators=(",", ": "))
            )
        os.chmod(json_path, 0o777)

    @staticmethod
    def _clean_model_dict(d):
        if isinstance(d, dict):
            return {k: ModelWrapper._clean_model_dict(v) for k, v in d.items()}
        elif isinstance(d, (tuple, list)):
            return [ModelWrapper._clean_model_dict(v) for v in d]
        elif isinstance(d, np.ndarray):
            return d.tolist()
        elif callable(d):
            # Replace functions with their name
            if hasattr(d, "__name__"):
                return d.__name__
            elif hasattr(d, "name"):
                return d.name
            elif hasattr(d, "__repr__"):
                return d.__repr__()
            else:
                return "(unknown)"
        else:
            try:
                json.dumps(d)
                return d
            except TypeError:
                try:
                    return d.__name__
                except AttributeError:
                    try:
                        return d.__repr__()
                    except AttributeError:
                        return "(not serializable)"

    def architecture_search(
        self,
        modules,
        depths,
        widths,
        train_src,
        valid_src,
        test_src,
        batch_size_schedule,
        num_steps_schedule,
        test_batch_size=100,
        chunk_size_schedule=None,
        freeze_schedule=None,
        loss_threshold=1.0,
        train=True,
        test=True,
        eval_fun=None,
    ):
        """Train and test different neural network architectures.

        Args:
            modules:                list of module tuples
                                      (e.g. [("fc_relu", "equal"), ("fc_out",)])
            depths:                 list of depths of each module in modules
            widths:                 list of widths of each module in modules
            train_src:              list of samples/sample function (training)
            valid_src:              list of samples/sample function (validation)
            test_src:               list of samples/sample function (test)
            batch_size_schedule:    batch sizes during schedule phases
            num_steps_schedule:     number of training steps for each phase
            test_batch_size:        batch size during inference
                                      (required for sample generator function)
            freeze_schedule:        opt.: freeze booleans during schedule phases
            chunk_size_schedule:    opt.: Chunk sizes during schedule phases
            loss_threshold:         threshold loss checked after first phase
            train:                  boolean: Perform training?
            test:                   boolean: Perform testing?
            eval_fun:               function evaluating (e.g. plots) X, Y, Y_
        """

        reference_layers = self.layers  # store fixed architecture

        try:

            for width in widths:
                for depth in depths:

                    ##############
                    # ARCHITECTURE
                    layers = []

                    for module in modules:
                        layer_type = module[0]
                        if isinstance(module[1], str):
                            layer_mode = module[1]
                            if layer_mode == "equal":
                                layers += self._equal_architecture(
                                    layer_type, depth, width
                                )
                            elif layer_mode == "expand":
                                layers += self._expand_architecture(
                                    layer_type, depth, width
                                )
                            else:
                                raise ValueError("arch_mode not recognized.")
                        else:
                            layers += [(layer_type, module[1])]

                    self.layers = layers

                    if self.VERBOSITY:
                        print("")
                        print(
                            self.INDENT + self.name,
                            ": Architecture: \n   ",
                            [
                                (a if not callable(a) else a.__name__ for a in l)
                                for l in self.layers
                            ],
                        )

                    ##########
                    # TRAINING

                    if train:
                        e, ev, ev_min = self.training_schedule(
                            train_src,
                            valid_src,
                            batch_size_schedule,
                            num_steps_schedule,
                            chunk_size_schedule=chunk_size_schedule,
                            freeze_schedule=freeze_schedule,
                            loss_threshold=loss_threshold,
                        )

                    #########
                    # TESTING

                    if test:
                        X, Y, Y_ = self.infer(test_src, test_batch_size=test_batch_size)
                        et, metrics, test_time = self.eval(
                            test_src, test_batch_size=test_batch_size, write=True
                        )

                        ################
                        # Postprocessing

                        if (
                            X is not None
                            and Y is not None
                            and Y_ is not None
                            and et is not None
                            and metrics is not {}
                            and test_time is not None
                        ):

                            if eval_fun:
                                if callable(eval_fun):
                                    # Plot
                                    if self.VERBOSITY:
                                        print(
                                            self.INDENT
                                            + self.name
                                            + ": Plotting results."
                                        )
                                    eval_fun(X, Y, Y_)
                        else:
                            if self.VERBOSITY:
                                print(self.INDENT + "Testing failed.")

            if self.VERBOSITY:
                print(
                    self.INDENT + self.name + ": Parameter study finished successfully."
                )
        except (KeyboardInterrupt, SystemExit):
            if self.VERBOSITY:
                print(
                    self.INDENT + self.name + ": Parameter study interrupted by user."
                )
        finally:
            self.layers = reference_layers  # ensure model layers are reset

    def _equal_architecture(self, layer_type, depth, width):
        layers = [(layer_type, width)] * depth
        return layers

    def _expand_architecture(self, layer_type, depth, width):
        raise NotImplementedError
        return layers

    def random_search(
        self,
        param_dict,
        param_functions,
        num_searches,
        set_params=False,
        train_function=None,
        write_csv=True,
        train_args=(),
        train_kwargs={},
    ):
        """Randomly search hyper parameters.

        Args:
            param_dict:         Dictionary of parameters to operate on
            param_functions:    Dictionary of keys and functions (hierarchical)
            num_searches:       Number of searches to perform
            set_params:         Set parameters to best after training
            train_function:     Function that is evaluated with the parameters
            write:              Write results to a csv file
            train_args:         Arguments passed to training_schedule
            train_kwargs:       Keyword arguments passed to training_schedule

        """
        assert isinstance(param_dict, (dict, SortedDict))
        assert param_dict in self.__dict__.values()
        if train_function is None:
            train_function = self.training_schedule
        # Store old settings
        try:
            old_host = self.nn_kwargs["host"]
        except KeyError:
            old_host = None
        old_VERBOSITY = self.VERBOSITY
        self.VERBOSITY = 0
        # Create dictionary
        param_sets = SortedDict()
        best_param_set_name = ""
        best_param_set_valid_loss = np.finfo(np.float64).max
        try:
            # Store initial parameters
            base_name = self.name
            base_param_dict = deepcopy(param_dict)
            num_previous_searches = len(
                glob.glob(
                    os.path.join(self.result_dir, self.name, f"{base_name:s}-params*")
                )
            )
            with trange(
                num_searches,
                file=sys.stdout,
                dynamic_ncols=True,
                desc=self.name + ": Random hyperparameter search",
            ) as pbar:
                for search in pbar:
                    # Generate parameter set name
                    param_set_name = "params{:04d}".format(
                        search + num_previous_searches
                    )
                    # Use host to identify param set
                    self.nn_kwargs["host"] = param_set_name
                    # Generate new parameter set
                    param_set = {k: fun() for k, fun in param_functions.items()}
                    # Set postfix
                    postfix = SortedDict()
                    unique_name = self.NN.assemble_model_name(
                        self.name, param_set_name, self.layers
                    )
                    postfix["name"] = unique_name
                    postfix.update(param_set)
                    pbar.set_postfix(postfix)
                    # Assign parameter set
                    param_dict.update(param_set)
                    # Train and evaluate
                    e, ev, ev_min = train_function(*train_args, **train_kwargs)
                    # Store parameters
                    param_set["train_loss"] = e
                    param_set["valid_loss"] = ev
                    param_set["min_valid_loss"] = ev_min
                    param_sets[param_set_name] = param_set
                    pbar.write(pd.DataFrame({param_set_name: param_set}).T.to_string())
                    # Update best set
                    if ev < best_param_set_valid_loss:
                        best_param_set_name = param_set_name
                        best_param_set_valid_loss = ev
            # Extract best performance
            if not set_params:
                # Reset parameters
                param_dict.update(base_param_dict)
            else:
                # Set parameters to best
                best_param_set = param_sets[best_param_set_name]
                param_dict = {
                    best_param_set[k]
                    for k in param_dict.keys()
                    if k in best_param_set.keys()
                }
        except KeyboardInterrupt:
            if self.VERBOSITY:
                print(self.INDENT + "Random hyperparameter search interrupted by user.")
            try:
                if self.VERBOSITY:
                    print(self.INDENT + "... waiting for further interrupts.", end="\r")
                time.sleep(3.0)
            except KeyboardInterrupt:
                raise
        finally:
            self.VERBOSITY = old_VERBOSITY
            # Reset name
            self.name = base_name
            self.nn_kwargs["host"] = old_host
            # Write results to file
            file = os.path.join(self.result_dir, self.name, "random_search.csv")
            try:
                df0 = pd.read_csv(file)
                df = pd.concat([df0, pd.DataFrame(dict(param_sets)).T])
            except FileNotFoundError:
                df = pd.DataFrame(dict(param_sets)).T
            df.to_csv(file)
        return df

    def read_tfrecords(self, tfr_files):
        """Reads a single tfrecord to check its content."""
        if isinstance(tfr_files, str):
            tfr_files = [tfr_files]
        data_reader = DataReader(
            self._data_shape,
            self._data_format,
            len(tfr_files),
            chunk_size=None,
            **self.dr_kwargs,
        )
        return data_reader.read_tfrecords(tfr_files)

    def _calc_and_enqueue(self, src_fun, stop_event):
        while stop_event.is_set():
            try:
                sample = src_fun()
                self.train_queue.put(sample)
            except Exception as e:
                raise e
            finally:
                self.train_queue.task_done()

    def _enqueue_train_thread(self, src_fun):
        thread = Thread(
            target=self._calc_and_enqueue,
            args=(src_fun, self.stop_event),
        )
        thread.start()
        return thread

    def _deque_train(self, feed_batch_size):
        while self.train_queue.qsize() < feed_batch_size:
            time.sleep(0.0001)
        feed_samples = [self.train_queue.get() for _ in range(feed_batch_size)]
        feed_batch = np.asarray(feed_samples)
        # Enqueue sample again if queue is empty
        if self.train_queue.qsize() < self.train_queue.maxsize // 2:
            for sample in feed_samples:
                self.train_queue.put(sample)
        return feed_batch

    def _close_all_threads(self):
        if self.threads:
            print("delete threads")
            self.stop_event.set()
            for thread in self.threads:
                thread.join(timeout=5.0)
                thread.task_done()
            self.threads = []
        if self.train_queue is not None:
            self.train_queue.task_done()
            self.train_queue = None

    def _start_generating_train(self, max_num_samples, src_fun, num_threads=1):
        # initialise training queue
        if self.train_queue is None:
            if self.VERBOSITY:
                print(
                    self.INDENT
                    + self.name
                    + ": Starting train sample generation in"
                    + f"{self.feed_threads:d} threads."
                )
            self.train_queue = Queue(maxsize=max_num_samples)
        for _ in range(num_threads):
            if len(self.threads) < self.feed_threads:
                thread = self._enqueue_train_thread(src_fun)
                self.threads.append(thread)

    def _generate_feed_train(
        self,
        num_train_samples=0,
        train_src=None,
        valid_src=None,
        freeze=None,
        use_feed=False,
    ):
        """Generate a dictionary for feeding."""
        feed_dict = dict()
        if train_src is not None:
            if callable(train_src):
                # Source is a function
                assert (
                    num_train_samples != 0
                ), "num_train_samples > 0 required when train_src is function."
                if self.VERBOSITY:
                    print(
                        self.INDENT
                        + self.name
                        + ": Generating feed dictionary of"
                        + f" {num_train_samples:d} train samples."
                    )
                train_samples = [
                    train_src()
                    for _ in tqdm(
                        range(num_train_samples),
                        desc=self.name + ": Train data",
                        file=sys.stdout,
                    )
                ]
            else:
                # Source is a list or ndarray
                train_samples = train_src
            if use_feed:
                feed_dict[self.nn.train_feed] = train_samples
            feed_dict[self.nn.train_data] = train_samples
        if valid_src is not None:
            if callable(valid_src):
                # Source is a function
                assert (
                    num_train_samples != 0
                ), "num_train_samples > 0 required when valid_src is function."
                num_valid_samples = num_train_samples // 4
                if self.VERBOSITY:
                    print(
                        self.INDENT
                        + self.name
                        + ": Generating feed dictionary of"
                        + f" {num_valid_samples:d} valid samples."
                    )
                valid_samples = [
                    valid_src()
                    for _ in tqdm(
                        range(num_valid_samples),
                        desc=self.name + ": Valid data",
                        file=sys.stdout,
                    )
                ]
            else:
                # Source is a list or ndarray
                valid_samples = valid_src
            if use_feed:
                feed_dict[self.nn.valid_feed] = valid_samples
                feed_dict[self.nn.BYPASS] = True
            feed_dict[self.nn.valid_data] = valid_samples
            # Freeze
            if freeze is not None:
                feed_dict[self.nn.idp.freeze] = freeze
                feed_dict[self.nn.odp.freeze] = freeze
            feed_dict[self.nn.TRAIN] = True
        return feed_dict

    def _generate_feed_test(self, num_test_samples, test_src, use_feed=False):
        """Generate a dictionary for feeding."""
        if self.VERBOSITY:
            print(
                self.INDENT
                + self.name
                + ": Generating feed dictionary of"
                + f" {num_test_samples:d} test samples."
            )
        feed_dict = dict()
        if test_src is not None:
            if callable(test_src):
                # Source is a function
                test_samples = [
                    test_src()
                    for _ in tqdm(
                        range(num_test_samples),
                        desc=self.name + ": Test data",
                        file=sys.stdout,
                    )
                ]
            else:
                # Source is a list or ndarray
                test_samples = test_src
            if use_feed:
                # When feeding directly change data reader src_type to placeholder!
                if "I" in self._data_format:
                    # Remove iteration axis
                    # TODO: hard-coded
                    feed_test_samples = test_samples[..., 0, :]
                else:
                    feed_test_samples = test_samples
                feed_dict[self.nn.feed] = feed_test_samples
                feed_dict[self.nn.BYPASS] = True
            feed_dict[self.nn.test_data] = test_samples
            feed_dict[self.nn.TRAIN] = False
        return feed_dict

    def _define_test_graph(self, test_batch_size, host):
        """Defines the graph used for testing."""
        test_chunk_size = None
        # DO NOT CHANGE THESE, ESPECIALLY NOT KEEP_PROB SINCE THATS USED FOR SCALING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # test_learning_rate = 0.0
        # test_decay_rate = 1.0
        # test_decay_steps = 0
        # test_keep_prob = 1.0
        # test_grad_clip = None
        # Define data reading
        test_data_reader = DataReader(
            self._data_shape,
            self._data_format,
            test_batch_size,
            chunk_size=test_chunk_size,
            **self.dr_kwargs,
        )
        test_data_reader.VERBOSITY = self.VERBOSITY
        # Define data processing
        # TODO: move to DataReader
        input_shape = deepcopy(self.input_shape)
        output_shape = deepcopy(self.output_shape)
        if test_chunk_size:
            input_shape[self.data_format.index("S")] = test_chunk_size
            output_shape[self.data_format.index("S")] = test_chunk_size
        idp_kwargs = deepcopy(self.idp_kwargs)
        if "data_shape" not in idp_kwargs.keys():
            idp_kwargs["data_shape"] = input_shape
        idp = DataProcessor(**idp_kwargs)
        odp_kwargs = deepcopy(self.odp_kwargs)
        if "data_shape" not in odp_kwargs.keys():
            odp_kwargs["data_shape"] = output_shape
        odp = DataProcessor(**odp_kwargs)
        # Define neural network
        nn_test_kwargs = deepcopy(self.nn_kwargs)
        nn_test_kwargs["batch_size"] = test_batch_size
        # DO NOT CHANGE THESE, ESPECIALLY NOT KEEP_PROB SINCE THATS USED FOR SCALING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # nn_test_kwargs["init_learning_rate"] = test_learning_rate
        # nn_test_kwargs["decay_rate"] = test_decay_rate
        # nn_test_kwargs["decay_steps"]= test_decay_steps
        # nn_test_kwargs["keep_prob"] = test_keep_prob  # DO NOT SET THIS TO ANYTHING ELSE
        # nn_test_kwargs["grad_clip"] = test_grad_clip
        nn_test_kwargs["host"] = host
        nn = self.NN(
            self.data_shape,
            self.input_idx,
            self.output_idx,
            self.layers,
            dr=test_data_reader,
            idp=idp,
            odp=odp,
            name=self.name,
            **nn_test_kwargs,
        )
        nn.build()
        nn.VERBOSITY = self.VERBOSITY
        nn.INDENT = self.INDENT + "    "
        return nn

    def _define_train_graph(self, batch_size, chunk_size=None):
        """Define the graph used for training."""
        # Define data reading
        data_reader = DataReader(
            self._data_shape,
            self._data_format,
            batch_size,
            chunk_size=chunk_size,
            **self.dr_kwargs,
        )
        data_reader.VERBOSITY = self.VERBOSITY
        # TODO: move to DataReader
        input_shape = deepcopy(self.input_shape)
        output_shape = deepcopy(self.output_shape)
        if chunk_size:
            input_shape[self.data_format.index("S")] = chunk_size
            output_shape[self.data_format.index("S")] = chunk_size
        # Define data processing
        idp_kwargs = deepcopy(self.idp_kwargs)
        if "data_shape" not in idp_kwargs.keys():
            idp_kwargs["data_shape"] = input_shape
        idp = DataProcessor(**idp_kwargs)
        odp_kwargs = deepcopy(self.odp_kwargs)
        if "data_shape" not in odp_kwargs.keys():
            odp_kwargs["data_shape"] = output_shape
        odp = DataProcessor(**odp_kwargs)
        # Define neural network
        if self.global_seed is not None:
            tf.set_random_seed(seed=self.global_seed)
            # TODO: evtl. tf.get_default_graph().finalize() irgendwo?
            # https://stackoverflow.com/questions/51249811/reproducible-results-in-tensorflow-with-tf-set-random-seed
        nn = self.NN(
            self.data_shape,
            self.input_idx,
            self.output_idx,
            self.layers,
            batch_size=batch_size,
            dr=data_reader,
            idp=idp,
            odp=odp,
            name=self.name,
            writer=self.writer,
            **self.nn_kwargs,
        )
        nn.build()
        nn.VERBOSITY = self.VERBOSITY
        nn.INDENT = self.INDENT + "    "
        return nn

    def _get_num_devices(self):
        try:
            num_devices = len(self.nn.comp_devices)
        except AttributeError:
            num_devices = 1
        return num_devices

    def _get_num_train_samples(self, num_steps, batch_size):
        num_devices = self._get_num_devices()
        if isinstance(batch_size, int):
            bs = batch_size
        elif hasattr(batch_size, "keys"):
            bs = batch_size["train"]
        else:
            raise ValueError("Variable batch_size must be type int or dict.")
        num_train_samples = min(num_steps * bs * num_devices, self.nn.dr.buffer_size)
        return num_train_samples
