"""CIDS computational intelligence library"""
# FIXME: effective batch size depends on number of devices (iterator)
#       TODO: shard before shuffle :(
#       TODO: unique shard of filenames for each worker
# TODO: easier exchange of preprocessing pipeline
# TODO: docstrings for training functions
# ENHANCEMENT: gradients stage on CPU? copy stage?
# ENHANCEMENT: Ensured pruning and reloading on CPU
# ENHANCEMENT: check optimizer options (locking, gating, ...)
# ENHANCEMENT: valid and train paths saved in constants instead of fed
# ENHANCEMENT: Move auto-train-infer mode to to __call__?
# ENHANCEMENT: Split pre and postprocessing off for multi-inheritance?
# ENHANCEMENT: Variable model size with inherited weights (growing models)
# ENHANCEMENT: Accelerate inference?
# ENHANCEMENT: Store model and model params (pickle? dill? json? manually?)
from cids.legacy.decorators import *
from cids.legacy.nn.base_neural_network import BaseNeuralNetwork
from cids.legacy.nn.modules import *
from cids.tensorflow.utility import *


class SingleGPUNeuralNetwork(BaseNeuralNetwork):
    def __init__(
        self,
        data_shape,
        input_idx,
        output_idx,
        layers,
        batch_size=1000,
        init_learning_rate=0.01,
        num_epochs=None,
        decay_rate=1.0,
        decay_steps=0,
        keep_prob=1.0,
        grad_clip=None,
        use_batch_norm=False,
        data_format="NF",
        dtype=tf.float32,
        dr=None,
        idp=None,
        odp=None,
        optimizer=None,
        prep_device=None,
        comp_device=None,
        name=None,
        host=None,
        writer=None,
        result_dir="RESULTS/",
        sessrun_kwargs=None,
    ):
        """A fully-connected, feed-forward neural network for up to 1 GPU.

        Args:
            data_shape:     Shape of the data tensor
            input_idx:      Indices of data for the input
            output_idx:     Indices of data for the output
            layers:         A tuple of layer sizes
            batch_size:     Number of samples in batch
            init_learning_rate:  The initial learning rate
            num_epochs:     The maximum number of epochs
            decay_rate:     The learning rate reduction factor over decay_steps
            decay_steps:    The number of steps over which decay_rate is applied
            keep_prob:      The dropout keep probability during training
            grad_clip:      The value used for L2 gradient clipping
            use_batch_norm: Boolean: use batch norm if available
            data_format:    Data format used ("NF", NSF", or "NHWC")
                                 N:         batch axis,
                                 S/D:       sequence axis / depth axis
                                 H/W/X/Y:   spatial axes (height, width, x, y)
                                 F/C:       feature or channel axis
            dtype:          Data type (defaults to tf.float32)
            dr:             A Datareader object (None uses default)
            idp:            The input DataProcessor (None uses default)
            odp:            The output DataProcessor (None uses default)
            optimizer:      The optimizer (class) used (None uses default)
            prep_device:    The comp_devices used for preprocessing
                            (default: cpu[0])
            comp_device:    The comp_devices used for training (default: gpu[0])
            name:          The shared name scope (None defaults to class name)
            host:           Host name (None defaults to environment hostname)
            result_dir:    Directory to store results, summaries and
                            checkpoints
            sessrun_kwargs: Keyword arguments for tensorflow session.run()

        """
        super().__init__(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=init_learning_rate,
            num_epochs=num_epochs,
            decay_rate=decay_rate,
            decay_steps=decay_steps,
            keep_prob=keep_prob,
            grad_clip=grad_clip,
            use_batch_norm=use_batch_norm,
            data_format=data_format,
            dtype=dtype,
            dr=dr,
            idp=idp,
            odp=odp,
            optimizer=optimizer,
            name=name,
            host=host,
            writer=writer,
            result_dir=result_dir,
            sessrun_kwargs=sessrun_kwargs,
        )
        self._setup_devices(prep_device=prep_device, comp_device=comp_device)

    def _setup_devices(self, prep_device=None, comp_device=None):
        # All devices
        self.cpus = get_available_cpus()
        self.gpus = get_available_gpus()
        # Preprocessing
        if not prep_device:
            # Select compute_device
            self.prep_device = self.cpus[0]
        else:
            self.prep_device = prep_device
        # if self.VERBOSITY >= 2:
        #     print(self.INDENT + self.name
        #           + ": Preprocessing device:", self.prep_device)
        # Computation
        if not comp_device:
            # Select compute_device
            if len(self.gpus) >= 1:
                self.comp_device = self.gpus[0]
            else:
                self.comp_device = self.cpus[0]
        else:
            self.comp_device = comp_device
        # if self.VERBOSITY >= 2:
        #     print(self.INDENT + self.name
        #           + ": Compute device:", self.comp_device)

    def build(self):
        self.preprocess
        with tf.device(self.comp_device):
            with tf.variable_scope(self.name):
                self.forward_pass
                self.loss
                self.gradients
                self.train
                with tf.variable_scope("metrics"):
                    self.mean_absolute_error
        self.postprocess
        self.infer
        # Setup saver
        self.saver = tf.train.Saver()

    @variable_scoped_lazy_property
    def data_pipeline(self):
        with tf.device(self.prep_device):
            return self._data_pipeline()

    @variable_scoped_lazy_property
    def preprocess(self):
        with tf.device(self.prep_device):
            batch = self._preprocess(self.data_pipeline)
        # Staging
        with tf.device(self.prep_device):
            # Create gpu copy staging area
            self.gpu_copy_staging_area = tf.contrib.staging.StagingArea(
                [self.dtype, self.dtype],
                shapes=[[None] + self.input_shape[1:], [None] + self.output_shape[1:]],
            )
            self.staging_areas.append(self.gpu_copy_staging_area)
            # Do not put anything into stage when evaluating
            put_op = tf.cond(
                tf.logical_or(self.BYPASS, tf.logical_not(self.TRAIN)),
                lambda: tf.no_op(),
                lambda: self.gpu_copy_staging_area.put(batch),
            )
            self.staging_ops.append(put_op)
            # Summarize staging
            if self.DEBUG:
                with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                    tf.contrib.summary.scalar(
                        (self.name + ".gpu_copy_staging_area.size: "),
                        self.gpu_copy_staging_area.size(),
                        step=self.global_step,
                    )
        with tf.device(self.comp_device):
            # Create gpu computation staging area
            self.gpu_compute_staging_area = tf.contrib.staging.StagingArea(
                [self.dtype, self.dtype],
                shapes=[[None] + self.input_shape[1:], [None] + self.output_shape[1:]],
            )
            self.staging_areas.append(self.gpu_compute_staging_area)
            # Do not put anything into stage when evaluating
            put_op = tf.cond(
                tf.logical_or(self.BYPASS, tf.logical_not(self.TRAIN)),
                lambda: tf.no_op(),
                lambda: self.gpu_compute_staging_area.put(
                    self.gpu_copy_staging_area.get()
                ),
            )
            self.staging_ops.append(put_op)
            # Summarize staging
            if self.DEBUG:
                with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                    tf.contrib.summary.scalar(
                        (self.name + ".gpu_compute_staging_area.size: "),
                        self.gpu_compute_staging_area.size(),
                        step=self.global_step,
                    )
            # Bypass stating area during evaluation and explicit bypassing
            batch = tf.cond(
                tf.logical_or(self.BYPASS, tf.logical_not(self.TRAIN)),
                lambda: list(batch),
                lambda: self.gpu_compute_staging_area.get(),
            )
        # Add summaries
        with self.writer.as_default(), self._record_summary_if(self.TRAIN):
            tf.contrib.summary.scalar("count", self.idp.count, step=self.global_step)
            tf.contrib.summary.histogram(
                "input_mean", self.idp.mean, step=self.global_step
            )
            tf.contrib.summary.histogram(
                "output_mean", self.odp.mean, step=self.global_step
            )
            tf.contrib.summary.histogram("x", batch[0], step=self.global_step)
            tf.contrib.summary.histogram("y", batch[1], step=self.global_step)
            for fi in range(self.input_shape[self.feature_axis]):
                tf.contrib.summary.histogram(
                    f"x_feature_{fi:d}",
                    tf.gather(batch[0], [fi], axis=-1),
                    step=self.global_step,
                )
            for fi in range(self.output_shape[self.feature_axis]):
                tf.contrib.summary.histogram(
                    f"y_feature_{fi:d}",
                    tf.gather(batch[1], [fi], axis=-1),
                    step=self.global_step,
                )
        return batch

    @variable_scoped_lazy_property
    def forward_pass(self):
        return self._forward_pass(*self.preprocess)

    @variable_scoped_lazy_property
    def loss(self):
        _, y, y_ = self.forward_pass
        l = self._loss(y, y_)
        tf.losses.add_loss(l)
        # Add summary
        with self.writer.as_default(), self._record_summary_if(self.TRAIN):
            tf.contrib.summary.scalar("loss_train", l, step=self.global_step)
        with self.writer.as_default(), self._record_summary_if(
            tf.logical_not(self.TRAIN)
        ):
            tf.contrib.summary.scalar("loss_valid", l, step=self.global_step)
        return l

    @variable_scoped_lazy_property
    def gradients(self):
        grads_and_vars = self._gradients(self.loss)
        if self.grad_clip:
            # Clip gradients
            grads_and_vars = [
                (tf.clip_by_norm(grad, self.grad_clip), var)
                for grad, var in grads_and_vars
            ]
        # Add gradient summaries
        for grad, var in grads_and_vars:
            if grad is not None:
                with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                    tf.contrib.summary.histogram(
                        var.op.name.replace(self.name, ""), grad, step=self.global_step
                    )
        return grads_and_vars

    @variable_scoped_lazy_property
    def train(self):
        train_op = self._train(self.gradients)
        # Add variable summaries
        for var in tf.trainable_variables():
            with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                tf.contrib.summary.histogram(
                    var.op.name.replace(self.name, ""), var, step=self.global_step
                )
        # Add summaries for layer updates
        if self.DEBUG:
            update_vars = [
                var
                for var in tf.global_variables()
                if ("gradient" in var.name or "Adam" in var.name)
            ]
            for uv in update_vars:
                uv_abs = tf.abs(uv)
                mean_tag = uv.name + "/abs/mean"
                uv_mean = tf.reduce_mean(uv_abs)
                max_tag = uv.name + "/abs/max"
                uv_max = tf.reduce_max(uv_abs)
                with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                    tf.contrib.summary.scalar(mean_tag, uv_mean, step=self.global_step)
                    tf.contrib.summary.scalar(max_tag, uv_max, step=self.global_step)
        return train_op

    @variable_scoped_lazy_property
    def postprocess(self):
        return self._postprocess(*self.forward_pass)

    @variable_scoped_lazy_property
    def infer(self):
        return self._infer(*self.forward_pass)

    @variable_scoped_lazy_property
    def predict(self):
        return self._infer(*self.forward_pass, prediction_only=True)

    def sess_initialize(self, sess, feed_dict, mode="train", checkpoint=None):
        super().sess_initialize(sess, feed_dict, mode=mode, checkpoint=checkpoint)
        if mode == "train":
            # Warm up StagingArea (IMPORTANT: must be after loading checkpoint!)
            for i in range(len(self.staging_ops)):
                sess.run(self.staging_ops[: i + 1], feed_dict=feed_dict)
            # Group staging operations together
            self.stage = tf.group(*self.staging_ops)


class NeuralNetworkTower(BaseNeuralNetwork):
    def __init__(self, foundation, index, comp_device, name=None):
        """A tower for a MultiGPU Neural network.

        Args:
            foundation:     A Multi-GPU neural network
            index:          The index of this tower in foundation"s tower list
            comp_device:    The comp_devices used for computation
            name:          The shared name scope (None defaults to class
                             name plus device name)
        """
        # Read inputs
        self.foundation = foundation
        # Set flags
        self.VERBOSITY = 2
        self.BYPASS = foundation.BYPASS
        self.TRAIN = foundation.TRAIN
        # Properties
        self.input = foundation.preprocessors[index][0]
        self.target = foundation.preprocessors[index][1]
        self.input_shape = self.input.get_shape().as_list()
        self.output_shape = self.target.get_shape().as_list()
        self.data_format = foundation.data_format
        axes = read_axes(self.data_format)
        self.batch_axis = axes[0]
        self.feature_axis = axes[1]
        self.sequence_axis = axes[2]
        self.spatial_axes = axes[3]
        self.layers = foundation.layers
        self.keep_prob = foundation.keep_prob
        self.grad_clip = foundation.grad_clip
        self.use_batch_norm = foundation.use_batch_norm
        self.task = foundation.task
        self.num_categories = foundation.num_categories
        self.loss_function = foundation.loss_function
        # Select Optimizer
        self.optimizer = foundation.optimizer
        # Setup name name
        if name is None:
            # TODO: Names must be equal, else variables generated on GPU cannot
            #       be read from gpu only models
            # self.scope = ((self.__class__.__name__ + "_" + comp_device)
            #                 .replace(":", "")
            #                 .replace("/", ""))
            self.scope = self.__class__.__name__
        else:
            self.scope = name
        self.dtype = foundation.dtype
        self.state = {}
        # Create list for summaries
        self.summaries = []
        self.valid_summaries = []
        # Create init ops
        self.prep_device = foundation.prep_devices[0]
        self._setup_devices(self.prep_device, comp_device)

    @property
    def writer(self):
        return self.foundation.writer

    @property
    def global_step(self):
        return self.foundation.global_step

    def _setup_devices(self, prep_device=None, comp_device=None):
        # Setup device
        self.prep_device = prep_device
        self.comp_device = comp_device
        # Setup staging ops
        self.staging_areas = []
        self.staging_ops = []

    def build(self):
        with tf.name_scope(self.scope):
            with tf.device(self.prep_device):
                self.copy_stage
            with tf.device(self.comp_device):
                self.compute_stage
                self.forward_pass
                self.loss
                self.gradients
                self.gradients_stage

    @name_scoped_lazy_property
    def copy_stage(self):
        """Create gpu copy staging area for this gpu on cpu."""
        x = self.input
        y = self.target
        self.gpu_copy_staging_area = tf.contrib.staging.StagingArea(
            [self.dtype, self.dtype],
            shapes=[[None] + self.input_shape[1:], [None] + self.output_shape[1:]],
        )
        self.staging_areas.append(self.gpu_copy_staging_area)
        # Fill staging area when not evaluating
        put_op = tf.cond(
            tf.logical_or(self.BYPASS, tf.logical_not(self.TRAIN)),
            lambda: tf.no_op(),
            lambda: self.gpu_copy_staging_area.put([x, y]),
        )
        self.staging_ops.append(put_op)
        # Get batch from staging area
        x, y = tf.cond(
            tf.logical_or(self.BYPASS, tf.logical_not(self.TRAIN)),
            lambda: [x, y],
            lambda: self.gpu_copy_staging_area.get(),
        )
        if self.DEBUG:
            with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                tf.contrib.summary.scalar(
                    (self.scope + ".gpu_copy_staging_area.size: "),
                    self.gpu_copy_staging_area.size(),
                    step=self.global_step,
                )
        return x, y

    @name_scoped_lazy_property
    def compute_stage(self):
        """Create gpu computation staging area for this gpu."""
        x, y = self.copy_stage
        self.gpu_compute_staging_area = tf.contrib.staging.StagingArea(
            [self.dtype, self.dtype],
            shapes=[[None] + self.input_shape[1:], [None] + self.output_shape[1:]],
        )
        self.staging_areas.append(self.gpu_compute_staging_area)
        # Fill staging area when not evaluating
        put_op = tf.cond(
            tf.logical_or(self.BYPASS, tf.logical_not(self.TRAIN)),
            lambda: tf.no_op(),
            lambda: self.gpu_compute_staging_area.put([x, y]),
        )
        self.staging_ops.append(put_op)
        # Bypass staging areas during evaluation and explicit bypassing
        x, y = tf.cond(
            tf.logical_or(self.BYPASS, tf.logical_not(self.TRAIN)),
            lambda: [x, y],
            lambda: self.gpu_compute_staging_area.get(),
        )
        if self.DEBUG:
            with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                tf.contrib.summary.scalar(
                    (self.scope + ".gpu_compute_staging_area.size: "),
                    self.gpu_compute_staging_area.size(),
                    step=self.global_step,
                )
        return x, y

    @name_scoped_lazy_property
    def forward_pass(self):
        x, y = self.compute_stage
        return self._forward_pass(x, y)

    @name_scoped_lazy_property
    def loss(self):
        _, y, y_ = self.forward_pass
        l = self._loss(y, y_)
        tf.losses.add_loss(l)
        with self.writer.as_default(), self._record_summary_if(self.TRAIN):
            tf.contrib.summary.scalar("loss_train", l, step=self.global_step)
        with self.writer.as_default(), self._record_summary_if(
            tf.logical_not(self.TRAIN)
        ):
            tf.contrib.summary.scalar("loss_valid", l, step=self.global_step)
        return l

    @name_scoped_lazy_property
    def gradients(self):
        return self._gradients(self.loss)

    @name_scoped_lazy_property
    def gradients_stage(self):
        """Create gradients staging area for this gpu"""
        # Split gradients and variables
        grads_and_vars = self.gradients
        grads, vars = zip(*grads_and_vars)
        # Setup staging area
        grad_dtypes = [grad.dtype for grad in grads]
        grad_shapes = [grad.shape for grad in grads]
        self.gpu_gradients_staging_area = tf.contrib.staging.StagingArea(
            grad_dtypes, grad_shapes
        )
        self.staging_areas.append(self.gpu_gradients_staging_area)
        # Fill staging area
        put_op = tf.cond(
            tf.logical_or(self.BYPASS, tf.logical_not(self.TRAIN)),
            lambda: tf.no_op(),
            lambda: self.gpu_gradients_staging_area.put(grads),
        )
        self.staging_ops.append(put_op)
        # Get from staging area
        grads = tf.cond(
            tf.logical_or(self.BYPASS, tf.logical_not(self.TRAIN)),
            lambda: list(grads),
            lambda: self.gpu_gradients_staging_area.get(),
        )
        if self.DEBUG:
            with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                tf.contrib.summary.scalar(
                    (self.scope + ".gpu_gradients_staging_area.size: "),
                    self.gpu_gradients_staging_area.size(),
                    step=self.global_step,
                )
        # Merge parameters and variables
        grads_and_vars = list(zip(grads, vars))
        # Bypass staging areas during evaluation and explicit bypassing
        # grads_and_vars = tf.cond(
        #   tf.logical_or(self.BYPASS, tf.logical_not(self.TRAIN)),
        #   lambda: self.gradients,
        #   lambda: grads_and_vars)
        return grads_and_vars


class NeuralNetwork(BaseNeuralNetwork):

    Tower = NeuralNetworkTower

    def __init__(
        self,
        data_shape,
        input_idx,
        output_idx,
        layers,
        batch_size=1000,
        init_learning_rate=0.01,
        num_epochs=None,
        decay_rate=1.0,
        decay_steps=0,
        keep_prob=1.0,
        grad_clip=None,
        use_batch_norm=False,
        data_format="NF",
        dtype=tf.float32,
        dr=None,
        idp=None,
        odp=None,
        optimizer=None,
        prep_devices=None,
        comp_devices=None,
        name=None,
        host=None,
        writer=None,
        result_dir="RESULTS/",
        sessrun_kwargs=None,
    ):
        """A fully-connected, feed-forward neural network for multiple GPUs.

        Args:
            data_shape:     Shape of the data tensor
            batch_size:     Number of samples in batch
            input_idx:      Indices of data for the input
            output_idx:     Indices of data for the output
            layers:         The layers of the model
            init_learning_rate:  The initial learning rate
            num_epochs:     The maximum number of epochs
            decay_rate:     The learning rate reduction factor over decay_steps
            decay_steps:    The number of steps over which decay_rate is applied
            keep_prob:      Dropout keep probability
            grad_clip:      The L2-Norm value used for gradient clipping
            use_batch_norm: Boolean: Use batch norm in layers?
            data_format:    Data format used ("NF", NSF", or "NCHW")
                                 N:         batch axis,
                                 S/D:       sequence axis / depth axis
                                 H/W/X/Y:   spatial axes (height, width, x, y)
                                 F/C:       feature or channel axis
            dtype:          Data type (defaults to tf.float32)
            dr:             A Datareader object (None uses default)
            idp:            The input DataProcessor (None uses default)
            odp:            The output DataProcessor (None uses default)
            optimizer:      The optimizer used for training (None uses default)
            prep_devices:   The comp_devices used for preprocessing
                            (default: cpus[0:1])
            comp_devices:   The comp_devices used for training (default: gpus)
            name:          The shared name scope (None defaults to class name)
            host:           Host name (None defaults to environment hostname)
            result_dir:    Directory to store results, summaries and
                            checkpoints
            sessrun_kwargs: Keyword arguments for tensorflow session.run()

        """
        super().__init__(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=init_learning_rate,
            num_epochs=num_epochs,
            decay_rate=decay_rate,
            decay_steps=decay_steps,
            keep_prob=keep_prob,
            grad_clip=grad_clip,
            use_batch_norm=use_batch_norm,
            data_format=data_format,
            dtype=dtype,
            dr=dr,
            idp=idp,
            odp=odp,
            optimizer=optimizer,
            name=name,
            host=host,
            writer=writer,
            result_dir=result_dir,
            sessrun_kwargs=sessrun_kwargs,
        )
        self._setup_devices(prep_devices=prep_devices, comp_devices=comp_devices)

    def _setup_devices(self, prep_devices=None, comp_devices=None):
        # All devices
        self.cpus = get_available_cpus()
        self.gpus = get_available_gpus()
        # Preprocessing
        if not prep_devices:
            # Select compute_device
            self.prep_devices = self.cpus
        else:
            self.prep_devices = prep_devices
        # if self.VERBOSITY >= 2:
        #     print(self.INDENT + self.name
        #           + ": Preprocessing devices:", self.prep_devices)
        # Computation
        if not comp_devices:
            # Select compute_device
            if len(self.gpus) >= 1:
                self.comp_devices = self.gpus
            else:
                self.comp_devices = self.cpus
        else:
            self.comp_devices = comp_devices
        # if self.VERBOSITY >= 2:
        #     print(self.INDENT + self.name
        #           + ": Compute devices:", self.comp_devices)

    def build(self):
        self.data_pipeline
        with tf.variable_scope(self.name):
            # GPU forward pass
            self.preprocessors = []
            self.towers = []
            with tf.variable_scope("towers"):
                for i, device in enumerate(self.comp_devices):
                    self.preprocessors.append(tuple(self.preprocess()))
                    tower = self.Tower(self, i, device)
                    tower.build()
                    self.towers.append(tower)
                    # Enforces reusing variables
                    tf.get_variable_scope().reuse_variables()
            # Collect all staging areas
            tower_staging_areas = [t.staging_areas for t in self.towers]
            self.staging_areas = [sa for sas in tower_staging_areas for sa in sas]
            # Sort staging ops by location in each tower
            tower_staging_ops = [t.staging_ops for t in self.towers]
            self.staging_ops = [op for ops in zip(*tower_staging_ops) for op in ops]
            # Own forward pass (e.g. for inference or validation)
            self.forward_pass  # calls tower 0
            self.loss  # calls tower 0
            # Optimize
            self.gradients  # brings the towers together
            self.train  # updates the variables across all towers
            # with tf.variable_scope("metrics"):
            #     self.mean_absolute_error
        # Postprocess
        self.postprocess
        self.infer
        # Setup saver
        self.saver = tf.train.Saver()

    @name_scoped_lazy_property
    def data_pipeline(self):
        with tf.device(self.prep_devices[0]):
            iterator = self._data_pipeline()
        return iterator

    @name_scoped_method  # need to call this once for each tower!
    def preprocess(self):
        with tf.device(self.prep_devices[0]):
            batch = self._preprocess(self.data_pipeline)
        # Add summaries
        with self.writer.as_default(), self._record_summary_if(self.TRAIN):
            tf.contrib.summary.scalar(
                self.learning_rate.name, self.learning_rate, step=self.global_step
            )
            tf.contrib.summary.scalar("count", self.idp.count, step=self.global_step)
            tf.contrib.summary.histogram(
                "input_mean", self.idp.mean, step=self.global_step
            )
            tf.contrib.summary.histogram(
                "output_mean", self.odp.mean, step=self.global_step
            )
            tf.contrib.summary.histogram("x", batch[0], step=self.global_step)
            tf.contrib.summary.histogram("y", batch[1], step=self.global_step)
            for fi in range(self.input_shape[self.feature_axis]):
                tf.contrib.summary.histogram(
                    f"x_feature_{fi:d}",
                    tf.gather(batch[0], [fi], axis=-1),
                    step=self.global_step,
                )
            for fi in range(self.output_shape[self.feature_axis]):
                tf.contrib.summary.histogram(
                    f"y_feature_{fi:d}",
                    tf.gather(batch[1], [fi], axis=-1),
                    step=self.global_step,
                )
        return batch

    @name_scoped_lazy_property
    def forward_pass(self):
        return self.towers[0].forward_pass

    @name_scoped_lazy_property
    def loss(self):
        return self.towers[0].loss

    @name_scoped_lazy_property
    def gradients(self):
        # Calculate tower gradients
        tower_grads_and_vars = [t.gradients_stage for t in self.towers]
        # tower_grads:
        #   A list (tower) of lists (gradients and variables in tower) of tuples
        #   (grad, var).
        #
        #     [[(t0g0, t0v0),
        #       (t0g1, t0v1), ...],
        #      [(t1g0, t1v0),
        #       (t1g1, t1v1), ...], ...]

        # Average across towers
        grads_and_vars = []
        for grad_and_var in zip(*tower_grads_and_vars):
            # grad_and_vars:
            #   Tuple of tuples for each gradient/variable combination across
            #   all towers
            #
            #     ((t0g0, t0v0), (t1g0, t1v0))

            # Concatenate all the values of each gradient across all towers
            if grad_and_var[0][0] is not None:
                grads = [tf.expand_dims(g, axis=0) for g, _ in grad_and_var]
                grads = tf.concat(grads, 0)
                # Calculate mean of all gradients along the tower axis
                avg_grad = tf.reduce_mean(grads, axis=0)
                # Join with shared variable again
                avg_grad_and_var = (avg_grad, grad_and_var[0][1])
                grads_and_vars.append(avg_grad_and_var)
        # Clip gradients
        if self.grad_clip:
            grads_and_vars = [
                (tf.clip_by_norm(grad, self.grad_clip), var)
                for grad, var in grads_and_vars
            ]
        # Add gradient summaries
        for grad, var in grads_and_vars:
            if grad is not None:
                with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                    tf.contrib.summary.histogram(
                        var.op.name.replace(self.name, ""), grad, step=self.global_step
                    )
        return grads_and_vars

    @name_scoped_lazy_property
    def train(self):
        # Calculate moving average
        variable_averages = tf.train.ExponentialMovingAverage(0.99, self.global_step)
        variables_averages_op = variable_averages.apply(tf.trainable_variables())
        # Apply gradients
        apply_gradients_op = self._train(self.gradients)
        # Add variable summaries
        for var in tf.trainable_variables():
            with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                tf.contrib.summary.histogram(
                    var.op.name.replace(self.name, ""), var, step=self.global_step
                )
        # Add summaries for layer updates
        if self.DEBUG:
            update_vars = [
                var
                for var in tf.global_variables()
                if ("gradient" in var.name or "Adam" in var.name)
            ]
            for uv in update_vars:
                uv_abs = tf.abs(uv)
                mean_tag = uv.name + "/abs/mean"
                uv_mean = tf.reduce_mean(uv_abs)
                max_tag = uv.name + "/abs/max"
                uv_max = tf.reduce_max(uv_abs)
                with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                    tf.contrib.summary.scalar(mean_tag, uv_mean, step=self.global_step)
                    tf.contrib.summary.scalar(max_tag, uv_max, step=self.global_step)
        return tf.group(apply_gradients_op, variables_averages_op)

    @name_scoped_lazy_property
    def postprocess(self):
        return self._postprocess(*self.forward_pass)

    @name_scoped_lazy_property
    def infer(self):
        return self._infer(*self.forward_pass)

    @name_scoped_lazy_property
    def predict(self):
        return self._infer(*self.forward_pass, prediction_only=True)

    def sess_initialize(self, sess, feed_dict, mode="train", checkpoint=None):
        super().sess_initialize(sess, feed_dict, mode=mode, checkpoint=checkpoint)
        if mode == "train":
            # Warm up StagingArea (IMPORTANT: must be after loading checkpoint!)
            for i in range(len(self.staging_ops)):
                sess.run(self.staging_ops[: i + 1], feed_dict=feed_dict)
            # Group staging operations together
            self.stage = tf.group(*self.staging_ops)
