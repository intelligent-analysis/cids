"""CIDS computational intelligence library"""
import copy
import os
import timeit
from collections import OrderedDict

import tensorflow as tf
from tensorflow.python.keras.utils import generic_utils
from tensorflow.python.ops.summary_ops_v2 import _record_summaries
from tqdm.autonotebook import tqdm

from cids.legacy.data.dataprocessor import DataProcessor
from cids.legacy.data.datareader import DataReader
from cids.legacy.decorators import variable_scoped_lazy_property
from cids.legacy.nn.modules import fc_out
from cids.legacy.nn.modules import fc_relu
from cids.legacy.nn.modules import fc_relu_residual
from cids.legacy.nn.modules import fc_sigmoid
from cids.legacy.nn.modules import fc_tanh
from cids.tensorflow.utility import read_axes


class BaseNeuralNetwork:

    DEBUG = False
    INDENT = ""

    def __init__(
        self,
        data_shape,
        input_idx,
        output_idx,
        layers,
        batch_size=1000,
        init_learning_rate=0.01,
        num_epochs=None,
        decay_rate=1.0,
        decay_steps=0,
        keep_prob=1.0,
        grad_clip=None,
        use_batch_norm=False,
        data_format="NF",
        dtype=tf.float32,
        dr=None,
        idp=None,
        odp=None,
        optimizer=None,
        name=None,
        host=None,
        result_dir="RESULTS/",
        writer=None,
        sessrun_kwargs=None,
        task="regression",
    ):
        """Base class for a fully-connected, feed-forward neural network.

        Args:
            data_shape:     Shape of the data tensor
            input_idx:      Indices of data for the input
            output_idx:     Indices of data for the output
            layers:         The layers and layer sizes
            batch_size:     Number of samples in batch
            init_learning_rate:  The initial learning rate
            num_epochs:     The maximum number of epochs
            decay_rate:     The learning rate reduction factor over decay_steps
            decay_steps:    The number of steps over which decay_rate is applied
            keep_prob:      The dropout keep probability during training
            grad_clip:      The value used for L2 gradient clipping
            use_batch_norm: Boolean: Use batch norm in layers?
            data_format:    Data format used ("NF", NSF", or "NCHW")
                                 N:         batch axis,
                                 S/D:       sequence axis / depth axis
                                 H/W/X/Y:   spatial axes (height, width, x, y)
                                 F/C:       feature or channel axis
            dtype:          Data type (defaults to tf.float32)
            dr:             A Datareader object (None uses default)
            idp:            The input DataProcessor (None uses default)
            odp:            The output DataProcessor (None uses default)
            optimizer:      The optimizer (class) used (None uses default)
            name:          The shared name scope (None defaults to class name)
            host:           Host name (None defaults to environment hostname)
            result_dir:    Directory to store results, summaries and
                            checkpoints (defaults to "RESULTS/" + scope)
            sessrun_kwargs: Keyword arguments for tensorflow session.run()

        """
        self.DEBUG = False
        # Properties
        axes = read_axes(data_format)
        self.batch_axis = axes[0]
        self.feature_axis = axes[1]
        self.sequence_axis = axes[2]
        self.spatial_axes = axes[3]
        self.data_shape = data_shape
        self.data_format = data_format
        self.dtype = dtype
        self.input_idx = input_idx
        self.output_idx = output_idx
        self.input_shape = list(self.data_shape)
        self.input_shape[self.feature_axis] = len(input_idx)
        self.output_shape = list(self.data_shape)
        self.output_shape[self.feature_axis] = len(output_idx)
        self.batch_size = batch_size
        self.num_epochs = num_epochs
        self.layers = layers
        self.keep_prob = keep_prob
        self.grad_clip = grad_clip
        self.use_batch_norm = use_batch_norm
        self.task = task
        self.num_categories = None
        self.loss_function = "mean_square_error"
        # Create init ops
        self.state = {}
        self.staging_areas = []
        self.staging_ops = []
        self.stage = None
        # Setup name and host name
        if name is None:
            self.name = self.__class__.__name__
        else:
            self.name = name
        if host is None:
            self.host = os.getenv("HOSTNAME", "win").split(".")[0].split("-")[0]
        else:
            self.host = host
        # Setup
        self.model_name = self.assemble_model_name(self.name, self.host, layers)
        self.result_dir = os.path.join(result_dir, self.name)
        self._create_dir(self.result_dir)
        # Setup summaries
        self.writer = writer or tf.contrib.summary.create_file_writer(
            self.summary_dir, flush_millis=10000
        )
        with self.writer.as_default():
            tf.contrib.summary.always_record_summaries()
        # Create DataReaders and DataProcessors
        self.dr = dr or DataReader(
            self.data_shape, self.data_format, self.batch_size, slice_tensors=True
        )
        self.feed_dict = dict()
        self.mode = "train"
        # Reduce size of input_shape and output_shape when chunking
        if self.sequence_axis and self.dr.chunk_size:
            self.input_shape[self.sequence_axis] = self.dr.chunk_size
            self.output_shape[self.sequence_axis] = self.dr.chunk_size
        self.idp = idp or DataProcessor(
            self.input_shape, data_format=data_format, scope="InputDataProcessor"
        )
        self.odp = odp or DataProcessor(
            self.output_shape, data_format=data_format, scope="OutputDataProcessor"
        )
        self.train_freeze = False  # Keep same as DataProcessor.freeze default
        # Control variables and flags
        self.VERBOSITY = 2
        with tf.variable_scope("CONTROL"):
            self.TRAIN = tf.placeholder_with_default(True, (), name="TRAIN")
            self.BYPASS = tf.placeholder_with_default(False, (), name="BYPASS")
            self.global_step = tf.get_variable(
                "global_step",
                [],
                dtype=tf.int64,
                initializer=tf.constant_initializer(0),
                trainable=False,
            )
            self.init_learning_rate = tf.placeholder_with_default(
                init_learning_rate, shape=[], name="init_learning_rate"
            )
            self.learning_rate = tf.placeholder_with_default(
                tf.train.exponential_decay(
                    self.init_learning_rate,
                    self.global_step,
                    decay_steps=decay_steps,
                    decay_rate=decay_rate,
                    staircase=False,
                ),
                shape=[],
                name="learning_rate",
            )
        # Add summary for learning rate
        with self.writer.as_default(), self._record_summary_if(self.TRAIN):
            tf.contrib.summary.scalar(
                self.learning_rate.name, self.learning_rate, step=self.global_step
            )
        if optimizer is None:
            self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
        else:
            self.optimizer = optimizer(learning_rate=self.learning_rate)
        # Session control
        if sessrun_kwargs is None:
            self.sessrun_kwargs = {}
        else:
            self.sessrun_kwargs = sessrun_kwargs
        self.init_time = -1.0

    @staticmethod
    def _create_dir(dir):
        os.makedirs(dir, exist_ok=True)
        try:
            os.chmod(dir, 0o777)
        except PermissionError:
            pass

    @property
    def model_dir(self):
        model_dir = os.path.join(self.result_dir, self.model_name)
        self._create_dir(model_dir)
        return model_dir

    @property
    def summary_dir(self):
        summary_dir = os.path.join(self.model_dir, "summary")
        self._create_dir(summary_dir)
        return summary_dir

    @property
    def checkpoint_dir(self):
        checkpoint_dir = os.path.join(self.model_dir, "checkpoint")
        self._create_dir(checkpoint_dir)
        return checkpoint_dir

    @property
    def log_dir(self):
        log_dir = os.path.join(self.model_dir, "log")
        self._create_dir(log_dir)
        return log_dir

    @property
    def profile_dir(self):
        profile_dir = os.path.join(self.model_dir, "profile")
        self._create_dir(profile_dir)
        return profile_dir

    @property
    def tmp_dir(self):
        tmp_dir = os.path.join(self.model_dir, "tmp")
        self._create_dir(tmp_dir)
        return tmp_dir

    @property
    def plot_dir(self):
        plot_dir = os.path.join(self.model_dir, "plot")
        self._create_dir(plot_dir)
        return plot_dir

    @staticmethod
    def assemble_model_name(name, host, layers):
        layer_strings = [
            l.units
            if hasattr(l, "units")
            else l.layer.units
            if (hasattr(l, "layer") and hasattr(l.layer, "units"))
            else None
            if isinstance(l, tf.keras.layers.Layer)
            else l[1]
            if isinstance(l[1], int)
            else l[1][0]
            if (
                isinstance(l[1], (list, tuple))
                and len(l[1]) > 0
                and isinstance(l[1][0], int)
            )
            else l[2]["num_units"]
            if (len(l) > 1 and "num_units" in l[2].keys())
            # else l[0].__name__ if callable(l[0])
            else None
            for l in layers
        ]
        layer_strings = [ls for ls in layer_strings if ls not in (None, 0, -1)]
        if host is None:
            host = os.getenv("HOSTNAME", "win").split(".")[0].split("-")[0]
        return "-".join([name, host] + list(map(str, layer_strings)))

    def _setup_devices(self, prep_device=None, comp_device=None):
        """Select devices for preprocessing and computation."""
        raise NotImplementedError("Must be implemented in child class.")

    def build(self):
        """Build the computation graph."""
        raise NotImplementedError("Must be implemented in child class.")

    def _data_pipeline(self):
        """Read the input data and return a new batch.

        This method must provide potential placeholders for the input data.

        Returns:
            iterator:  An iterator that can be used to create a batch of data

        """
        if self.dr.src_type == "file":
            feed_shape = [None]
            blank_shape = (1,)
            self.blank = tf.constant("", dtype=tf.string, shape=blank_shape)
        else:
            feed_shape = list(self.dr.data_shape)
            feed_shape[self.batch_axis] = None
            if self.sequence_axis:
                feed_shape[self.sequence_axis] = None
            if "I" in self.dr.data_format:
                feed_shape[self.dr.data_format.index("I")] = None
            blank_shape = [d if d else 1 for d in feed_shape]
            self.blank = tf.zeros(dtype=self.dtype, shape=blank_shape)
        # Training pipeline
        with tf.name_scope("train_pipeline"):
            self.train_data = tf.placeholder_with_default(
                self.blank, shape=feed_shape, name="train_data"
            )
            self.train_dataset = self.dr.generate_batch_dataset(self.train_data)
        # Validation pipeline
        with tf.name_scope("valid_pipeline"):
            self.valid_data = tf.placeholder_with_default(
                self.blank, shape=feed_shape, name="valid_data"
            )
            self.valid_dataset = self.dr.generate_batch_dataset(
                self.valid_data, mode="valid"
            )
        # Test pipeline
        with tf.name_scope("test_pipeline"):
            self.test_data = tf.placeholder_with_default(
                self.blank, shape=feed_shape, name="test_data"
            )
            self.test_dataset = self.dr.generate_test_dataset(self.test_data)
        # Create iterator and initializers
        iterator_output_shape = list(copy.deepcopy(self.data_shape))
        if self.sequence_axis:
            iterator_output_shape[self.sequence_axis] = None
        self.iterator = tf.data.Iterator.from_structure(
            self.train_dataset.output_types, iterator_output_shape
        )
        self.train_init = self.iterator.make_initializer(self.train_dataset)
        self.valid_init = self.iterator.make_initializer(self.valid_dataset)
        self.test_init = self.iterator.make_initializer(self.test_dataset)
        return self.iterator

    @variable_scoped_lazy_property
    def data_pipeline(self):
        """Read the input data and return a new batch.

        Returns:
            batch:  A batch of data

        """
        raise NotImplementedError("Must be implemented in child class.")

    def _preprocess(self, iterator):
        """Preprocess the input data before a forward pass.

        Args:
            iterator:   An iterator across a dataset

        Returns:
            x:  The input batch Tensor
            y:  The target batch Tensor

        """
        # Read data
        batch = iterator.get_next()
        # Create data bypass for direct feeding
        partial_shape = list(self.data_shape)
        partial_shape[self.batch_axis] = None
        if self.sequence_axis and self.dr.chunk_size:
            partial_shape[self.sequence_axis] = self.dr.chunk_size
        if not hasattr(self, "train_feed"):
            # Ensure that only one train_feed (the first created) exists and
            # is not overwritten!
            self.train_feed = tf.placeholder_with_default(
                batch, partial_shape, name="train_feed"
            )
            self.valid_feed = tf.placeholder_with_default(
                batch, partial_shape, name="valid_feed"
            )
            batch = tf.cond(
                self.TRAIN, lambda: self.train_feed, lambda: self.valid_feed
            )
            self.feed = tf.placeholder_with_default(batch, partial_shape, name="feed")
            # Ensure shape is specified
            #    (requires num_features to be known and static)
            self.feed.set_shape(partial_shape)
        # Split
        self.x, self.y = self.idp.split_features(
            self.feed, self.input_idx, self.output_idx
        )
        # Normalize
        x = self.idp.online_normalize(self.x)
        if self.task == "regression":
            y = self.odp.online_normalize(self.y)
        elif self.task == "binary-classification":
            y = tf.identity(self.y)
        elif self.task == "categorical-classification":
            y = tf.identity(self.y)
        elif self.task == "sparse-categorical-classification":
            y = tf.one_hot(tf.cast(self.y, tf.int32), self.num_categories)
        else:
            raise ValueError("Unknown task: " + self.task)
        x = tf.cast(x, self.dtype)
        y = tf.cast(y, self.dtype)
        return x, y

    @variable_scoped_lazy_property
    def preprocess(self):
        """Optionally calculate and return the preprocessed input data.

        Returns:
            x:  The input batch Tensor
            y:  The target batch Tensor

        """
        raise NotImplementedError("Must be implemented in child class.")

    def _forward_pass(self, x, y, prediction_only=False):
        """Perform a forward pass to calculate predictions.

        Args:
            x: The input batch Tensor
            y: the target batch Tensor

        Returns:
            x:  The input batch Tensor
            y:  The target batch Tensor
            y_: The prediction batch Tensor

        """
        y_ = x
        # Build forward pass graph
        for i, layer in enumerate(self.layers):
            if isinstance(layer, tf.keras.layers.Layer):
                layer.built = False
                if generic_utils.has_arg(layer, "training"):
                    y_ = layer(y_, training=self.TRAIN)
                else:
                    y_ = layer(y_)
                continue
            layer_name = layer[0]
            layer_size = layer[1]
            if layer_size == -1:
                layer_size = self.output_shape[self.feature_axis]
            # Select cell or layer
            if layer_name == "fc_relu":
                y_ = fc_relu(
                    y_,
                    layer_size,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                )
            elif layer_name == "fc_relu_residual":
                y_ = fc_relu_residual(
                    y_,
                    layer_size,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                )
            elif layer_name == "fc_sigmoid":
                y_ = fc_sigmoid(
                    y_,
                    layer_size,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                )
            elif layer_name == "fc_tanh":
                y_ = fc_tanh(
                    y_,
                    layer_size,
                    keep_prob=self.keep_prob,
                    use_batch_norm=self.use_batch_norm,
                    is_training=self.TRAIN,
                )
            elif layer_name == "fc_out":
                y_ = fc_out(y_, layer_size)
            elif callable(layer[0]):  # is function
                args = copy.deepcopy(layer[1])
                kwargs = copy.deepcopy(layer[2])
                # If layer size is unknown asssume output shape
                if len(args) > 0:
                    if args[0] == -1:
                        args[0] = self.output_shape[self.feature_axis]
                if "keep_prob" not in kwargs.keys() and i == len(self.layers) - 1:
                    kwargs["keep_prob"] = self.keep_prob
                if "is_training" not in kwargs.keys():
                    kwargs["is_training"] = self.TRAIN
                # Create layer
                y_ = layer_name(y_, *args, **kwargs)
                if isinstance(y_, list):
                    y_, self.state[y_] = y_[0], y_[1:]
            else:
                raise ValueError("Unknown layer name:", layer_name)
            # Add summaries
            with self.writer.as_default(), self._record_summary_if(self.TRAIN):
                tf.contrib.summary.histogram("y_", y_, step=self.global_step)
            # Print layer shape
            if self.VERBOSITY >= 2:
                print(self.INDENT + f"Layer {i:03d}: output_shape:", y_.shape)
        with self.writer.as_default(), self._record_summary_if(self.TRAIN):
            for fi in range(self.output_shape[self.feature_axis]):
                tf.contrib.summary.histogram(
                    f"y__feature_{fi:d}", y_[..., fi], step=self.global_step
                )
        if prediction_only:
            return y_
        else:
            return x, y, y_

    @variable_scoped_lazy_property
    def forward_pass(self):
        """Optionally calculate and return predictions using a forward pass.

        Returns:
            x:  The input batch Tensor
            y:  The target batch Tensor
            y_: The prediction batch Tensor

        """
        raise NotImplementedError("Must be implemented in child class.")

    def _mask_differences_across_sequence(self, diff):
        try:
            output_sequence_mask = getattr(self, "output_sequence_mask")
            sequence_lengths = getattr(self, "sequence_lengths")
        except AttributeError:
            output_sequence_mask = None
            sequence_lengths = None
        # Apply reduction for masked (spatio-temporal) axes
        if (
            self.sequence_axis is not None
            and output_sequence_mask is not None
            and sequence_lengths is not None
        ):
            # Apply mask
            diff = tf.where(output_sequence_mask, diff, tf.zeros_like(diff))
            # Reduce
            diff = tf.reduce_sum(diff, axis=self.sequence_axis)
            # Average across sequence
            divisor = tf.cast(sequence_lengths, self.dtype)
            for df in self.data_format:
                if df not in "NS":
                    divisor = tf.expand_dims(divisor, axis=-1)
            diff = diff / divisor
        return diff

    def _mean_square_error(self, y, y_):
        """Calculate the mean square error of the predictions against targets.

        Args:
            y:  The target batch Tensor
            y_: The prediction batch Tensor

        Returns:
            loss:  The scalar loss
        """
        # Calculate loss per entry
        squared_diff = tf.squared_difference(y, y_)
        # Mask across sequences
        squared_diff = self._mask_differences_across_sequence(squared_diff)
        # Average across remaining axes
        mse = tf.reduce_mean(squared_diff)
        return mse

    def _mean_absolute_error(self, y, y_):
        """Calculate the mean absolute error.

        Args:
            y:   The target
            y_:  The prediction

        Returns:
            mae:   The mean absolute error
        """
        # Calculate error per entry
        diff = tf.subtract(y, y_)
        abs_diff = tf.abs(diff)
        # Mask across sequence
        abs_diff = self._mask_differences_across_sequence(abs_diff)
        # Average across remaining axes
        mae = tf.reduce_mean(abs_diff)
        return mae

    def _binary_cross_entropy(self, y, y_):
        """Calculate the binary cross entropy of the predictions against targets.

        Args:
            y:  The target batch Tensor
            y_: The prediction batch Tensor

        Returns:
            loss:  The scalar loss
        """
        # Calculate loss per entry
        ce = tf.keras.losses.binary_crossentropy(y, y_, from_logits=False)
        # Mask across sequences
        ce = self._mask_differences_across_sequence(ce)
        # Average across remaining axes
        mce = tf.reduce_mean(ce)
        return mce

    def _categorical_cross_entropy(self, y, y_):
        """Calculate the binary cross entropy of the predictions against targets.

        Args:
            y:  The target batch Tensor
            y_: The prediction batch Tensor

        Returns:
            loss:  The scalar loss
        """
        # Calculate loss per entry
        ce = tf.keras.losses.categorical_crossentropy(y, y_, from_logits=False)
        # Mask across sequences
        ce = self._mask_differences_across_sequence(ce)
        # Average across remaining axes
        mce = tf.reduce_mean(ce)
        return mce

    def _sparse_categorical_cross_entropy(self, y, y_):
        """Calculate the binary cross entropy of the predictions against targets.

        Args:
            y:  The target batch Tensor
            y_: The prediction batch Tensor

        Returns:
            loss:  The scalar loss
        """
        # Calculate loss per entry
        ce = tf.keras.losses.sparse_categorical_crossentropy(y, y_, from_logits=False)
        # Mask across sequences
        ce = self._mask_differences_across_sequence(ce)
        # Average across remaining axes
        mce = tf.reduce_mean(ce)
        return mce

    def _logcosh_error(self, y, y_):
        """Calculate the logarithmic hyperbolic cosine error.

        Args:
            y:   The target
            y_:  The prediction

        Returns:
            lce:   The logcosh error
        """
        # Calculate error per entry
        diff = tf.subtract(y, y_)
        log_cosh_diff = tf.log(tf.cosh(diff))
        # Mask across sequence
        log_cosh_diff = self._mask_differences_across_sequence(log_cosh_diff)
        # Average across remaining axes
        lce = tf.reduce_mean(log_cosh_diff)
        return lce

    def _loss(self, y, y_):
        """Calculate evaluation metrics of the predictions against targets.

        Args:
            y:  The target batch Tensor
            y_: The prediction batch Tensor

        Returns:
            loss:  The scalar loss

        """
        if self.loss_function == "mean_square_error":
            return self._mean_square_error(y, y_)
        elif self.loss_function == "binary_cross_entropy":
            return self._binary_cross_entropy(y, y_)
        elif self.loss_function == "sparse_categorical_cross_entropy":
            return self._sparse_categorical_cross_entropy(y, y_)
        else:
            raise ValueError("Unknown loss function: " + self.loss_function)

    @variable_scoped_lazy_property
    def loss(self):
        """Optionally calculate and return evaluation metrics of the predictions.

        Returns:
            loss:  The scalar loss

        """
        raise NotImplementedError("Must be implemented in child class.")

    def _gradients(self, loss):
        """Calculate the neural network gradients with respect to the loss.

        Args:
            loss:  The scalar loss

        Returns:
            grads_and_vars:   The gradients and associate variables

        """
        grads_and_vars = self.optimizer.compute_gradients(loss)
        return grads_and_vars

    @variable_scoped_lazy_property
    def gradients(self):
        """Optionally calculate and return the gradients of the neural network.

        Returns:
            grads_and_vars:   The gradients and associate variables

        """
        raise NotImplementedError("Must be implemented in child class.")

    def _train(self, grads_and_vars):
        """Train the neural network to improve predictions.

        Args:
            grads_and_vars:   The gradients and associate variables

        Returns:
            train_op:   Training operation

        """
        # Get update ops (e.g. for batch normalization) and execute them!
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            train_op = self.optimizer.apply_gradients(
                grads_and_vars, global_step=self.global_step
            )
        return train_op

    @variable_scoped_lazy_property
    def train(self):
        """Optionally calculate and return a training step of the neural network.

        Returns:
            train_op:   Training operation

        """
        raise NotImplementedError("Must be implemented in child class.")

    def _postprocess(self, x, y, y_, prediction_only=False):
        """Postprocess the output data after a forward pass.

        Args:
            x:  The inputs Tensor (scaled down)
            y:  The target batch Tensor (scaled down)
            y_: The prediction batch Tensor (scaled down)

        Returns:
            x:  The inputs Tensor (original scale)
            y:  The target batch Tensor (original scale)
            y_: The prediction batch Tensor (original scale)

        """
        if prediction_only:
            y_ = tf.identity(self.odp.online_rescale(y_), name="prediction_rescaled")
            return y_
        else:
            x = tf.identity(self.idp.online_rescale(x), name="input_rescaled")
            y = tf.identity(self.odp.online_rescale(y), name="target_rescaled")
            y_ = tf.identity(self.odp.online_rescale(y_), name="prediction_rescaled")
            return x, y, y_

    def _infer(self, x, y, y_, prediction_only=False):
        """Perform a fast, full evaluation forward pass with postprocessing.

        Args:
            x:  The inputs Tensor (scaled down)
            y:  The target batch Tensor (scaled down)
            y_: The prediction batch Tensor (scaled down)

        Returns:
            x:  The inputs Tensor (original scale)
            y:  The target batch Tensor (original scale)
            y_: The prediction batch Tensor (original scale)

        """
        # TODO: Possibly shrink model for performance on production systems
        # TODO: Use NHWC here on CPU only
        # TODO: CPU only
        # TODO: bypass the entire graph for input and target and only feed y_ through
        x, y, y_ = self._postprocess(x, y, y_, prediction_only=prediction_only)
        return x, y, y_

    @variable_scoped_lazy_property
    def infer(self):
        """Optionally calculate and return a fast, full evaluation forward pass with postprocessing.

        Returns:
            x:  The inputs Tensor (original scale)
            y:  The target batch Tensor (original scale)
            y_: The prediction batch Tensor (original scale)

        """
        raise NotImplementedError("Must be implemented in child class.")

    @variable_scoped_lazy_property
    def predict(self):
        """Optionally calculate and return a fast, forward pass with postprocessing.

        Returns:
            y_: The prediction batch Tensor (original scale)

        """
        raise NotImplementedError("Must be implemented in child class.")

    @variable_scoped_lazy_property
    def mean_absolute_error(self):
        """Optionally calculate the mean absolute error.

        Returns:
            mae:   The mean absolute error
        """
        _, y, y_ = self.forward_pass
        mae = self._mean_absolute_error(y, y_)
        # Add summary
        with self.writer.as_default(), self._record_summary_if(self.TRAIN):
            tf.contrib.summary.scalar("MAE", mae, step=self.global_step)
        return mae

    def sess_initialize(self, sess, feed_dict, mode="train", checkpoint=None):
        """Initialize the graph.

        Args:
            sess:       The tensorflow session
            feed_dict:  A feed dictionary for all placeholders in the graph
            mode:       The current mode ("train", "valid" or "test")
            checkpoint: An optional checkpoint file or "last" (default: None)

        """
        assert (
            self.train_data in feed_dict.keys()
            or self.valid_data in feed_dict.keys()
            or self.test_data in feed_dict.keys()
            or self.train_feed in feed_dict.keys()
            or self.valid_feed in feed_dict.keys()
            or self.feed in feed_dict.keys()
        )
        self.feed_dict = feed_dict
        if self.idp.freeze in self.feed_dict.keys():
            self.train_freeze = self.feed_dict[self.idp.freeze]
        # Initialize
        sess.run(
            [tf.local_variables_initializer(), tf.global_variables_initializer()],
            feed_dict=self.feed_dict,
        )
        with self.writer.as_default(), self._record_summary_if(self.TRAIN):
            tf.contrib.summary.initialize(graph=sess.graph, session=sess)
        self.sess_update_data_pipeline(sess, mode=mode, feed_dict=self.feed_dict)
        # Load checkpoint
        if checkpoint is not None:
            self.sess_load(sess, checkpoint=checkpoint)
        # Output
        if self.VERBOSITY:
            if self.VERBOSITY >= 2:
                # Count and print number of free (trainable) parameters
                total_params = 0
                for variable in tf.trainable_variables():
                    shape = variable.get_shape()
                    variable_parameters = 1
                    if shape.dims is not None:
                        for dim in shape:
                            variable_parameters *= dim.value
                    total_params += variable_parameters
                print(
                    self.INDENT
                    + self.name
                    + ": Trainable model parameters (excluding cudnn): {:d}".format(
                        total_params
                    )
                )
            if self.VERBOSITY >= 3:
                # Print all global variables
                print(self.INDENT + self.name + ": Global model variables:")
                for variable in tf.global_variables():
                    print(
                        self.INDENT
                        + "      "
                        + variable.name
                        + " "
                        + str(variable.get_shape())
                    )
                # Print all local variables
                print(self.INDENT + self.name + ": Local model variables:")
                for variable in tf.local_variables():
                    print(
                        self.INDENT
                        + "      "
                        + variable.name
                        + " "
                        + str(variable.get_shape())
                    )
            print(self.INDENT + self.name + ": Session intialized successfully.")
            self.init_time = timeit.default_timer()

    def sess_update_data_pipeline(self, sess, mode, feed_dict=None):
        """Update the data source and/or switch to different dataset"""
        if feed_dict is not None:
            self.feed_dict.update(feed_dict)
            # Memorize freeze state
            if self.idp.freeze in feed_dict.keys():
                self.train_freeze = feed_dict[self.idp.freeze]
        assert (
            self.train_data in self.feed_dict.keys()
            or self.valid_data in self.feed_dict.keys()
            or self.test_data in self.feed_dict.keys()
            or self.train_feed in self.feed_dict.keys()
            or self.valid_feed in self.feed_dict.keys()
            or self.feed in self.feed_dict.keys()
        )
        # Select mode
        self.mode = mode
        if mode == "train":
            # Use staging areas
            self.feed_dict[self.TRAIN] = True
            if feed_dict:
                if self.TRAIN in feed_dict:
                    assert feed_dict[self.TRAIN]
                if self.BYPASS not in feed_dict.keys():
                    self.feed_dict[self.BYPASS] = False
            else:
                self.feed_dict[self.BYPASS] = False
            # Reset freeze state to train value
            if self.idp.freeze in self.feed_dict.keys():
                self.feed_dict[self.idp.freeze] = self.train_freeze
                self.feed_dict[self.odp.freeze] = self.train_freeze
            # Choose init op
            init_op = self.train_init
        else:
            # Do not use staging areas
            if feed_dict:
                if self.TRAIN in feed_dict:
                    assert not feed_dict[self.TRAIN]
                if self.BYPASS in feed_dict:
                    assert feed_dict[self.BYPASS]
            self.feed_dict[self.TRAIN] = False
            self.feed_dict[self.BYPASS] = True
            # Overwrite freeze state to True
            self.feed_dict[self.idp.freeze] = True
            self.feed_dict[self.odp.freeze] = True
            # Choose init op
            if mode == "valid":
                init_op = self.valid_init
            elif mode == "test":
                init_op = self.test_init
            else:
                raise NotImplementedError("Unknown mode =", mode)
        # Run
        sess.run(init_op, feed_dict=self.feed_dict)

    def sess_forward_pass(self, sess, feed_dict=None, **kwargs):
        if feed_dict is None:
            feed_dict = self.feed_dict
        assert (
            self.train_data in feed_dict.keys()
            or self.valid_data in feed_dict.keys()
            or self.test_data in feed_dict.keys()
            or self.train_feed in feed_dict.keys()
            or self.valid_feed in feed_dict.keys()
            or self.feed in self.feed_dict.keys()
        )
        if self.mode == "train":
            _, (x, y, y_) = sess.run(
                [self.stage, self.forward_pass], feed_dict=feed_dict, **kwargs
            )
        else:
            x, y, y_ = sess.run(self.forward_pass, feed_dict=feed_dict, **kwargs)
        return x, y, y_

    def sess_loss(self, sess, feed_dict=None, **kwargs):
        if feed_dict is None:
            feed_dict = self.feed_dict
        assert (
            self.train_data in feed_dict.keys()
            or self.valid_data in feed_dict.keys()
            or self.test_data in feed_dict.keys()
            or self.train_feed in feed_dict.keys()
            or self.valid_feed in feed_dict.keys()
            or self.feed in feed_dict.keys()
        )
        if self.mode == "train":
            _, l = sess.run([self.stage, self.loss], feed_dict=feed_dict, **kwargs)
        else:
            l = sess.run(self.loss, feed_dict=feed_dict, **kwargs)
        return l

    def sess_train(
        self,
        sess,
        feed_dict=None,
        summarize=False,
        pbar=None,
        full_trace=False,
        **kwargs,
    ):
        """Perform one training step and report the result."""
        if feed_dict is None:
            feed_dict = self.feed_dict
        assert (
            self.train_data in feed_dict.keys()
            or self.valid_data in feed_dict.keys()
            or self.test_data in feed_dict.keys()
            or self.train_feed in feed_dict.keys()
            or self.valid_feed in feed_dict.keys()
            or self.feed in feed_dict.keys()
        )
        assert self.mode == "train", f"Invalid mode for training: {self.mode:s}"
        tf.keras.backend.set_learning_phase(1)
        if isinstance(pbar, tqdm):
            write = pbar.write
        else:
            write = print
        if full_trace:
            kwargs["options"] = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            kwargs["run_metadata"] = tf.RunMetadata()
        if summarize:
            # Calculate train step
            with sess.graph.as_default(), self.writer.as_default():
                (_, step, learning_rate, _, loss_train, _) = sess.run(
                    [
                        self.stage,
                        self.global_step,
                        self.learning_rate,
                        self.train,
                        self.loss,
                        tf.contrib.summary.all_summary_ops(),
                    ],
                    feed_dict=feed_dict,
                    **kwargs,
                )
                if (
                    self.valid_data in feed_dict.keys()
                    or self.valid_feed in feed_dict.keys()
                ):
                    # Reinitialize data iterator for validation
                    self.sess_update_data_pipeline(sess, "valid")
                    # Calculate validation loss
                    _, loss_valid, _ = sess.run(
                        [self.stage, self.loss, tf.contrib.summary.all_summary_ops()],
                        feed_dict=feed_dict,
                        **kwargs,
                    )
                    # Reinitialize data iterator for training
                    self.sess_update_data_pipeline(sess, "train")
                else:
                    loss_valid = -1.0
            # Print
            if self.VERBOSITY:
                # Calculate time
                time = timeit.default_timer() - self.init_time
                seconds = int(time)
                milliseconds = int((time - seconds) * 1000)
                minutes, seconds = divmod(seconds, 60)
                hours, minutes = divmod(minutes, 60)
                # Print update
                if pbar:
                    postfix = OrderedDict()
                    postfix["step"] = f"{step:07d} "
                    postfix["loss_train"] = f"{loss_train:2.3e} "
                    postfix["loss_valid"] = f"{loss_valid:2.3e} "
                    postfix["time"] = "{:d}h {:02d}m {:02d}s ".format(
                        hours, minutes, seconds
                    )
                    pbar.set_postfix(postfix)
                else:
                    if self.VERBOSITY >= 3:
                        write_string = (
                            self.name
                            + ": "
                            + f"Step: {step:07d} "
                            +
                            # "learn_rate: {:2.3e} ".format(
                            #     learning_rate) +
                            "loss: "
                            + f"train: {loss_train:2.3e} "
                            + f"valid: {loss_valid:2.3e} "
                            + "time: {:d}h {:02d}m {:02d}s ".format(
                                hours, minutes, seconds
                            )
                        )
                        write(write_string)
            # Perform full trace
            # if full_trace:
            # self.writer.add_run_metadata(kwargs["run_metadata"],
            #                             "step_{:06d}".format(step))
        else:
            # Run minimal training step plus loss calculation
            _, step, _, loss_train = sess.run(
                [self.stage, self.global_step, self.train, self.loss],
                feed_dict=feed_dict,
                **kwargs,
            )
            loss_valid = -1.0
        return loss_train, loss_valid

    def sess_infer(self, sess, feed_dict=None, fetches=None, **kwargs):
        if feed_dict is None:
            feed_dict = self.feed_dict
        if fetches is None:
            fetches = []
        else:
            fetches = list(fetches)
        assert (
            self.train_data in feed_dict.keys()
            or self.valid_data in feed_dict.keys()
            or self.test_data in feed_dict.keys()
            or self.train_feed in feed_dict.keys()
            or self.valid_feed in feed_dict.keys()
            or self.feed in feed_dict.keys()
        )
        if self.mode == "train":
            tf.keras.backend.set_learning_phase(1)
            if fetches:
                _, results, fresults = sess.run(
                    [self.stage, self.infer] + fetches, feed_dict=feed_dict, **kwargs
                )
                return results, fresults
            else:
                _, results = sess.run(
                    [self.stage, self.infer], feed_dict=feed_dict, **kwargs
                )
                return results
        else:
            tf.keras.backend.set_learning_phase(0)
            if fetches:
                return sess.run([self.infer] + fetches, feed_dict=feed_dict, **kwargs)
            else:
                return sess.run(self.infer, feed_dict=feed_dict, **kwargs)

    def sess_save(self, sess, global_step, pbar=None):
        """Save the graph.

        Args:
            sess:           The tensorflow session
            global_step:    The training step (or any unique integer)

        """
        if isinstance(pbar, tqdm):
            write = pbar.write
        else:
            write = print
        if isinstance(global_step, int):
            self.saver.save(
                sess,
                os.path.join(self.checkpoint_dir, "checkpoint"),
                global_step=global_step,
            )
        elif isinstance(global_step, str):
            self.saver.save(
                sess,
                os.path.join(self.checkpoint_dir, f"checkpoint-{global_step:s}"),
            )
        else:
            raise TypeError("global_step must be of type int or str")
        # Output
        if self.VERBOSITY >= 2:
            write(
                self.INDENT
                + self.name
                + ": Saved checkpoint: {:s}/checkpoint-{:s}".format(
                    self.checkpoint_dir, str(global_step)
                )
            )

    def sess_load(self, sess, checkpoint="last"):
        """Restore the graph.

        Graph must be initialized and StagingAreas should be cleared beforehand.

        Args:
            sess:       The tensorflow session
            checkpoint: "last", "best" or path to checkpoint

        """
        # Find checkpoint
        if checkpoint == "best":
            # assemble checkpoint file path
            checkpoint_file = os.path.join(
                self.checkpoint_dir, f"checkpoint-{checkpoint:s}"
            )
        elif checkpoint == "last":
            # find checkpoint file path
            checkpoint = tf.train.get_checkpoint_state(self.checkpoint_dir)
            if checkpoint is not None:
                # checkpoint found
                checkpoint_file = checkpoint.model_checkpoint_path
            else:
                # No checkpoint found
                checkpoint_file = None
        else:
            # assemble checkpoint file path manually
            checkpoint_file = os.path.join(self.checkpoint_dir, checkpoint)

        if checkpoint_file is not None:
            try:
                # Load checkpoint
                self.saver.restore(sess, checkpoint_file)
                # Output
                if self.VERBOSITY:
                    print(
                        self.INDENT
                        + self.name
                        + ": Loaded checkpoint: \n{:s}    {:s}".format(
                            self.INDENT, checkpoint_file
                        )
                    )
            except (tf.errors.NotFoundError, ValueError) as e:
                if self.VERBOSITY:
                    print(
                        self.INDENT
                        + self.name
                        + f": No suitable checkpoint found. ({e.message:s})"
                    )
        else:
            if self.VERBOSITY:
                print(self.INDENT + self.name + ": No suitable checkpoint found.")

    def _record_summary_if(self, cond):
        return _record_summaries(cond)
