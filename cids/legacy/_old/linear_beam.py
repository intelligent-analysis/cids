# # #    LINEAR BEAM     # # #
import glob
import os

import intelligentanalysis as ia
import pandas


# import numpy as np


print("________________________________________________________________________")
print("________________________________________________________________________")
print("____________________ LINEAR BEAM ANALYSIS ______________________________")
print("________________________________________________________________________")
print("________________________________________________________________________")

# DATA
# print("___ Reading data _______________________________________________________")
# data_table = pandas.read_csv("./data/input_output_endload.txt", header=None)
data_table = pandas.read_csv(
    "./data/linear/input_output_random.txt", header=None, nrows=10000
)
data = data_table.as_matrix()

# extract inputs and targets
x_data = data[:, 9:]
y_data = data[:, :9]

# print("   input data shape:", x_data.shape)
# print("   output data shape:", y_data.shape)

# DEFINTIONS
# print("___ Reading model parameters ___________________________________________")
# model architecture
n_inputs = x_data.shape[1]
n_outputs = y_data.shape[1]

# model parameters
init_learning_rate = 1e-3
n_steps = 100
decay_step = 1000
decay_rate = 0.9
reg = 0.0
keep_prob = 1.0
display_step = 10

# genetic algorithm parameters
n_attributes = 1
attribute_range = (0, 24)
n_individuals = 4
n_generations = 5
mate_prob = 0.7
mutation_prob = 0.2


def train_eval_nn(individual):

    for f in glob.glob("./checkpoint/*"):
        os.remove(f)

    n_nodes = [n_inputs, int(individual[0]), n_outputs]

    # print("___ Setting up model ___________________________________________________")
    nn = ia.NeuralNetwork(
        n_nodes,
        n_steps,
        init_learning_rate,
        decay_step=decay_step,
        decay_rate=decay_rate,
        reg=reg,
        keep_prob=keep_prob,
        display_step=display_step,
    )

    # print("___ Preprocessing data _________________________________________________")
    feed_train, feed_valid, feed_test = nn.preprocess(
        x_data, y_data, test_split=0.2, valid_split=0.2
    )

    # print("___ Training ___________________________________________________________")
    loss_list = nn.run_train(feed_train, feed_valid, plot_train=False)
    # loss_array = np.concatenate([np.transpose(l[:, np.newaxis]) for l in loss_list])
    # np.savetxt("loss.csv", loss_array, fmt="%d,%.9g,%.9g",
    #           header="step, loss_train, loss_valid", delimiter=",", comments="")

    # print("Validation loss after training:",)
    _, loss_valid = nn.run_eval(feed_valid)

    print(
        "-- Individual: " + str(individual) + " | Validation Loss: " + str(loss_valid)
    )

    return (loss_valid.tolist(),)  # must be sequence


# GENETIC ALGORITHM
ga = ia.GeneticAlgorithm(
    n_attributes,
    attribute_range,
    n_individuals,
    n_generations,
    eval_funct=train_eval_nn,
    mate_prob=mate_prob,
    mutation_prob=mutation_prob,
)

# RUN GENETIC ALGORITHM
ga.run()

# EVALUATE TEST SET
print("___ Evaluating test set ________________________________________________")
y_pred_test, loss_test = nn.run_eval(feed_test)


# POSTPROCESS TEST SET
print("___ Postprocessing data ________________________________________________")
x_test_scaled, y_test_scaled = nn.postprocess(feed_test)
_, y_pred_test_scaled = nn.postprocess({nn.x: feed_test[nn.x], nn.y: y_pred_test})

print("Test loss =", loss_test)
print("load_vector =")
print(x_test_scaled[0])
print("Predicted displacements = ")
print(y_pred_test_scaled[0])
print("Correct displacements = ")
print(y_test_scaled[0])

# PREDICT USER INPUT
print("___Predict user input___________________________________________________")
x_eval = np.zeros(x_data.shape[1])
x_eval[-2] = 10000.0
x_eval = x_eval[np.newaxis, :]
y_eval = np.zeros(y_data.shape[1])
y_eval = y_eval[np.newaxis, :]

feed_eval = nn.preprocess_eval(x_eval, y_eval)
y_pred_eval, _ = nn.run_eval(feed_eval)
_, y_pred_eval_scaled = nn.postprocess({nn.x: feed_eval[nn.x], nn.y: y_pred_eval})

print("load_vector =")
print(x_eval)
print("Predicted displacements = ")
print(y_pred_eval_scaled)


# np.savetxt("result.csv", result, fmt="%.9g,%.9g,%.9g",
#           header="f_fe, u_fe, u_nn", delimiter=",", comments="")
