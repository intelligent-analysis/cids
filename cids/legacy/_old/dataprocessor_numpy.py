"""IAM computational intelligence library"""
import numpy as np


class Dataprocessor:
    """Processes data."""

    DEBUG = False

    def __init__(
        self,
        x_mean=0.0,
        x_min=-1.0,
        x_max=1.0,
        y_mean=1.0,
        y_min=-1.0,
        y_max=1.0,
        shuffle_order=np.arange(0),
    ):
        """Initialises the data processor.

        Args:
            x_mean:            mean input values (Default: 0.0)
            x_min:             minimal input values (Default: -1.0)
            x_max:             maximal input values (Default: 1.0)
            y_mean:            mean output values (Default: 0.0)
            y_min:             minimal output values (Default: -1.0)
            y_max:             maximal output values (Default: 1.0)
            shuffle_order:     shuffle order (Default: np.arange(0))
        """

        self.x_mean = x_mean
        self.x_min = x_min
        self.x_max = x_max
        self.y_mean = y_mean
        self.y_min = y_min
        self.y_max = y_max
        self.shuffle_order = shuffle_order

    def shuffle(self, x_data, y_data):
        """Shuffles the order of a data set.

        Args:
            x_data:        input data
            y_data:        output data

        Returns:
            x_shuffled:    shuffled input data
            y_shuffled:    shuffled output data
        """

        # shuffle order
        n_samples = x_data.shape[0]
        shuffle_order = np.random.permutation(n_samples)

        # shuffle data
        x_shuffled = x_data[shuffle_order, :]
        y_shuffled = y_data[shuffle_order, :]

        # store shuffle order
        self.shuffle_order = shuffle_order

        if self.DEBUG:
            print("Shuffled input data:", x_shuffled)
            print("Shuffled output data:", y_shuffled)

        return x_shuffled, y_shuffled

    def unshuffle(self, x_shuffled, y_shuffled):
        """Unshuffles a previously shuffled data set.

        Args:
            x_shuffled:    shuffled input data
            y_shuffled:    shuffled output data

        Returns:
            x_unshuffled: unshuffled input data
            y_unshuffled: unshuffled output data

        """

        # recall shuffle order
        shuffle_order = self.shuffle_order

        # unshuffle data
        x_unshuffled = np.zeros(x_shuffled.shape)
        y_unshuffled = np.zeros(y_shuffled.shape)
        x_unshuffled[shuffle_order, :] = x_shuffled
        y_unshuffled[shuffle_order, :] = y_shuffled

        if self.DEBUG:
            print("Unshuffled input data:", x_unshuffled)
            print("Unshuffled output data:", y_unshuffled)

        return x_unshuffled, y_unshuffled

    def split(self, data_list, test_split, valid_split):
        """Splits data into training, validation and test sets.

        Args:
            data_list:  list of data arrays (must have same number of rows)
            test_split: relative size of test dataset
            valid_split: relative size of validation dataset

        Returns:
            train_list:    list of training data
            valid_list:    list of validation data
            test_list:     list of test data
        """

        # number of entries in each data set
        n_data = data_list[0].shape[0]
        n_data_test = int(np.ceil(test_split * n_data))
        n_data_valid = int(np.ceil(valid_split * n_data))
        n_data_train = n_data - n_data_test - n_data_valid

        # split data into data sets
        train_list = [data[:n_data_train, :] for data in data_list]
        valid_list = [
            data[n_data_train : (n_data_train + n_data_valid), :] for data in data_list
        ]
        test_list = [data[(n_data_train + n_data_valid) :, :] for data in data_list]

        if self.DEBUG:
            print("Training set shape:", train_list[0].shape)
            print("Validation set shape:", valid_list[0].shape)
            print("Training set shape:", test_list[0].shape)

        return train_list, valid_list, test_list

    def mean_normalize(self, x_data, y_data, overwrite=False):
        """Mean normalizes the data set using min-max rule.

        Args:
            x_data:        input data
            y_data:        output data
            overwrite:     If True recalculates mean/min/max values and updates
                           stored values (usually True for training data set)

        Returns:
            x_norm:        mean normalized input data
            y_norm:        mean normalized output data
        """

        if overwrite:
            # recalculate mean, min and max values
            dimensions = list(range(x_data.ndim))
            dimensions.remove(1)
            x_mean = np.apply_over_axes(np.mean, x_data, dimensions)
            x_min = np.apply_over_axes(np.min, x_data, dimensions)
            x_max = np.apply_over_axes(np.max, x_data, dimensions)
            y_mean = np.apply_over_axes(np.mean, y_data, dimensions)
            y_min = np.apply_over_axes(np.min, y_data, dimensions)
            y_max = np.apply_over_axes(np.max, y_data, dimensions)

            # overwrite stored values
            self.x_mean = x_mean
            self.x_min = x_min
            self.x_max = x_max
            self.y_mean = y_mean
            self.y_min = y_min
            self.y_max = y_max

        else:
            # recall stored values
            x_mean = self.x_mean
            x_min = self.x_min
            x_max = self.x_max
            y_mean = self.y_mean
            y_min = self.y_min
            y_max = self.y_max

        x_norm = (x_data - x_mean) / np.abs(x_max - x_min) * 2
        y_norm = (y_data - y_mean) / np.abs(y_max - y_min) * 2

        if self.DEBUG:
            print(
                "Normalized input data:",
            )
            print(x_mean.shape)
            print(np.min(x_norm), np.mean(x_norm), np.max(x_norm))
            print(
                "Normalized output data:",
            )
            print(y_mean.shape)
            print(np.min(y_norm), np.mean(y_norm), np.max(y_norm))

        return x_norm, y_norm

    def rescale(self, x_norm, y_norm):
        """Rescales a mean normalized data set back to its original value range

        Args:
            x_norm:        mean normalized input data
            y_norm:        mean normalized output data

        Returns:
            x_rescaled:    rescaled input data
            y_rescaled:    rescaled output data
        """

        # recall stored values
        x_mean = self.x_mean
        x_min = self.x_min
        x_max = self.x_max
        y_mean = self.y_mean
        y_min = self.y_min
        y_max = self.y_max

        # rescale data
        x_rescaled = x_norm * np.abs(x_max - x_min) / 2 + x_mean
        y_rescaled = y_norm * np.abs(y_max - y_min) / 2 + y_mean

        if self.DEBUG:
            print("Rescaled input data:", x_rescaled)
            print("Rescaled output data:", y_rescaled)

        return x_rescaled, y_rescaled

    def error_metrics(self, y, y_):

        squared_difference = np.power(np.asarray(y) - np.asarray(y_), 2)

        # sum up array over all axes except for axis=0
        squared_distance = squared_difference
        N = np.ndim(squared_difference)
        for a in range(1, N):
            squared_distance = squared_distance.sum(axis=1)

        distance = np.sqrt(squared_distance)
        abs_error = np.mean(distance, axis=0)  # calculate mean over batch

        return abs_error


# FUNCTION DEFINTIONS

# def input_pipeline_csv(filenames, n_batch, num_epochs=None):
#
#     filename_queue = tf.train.string_input_producer(filenames, num_epochs=num_epochs, shuffle=True)
#     x_file, y_file = read_csv_files(filename_queue)
#     #min_after_dequeue = 10000
#     #capacity = min_after_dequeue + 3 * n_batch
#     #x_batch, y_batch = tf.train.shuffle_batch([x_file, y_file], batch_size=n_batch,
#     #                                          capacity=capacity, min_after_dequeue=min_after_dequeue)
#     x_batch = x_file
#     y_batch = y_file
#
#     return x_batch, y_batch

# def read_csv_files(filename_queue):
#
#     reader = tf.TextLineReader()
#     key, values = reader.read(filename_queue)
#     record_defaults = [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0],]
#     t, RF11, RF12, R13, RF14, RF21, RF22, RF23, RF24, U11, U12, U13, U14, U21, U22, U23, U24 = tf.decode_csv(
#         values, record_defaults=record_defaults)
#     x_file = tf.pack([U11, U12, U13, U14, U21, U22, U23, U24])
#     y_file = tf.pack([RF11, RF12, R13, RF14, RF21, RF22, RF23, RF24])
#
#     return x_file, y_file
