#!/usr/bin/env zsh

if [ $1 = "gpu" ]; then

else
  module load python/3.5.2
  pip install --user tensorflow==0.12
  pip install --user --upgrade https://storage.googleapis.com/tensorflow/linux/cpu/protobuf-3.1.0-cp35-none-linux_x86_64.whl
  pip install --user jupyter matplotlib scipy pandas
