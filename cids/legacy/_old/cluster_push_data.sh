#!/bin/bash
USER=ak584521
SERVER=cluster-copy.rz.rwth-aachen.de
LOCAL_PATH=./INPUT/brittle_fracture_TFR/   # trailing slash: copy contents
REMOTE_PATH=/work/$USER/intelligent_analysis/INPUT/brittle_fracture_TFR   # no trailing slash
#rsync -aq --rsync-path="mkdir -p $REMOTE_PATH && rsync --progress" $LOCAL_PATH $USER@$SERVER:$REMOTE_PATH
rsync -avK --progress $LOCAL_PATH $USER@$SERVER:$REMOTE_PATH
