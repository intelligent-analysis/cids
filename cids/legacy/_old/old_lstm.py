"""IAM computational intelligence library"""
import tensorflow as tf
from tensorflow.python.ops import rnn_cell

from .old_neural_network import NeuralNetwork


class LSTM(NeuralNetwork):
    """Long Short Term Memory network"""

    def __init__(
        self,
        n_nodes,
        n_sequence,
        n_steps,
        init_learning_rate,
        decay_step=1,
        decay_rate=1.0,
        reg=0.0,
        keep_prob=1.0,
        display_step=100,
        target_loss=0.0,
        checkpoint_path="",
        verbose=False,
    ):
        """Build the SingleGPURecurrent network model.

        Args:
            n_nodes:              vector of the sizes of the different layers
            n_sequence:              number of steps in the sequential data
            n_steps:               number of training epochs
            init_learning_rate:  initial learning rate
            decay_step:             number of iterations between learning rate
                                    updates
            decay_rate:             factor by which the learning rate is changed
            reg:                    regularization factor
            keep_prob:            probability to keep value with dropout
            display_step:           number of iterations between output
        """
        self.n_sequence = n_sequence
        super().__init__(
            n_nodes,
            n_steps,
            init_learning_rate,
            decay_step=decay_step,
            decay_rate=decay_rate,
            reg=reg,
            keep_prob=keep_prob,
            display_step=display_step,
            target_loss=target_loss,
            checkpoint_path=checkpoint_path,
            verbose=verbose,
        )

    def preprocess(self, x_data, y_data, init_state, test_split, valid_split=0.0):
        """Preprocess the data.

        Args:
            x_data:         input data
            y_data:         output data
            test_split:     percentage of data used for testing
            valid_split:    percentage of data used for validation

        Returns:
            feed_train:    training set data feed
            feed_valid:    validation set data feed
            feed_test:     test set data feed
        """

        feed_train, feed_valid, feed_test = super().preprocess(
            x_data=x_data, y_data=y_data, test_split=test_split, valid_split=valid_split
        )
        # split initial state as well
        train_list, valid_list, test_list = self.dp.split(
            [init_state], test_split=test_split, valid_split=valid_split
        )

        # setup feeds
        feed_train[self.init_state] = train_list[0]
        feed_valid[self.init_state] = valid_list[0]
        feed_test[self.init_state] = test_list[0]

        return feed_train, feed_valid, feed_test

    def stacked_lstm(self, n_nodes, keep_prob=1.0):
        cell_list = [
            rnn_cell.DropoutWrapper(
                rnn_cell.LSTMCell(n, state_is_tuple=True), input_keep_prob=keep_prob
            )
            for n in n_nodes
        ]
        stacked_lstm = rnn_cell.MultiRNNCell(cell_list, state_is_tuple=True)
        return stacked_lstm

    def setup_eval(self, x, init_state):
        """Sets up evaluation operation for the neural network.

        Args:
            x:                input tensor
            init_state:       initial state of the SingleGPURecurrent cell

        Returns:
            y_:               predicted output tensor
        """

        # input layer input: (n_batch, n_inputs, n_sequence)
        # transpose to (n_sequence, n_batch, n_inputs)
        activation = tf.transpose(x, [2, 0, 1])
        # reshape to (n_sequence*n_batch, n_inputs)
        activation = tf.reshape(activation, [-1, self.n_nodes[0]])

        # hidden layer
        # Taken out to be replaced with lstm layer.
        # activation = self.hidden_layer(activation, self.n_nodes[1],
        #                               keep_prob=self.keep_prob,
        #                               reg=self.reg)

        # SingleGPURecurrent-cell
        # split activation into list of individual time steps to
        # n_sequence * (n_batch, n_inputs)
        lstm_input = tf.split(0, self.n_sequence, activation)
        stacked_lstm = self.stacked_lstm(self.n_nodes[1:-1], keep_prob=self.keep_prob)
        lstm_output, layers = tf.nn.rnn(stacked_lstm, lstm_input, dtype="float32")

        # concatenate to (n_sequence*n_batch, n_inputs)
        activation = tf.concat(0, lstm_output)

        # output layer
        y_ = self.output_layer(activation, self.n_nodes[-1])
        # reshape to (n_sequence, n_batch, n_inputs)
        y_ = tf.reshape(y_, [self.n_sequence, -1, self.n_nodes[-1]])
        # transpose to (n_batch, n_inputs, n_sequence)
        y_ = tf.transpose(y_, [1, 2, 0])

        return y_

    def setup_graph(self):
        """Sets up the graph for the neural network."""

        # inputs
        self.x = tf.placeholder(
            tf.float32, [None, self.n_nodes[0], self.n_sequence], name="input"
        )
        self.y = tf.placeholder(
            tf.float32, [None, self.n_nodes[-1], self.n_sequence], name="target"
        )
        self.init_state = tf.placeholder(
            tf.float32, [None, 2 * self.n_nodes[-2]], name="init_state"
        )

        self.y_ = self.setup_eval(self.x, self.init_state)

        self.loss = self.setup_loss(self.y, self.y_)

        self.train = self.setup_train(self.loss)
