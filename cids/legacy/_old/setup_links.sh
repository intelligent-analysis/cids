#!/bin/bash
if [ $# -eq 0 ]
then
  echo "Error: No target parent directory (e.g. ~/DATA) for links supplied!"
else
  DATA_PATH=$1
  echo "Creating folder in $DATA_PATH..."
  mkdir -p $DATA_PATH/INPUT
  mkdir -p $DATA_PATH/RESULT
  mkdir -p $DATA_PATH/SUMMARY
  mkdir -p $DATA_PATH/LOG
  mkdir -p $DATA_PATH/PROFILE
  mkdir -p $DATA_PATH/CHECKPOINT
  echo "Creating symbolic links to $DATA_PATH..."
  ln --symbolic $DATA_PATH/INPUT INPUT
  ln --symbolic $DATA_PATH/RESULT RESULT
  ln --symbolic $DATA_PATH/SUMMARY SUMMARY
  ln --symbolic $DATA_PATH/LOG LOG
  ln --symbolic $DATA_PATH/PROFILE PROFILE
  ln --symbolic $DATA_PATH/CHECKPOINT CHECKPOINT
  echo "Setup successful."
fi
