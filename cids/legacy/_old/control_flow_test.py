import numpy as np
import tensorflow as tf

# Input
dataset = np.reshape(np.linspace(1000, 1001, 30, dtype=np.float64), (10, 3))

# We define our Variables and placeholders
x = tf.placeholder(tf.float64)
FREEZE = tf.placeholder_with_default(tf.constant(False, dtype=tf.bool), shape=[])
COUNT = tf.get_variable(
    "COUNT", [], initializer=tf.constant_initializer(0), dtype=tf.int32, trainable=False
)
MEAN = tf.get_variable(
    "MEAN",
    [],
    initializer=tf.constant_initializer(0.0),
    dtype=tf.float64,
    trainable=False,
)
ONE = tf.get_variable(
    "ONE", [], initializer=tf.constant_initializer(1), dtype=tf.int32, trainable=False
)

# Deep copy
N = tf.add(COUNT, 0)
Mu = tf.add(MEAN, 0.0)
# Calculate
delta = tf.cond(FREEZE, lambda: tf.zeros([], dtype=tf.int32), lambda: tf.add(ONE, 0))
delta = tf.Print(delta, [delta], "delta\n", summarize=20)
delta_scaled = tf.cast(delta, tf.float64) / tf.cast(N + 1, tf.float64)
delta_scaled = tf.Print(delta_scaled, [delta_scaled], "delta_scaled\n", summarize=20)
# Assign
with tf.control_dependencies([delta, delta_scaled]):
    assign_N = tf.assign_add(COUNT, delta)
    assign_N = tf.Print(
        assign_N,
        [assign_N.name, assign_N, COUNT.name, COUNT],
        "assign_N\n",
        summarize=20,
    )
    assign_Mu = tf.assign_add(MEAN, delta_scaled)
    assign_Mu = tf.Print(
        assign_Mu,
        [assign_Mu.name, assign_Mu, MEAN.name, MEAN],
        "assign_Mu\n",
        summarize=20,
    )
# group
pop_N, pop_Mu = tf.tuple([assign_N, assign_Mu])
# output
out = x * pop_Mu

with tf.Session() as sess:
    writer = tf.summary.FileWriter("./SUMMARY", sess.graph)
    sess.run(tf.global_variables_initializer())
    for i in range(dataset.shape[0] - 2):
        print(
            "output:",
            sess.run([out, COUNT, MEAN, delta], feed_dict={x: dataset[i : i + 2]}),
        )
    print(
        "output:",
        sess.run([out, COUNT, MEAN, delta], feed_dict={x: dataset[-2:], FREEZE: True}),
    )
    print(
        "output:",
        sess.run([out, COUNT, MEAN, delta], feed_dict={x: dataset[-2:], FREEZE: True}),
    )
    writer.close()
