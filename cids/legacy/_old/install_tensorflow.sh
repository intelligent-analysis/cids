#!/bin/bash

if [ $1 = "gpu" ]; then
  pip install tensorflow-gpu
  #export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/cuda/lib64:/usr/local/cuda/extras/CUPTI/lib64"
  #export CUDA_HOME=/usr/local/cuda
  #tar xvzf cudnn-8.0-linux-x64-v5.1-ga.tgz
  #sudo cp -P cuda/include/cudnn.h /usr/local/cuda/include
  #sudo cp -P cuda/lib64/libcudnn* /usr/local/cuda/lib64
  #sudo chmod a+r /usr/local/cuda/include/cudnn.h /usr/local/cuda/lib64/libcudnn*
else
  pip install tensorflow
fi
# Upgrade protobuf C++ instead of python libraries
pip install --upgrade https://storage.googleapis.com/tensorflow/linux/cpu/protobuf-3.1.0-cp35-none-linux_x86_64.whl
# install other packages
conda install jupyter matplotlib scipy pandas
