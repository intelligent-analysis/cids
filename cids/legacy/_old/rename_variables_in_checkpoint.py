# Copyright 2022 Arnd Koeppe
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import glob
import os

import tensorflow as tf

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"

old_checkpoint_files = glob.glob("RESULTS/_old/*/CHECKPOINT/checkpoint*.meta")
old_checkpoints = [f.replace(".meta", "") for f in old_checkpoint_files]

vars_to_rename = {
    "epF-LSTM/towers/forward_pass/cell_5/lstm_fused_cell/weights": "epF-LSTM/towers/forward_pass/cell_5/lstm_fused_cell/kernel",  # pylint: disable=line-too-long
    "epF-LSTM/towers/forward_pass/cell_5/lstm_fused_cell/biases": "epF-LSTM/towers/forward_pass/cell_5/lstm_fused_cell/bias",  # pylint: disable=line-too-long
}

for old_checkpoint_file in old_checkpoint_files:
    try:
        new_checkpoint_file = old_checkpoint_file
        old_checkpoint_file = old_checkpoint_file.replace(".meta", "-backup.meta")
        old_checkpoint = old_checkpoint_file.replace(".meta", "")
        new_checkpoint = new_checkpoint_file.replace(".meta", "")
        for subfile in glob.glob(new_checkpoint + ".*"):
            print(subfile)
            os.rename(subfile, subfile.replace(new_checkpoint, old_checkpoint))
        new_checkpoint_vars = {}
        reader = tf.train.NewCheckpointReader(old_checkpoint)
        for old_name in reader.get_variable_to_shape_map():
            new_name = vars_to_rename.get(old_name, default=old_name)
            new_checkpoint_vars[new_name] = tf.Variable(reader.get_tensor(old_name))

        init = tf.global_variables_initializer()
        saver = tf.train.Saver(new_checkpoint_vars)

        with tf.Session() as sess:
            sess.run(init)
            saver.save(sess, new_checkpoint)
    except tf.errors.OutOfRangeError:
        print("failed")
        continue
