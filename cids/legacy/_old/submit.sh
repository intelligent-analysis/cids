#!/usr/bin/env zsh

MODEL_NAME=lstm

#### submit with bsub < submit.sh

### Job name
#BSUB -J SingleGPULSTM

### File / path where STDOUT & STDERR will be written
###    %J is the job ID, %I is the array ID
#BSUB -o ./LOG/%J.%I

### Request the time you need for execution in minutes
### The format for the parameter is: [hour:]minute,
### that means for 80 minutes you could also use this: 1:20
#BSUB -W 20

### Request memory you need for your job in TOTAL in MB
#BSUB -M 8192

### Request multiple cores
###BSUB -n 8

# Request gpu
#BSUB -a gpu

### Change to the work directory
module load cuda/80
module load python/3.5
#export PATH="/home/ak584521/anaconda3/bin:$PATH"
cd $WORK/intelligent_analysis/
#source activate tf12-cuda
jupyter nbconvert --to python $MODEL_NAME.ipynb
### Execute your application
/home/ak584521/anaconda3/envs/tf12/bin/python $MODEL_NAME.py
# clean-up
rm $MODEL_NAME.py
source deactivate
