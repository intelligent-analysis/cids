"""IAM computational intelligence library"""
# TODO: cuddnn compatible rnn
# https://github.com/tensorflow/tensorflow/blob/r1.3/tensorflow/contrib/cudnn_rnn/python/kernel_tests/cudnn_rnn_ops_test.py
# TODO: Variable sequence length and padding (Not available for CUDNN)
import os

import tensorflow as tf
from tensorflow.contrib import cudnn_rnn

from cids.legacy.nn.recurrent import *


class CudnnLSTM(SingleGPURecurrent):
    def __init__(
        self,
        data_shape,
        input_idx,
        output_idx,
        layers,
        batch_size=1000,
        init_learning_rate=0.01,
        decay_rate=1.0,
        decay_steps=0,
        keep_prob=0.5,
        grad_clip=3.0,
        data_format="NSF",
        dtype=tf.float32,
        dr=None,
        idp=None,
        odp=None,
        optimizer=None,
        prep_device=None,
        comp_device=None,
        scope=None,
        results_dir="RESULTS/",
        sessrun_kwargs=None,
    ):
        """A CUDNN SingleGPURecurrent neural network for up to 1 GPU.

        Args:
            data_shape:     Shape of the data tensor
            batch_size:     Number of samples in batch
            input_idx:      Indices of data for the input
            output_idx:     Indices of data for the output
            layers:  A tuple of layer sizes
            init_learning_rate:  The initial learning rate
            decay_rate:     The learning rate reduction factor over decay_steps
            decay_steps:    The number of steps over which decay_rate is applied
            keep_prob:      The dropout keep probability during training
            grad_clip:      The value used for L2 gradient clipping
            data_format:    Data format used ("NF", NSF", or "NCHW")
                                 N:         batch axis,
                                 S/D:       sequence axis / depth axis
                                 H/W/X/Y:   spatial axes (height, width, x, y)
                                 F/C:       feature or channel axis
            dtype:          Data type (defaults to tf.float32)
            dr:             A Datareader object (None uses default)
            idp:            The input DataProcessor (None uses default)
            odp:            The output DataProcessor (None uses default)
            optimizer:      The optimizer used for training (None uses default)
            prep_device:    The comp_devices used for preprocessing
                            (default: cpu[0])
            comp_device:    The comp_devices used for training (default: gpu[0])
            scope:          The shared name scope (None defaults to class name)
            results_dir:    Directory to store results, summaries and
                            checkpoints
            sessrun_kwargs: Keyword arguments for tensorflow session.run()

        """
        super().__init__(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=init_learning_rate,
            decay_rate=decay_rate,
            decay_steps=decay_steps,
            keep_prob=keep_prob,
            grad_clip=grad_clip,
            data_format=data_format,
            dtype=dtype,
            dr=dr,
            idp=idp,
            odp=odp,
            optimizer=optimizer,
            prep_device=prep_device,
            comp_device=comp_device,
            scope=scope,
            results_dir=results_dir,
            sessrun_kwargs=sessrun_kwargs,
        )

    def _forward_pass(self, x, y):
        # Define cells
        def cudnn_cell(inputs, num_inputs, num_units, index):
            with tf.variable_scope(f"cell_{index:d}"):
                # Each cell is one CudnnLSTM (only supports equisized layers)
                num_layers = 1
                seq_length = self.input_shape[self.sequence_axis]
                batch_size = self.batch_size
                # Base cell
                cell = cudnn_rnn.CudnnLSTM(
                    num_layers, num_inputs, num_units, dropout=self.keep_prob
                )
                # FIXME: Parameter size cannot be dynamic (TF Bug
                # params_size_t = cell.params_size()
                # Calculate static shape manually
                params_size_t = (
                    (num_inputs * num_units * 4)
                    + (num_units * num_units * 4)
                    + (num_units * 2 * 4)
                )
                # Create zero state
                h = tf.get_variable(
                    "hidden_state",
                    [seq_length, batch_size, num_units],
                    dtype=tf.float32,
                    initializer=tf.zeros_initializer,
                )
                c = tf.get_variable(
                    "cell_state",
                    [seq_length, batch_size, num_units],
                    dtype=tf.float32,
                    initializer=tf.zeros_initializer,
                )
                # Create parameter buffer
                params = tf.Variable(tf.random_uniform([params_size_t]))
                # Define pass
                y_, h, c = cell(
                    inputs, h, c, params, is_training=True
                )  # FIXME: variable
                # Convert buffered params to tensorflow variables
                params_saveable = cudnn_rnn.RNNParamsSaveable(
                    cell, cell.params_to_canonical, cell.canonical_to_params, [params]
                )
                tf.add_to_collection(tf.GraphKeys.SAVEABLE_OBJECTS, params_saveable)
                # Add summaries
                self.summaries.append(tf.summary.histogram("y_", y_))
            return y_

        # def output_layer(inputs, num_outputs):
        #     with tf.variable_scope("output_layer") as scope:
        #         # All timesteps of the output layer use shared weights
        #         ysteps = tf.unstack(inputs, axis=self.sequence_axis)
        #         ysteps[0] = fully_connected(
        #             ysteps[0],
        #             num_outputs,
        #             activation_fn=None,
        #             weights_initializer=xavier_initializer(),
        #             scope=scope)
        #         for i, yy in enumerate(ysteps[1:]):
        #             ysteps[i+1] = fully_connected(
        #                 yy,
        #                 num_outputs,
        #                 activation_fn=None,
        #                 weights_initializer=xavier_initializer(),
        #                 scope=scope,
        #                 reuse=True)
        #         y_ = tf.stack(ysteps)
        #     return y_

        # Transpose to SNF
        y_ = tf.transpose(x, [self.sequence_axis, self.batch_axis, self.feature_axis])
        # Build forward pass graph
        num_inputs = self.input_shape[self.feature_axis]
        num_outputs = self.output_shape[self.feature_axis]
        layers = [num_inputs] + self.layers + [num_outputs]
        for i in range(1, len(layers)):
            layer_input_size = layers[i - 1]
            num_units = layers[i]
            y_ = cudnn_cell(y_, layer_input_size, num_units, i)
        # Transpose back to NSF
        y_ = tf.transpose(y_, [self.sequence_axis, self.batch_axis, self.feature_axis])
        # Output layer
        # y_ = output_layer(y_, self.output_shape[self.feature_axis])
        return x, y, y_


class CudnnLSTMTower(RecurrentTower):
    def __init__(
        self,
        input,
        target,
        sequence_lengths,
        output_sequence_mask,
        layers,
        optimizer,
        prep_device,
        comp_device,
        BYPASS,
        TRAIN,
        data_format="NSF",
        keep_prob=0.5,
        grad_clip=3.0,
        dtype=tf.float32,
        scope=None,
    ):
        """A tower for a MultiGPU SingleGPUGRU network.

        Args:
            input:                  The input batch Tensor
            target:                 The target batch Tensor
            sequence_lengths:       The sequence lengths of the batch Tensor
            output_sequence_mask:   The sequence mask for the output Tensor
            layers:          The hidden layers of the model
            optimizer:              The optimizer used for training
            prep_device:            The comp_devices used for the copy stage
            comp_device:            The comp_devices used for computation
            BYPASS:                 Boolean flag that controls bypass of staging
            TRAIN:                  Boolean flat that controls training mode
            data_format:            Data format used ("NF", NSF", or "NCHW")
                                         N:         batch axis,
                                         S/D:       sequence axis / depth axis
                                         H/W/X/Y:   spatial axes (height, width,
                                                                  x, y)
                                         F/C:       feature or channel axis
            keep_prob:              The dropout keep probability
            grad_clip:              The value used for L2 gradient clipping
            dtype:                  The towers data type (defaults to float32)
            scope:                  The shared name scope (None defaults to
                                    class name plus device name)

        """
        super().__init__(
            input,
            target,
            sequence_lengths,
            output_sequence_mask,
            layers,
            optimizer,
            prep_device,
            comp_device,
            BYPASS,
            TRAIN,
            data_format=data_format,
            keep_prob=keep_prob,
            grad_clip=grad_clip,
            dtype=dtype,
            scope=scope,
        )

    def _forward_pass(self, x, y):
        # Define cells
        def rnn_cell(inputs, num_inputs, num_units, index):
            with tf.variable_scope(f"cell_{index:d}"):
                # Each cell is one CudnnLSTM (only supports equisized layers)
                num_layers = 1
                # Base cell
                cell = cudnn_rnn.CudnnLSTM(
                    num_layers, num_inputs, num_units, dropout=self.keep_prob
                )
                params_size_t = cell.params_size()
                # Create zero state
                seq_length = self.input_shape[self.sequence_axis]
                batch_size = self.batch_size
                num_features = self.input_shape[self.feature_axis]
                h = tf.get_variable(
                    "hidden_state",
                    [seq_length, batch_size, num_features],
                    dtype=tf.float32,
                    initializer=tf.zeros_initializer,
                )
                c = tf.get_variable(
                    "cell_state",
                    [seq_length, batch_size, num_features],
                    dtype=tf.float32,
                    initializer=tf.zeros_initializer,
                )
                # Create parameter buffer
                # FIXME: Specifically init biases of forget to 1
                params = tf.Variable(
                    tf.random_uniform([params_size_t]), validate_shape=False
                )
                # Define pass
                y_, h, c = cell(inputs, h, c, params, is_training=self.TRAIN)
                # Convert buffered params to tensorflow variables
                params_saveable = cudnn_rnn.RNNParamsSaveable(
                    cell, cell.params_to_canonical, cell.canonical_to_params, [params]
                )
                tf.add_to_collection(tf.GraphKeys.SAVEABLE_OBJECTS, params_saveable)
                # Add summaries
                self.summaries.append(tf.summary.histogram("y_", y_))
            return y_

        # def output_layer(inputs, num_outputs):
        #     with tf.variable_scope("output_layer") as scope:
        #         # All timesteps of the output layer use shared weights
        #         ysteps = tf.unstack(inputs, axis=self.sequence_axis)
        #         ysteps[0] = fully_connected(
        #             ysteps[0],
        #             num_outputs,
        #             activation_fn=None,
        #             weights_initializer=xavier_initializer(),
        #             scope=scope)
        #         for i, yy in enumerate(ysteps[1:]):
        #             ysteps[i+1] = fully_connected(
        #                 yy,
        #                 num_outputs,
        #                 activation_fn=None,
        #                 weights_initializer=xavier_initializer(),
        #                 scope=scope,
        #                 reuse=True)
        #         y_ = tf.stack(ysteps)
        #     return y_

        # Transpose to SNF
        y_ = tf.transpose(x, [self.sequence_axis, self.batch_axis, self.feature_axis])
        # Build forward pass graph
        num_inputs = self.input_shape[self.feature_axis]
        num_outputs = self.output_shape[self.feature_axis]
        layers = [num_inputs] + self.layers + [num_outputs]
        for i in range(1, len(layers)):
            layer_input_size = layers[i - 1]
            num_units = layers[i]
            y_ = rnn_cell(y_, layer_input_size, num_units, i)
        # Transpose back to NSF
        y_ = tf.transpose(y_, [self.sequence_axis, self.batch_axis, self.feature_axis])
        # Output layer
        # y_ = output_layer(y_, self.output_shape[self.feature_axis])
        return x, y, y_


class MultiGPUCudnnLSTM(Recurrent):

    Tower = CudnnLSTMTower

    def __init__(
        self,
        data_shape,
        input_idx,
        output_idx,
        layers,
        batch_size=1000,
        init_learning_rate=0.01,
        decay_rate=1.0,
        decay_steps=0,
        keep_prob=0.5,
        grad_clip=3.0,
        data_format="NSF",
        dtype=tf.float32,
        dr=None,
        idp=None,
        odp=None,
        optimizer=None,
        prep_devices=None,
        comp_devices=None,
        scope=None,
        results_dir="RESULTS/",
        sessrun_kwargs=None,
    ):
        """A CUDNN SingleGPURecurrent neural network for multiple GPUs.

        Args:
            data_shape:     Shape of the data tensor
            batch_size:     Number of samples in batch
            input_idx:      Indices of data for the input
            output_idx:     Indices of data for the output
            layers:  A tuple of layer sizes
            init_learning_rate:  The initial learning rate
            decay_rate:     The learning rate reduction factor over decay_steps
            decay_steps:    The number of steps over which decay_rate is applied
            keep_prob:      Dropout keep probability
            grad_clip:      The L2-Norm value used for gradient clipping
            data_format:    Data format used ("NF", NSF", or "NCHW")
                                 N:         batch axis,
                                 S/D:       sequence axis / depth axis
                                 H/W/X/Y:   spatial axes (height, width, x, y)
                                 F/C:       feature or channel axis
            dtype:          Data type (defaults to tf.float32)
            dr:             A Datareader object (None uses default)
            idp:            The input DataProcessor (None uses default)
            odp:            The output DataProcessor (None uses default)
            optimizer:      The optimizer used for training (None uses default)
            prep_devices:   The comp_devices used for preprocessing
                            (default: cpus[0:1])
            comp_devices:   The comp_devices used for training (default: gpus)
            scope:          The shared name scope (None defaults to class name)
            results_dir:    Directory to store results, summaries and
                            checkpoints
            sessrun_kwargs: Keyword arguments for tensorflow session.run()

        """
        super().__init__(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=init_learning_rate,
            decay_rate=decay_rate,
            decay_steps=decay_steps,
            keep_prob=keep_prob,
            grad_clip=grad_clip,
            data_format=data_format,
            dtype=dtype,
            dr=dr,
            idp=idp,
            odp=odp,
            optimizer=optimizer,
            prep_devices=prep_devices,
            comp_devices=comp_devices,
            scope=scope,
            results_dir=results_dir,
            sessrun_kwargs=sessrun_kwargs,
        )
