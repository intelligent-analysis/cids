# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Computational Intelligence and Data Science toolbox.


.. autosummary::
    :toctree: _autosummary

    DataDefinition
    Feature
    DataReader
    DataWriter
    Preprocessor
    CIDSModel
    CIDSModelTF
    CIDSModelSK
    base
    data
    legacy
    sklearn
    statistics
    tensorflow
    visualize
"""
from . import base
from . import data
from . import external
from . import legacy
from . import sklearn
from . import statistics
from . import tensorflow
from . import visualize
from .data.definition import DataDefinition
from .data.definition import Feature
from .data.preprocessor import Preprocessor
from .data.reader import DataReader
from .data.writer import DataWriter
from .sklearn.model import CIDSModelSK
from .tensorflow.model import CIDSModel
from .tensorflow.model import CIDSModelTF
