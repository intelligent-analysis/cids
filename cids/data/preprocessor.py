# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Data conversion and preprocessing functionality. Part of the CIDS toolbox.

    Classes:
        Preprocessor:   defines, organizes and executes preprocessing
        expand_ndarray_for_feature: increase numpy array dimensionality to feature shape

"""
import copy
import json
import re
from enum import Enum
from functools import wraps
from pathlib import Path
from typing import Iterable
from typing import Union

import numpy as np
import tensorflow as tf
import tqdm
from scipy import interpolate
from scipy import signal
from scipy import stats
from sklearn.decomposition import PCA
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler

from ..base.logging import BaseLoggerMixin
from .definition import DataDefinition
from .definition import Feature


class StrEnum(str, Enum):
    # TODO: python 3.11: import StrEnum directly

    def __str__(self) -> str:
        return str.__str__(self)


class OptionsAxis(StrEnum):
    """Enum that holds possible axis options."""

    N: str = "N"  # batch
    S: str = "S"  # sequence
    X: str = "X"  # x-axis
    Y: str = "Y"  # y-axis
    Z: str = "Z"  # z-axis
    F: str = "F"  # feature


class OptionsTransformMethods(StrEnum):
    """Enum that holds possible transform method options."""

    std: str = "std"
    minmax: str = "minmax"
    pca: str = "pca"


class OptionsOutlierDetection(StrEnum):
    """Enum that holds possible outlier detection options."""

    zscore: str = "z-score"
    abs_value: str = "abs_value"


class OptionsOutlierReplacement(StrEnum):
    """Enum that holds possible outlier replacement options."""

    interpolate: str = "interpolate"
    clip_abs: str = "clip_abs"


class OptionsResampleMethods(StrEnum):
    """Enum that holds possible resampling method options."""

    decimate: str = "decimate"
    fft: str = "fft"
    poly: str = "poly"


class OptionsReduceMethods(StrEnum):
    """Enum that holds possible resampling method options."""

    mean: str = "mean"
    std: str = "std"
    min: str = "min"
    max: str = "max"
    ravel: str = "ravel"
    index0: str = "0"
    index1: str = "1"
    index2: str = "2"
    index3: str = "3"
    index4: str = "4"


def nested_preprocessing_function(func):
    """Decorator that applies function to nested data structures

    Args:
        func (callable): a function to be decorated
    """

    @wraps(func)
    def decorated(
        self,
        samples: Union[dict, Iterable[dict]],
        *args,
        update_data_definition=True,
        **kwargs,
    ):
        if isinstance(samples, dict):
            samples = [samples]
            update_data_definition = False
        # Ensure data definitions are updated
        self.tmp_data_definition = copy.deepcopy(self.data_definition)
        data_definition_container = [copy.deepcopy(self.data_definition)]

        def _map_fun(sample):
            samples, data_definition = func(self, sample, *args, **kwargs)
            data_definition_container[0] = data_definition
            if isinstance(samples, dict):
                samples = [samples]
            return samples

        progbar = tqdm.tqdm(
            samples,
            total=len(samples),
            file=self.stream_to_logger(),
            leave=True,
            desc=f"Step {func.__name__}",
            unit="samples",
            dynamic_ncols=True,
        )
        samples = [sample for samples in progbar for sample in _map_fun(samples)]
        data_definition = data_definition_container[0]
        if update_data_definition:
            self.data_definition = copy.deepcopy(data_definition)
        return samples, data_definition

    return decorated


def register(collection):
    """Decorator to register a function in a collection data structure.

    Args:
        func (callable): a function to be decorated
    """

    def decorate(func):
        collection.append(func.__name__)

        @wraps(func)
        def decorated(*args, **kwargs):
            self = args[0]
            if self.mode == "record":
                # Remove self and sample/samples argument
                step = [func.__name__, args[2:], kwargs]
                self.add_preprocessing_step(step)
            return func(*args, **kwargs)

        return decorated

    return decorate


class PreprocessingContainer:

    # Names of the preprocessing functions defined in this class. Each function must
    # have a signature with types for each parameter and must consume the full `sample`
    # dictionary as first argument
    REGISTERED_FUNCTIONS = []


class Preprocessor(PreprocessingContainer, BaseLoggerMixin):
    def __init__(self, data_definition):
        """Preprocesses samples of defined data.

        Args:
            data_definition:    a DataDefinition instance
        """
        self._start_logging()
        self.data_definition = data_definition
        self.tmp_data_definition = copy.deepcopy(data_definition)
        self.mode = "record"
        self._preprocessing_steps = []

    @classmethod
    def from_json(cls, data_definition, path):
        """Instantiates Preprocessor from data definition (file) and preprocessing file.

        Contains a list of preprocessing steps to perform during preprocessing.

        Args:
            data_definition:    a DataDefinition or a file containing a data definition
            path:               a json file containing serialized preprocessing steps
        """
        warn_messages = []
        try:
            if not isinstance(data_definition, DataDefinition):
                data_definition = DataDefinition.from_json(data_definition)
        except (FileNotFoundError, PermissionError):
            warn_messages.append(
                "Data definition could not be loaded. Does it exist yet?"
            )
            data_definition = DataDefinition()
        try:
            path = Path(path)
            if path.is_dir():
                file = path / "preprocessing.json"
            else:
                file = path
            with file.open("r", encoding="utf8") as ofile:
                json_dict = json.load(ofile)
        except (FileNotFoundError, PermissionError):
            warn_messages.append(
                "Preprocessing definition could not be loaded. Does it exist yet?"
            )
            json_dict = {}
        # Instantiate class
        inst = cls(data_definition)
        # Load attributes
        inst.preprocessing_steps = json_dict.get("preprocessing_steps", [])
        # Warnings
        for w in warn_messages:
            inst.warn(w)
        return inst

    @property
    def preprocessing_steps(self):
        return self._preprocessing_steps

    def to_json(self, path=None):
        """Serialize all preprocessing steps to a json file.

        Args:
            path:     a path to a json file
        """
        # Gather in dictionary
        json_dict = {"preprocessing_steps": self.preprocessing_steps}
        # Write json to file
        if path is not None:
            path = Path(path)
            if path.is_dir():
                file = path / "preprocessing.json"
            else:
                file = path
            file.parent.mkdir(parents=True, exist_ok=True)
            with file.open("w", encoding="utf8") as f:
                f.write(
                    json.dumps(
                        json_dict, sort_keys=True, indent=4, separators=(",", ": ")
                    )
                )
        return json_dict

    def _validate_preprocessing_step(self, step):
        if len(step) != 3:
            raise ValueError(
                "Preprocessing steps must be a tuple of (function, args, kwargs)"
            )
        if not isinstance(step[0], str) or step[0] not in self.REGISTERED_FUNCTIONS:
            raise ValueError(
                "Preprocessing function must be a string "
                + f"in {str(self.REGISTERED_FUNCTIONS)}. "
                + f"Received: {str(step[0])}"
            )
        if not isinstance(step[1], (list, tuple)):
            raise ValueError(
                "Preprocessing function arguments must be a list or tuple. "
                + f"Received: {str(step[1])}"
            )
        if not isinstance(step[2], dict):
            raise ValueError(
                "Preprocessing function arguments must be a dictionary. "
                + f"Received: {str(step[2])}"
            )
        return step

    def _validate_data_definition(self, data_definition, samples):
        for sample in samples:
            feature_names = list(data_definition.features.keys())
            for feature_name in feature_names:
                # Check existence
                if feature_name not in sample.keys():
                    del data_definition.features[feature_name]
                # Get true dtype (expected)
                feature = data_definition.features[feature_name]
                expected_dtype = feature.decode_str_to or feature.dtype
                expected_dtype = expected_dtype.as_numpy_dtype
                if expected_dtype == object:
                    expected_dtype = str
                # Ensure encoding strategy of string dtype
                if np.issubdtype(expected_dtype, str):
                    feature.dtype = tf.string
                    if len(feature.data_format) > 2:
                        # Nonscalar string feature, need decoding
                        feature.decode_str_to = expected_dtype
                    else:
                        # Scalar string feature, no decoding necessary
                        feature.decode_str_to = None
                    continue
                # Ensure encoding strategy of numeric dtypes
                # TODO: encoding strategies for numerical data diverge from string data
                feature.dtype = tf.string
                feature.decode_str_to = expected_dtype
                # Check if numeric data can be cast to expected dtype and unify
                try:
                    np.asarray(sample[feature_name], dtype=expected_dtype)
                except ValueError:
                    # Fallback to full string feature (previous type detection was
                    # likely wrong)
                    feature.dtype = tf.string
                    if len(feature.data_format) > 2:
                        feature.decode_str_to = tf.string
                    else:
                        feature.decode_str_to = None
                    data_definition.features[feature_name] = feature
        return data_definition

    def _validate_samples(self, samples, data_definition):
        for sample in samples:
            feature_names = list(sample.keys())
            for feature_name in feature_names:
                if feature_name in data_definition.features.keys():
                    continue
                del sample[feature_name]
        return samples

    @preprocessing_steps.setter
    def preprocessing_steps(self, steps):
        self._preprocessing_steps = [
            self._validate_preprocessing_step(s) for s in steps
        ]

    def add_preprocessing_step(self, step):
        """Add a preprocessing step.

        Args:
            step (tuple): Tuple of (function_name:str, args:tuple, kwargs:dict).
                The function name must be in self.REGISTERED_FUNCTIONS
        """
        self._preprocessing_steps.append(self._validate_preprocessing_step(step))

    def expand_sample_arrays(self, sample):
        """Expand array dimensionality in a sample to match feature shapes."""
        return {
            k: expand_ndarray_for_feature(v, self.data_definition.features[k])
            for k, v in sample.items()
        }

    def _update_feature_after_split(self, feature_name, axis, data_definition=None):
        # Remove Z axis (XYZ describe the axis in the coordinate system
        # the Neural network sees. Thus, 2d image will always be XY)
        data_definition = data_definition or self.data_definition
        feature = copy.deepcopy(data_definition.features[feature_name])
        if axis in "XYZ":
            del feature.data_shape[feature.data_format.index(axis)]
            if "Z" in feature.data_format:
                # 3D to 2D slice
                feature.data_format = feature.data_format.replace("Z", "")
            elif "Y" in feature.data_format:
                # 2D to 1D slice
                feature.data_format = feature.data_format.replace("Y", "")
            elif "X" in feature.data_format:
                # 1D to 0D slice
                feature.data_format = feature.data_format.replace("X", "")
        elif axis == "S":
            # 1D to 0D slice
            del feature.data_shape[feature.data_format.index(axis)]
            feature.data_format = feature.data_format.replace("S", "")
        elif axis == "F":
            # Keep feature axis but reduce dimensionality to 1
            feature.data_shape[feature.data_format.index("F")] = 1
        return feature

    def match_sample_and_data_definition(self, samples, update_data_definition=True):
        # Expand dimensionality to data definition
        samples = [self.expand_sample_arrays(sample) for sample in samples]
        # Collapse known sequential dimensionality in data definition to 1
        data_definition = copy.deepcopy(self.data_definition)
        for feature in data_definition.features.values():
            feature.collapse_dimensionality()
        # Expand feature sequential dimensionality to maximum in dataset
        for sample in samples:
            for k, ndarray in sample.items():
                feature = copy.deepcopy(data_definition.features[k])
                if feature.sequence_axis is not None:
                    index = feature.sequence_axis
                    feature.data_shape[index] = ndarray.shape[index]
                    data_definition.add_feature(feature)
        # Update data definition
        if update_data_definition:
            self.data_definition = data_definition
        return samples, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def join_strings(
        self,
        sample: dict,
        features: Iterable[str] = None,
        new_feature_name: str = "NEW",
        before_string: str = "",
        after_string: str = "",
        separator: str = "_",
    ) -> tuple:
        """Join strings from multiple features into a new feature.

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names.
                Defaults to None.
            new_feature_name (str): name of the new feature to create
            before_string (str): string before the joined strings
            after_string (str): string after the joined strings
            separator (str, optional): separator between the joined strings

        Raises:
            ValueError: if new_feature_name is empty.

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        if not new_feature_name:
            raise ValueError("Need a valid name for the new feature.")
        # Join strings
        values = [sample[feature] for feature in features]
        strings = [
            str(v.item()) if isinstance(v, np.ndarray) else str(v) for v in values
        ]
        if before_string:
            strings = [before_string] + strings
        if after_string:
            strings = strings + [after_string]
        joined = separator.join(strings)
        # Add to sample
        sample[new_feature_name] = joined
        # Add to data definition
        data_definition.add_feature(
            self._create_scalar_feature(new_feature_name, joined)
        )
        return sample, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def extract_substring(
        self,
        sample: dict,
        features: Iterable[str] = None,
        new_feature_name: str = "NEW",
        before_string: str = "",
        after_string: str = "",
        split_as_path: bool = False,
        convert_numbers: bool = True,
    ) -> tuple:
        """Extract a substring from a string feature and create a new feature.

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names.
                Defaults to None.
            new_feature_name (str): name of the new feature to create
            before_string (str): unique string before the substring to extract
            after_string (str): unique string after the substring to extract
            split_as_path (bool, optional): treat string as path and get base name?
                Defaults to False.
            convert_numbers (bool, optional): attempt to convert substring to number?

        Raises:
            ValueError: if more than 1 feature is given as input.
            ValueError: if new_feature_name is empty.

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        if not new_feature_name:
            raise ValueError("Need a valid name for the new feature.")
        if len(features) != 1:
            raise ValueError("Can only extract substring from single feature")
        # Extract substring
        input_feature = features[0]
        string = sample[input_feature]
        if isinstance(string, np.ndarray):
            string = string.item()
        if split_as_path:
            string = Path(string).with_suffix("").parts[-1]
        substring = string.split(before_string, maxsplit=1)[-1]
        substring = substring.split(after_string, maxsplit=1)[0]
        # Convert feature
        if convert_numbers:
            value = self._maybe_parse_number(substring)
        else:
            value = substring
        # Add to sample
        sample[new_feature_name] = value
        # Add to data definition
        data_definition.add_feature(
            self._create_scalar_feature(new_feature_name, value)
        )
        return sample, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def auto_parse_numbers_from_string(
        self,
        sample: dict,
        features: Iterable[str] = None,
        underscore_marks_decimal: bool = False,
        split_as_path: bool = False,
        separators: str = "",
    ) -> tuple:
        """Parse all numbers from string and attempt to name the features automatically.

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names.
                Defaults to None.
            underscore_marks_decimal (bool, optional): treat underscore as decimal
                delimiter? Defaults to False.
            split_as_path (bool, optional): treat string as path and split by directory?
                Defaults to False.
            separators (str, optional): separators when not treating string as path.
                Defaults to "".

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        if underscore_marks_decimal:
            regex_pattern = r"[-+]?[.]?[\d]+(?:,\d\d\d)*[\._]?\d*(?:[eE][-+]?\d+)?"
        else:
            regex_pattern = r"[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?"
        for feature_name in features:
            # Extract string
            string = sample[feature_name]
            if isinstance(string, np.ndarray):
                string = string.item()
            # Perform split on parts
            if split_as_path:
                strings = Path(string).with_suffix("").parts
            else:
                strings = [string]
            # Split on separators and flatten
            if separators:
                string_lists = [re.split(separators, s) for s in strings]
                strings = [s for li in string_lists for s in li]
            # Extract numbers and labels from parts
            numbers = [re.findall(regex_pattern, s) for s in strings]
            elements = [re.split(regex_pattern, s) for s in strings]
            for element_numbers, element_parts in zip(numbers, elements):
                # Remove empty elements
                element_parts = [p for p in element_parts if p]
                # Assemble labels and parse numbers
                if len(element_numbers) == 1:
                    label = "".join(element_parts)
                    value = self._maybe_parse_number(element_numbers[0])
                    # Add to sample
                    sample[label] = value
                    # Add to data definition
                    data_definition.add_feature(
                        self._create_scalar_feature(label, value)
                    )
                elif len(element_numbers) == len(element_parts):
                    for n, p in zip(element_numbers, element_parts):
                        label = p
                        value = self._maybe_parse_number(n)
                        # Add to sample
                        sample[label] = value
                        # Add to data definition
                        data_definition.add_feature(
                            self._create_scalar_feature(label, value)
                        )
                else:
                    base_label = "".join(element_parts)
                    for i, n in enumerate(element_numbers):
                        label = base_label + f"{i:d}"
                        value = self._maybe_parse_number(n)
                        # Add to sample
                        sample[label] = value
                        # Add to data definition
                        data_definition.add_feature(
                            self._create_scalar_feature(label, value)
                        )
        return sample, data_definition

    def _create_scalar_feature(self, name, value):
        if isinstance(value, int):
            return Feature(
                name,
                data_shape=[None, 1],
                data_format="NF",
                dtype=tf.string,
                decode_str_to=tf.int64,
            )
        if isinstance(value, float):
            return Feature(
                name,
                data_shape=[None, 1],
                data_format="NF",
                dtype=tf.string,
                decode_str_to=tf.float64,
            )
        return Feature(
            name,
            data_shape=[None, 1],
            data_format="NF",
            dtype=tf.string,
            decode_str_to=None,
        )

    def _maybe_parse_number(self, string):
        try:
            return int(string)
        except ValueError:
            try:
                return float(string)
            except ValueError:
                return string

    @staticmethod
    def _safe_div(a, b):
        a = np.asarray(a, dtype=np.float)
        b = np.asarray(b, dtype=np.float)
        return np.divide(a, b, out=np.zeros_like(a), where=b != 0)

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    def transform(
        self,
        samples: Iterable[dict],
        features: Iterable[str] = None,
        method: OptionsTransformMethods = OptionsTransformMethods.std,
        update_data_definition: bool = True,
    ) -> tuple:
        """Transform features with selected transformer.

        Args:
            samples (Iterable[dict]): List of sample dictionaries
            features (Iterable[str], optional): list of feature names. Defaults to None.
            method (OptionsTransformMethods, optional): method to use for the transform
                operation. Defaults to "std"(standardization).
            update_data_definition (bool): update data definition afterwards?

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        for feature_name in features:
            # Get selected transformer
            if method == OptionsTransformMethods.std:
                transformer = StandardScaler()
            elif method == OptionsTransformMethods.minmax:
                transformer = MinMaxScaler()
            elif method == OptionsTransformMethods.pca:
                transformer = PCA()
            else:
                raise ValueError(
                    f"Invalid transformation method: '{str(method)}'. "
                    + f"Must be in {str(list(OptionsTransformMethods))}."
                )
            # Assemble design matrix
            new_feature = copy.deepcopy(data_definition.features[feature_name])
            sample_index = new_feature.data_format.index("N")
            ndarray = np.concatenate(
                [
                    np.reshape(
                        sample[feature_name], [-1, sample[feature_name].shape[-1]]
                    )
                    for sample in samples
                ],
                axis=sample_index,
            )
            # Fit transformer
            transformer.fit(ndarray)
            # New feature name
            base_new_feature_name = f"{feature_name}_{method}"
            feature_index = len(
                [
                    f
                    for f in data_definition.features.keys()
                    if base_new_feature_name in f
                ]
            )
            new_feature_name = base_new_feature_name + str(feature_index)
            # Transform all samples
            for sample in samples:
                ndarray = np.asarray(sample[feature_name])
                shape = np.shape(ndarray)
                matrix = np.reshape(ndarray, [-1, shape[-1]])
                transformed_matrix = transformer.transform(matrix)
                ndarray = np.reshape(transformed_matrix, shape)
                sample[new_feature_name] = ndarray
            # TODO: some settings may change dimensionality F
            # Update feature
            new_feature.name = new_feature_name
            # Update data definition
            data_definition.add_feature(new_feature)
            if update_data_definition:
                self.data_definition = copy.deepcopy(data_definition)
        return samples, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def merge_features(
        self,
        sample: dict,
        features: Iterable[str] = None,
        axis: OptionsAxis = OptionsAxis.F,
    ) -> tuple:
        """Merge features in a sample into single features along given axis.

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names.
                Defaults to None.
            axis (str, optional): axis of feature tensor to split. Defaults to "F".

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        assert axis != "N"
        tmp_data_definition = copy.deepcopy(self.tmp_data_definition)
        features = features or list(tmp_data_definition.features.keys())
        # Get axis index
        feature_instances = [
            tmp_data_definition.features[feature_name] for feature_name in features
        ]
        indices = {fi.data_format.index(axis) for fi in feature_instances}
        if len(indices) != 1:
            raise ValueError(
                "Inconsistent axes. Ensure features have the same data format."
            )
        index = list(indices)[0]
        # Concatenate all samples
        ndarray = np.concatenate(
            [sample[feature_name] for feature_name in features], axis=index
        )
        # New feature name
        base_new_feature_name = "merge"
        feature_index = len(
            [
                f
                for f in self.data_definition.features.keys()
                if base_new_feature_name in f
            ]
        )
        new_feature_name = base_new_feature_name + str(feature_index)
        # Save merge data
        sample[new_feature_name] = ndarray
        # Create new feature
        new_feature = copy.deepcopy(feature_instances[0])
        new_feature.name = new_feature_name
        new_feature.data_shape = [
            None if ds is None else ns
            for ds, ns in zip(new_feature.data_shape, ndarray.shape)
        ]
        # Update data definition
        tmp_data_definition.add_feature(new_feature)
        self.tmp_data_definition = tmp_data_definition
        data_definition = copy.deepcopy(tmp_data_definition)
        return sample, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def split_features(
        self,
        sample: dict,
        features: Iterable[str] = None,
        axis: OptionsAxis = OptionsAxis.F,
    ) -> tuple:
        """Splits features in a sample into multiple features along structural axis.

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names.
                Defaults to None.
            axis (str, optional): axis of feature tensor to split. Defaults to "F".

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        assert axis != "N"
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        for feature_name in features:
            feature = data_definition.features[feature_name]
            ndarray = copy.deepcopy(sample[feature_name])
            try:
                assert isinstance(ndarray, np.ndarray)
                index = feature.data_format.index(axis)
            except (AssertionError, ValueError):
                # Feature doesn't have this axis
                continue
            else:
                slices = np.split(ndarray, ndarray.shape[index], axis=index)
                # Update features
                feature = self._update_feature_after_split(feature_name, axis)
                for i, slice in enumerate(slices):
                    # Make derivatives of old feature
                    new_feature = copy.deepcopy(feature)
                    new_feature_name = feature_name + f"_{axis}{i:d}"
                    new_feature.name = new_feature_name
                    data_definition.add_feature(new_feature)
                    # Save slices
                    sample[new_feature_name] = np.squeeze(slice, index)
        return sample, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def split_features_into_samples(
        self,
        sample: dict,
        features: Iterable[str] = None,
        axis: OptionsAxis = OptionsAxis.S,
        in_place: bool = False,
    ) -> tuple:
        """Splits a sample into multiple samples along structural axis.

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names.
                Defaults to None.
            axis (str, optional): axis of feature tensor to split. Defaults to S.
            in_place (bool, optional): overwrite existing features or create new names?
                Defaults to False.

        Returns:
            tuple:  samples (list): list of multiple modified samples,
                    data_definition (DataDefinition): modified data definition
        """
        assert axis not in "NF"
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        dict_of_lists = {}
        for feature_name in features:
            feature = data_definition.features[feature_name]
            ndarray = copy.deepcopy(sample[feature_name])
            # New name
            if in_place:
                new_feature_name = feature_name
            else:
                base_new_feature_name = f"{feature_name}_split"
                feature_index = len(
                    [
                        f
                        for f in data_definition.features.keys()
                        if base_new_feature_name in f
                    ]
                )
                new_feature_name = base_new_feature_name + str(feature_index)
            try:
                assert isinstance(ndarray, np.ndarray)
                index = feature.data_format.index(axis)
            except (AssertionError, ValueError):
                # Feature doesn't have this axis
                dict_of_lists[feature_name] = ndarray
                continue
            else:
                slices = np.split(ndarray, ndarray.shape[index], axis=index)
                slices = [np.squeeze(s, axis=index) for s in slices]
                # Update feature
                feature = self._update_feature_after_split(feature_name, axis)
                feature.name = new_feature_name
                data_definition.features[new_feature_name] = feature
                # Save slices
                dict_of_lists[new_feature_name] = slices
        # Change dict of lists to list of dicts
        max_len = max(len(v) for v in dict_of_lists.values() if isinstance(v, list))
        dict_of_lists = {
            k: v if isinstance(v, list) else [v] * max_len
            for k, v in dict_of_lists.items()
        }
        samples = [dict(zip(dict_of_lists, t)) for t in zip(*dict_of_lists.values())]
        # Add original sample to each sample
        samples = [sample | s for s in samples]
        return samples, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def resample(
        self,
        sample: dict,
        features: Iterable[str] = None,
        axis: OptionsAxis = OptionsAxis.S,
        factor: int = None,
        size: int = None,
        method: OptionsResampleMethods = OptionsResampleMethods.fft,
    ) -> tuple:
        """Resamples features to a new dimensionality along a given axis.

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names to resample.
                Defaults to None.
            axis (str, optional): axis of feature tensor. Defaults to "S".
            factor (int, optional): downsampling factor for given axis. Mutually
                exclusive with size.
            size (int, optional): new (target) dimensionality for given axis.
                Mutually exclusive with factor.
            method (str, optional): Resampling method. One of 'poly', 'decimate', or
                'fft'.

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        assert axis != "N"
        tmp_data_definition = copy.deepcopy(self.tmp_data_definition)
        features = features or list(self.data_definition.features.keys())
        if size and factor:
            raise ValueError("Arguments size and factor are mutually exclusive")
        for feature_name in features:
            feature = tmp_data_definition.features[feature_name]
            ndarray = copy.deepcopy(sample[feature_name])
            try:
                assert isinstance(ndarray, np.ndarray)
                index = feature.data_format.index(axis)
            except (AssertionError, ValueError):
                # Feature doesn't have this axis
                continue
            # Update value
            if method == OptionsResampleMethods.poly:
                if size:
                    n = size
                else:
                    n = ndarray.shape[index] // factor
                value = signal.resample_poly(
                    ndarray, n, ndarray.shape[index], axis=index
                )
            elif method == OptionsResampleMethods.decimate:
                if factor:
                    q = factor
                else:
                    q = ndarray.shape[index] // size
                value = signal.decimate(ndarray, q, axis=index)
            elif method == OptionsResampleMethods.fft:
                if size:
                    n = size
                else:
                    n = ndarray.shape[index] // factor
                value = signal.resample(ndarray, n, axis=index)
            else:
                raise ValueError(
                    f"Invalid resampling method: '{str(method)}'. "
                    + f"Must be in {str(list(OptionsResampleMethods))}."
                )
            # Update data definition
            base_new_feature_name = f"{feature_name}_resampled"
            feature_index = len(
                [
                    f
                    for f in self.data_definition.features.keys()
                    if base_new_feature_name in f
                ]
            )
            new_feature_name = base_new_feature_name + str(feature_index)
            new_feature = copy.deepcopy(feature)
            new_feature.name = new_feature_name
            if new_feature.decode_str_to:
                new_feature.decode_str_to = str(value.dtype)
            elif new_feature.dtype:
                new_feature.dtype = str(value.dtype)
            new_feature.data_shape[index] = value.shape[index]
            tmp_data_definition.add_feature(new_feature)
            # Update global data definition
            self.tmp_data_definition = tmp_data_definition
            # Save value
            sample[new_feature_name] = value
        return sample, tmp_data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def reduce(
        self,
        sample: dict,
        features: Iterable[str] = None,
        axis: OptionsAxis = OptionsAxis.F,
        mode: OptionsReduceMethods = OptionsReduceMethods.mean,
    ) -> tuple:
        """Reduces the dimensionality of the given axis using given reduction mode.

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names.
                Defaults to None.
            axis (str, optional): axis of feature tensor to split. Defaults to "F".
            mode (str, optional): how to reduce the given axis. Defaults to "mean".
                One of "mean", "std", "min", "max", or an index integer

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        assert axis != "N"
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        mode_str = mode
        for feature_name in features:
            feature = data_definition.features[feature_name]
            ndarray = copy.deepcopy(sample[feature_name])
            try:
                assert isinstance(ndarray, np.ndarray)
                index = feature.data_format.index(axis)
            except (AssertionError, ValueError):
                # Feature doesn't have this axis
                continue
            # Update value
            if mode == OptionsReduceMethods.mean:
                value = np.mean(ndarray, axis=index)
            elif mode == OptionsReduceMethods.std:
                value = np.std(ndarray, axis=index)
            elif mode == OptionsReduceMethods.min:
                value = np.min(ndarray, axis=index)
            elif mode == OptionsReduceMethods.max:
                value = np.max(ndarray, axis=index)
            elif mode == OptionsReduceMethods.ravel:
                new_shape = [d for i, d in enumerate(ndarray.shape) if i != index]
                new_shape[0] = -1
                value = np.reshape(ndarray, newshape=new_shape)
            else:
                try:
                    # Assume mode is an index (integer)
                    element = int(mode)
                    idx = [slice(None)] * len(np.shape(ndarray))
                    idx[index] = element
                    value = ndarray[tuple(idx)]
                except ValueError as e:
                    help = "Must be one of 'mean', 'std', 'min', 'max', or an int."
                    raise ValueError(f"Invalid mode: {mode}. {help}") from e
                mode_str = "index"
            # Update data definition
            base_new_feature_name = feature_name + f"_{axis}_{mode_str}"
            feature_index = len(
                [
                    f
                    for f in data_definition.features.keys()
                    if base_new_feature_name in f
                ]
            )
            new_feature_name = base_new_feature_name + str(feature_index)
            new_feature = self._update_feature_after_split(
                feature_name, axis=axis, data_definition=data_definition
            )
            new_feature.name = new_feature_name
            if new_feature.decode_str_to:
                new_feature.decode_str_to = str(value.dtype)
            elif new_feature.dtype:
                new_feature.dtype = str(value.dtype)
            data_definition.add_feature(new_feature)
            # Save value
            sample[new_feature_name] = value
        return sample, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def permute_axes(
        self,
        sample: dict,
        features: Iterable[str] = None,
        new_data_formats: Iterable[str] = None,
    ) -> tuple:
        """Permute the axis of features according to new data_format.

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names.
                Defaults to None.
            new_data_formats (Iterable[str], optional): list of new data_formats.
                Defaults to None

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        assert len(new_data_formats) == len(features)
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        for i, feature_name in enumerate(features):
            feature = data_definition.features[feature_name]
            new_data_format = new_data_formats[i]
            old_data_format = feature.data_format
            assert len(old_data_format) == len(
                new_data_format
            ), "Old and new data_format have to have same length"
            ndarray = copy.deepcopy(sample[feature_name])
            perm_idx = []
            for axis in new_data_format:
                try:
                    perm_idx.append(old_data_format.index(axis))
                except ValueError as exc:
                    raise ValueError(
                        f"""The axis {axis} does not exist in old data_format.
                        Try using preprocessing function 'redefine_axis' instead."""
                    ) from exc
            # Update value
            ndarray_new = np.transpose(ndarray, perm_idx)
            # Update data definition
            feature.data_format = new_data_format
            feature.data_shape = ndarray_new.shape
            data_definition.add_feature(feature)
            # Save value
            sample[feature_name] = ndarray_new
        return sample, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def redefine_axes(
        self,
        sample: dict,
        features: Iterable[str] = None,
        new_data_formats: Iterable[str] = None,
    ) -> tuple:
        """Redefine the axis of features according to new data_format.

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names.
                Defaults to None.
            new_data_formats (Iterable[str], optional): list of new data_formats.
                Defaults to None

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        assert len(new_data_formats) == len(features)
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        for i, feature_name in enumerate(features):
            new_data_format = new_data_formats[i]
            old_data_format = copy.deepcopy(
                data_definition.features[feature_name].data_format
            )
            assert len(old_data_format) == len(
                new_data_format
            ), "Old and new data_format have to have same length"
            # Update data definition
            data_definition.features[feature_name].data_format = new_data_format
        return sample, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def clean_outliers(
        self,
        sample: dict,
        features: Iterable[str] = None,
        axis: OptionsAxis = OptionsAxis.S,
        detection: OptionsOutlierDetection = OptionsOutlierDetection.zscore,
        threshold: str = "3.0",
        replacement: OptionsOutlierReplacement = OptionsOutlierReplacement.interpolate,
    ) -> tuple:
        """Replace outlier in given feature along single axis.

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names. Defaults to None.
            axis (str, optional): axis of feature tensor. Defaults to "S".
            detection (str, optional): how to compute outliers. Defaults to "z-score".
                Must be in ["z-score", "abs_value"]
            threshold (str, optional): feature name or valid numeric string as threshold
                above which an absolute value is an outlier.
            replacement (str, optional): how to replace outliers. Defaults to
                "interpolate". Must be in ["interpolate", "clip_abs"]

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        # TODO: separate for each F
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        for feature_name in features:
            # Get new feature name
            base_new_feature_name = f"{feature_name}_cleaned"
            feature_index = len(
                [
                    f
                    for f in data_definition.features.keys()
                    if base_new_feature_name in f
                ]
            )
            new_feature_name = base_new_feature_name + str(feature_index)
            # Get ndarray and feature axes
            ndarray = copy.deepcopy(sample[feature_name])
            feature = data_definition.features[feature_name]
            try:
                assert isinstance(ndarray, np.ndarray)
                index = feature.data_format.index(axis)
            except (AssertionError, ValueError):
                # Feature doesn't have this axis
                continue
            # Parse threshold value
            if threshold in list(data_definition.features.keys()):
                threshold_feature = threshold
                threshold = copy.deepcopy(sample[threshold_feature])
                if threshold.shape != ndarray.shape:
                    raise ValueError(
                        f"Selected threshold Feature '{threshold_feature}' of shape "
                        + f"{str(threshold.shape)} is incompatible with Feature "
                        + f"'{feature_name}' of shape {str(feature.shape)}."
                    )
            else:
                threshold = self._maybe_parse_number(threshold)
            # Compute outlier mask
            if detection == OptionsOutlierDetection.zscore:
                zscores = stats.zscore(ndarray, axis=index)
                mask = np.abs(zscores) > threshold
            elif detection == OptionsOutlierDetection.abs_value:
                # TODO: threshold could be a feature?
                mask = np.abs(ndarray) > threshold
            else:
                raise ValueError(
                    f"Invalid outlier detection method: '{str(detection)}'. "
                    + f"Must be in {str(list(OptionsOutlierDetection))}."
                )
            # Replace outliers
            # TODO: drop instead of replace
            if replacement == OptionsOutlierReplacement.interpolate:
                x = np.arange(0, ndarray.shape[index])
                reduction_axes = [i for i in range(ndarray.ndim) if i != index]
                mask_1d = np.squeeze(
                    np.apply_over_axes(np.any, mask, axes=reduction_axes)
                )
                xp = x[~mask_1d]
                slc = [
                    slice(None) if i != index else mask_1d for i in range(ndarray.ndim)
                ]
                neg_slc = [
                    slice(None) if i != index else ~mask_1d for i in range(ndarray.ndim)
                ]
                yp = ndarray[tuple(neg_slc)]
                f = interpolate.interp1d(xp, yp, axis=index, fill_value="extrapolate")
                ndarray[mask] = f(x[mask_1d])[mask[tuple(slc)]]
            elif replacement == OptionsOutlierReplacement.clip_abs:
                if isinstance(threshold, np.ndarray):
                    ndarray[mask] = np.sign(ndarray[mask]) * threshold[mask]
                else:
                    ndarray[mask] = np.sign(ndarray[mask]) * threshold
            else:
                raise ValueError(
                    f"Invalid outlier replacement method: '{str(replacement)}'. "
                    + f"Must be in {str(list(OptionsOutlierReplacement))}."
                )
            # Update sample
            sample[new_feature_name] = ndarray
            # Update data definition
            new_feature = copy.deepcopy(data_definition.features[feature_name])
            new_feature.name = new_feature_name
            if new_feature.decode_str_to:
                new_feature.decode_str_to = str(ndarray.dtype)
            elif new_feature.dtype:
                new_feature.dtype = str(ndarray.dtype)
            data_definition.features[new_feature_name] = new_feature
        return sample, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    def encode_categorical(
        self,
        samples: Iterable[dict],
        features: Iterable[str] = None,
        update_data_definition: bool = True,
    ) -> tuple:
        """Encode categorical features into ordinal representation.

        Args:
            samples (Iterable[dict]): List of sample dictionaries
            features (Iterable[str], optional): list of feature names. Defaults to None.
            update_data_definition (bool): update data definition afterwards?

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        for feature_name in features:
            # Get classes of all sample
            encoder = LabelEncoder()
            classes = []
            for sample in samples:
                ndarray = sample[feature_name]
                encoder.fit(ndarray.ravel())
                classes.extend(encoder.classes_)
            encoder.classes_ = np.unique(np.asarray(classes))
            dtype = encoder.classes_.dtype
            # Transform all samples
            for sample in samples:
                ndarray = np.asarray(sample[feature_name], dtype=dtype)
                shape = np.shape(ndarray)
                vector = np.ravel(ndarray, order="C")
                transformed_vector = encoder.transform(vector)
                ndarray = np.reshape(transformed_vector, shape, order="C")
                sample[feature_name] = ndarray
            # Update feature
            feature = data_definition.features[feature_name]
            if feature.decode_str_to is not None:
                feature.decode_str_to = tf.uint8
            elif feature.dtype is tf.string:
                feature.dtype = tf.uint8
            feature.categories = list(str(c) for c in encoder.classes_)
            # Update data definition
            data_definition.features[feature_name] = feature
            if update_data_definition:
                self.data_definition = copy.deepcopy(data_definition)
        return samples, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def add(
        self, sample: dict, features: Iterable[str] = None, value: str = ""
    ) -> tuple:
        """Add feature or constant value to feature(s).

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names. Defaults to None.
            value (str, optional): feature name or valid numeric string to add to the
                features. Defaults to "".

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        for feature_name in features:
            # Get new feature name
            base_new_feature_name = f"{feature_name}_add"
            feature_index = len(
                [
                    f
                    for f in data_definition.features.keys()
                    if base_new_feature_name in f
                ]
            )
            new_feature_name = base_new_feature_name + str(feature_index)
            # Compute new value
            feature_value = copy.deepcopy(sample[feature_name])
            if value in list(data_definition.features.keys()):
                value = copy.deepcopy(sample[value])
            else:
                value = self._maybe_parse_number(value)
            try:
                ndarray = feature_value + value
            except (np.core._exceptions.UFuncTypeError, TypeError):
                # For string arrays, unpack arrays of incompatible dtypes and concat
                wrap_as_array = False
                if isinstance(feature_value, np.ndarray):
                    feature_value = str(feature_value.item())
                    wrap_as_array = True
                if isinstance(value, np.ndarray):
                    value = str(value.item())
                ndarray = feature_value + value
                if wrap_as_array:
                    ndarray = np.asarray(ndarray)
            # Update sample
            sample[new_feature_name] = ndarray
            # Update data definition
            new_feature = copy.deepcopy(data_definition.features[feature_name])
            new_feature.name = new_feature_name
            dtype = str(ndarray.dtype)
            if dtype == "object":
                dtype = "string"
            if new_feature.decode_str_to:
                new_feature.decode_str_to = dtype
            elif new_feature.dtype:
                new_feature.dtype = dtype
            data_definition.features[new_feature_name] = new_feature
        return sample, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def subtract(
        self, sample: dict, features: Iterable[str] = None, value: str = ""
    ) -> tuple:
        """Subtract feature or constant value from feature(s).

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names. Defaults to None.
            value (str, optional): feature name or valid numeric string to add to the
                features. Defaults to "".

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        for feature_name in features:
            # Get new feature name
            base_new_feature_name = f"{feature_name}_sub"
            feature_index = len(
                [
                    f
                    for f in data_definition.features.keys()
                    if base_new_feature_name in f
                ]
            )
            new_feature_name = base_new_feature_name + str(feature_index)
            # Compute new value
            feature_value = copy.deepcopy(sample[feature_name])
            if value in list(data_definition.features.keys()):
                value = copy.deepcopy(sample[value])
            else:
                value = self._maybe_parse_number(value)
            ndarray = feature_value - value
            # Update sample
            sample[new_feature_name] = ndarray
            # Update data definition
            new_feature = copy.deepcopy(data_definition.features[feature_name])
            new_feature.name = new_feature_name
            if new_feature.decode_str_to:
                new_feature.decode_str_to = str(ndarray.dtype)
            elif new_feature.dtype:
                new_feature.dtype = str(ndarray.dtype)
            data_definition.features[new_feature_name] = new_feature
        return sample, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def multiply(
        self, sample: dict, features: Iterable[str] = None, value: str = ""
    ) -> tuple:
        """Multiply feature(s) with another feature or a constant value.

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names. Defaults to None.
            value (str, optional): feature name or valid numeric string to add to the
                features. Defaults to "".

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        for feature_name in features:
            # Get new feature name
            base_new_feature_name = f"{feature_name}_mul"
            feature_index = len(
                [
                    f
                    for f in data_definition.features.keys()
                    if base_new_feature_name in f
                ]
            )
            new_feature_name = base_new_feature_name + str(feature_index)
            # Compute new value
            feature_value = copy.deepcopy(sample[feature_name])
            if value in list(data_definition.features.keys()):
                value = copy.deepcopy(sample[value])
            else:
                value = self._maybe_parse_number(value)
            ndarray = feature_value * value
            # Update sample
            sample[new_feature_name] = ndarray
            # Update data definition
            new_feature = copy.deepcopy(data_definition.features[feature_name])
            new_feature.name = new_feature_name
            if new_feature.decode_str_to:
                new_feature.decode_str_to = str(ndarray.dtype)
            elif new_feature.dtype:
                new_feature.dtype = str(ndarray.dtype)
            data_definition.features[new_feature_name] = new_feature
        return sample, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def divide(
        self, sample: dict, features: Iterable[str] = None, value: str = ""
    ) -> tuple:
        """Divide feature(s) by another feature or a constant value.

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names. Defaults to None.
            value (str, optional): feature name or valid numeric string to add to the
                features. Defaults to "".

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        for feature_name in features:
            # Get new feature name
            base_new_feature_name = f"{feature_name}_div"
            feature_index = len(
                [
                    f
                    for f in data_definition.features.keys()
                    if base_new_feature_name in f
                ]
            )
            new_feature_name = base_new_feature_name + str(feature_index)
            # Compute new value
            feature_value = copy.deepcopy(sample[feature_name])
            if value in list(data_definition.features.keys()):
                value = copy.deepcopy(sample[value])
            else:
                value = self._maybe_parse_number(value)
            ndarray = self._safe_div(feature_value, value)
            # Update sample
            sample[new_feature_name] = ndarray
            # Update data definition
            new_feature = copy.deepcopy(data_definition.features[feature_name])
            new_feature.name = new_feature_name
            if new_feature.decode_str_to:
                new_feature.decode_str_to = str(ndarray.dtype)
            elif new_feature.dtype:
                new_feature.dtype = str(ndarray.dtype)
            data_definition.features[new_feature_name] = new_feature
        return sample, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def drop_features(self, sample: dict, features: Iterable[str] = None) -> tuple:
        """Remove feature and tensor from data.

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of feature names. Defaults to None.

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        for feature_name in features:
            del sample[feature_name]
            del data_definition.features[feature_name]
        return sample, data_definition

    @register(PreprocessingContainer.REGISTERED_FUNCTIONS)
    @nested_preprocessing_function
    def rename_features(
        self,
        sample: dict,
        features: Iterable[str] = None,
        new_features: Iterable[str] = None,
    ) -> tuple:
        """Rename features in sample.

        Args:
            sample (dict): dictionary of feature tensors
            features (Iterable[str], optional): list of features to rename.
                Defaults to None.
            new_features (Iterable[str], optional): list of new feature names.
                Defaults to None.

        Returns:
            tuple:  sample (dict): modified dictionary of feature tensors,
                    data_definition (DataDefinition): modified data definition
        """
        data_definition = copy.deepcopy(self.data_definition)
        features = features or list(data_definition.features.keys())
        if len(features) != len(new_features):
            raise ValueError("Each feature to rename needs a new name.")
        for new_feature_name, feature_name in zip(new_features, features):
            # Update sample
            sample[new_feature_name] = copy.deepcopy(sample[feature_name])
            del sample[feature_name]
            # Update data definition
            data_definition.features[new_feature_name] = copy.deepcopy(
                data_definition.features[feature_name]
            )
            data_definition.features[new_feature_name].name = new_feature_name
            del data_definition.features[feature_name]
        return sample, data_definition

    def preprocess(self, samples, data_definition=None, steps=None, disable_tqdm=False):
        """Execute all preprocessing steps in order on given samples dictionary.

        Args:
            samples (list of dicts): list of sample dictionaries
            data_definition (DataDefinition): a data definition describing the samples.
                Defaults to a copy of self.data_definition.
            preprocessing_steps (list, optional): list of preprocessing steps.
                Defaults to self.preprocessing_steps.

        Returns:
            tuple:  samples (list): list of multiple modified samples,
                    data_definition (DataDefinition): modified data definition
        """
        if steps is None:
            steps = self.preprocessing_steps
        if data_definition is None:
            data_definition = copy.deepcopy(self.data_definition)
        # Change mode to execute
        self.mode = "execute"
        # Expand
        samples = [self.expand_sample_arrays(sample) for sample in samples]
        # TODO: auto transpose
        # Run preprocessing steps
        for step in tqdm.tqdm(
            steps,
            total=len(steps),
            file=self.stream_to_logger(),
            leave=True,
            desc="Preprocessing",
            unit="steps",
            dynamic_ncols=True,
            disable=disable_tqdm,
        ):
            self._validate_preprocessing_step(step)
            function = getattr(self, step[0])
            args = step[1]
            kwargs = step[2]
            samples, data_definition = function(samples, *args, **kwargs)
        # Validate data definition and samples
        samples, data_definition = self.validate(
            samples, data_definition=data_definition
        )
        # Change mode to record
        self.mode = "record"
        return samples, data_definition

    def validate(self, samples, data_definition=None, update_data_definition=True):
        """Validate converted samples and data definition for consistency.

        Args:
            samples (list of dicts): list of sample dictionaries
            data_definition (DataDefinition): a data definition describing the samples.
                Defaults to a copy of self.data_definition.
            update_data_definition (bool): update data definition afterwards?
        """
        if data_definition is None:
            data_definition = copy.deepcopy(self.data_definition)
        # Validate data definition and samples
        data_definition = self._validate_data_definition(data_definition, samples)
        samples = self._validate_samples(samples, data_definition)
        # Update data definition
        if update_data_definition:
            self.data_definition = copy.deepcopy(data_definition)
        return samples, data_definition


def expand_ndarray_for_feature(ndarray, feature):
    """Expand numpy array shape to match feature dimensionality.

    Args:
        ndarray (np.ndarray): Numpy ndarray
        feature (Feature): CIDS data Feature

    Returns:
        ndarray: expanded array to feature shape
    """
    # Note: You really need to do this in the following manual way.
    #       Full loop solutions do not work.
    ndarray = np.asarray(ndarray)
    data_shape = feature.data_shape
    data_format = feature.data_format
    # Append unitary feature axis to scalars or scalar structured tensors
    if len(ndarray.shape) < len(data_shape):
        if len(ndarray.shape) == 0 or (
            data_shape[data_format.index("F")] == 1 and ndarray.shape[-1] != 1
        ):
            ndarray = np.expand_dims(ndarray, axis=-1)
    # Add unitary batch axis
    if len(ndarray.shape) < len(data_shape):
        if data_shape[data_format.index("N")] in [None, 1]:
            ndarray = np.expand_dims(ndarray, axis=0)
    # See if any other unitary axes must be added
    for i, (d, f) in enumerate(zip(data_shape, data_format)):
        if f == "N":
            if d is not None:
                assert d == ndarray.shape[i], (
                    "Invalid number of samples (N) in tensor of feature "
                    + f"{feature.name}. Expected {d}, received {ndarray.shape[i]}."
                )
        elif f == "F":
            assert d == ndarray.shape[i], (
                "Invalid number of feature entries (F) in tensor of feature "
                + f"{feature.name}. Expected {d}, received {ndarray.shape[i]}."
            )
        else:
            if len(ndarray.shape) < len(data_shape):
                ndarray = np.expand_dims(ndarray, axis=i)
    return ndarray
