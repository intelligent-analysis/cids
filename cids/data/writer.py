# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Data writing functionality. Part of the CIDS toolbox.

    Classes:
        DataWriter:     write samples to files

"""
import os
from pathlib import Path

import numpy as np
import tensorflow as tf
import tqdm

from ..base.path_handler import BasePathHandler
from .definition import DataDefinition
from .preprocessor import expand_ndarray_for_feature


class DataWriter(BasePathHandler):
    def __init__(self, data_definition):
        """Writes defined data to files.

        Args:
            data_definition:    a DataDefinition instance
        """
        self._start_logging()
        self.data_definition = data_definition

    @classmethod
    def from_json(cls, data_definition):
        """Instantiates DataWriter from json file.

        Contains a list of preprocessing steps to perform during preprocessing.

        Args:
            data_definition:    a DataDefinition or a file containing a data definition
            path:               a json file containing serialized preprocessing steps
        """
        warn_messages = []
        try:
            if not isinstance(data_definition, DataDefinition):
                data_definition = DataDefinition.from_json(data_definition)
        except (FileNotFoundError, PermissionError):
            warn_messages.append(
                "Data definition could not be loaded. Does it exist yet?"
            )
            data_definition = DataDefinition()
        # Instantiate class
        inst = cls(data_definition)
        # Warnings
        for w in warn_messages:
            inst.warn(w)
        return inst

    def _convert_features_to_proto(self, ndarray_dict):
        """Serializes arrays in a dictionary based on the DataDefinition."""
        # Initialize example
        ex = tf.train.Example()
        # Loop over all features
        for feature_name, feature in self.data_definition.features.items():
            if feature.decode_str_to is not None:
                dtype = feature.decode_str_to
                if isinstance(dtype, tf.DType):
                    dtype = dtype.as_numpy_dtype
                ndarray = np.asarray(ndarray_dict[feature_name], dtype=dtype)
                ndarray = expand_ndarray_for_feature(ndarray, feature)
                # Check whether to encode as byte string
                if feature.dtype == tf.string:
                    # Ensure ndarray is the correct dtype
                    ndarray = ndarray.astype(dtype)
                    # Ensure ndarray has the correct shape
                    for fs, df, ns in zip(
                        feature.data_shape, feature.data_format, ndarray.shape
                    ):
                        if df in ["N", "F"] and fs is not None:
                            # Feature and batch axis must be matched exactly if given
                            assert fs == ns, (
                                f"Feature: {feature_name} shape"
                                + f" missmatch: {fs:d} != {ns:d}"
                            )
                        elif fs is not None:
                            # Structural axes may be smaller than given
                            assert fs >= ns, (
                                f"Feature: {feature_name} shape"
                                + f" missmatch: {fs:d} != {ns:d}"
                            )
                    # Convert to bytestring
                    if feature.decode_str_to == tf.string:
                        tensor = tf.convert_to_tensor(
                            ndarray.astype(str), dtype=tf.string
                        )
                        byte_string = tf.io.serialize_tensor(tensor).numpy()
                    else:
                        byte_string = ndarray.tobytes(order="C")
                    feature_shape_key = feature_name + "_shape"
                    # Add to example
                    ex.features.feature[feature_name].bytes_list.value.append(
                        byte_string
                    )
                    ex.features.feature[feature_shape_key].int64_list.value.extend(
                        ndarray.shape
                    )
                else:
                    raise NotImplementedError
            elif feature.dtype == tf.string:
                # Convert to bytestring
                value = ndarray_dict[feature_name]
                try:
                    if isinstance(value, np.ndarray):
                        value = value.astype(str)
                        value = value.item()
                    assert isinstance(value, str)
                    byte_string = value.encode("utf8")
                except (AttributeError, AssertionError) as e:
                    raise ValueError(
                        f"Invalid value for feature '{feature_name}'."
                        + "Expected type 'str' or 'ndarray' with single entry. "
                        + "Received: "
                        + str(type(ndarray_dict[feature_name]))
                    ) from e
                # Add to example
                ex.features.feature[feature_name].bytes_list.value.append(byte_string)
            else:
                raise ValueError(
                    f"Unknown dtype {str(feature.decode_str_to)} for {feature_name}"
                )
        return ex

    def write_example(self, file, sample):
        """Alias for write_sample. Write a sample dictionary to a tfrecord file.

        Args:
            file (PathLike): path to a (nonexistent) tfrecord file
            sample (dict): a dictionary of numpy ndarrays and python primitive types

        Raises:
            ValueError: If sample contains non-serializable items.
        """
        self.write_sample(file, sample)

    def write_sample(self, file, sample):
        """Write a sample dictionary to a tfrecord file.

        Args:
            file (PathLike): path to a (nonexistent) tfrecord file
            sample (dict): a dictionary of numpy ndarrays and python primitive types

        Raises:
            ValueError: If sample contains non-serializable items.
        """
        file = Path(file)
        file.parent.mkdir(parents=True, exist_ok=True)
        # Serialize sample dictionary
        if hasattr(sample, "items"):
            # Is dictionary-like
            proto = self._convert_features_to_proto(sample)
        else:
            raise ValueError(
                "Example must be dictionary mapping feature names to ndarrays."
            )
        # Write proto
        with tf.io.TFRecordWriter(os.fspath(file)) as writer:
            writer.write(proto.SerializeToString())
        # Add sample to data definition
        self.data_definition.add_sample(file)

    def write_samples(self, samples, directory, basename=None):
        """Write multiple samples to files in a directory.

        Args:
            samples (iterable): list of sample dictionaries
            directory (PathLike): directory to write sample files into.
            basename (str, optional): Basename of each sample file. Infers from each
                sample if possible.
        """
        directory = Path(directory)
        # Process samples
        orig_basename = basename
        n = 0
        for sample in tqdm.tqdm(
            samples,
            total=len(samples),
            file=self.stream_to_logger(),
            leave=True,
            desc="Writing samples",
            unit="samples",
            dynamic_ncols=True,
        ):
            if basename is None:
                # Read basename from file
                try:
                    basename = sample["name"]
                    if isinstance(basename, np.ndarray):
                        basename = basename.item()
                except KeyError:
                    basename = sample.get("branchfile", "sample")
                    if isinstance(basename, np.ndarray):
                        basename = basename.item()
                    # Extract path stem
                    basename = basename.split("/")[-1].split("\\")[-1]
                    basename = basename.rsplit(".", maxsplit=1)[0]
            sample_file = directory / f"{n:06d}_{basename:s}.tfrecord"
            basename = orig_basename
            try:
                self.write_sample(sample_file, sample)
            except KeyError as e:
                self.warn(f"Missing key {e.args[0]} in: {sample_file}")
                continue
            self.log(f"Done processing sample {n:06d}: {os.fspath(sample_file)}")
            n += 1
        self.write_data_definition(directory)

    def write_data_definition(self, file):
        """Writes the data definition to a json file."""
        self.data_definition.to_json(file, write=True)


DataConverter = DataWriter
