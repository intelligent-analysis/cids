# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Data conversion, reading and processing. Part of the CIDS toolbox.


.. autosummary::
    :toctree: _autosummary

    DataDefinition
    Feature
    Preprocessor
    DataReader
    DataWriter
    definition
    reader
    writer
    preprocessor
    offline_processing
"""
from .definition import DataDefinition
from .definition import Feature
from .offline_processing.files import extract_subjects  # Legacy imports
from .offline_processing.files import get_list_of_subject_numbers  # Legacy imports
from .offline_processing.files import get_max_sequence_length  # Legacy imports
from .offline_processing.files import leave_one_out_split  # Legacy imports
from .offline_processing.files import split_samples  # Legacy imports
from .offline_processing.files import split_samples_by_directory  # Legacy imports
from .offline_processing.files import split_samples_by_subjects  # Legacy imports
from .offline_processing.files import subject_wise_cross_validation  # Legacy imports
from .preprocessor import Preprocessor
from .reader import DataReader
from .writer import DataWriter
