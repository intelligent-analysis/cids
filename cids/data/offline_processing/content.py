# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Data conversion and processing for file content. Part of the CIDS toolbox."""
import os
import sys
from os import PathLike
from pathlib import Path
from typing import Dict
from typing import Iterable

import tqdm

from ..definition import DataDefinition
from ..definition import Feature
from ..preprocessor import Preprocessor
from .content_image import read_jpg
from .content_image import read_tif
from .content_pace import read_h
from .content_pace import read_infile
from .content_pace import read_p3s
from .content_pace import read_p3simgeo
from .content_text import read_csv
from .content_text import read_root_summary


def read_file(file: PathLike, summary: bool = False):
    """Process a file based on its extension.

    Args:
        file (PathLike): path to a file.
        root (bool): if file is a root summary file. Defaults to False.

    Raises:
        ValueError: if file extension is unknown.

    Returns:
        dict: a sample dictionary of numerical tensors in file.
        list: a list of Features in the sample dictionary
    """
    file = Path(file)
    extension = Path(file).suffix
    if extension in [".csv", ".txt"]:
        if summary:
            return read_root_summary(file)
        return read_csv(file)
    if extension == ".jpg":
        return read_jpg(file)
    if extension == ".p3s":
        return read_p3s(file)
    if extension == ".p3simgeo":
        return read_p3simgeo(file)
    if extension == ".h":
        return read_h(file)
    if extension in [".infile", ".infile_saved"]:
        return read_infile(file)
    if extension == ".tif":
        return read_tif(file)
    raise ValueError(f"Unknown file extension ({extension}) of file {os.fspath(file)}")


def read_file_branch(
    data_definition: DataDefinition,
    branch: PathLike,
    leaves: Iterable[PathLike],
    root: PathLike = None,
):
    """Join data from associated files (leaves) with major sample files (branches).

    Args:
        data_definition (DataDefinition): initial data definition.
        branch (PathLike): path to a major sample file (branch).
        leaves (list): list of leaf files associated with the branch file.
        root (PathLike, optional): root directory that contains all files. Defaults to
            None.

    Returns:
        dict: a sample dictionary of numerical tensors in file.
        data_definition: an updated data definition
    """
    branch = Path(branch)
    leaves = [Path(leaf) for leaf in leaves]
    sample = {}
    # Process branch directory path relative to root directory
    if root is not None:
        root = Path(root)
        rel_path = branch.resolve().relative_to(root.resolve())
        branchfile = rel_path.as_posix()
        sample["branchfile"] = branchfile
        data_definition.add_feature(
            Feature(
                "branchfile",
                [None, 1],
                data_format="NF",
                dtype="string",
                decode_str_to=None,
            )
        )
    # Process branch file
    new_items, new_features = read_file(branch)
    sample.update(new_items)
    for feature in new_features:
        data_definition.add_feature(feature)
    # Process leaves
    for leaf in tqdm.tqdm(
        leaves,
        total=len(leaves),
        file=sys.stdout,
        leave=True,
        desc="Processing file hierarchy",
        unit="leaves",
        dynamic_ncols=True,
    ):
        new_items, new_features = read_file(leaf)
        sample.update(new_items)
        for feature in new_features:
            data_definition.add_feature(feature)
    # TODO: All features must have the same sequence length or no sequence length.
    #       Some postprocessing tools are expensive and not evaluated for all frames.
    #       Either: split into multiple non-sequential features or
    #       remove frames that have not been postprocessed
    return sample, data_definition


def read_file_hierarchy(
    file_hierarchy: Dict[PathLike, PathLike],
    data_definition: DataDefinition = None,
    root: PathLike = ".",
):
    """Process a file hierchy of branches and associated leaves into samples.

    Args:
        file_hierarchy (dict):  a dictionary of branches (keys) and leaves (values)
        data_definition (DataDefinition, optional): initial data definition. Defaults
            to empty DataDefinition.
        root (Path, optional): root directory that contains all files. Defaults to None.

    Returns:
        dict: a sample dictionary of numerical tensors in file.
        data_definition: an updated data definition
    """

    # Ensure initial data definition
    data_definition = data_definition or DataDefinition(
        input_features=[], output_features=[]
    )
    root = Path(root)

    # Create samples and update data definition
    samples = []
    for branch, leaves in tqdm.tqdm(
        file_hierarchy.items(),
        total=len(file_hierarchy),
        file=sys.stdout,
        leave=True,
        desc="Converting hierarchical data",
        unit=" branches",
        dynamic_ncols=True,
    ):
        # Read sample and update data definition
        sample, data_definition = read_file_branch(
            data_definition, branch, leaves, root=root
        )
        samples.append(sample)

    return samples, data_definition


def read_file_hierarchy_and_preprocess(
    file_hierarchy: Dict[PathLike, PathLike],
    preprocessor: Preprocessor,
    data_definition: DataDefinition = None,
    root: PathLike = ".",
):
    """Process a file hierarchy of branches and associated leaves and preprocess files
    according to preprocessing steps to generate preprocessed samples.

    Args:
        file_hierarchy (dict):  a dictionary of branches (keys) and leaves (values)
        preprocessor (Preprocessor): a preprocessor object containing preprocessing
        steps.
        data_definition (DataDefinition, optional): initial data definition. Defaults
            to empty DataDefinition.
        root (Path, optional): root directory that contains all files. Defaults to None.

    Returns:
        dict: a sample dictionary of numerical tensors in file.
        data_definition: an updated data definition
    """

    # Ensure initial data definition
    data_definition = data_definition or DataDefinition(
        input_features=[], output_features=[]
    )
    root = Path(root)

    # Create samples and update data definition
    preprocessed_samples = []
    data_definition_pp = DataDefinition(input_features=[], output_features=[])
    for branch, leaves in tqdm.tqdm(
        file_hierarchy.items(),
        total=len(file_hierarchy),
        file=sys.stdout,
        leave=True,
        desc="Converting hierarchical data",
        unit=" branches",
        dynamic_ncols=True,
    ):
        # Read sample and update data definition
        sample, data_definition_sample = read_file_branch(
            data_definition, branch, leaves, root=root
        )
        preprocessor.data_definition = data_definition_sample
        # TODO Split preprocessing into two parts: 1) Execute preprocessing steps
        # which act on single samples only for every sample on its own.
        # 2) Execute preprocessing steps which which require all samples on all samples.
        preprocessed_sample, data_definition_sample = preprocessor.preprocess(
            [sample], disable_tqdm=True
        )
        preprocessed_samples.append(preprocessed_sample[0])
        data_definition_pp += data_definition_sample
    return preprocessed_samples, data_definition_pp
