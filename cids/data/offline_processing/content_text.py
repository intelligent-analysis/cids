# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Data conversion and processing for text file formats. Part of the CIDS toolbox."""
from pathlib import Path

import numpy as np
import pandas as pd
import tensorflow as tf

from ..definition import Feature


def read_csv(file, mode="auto"):
    """Process a comma-separated-value file.

    Args:
        file (PathLike): path to a csv file
        mod (bool, optional): For different or weird csv notations. Defaults to "auto".

    Returns:
        dict: a sample dictionary of numerical tensors in file.
        list: a list of Features in the sample dictionary
    """
    if mode == "strict":
        df = pd.read_csv(file)
    elif mode == "python":
        # TODO: find number of delimiters, choose delimiter with largest count, ensure
        #       constant lengtht throughout file
        df = pd.read_csv(
            file,
            engine="python",
            skipinitialspace=True,
        )
        # Add generic headers
        if "int" in str(df.columns.dtype):
            df = df.add_prefix("col_")
    elif mode == "auto":
        for delim in [";", ",", r"\s+", r"\t+"]:
            try:
                df = pd.read_csv(
                    file,
                    delimiter=delim,
                    skipinitialspace=True,
                )
                if len(df.columns) < 2:
                    continue
                break
            except pd.errors.ParserError:
                continue
    elif mode == "Paul":
        # This only works for Paul's csv data
        df = pd.read_csv(file, delimiter=" |,|;|:|=", engine="python", header=None)
        # Paul only!
        df.columns = ["frame"] + [
            f'tort_{file.stem.split("tort_")[-1]}{i:d}'
            for i in range(len(df.columns) - 1)
        ]
        del df["frame"]
    else:
        raise NotImplementedError
    # Clean-up
    df = df.dropna(how="all", axis=1)
    # Convert to sample dict
    sample = {k: np.asarray(df[k]) for k in df.keys()}

    # Get feature definition
    features = []
    for feature_name, ndarray in sample.items():
        # Data shape
        if ndarray.shape[0] > 1:
            # Multiple 1d slices: assume time sequence
            data_format = "NSF"
            data_shape = [None, ndarray.shape[0], 1]
        else:
            # Single 1d slice: assume constant
            data_format = "NF"
            data_shape = [None, 1]
        # True dtype
        try:
            true_dtype = tf.as_dtype(ndarray.dtype)
        except TypeError:
            true_dtype = tf.string
        # Serializing dtype
        if true_dtype == tf.string:
            dtype = true_dtype
            if len(data_format) > 2:
                decode_str_to = true_dtype
            else:
                decode_str_to = None
        else:
            dtype = tf.string
            decode_str_to = true_dtype

        # Create feature
        features.append(
            Feature(
                feature_name,
                data_shape=data_shape,
                data_format=data_format,
                dtype=dtype,
                decode_str_to=decode_str_to,
            )
        )
    return sample, features


def read_root_summary(file, headers=None, col=None, col_sep=None, col_headers=None):
    """Process a root summary file with data associated with multiple branches.

    Args:
        file (PathLike) : summary input file of all the readable files with params
        with_headers (bool): if headers present in file
        headers (list) : specify if headers not present in file
        col (str or int): columns stored file_name information in input file.
        col_sep (str) : how infos is seperated in this column
        col_headers (list): specify headers for file_name col if present
    Returns:
        dict: a sample dictionary of numerical tensors in file.
        list: a list of Features in the sample dictionary
    """

    file = Path(file)
    extension = Path(file).suffix

    assert extension in [".csv", ".json", ".dat"]

    # Initial processing
    if extension == ".dat":
        if headers:
            df = pd.read_table(str(file), sep=r"\s+", header=headers)
        else:
            df = pd.read_table(str(file), sep=r"\s+")

    if extension == ".csv":
        if headers:
            df = pd.read_csv(str(file), header=headers)
        else:
            df = pd.read_csv(str(file))

    if extension == ".json":
        df = pd.read_json(str(file))

    # Processing of file name columns
    if isinstance(col, str):
        df2 = df[col].str.split(col_sep, expand=True)

    if isinstance(col, int):
        df2 = df[df.columns[col]].str.split(col_sep, expand=True)

    if isinstance(col_headers, list):
        if len(df2.columns) == len(col_headers):
            df2.columns = col_headers
        else:
            raise ValueError("Input columns does not match dimension of file names")
    else:
        raise TypeError(
            "Headers for values expanded from file name columns must be a list"
        )

    # Add features from names into data definition
    df = pd.concat([df, df2], axis=1)
    # df = df.select_dtypes(include=np.number)
    features_dict = {k: np.asarray(df[k]) for k in df.keys()}

    # Features
    features = []
    for feature_name, ndarray in features_dict.items():
        dtype = tf.as_dtype(ndarray.dtype)
        try:
            if len(ndarray) > 1:
                # Multiple 1d slices: assume time sequence
                data_shape = [None, ndarray.shape[0], 1]
                features.append(
                    Feature(
                        feature_name,
                        data_shape=data_shape,
                        data_format="NSF",
                        dtype=tf.string,
                        decode_str_to=dtype,
                    )
                )

        except TypeError:
            if dtype == tf.string:
                data_shape = [None, 1]
                features.append(
                    Feature(
                        feature_name,
                        data_shape=data_shape,
                        data_format="NF",
                        dtype=tf.string,
                        decode_str_to=None,
                    )
                )
            else:
                data_shape = [None, 1]
                features.append(
                    Feature(
                        feature_name,
                        data_shape=data_shape,
                        data_format="NF",
                        dtype=tf.string,
                        decode_str_to=dtype,
                    )
                )

    sample_dict = {k: np.asarray(df[k]) for k in df.keys()}

    return sample_dict, features
