# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Data conversion and processing for filesystem paths. Part of the CIDS toolbox."""
import os
import random
import re
from collections import Counter
from copy import deepcopy
from pathlib import Path
from warnings import warn

import numpy as np
import tensorflow as tf
from sklearn import model_selection as skms

DEBUG = True


def extract_subjects(paths, subjects, subj_nr_prefix="subject_"):
    """
    Extracts paths to specific subjects from list of paths.
    :param paths: List of paths, must contain "subject_"
    :param subjects: List of subject numbers to extract
    :param subj_nr_prefix: String prefix of subject number. Example: "subject_number_"
    :return: paths to samples from specified subjects
    """
    paths_to_subj = [
        [path for path in paths if subj_nr_prefix + i in path] for i in subjects
    ]
    return np.concatenate(paths_to_subj, axis=0)


def get_list_of_subject_numbers(paths, subj_nr_prefix="subject_", subj_nr_postfix="."):
    """
        Returns list of all unique subject ids in dataset
    :param paths: paths to data samples, must contain subject numbers
    :param subj_nr_prefix: String before subject number
    :param subj_nr_postfix: String after subject number
    :return: list of unique subject numbers in list of pahts
    """
    subject_ids = [
        path.split(subj_nr_prefix)[-1].split(subj_nr_postfix)[0] for path in paths
    ]
    return np.unique(subject_ids)


def leave_one_out_split(paths, loo_id, p_val=None, margin=0.15):
    """Leave-one-out dataset split."""

    # extract test samples (all samples with subject id = loo_id)
    test_samples = extract_subjects(paths, [loo_id])

    # delete test subject from list and shuffle it
    all_subject_ids = get_list_of_subject_numbers(paths)
    subject_ids = all_subject_ids[all_subject_ids != loo_id]
    split_dict = {}

    if p_val is not None:
        random.shuffle(subject_ids)

        n_subjects = len(subject_ids)
        n_all_subjects = len(all_subject_ids)
        n_val_subjects = int(p_val * n_all_subjects)
        n_val_samples = int(p_val * len(paths))
        # print("Aspired number of validation subjects: ", n_val_subjects)
        # print("Aspired validation samples: ", n_val_samples)
        # print(
        #     "Min number of validation samples: ",
        #     n_val_samples - n_val_samples * margin
        # )

        # split valid and test set by subjects from remaining list
        val_samples = []
        val_subjects = []
        n_val_subjects -= 2
        while len(val_samples) < n_val_samples * (1 - margin):
            n_val_subjects += 1
            val_subjects = subject_ids[n_subjects - n_val_subjects :]
            val_samples = extract_subjects(paths, val_subjects)

        train_subjects = subject_ids[: n_subjects - n_val_subjects]
        train_samples = extract_subjects(paths, train_subjects)

        split_dict["train"] = train_subjects.tolist()
        split_dict["valid"] = val_subjects.tolist()
        split_dict["test"] = [loo_id]
        print("###########################################")
        print("Dataset split (train, val, test): ")
        print(np.shape(train_samples), np.shape(val_samples), np.shape(test_samples))
        print("Nr. Subjects: ")
        print(np.shape(train_subjects), np.shape(val_subjects), np.shape([loo_id]))
        print("###########################################")
    else:  # No validation set needed
        train_samples = extract_subjects(paths, subject_ids)
        val_samples = []

    return train_samples, val_samples, test_samples, split_dict


def _maybe_patch_deprecated_splits(split_off, valid_or_test_split):
    if valid_or_test_split is not None:
        warn(
            "Keyword argument 'valid_split' and 'test_split' are deprecated. "
            + "Use 'split_off=[0.15, 0.15]' instead."
        )
        split_off.append(valid_or_test_split)
    return split_off


def _split_to_num_split_samples(num_samples, split):
    if isinstance(split, float) and abs(split) <= 1.0:
        return int(split * num_samples)
    if isinstance(split, int) and abs(split) <= num_samples:
        return split
    raise ValueError(f"Illegal split: {str(split)}. Must be float or int.")


def split_samples(
    samples, split_off=None, shuffle=True, valid_split=None, test_split=None
):
    """Splits samples into training and other datasets defined by split fractions.

    Args:
        samples:      list of data samples
        split_off:    list of fractions (float) or number of samples (int) to split off
        shuffle:      shuffle samples before splitting? Defaults to True
        valid_split:  (DEPRECATED) relative size of validation dataset
        test_split:   (DEPRECATED) relative size of test dataset

    Returns:
        datasets    list of samples in training and other the split-off dataset
    """
    num_samples = len(samples)
    if split_off is None:
        split_off = []
    # Patch deprecated split keywords into splits
    split_off = _maybe_patch_deprecated_splits(split_off, valid_split)
    split_off = _maybe_patch_deprecated_splits(split_off, test_split)
    assert split_off, "Must define fraction or number of samples to split off."
    # Shuffle
    if shuffle:
        samples = list(deepcopy(samples))
        random.shuffle(samples)
    # Convert relative splits into number of samples per split
    num_dataset_samples = [
        _split_to_num_split_samples(num_samples, s) for s in split_off
    ]
    # Major dataset (remaining data that is not split off) should be first
    num_train_samples = num_samples - np.sum(num_dataset_samples)
    num_dataset_samples = [num_train_samples] + num_dataset_samples[:-1]
    # Compute indices
    indices = np.cumsum(num_dataset_samples)
    # Split into multiple datasets
    return [list(map(str, list(s))) for s in np.split(samples, indices)]


def split_samples_by_directory(samples, dirs=("train", "valid", "test"), shuffle=True):
    """Splits samples into datasets by parent directory names.

    Args:
        samples:      list of data samples
        dirs:         (partial) names of parent directories to group samples by
        shuffle:      shuffle samples before splitting? Defaults to True

    Returns:
        datasets:   list of samples per dataset in the same order as dirs
    """
    # Shuffle
    if shuffle:
        samples = list(deepcopy(samples))
        random.shuffle(samples)
    return [[s for s in samples if d in Path(s).parent.name] for d in dirs]


def _str_split_numbers(s):
    head_body = s.rstrip("0123456789")
    tail = s[len(head_body) :]
    body = head_body.lstrip("0123456789")
    head = head_body[: -len(body)]
    return head, body, tail


def _get_sample_groups(sample):
    sample = Path(sample)
    string = sample.stem
    parts = re.split(r"[ \-_,.:/]+", string)
    parts = [_str_split_numbers(p) for p in parts]
    parts = [pp for p in parts for pp in p if pp]
    groups = {}
    for i, p in enumerate(parts):
        if p.isnumeric():
            value = int(p)
            name = parts[i - 1]
            groups[name] = value
    groups["path"] = os.fspath(sample)
    return groups


def split_samples_stratified_group(samples, split_off=None, classes=None, groups=None):
    split_off = split_off or []
    num_samples = len(samples)
    if callable(classes):
        classes = [classes(s) for s in samples]
    if callable(groups):
        groups = [groups(s) for s in samples]
    # Workaround for lack of StratifiedGroupShuffleSplit
    # Compute number of folds that can be reassembled into splitted datasets
    if classes and groups:
        num_decimals = max(f"{s:f}"[::-1].find(".") for s in split_off)
        if num_decimals > 2:
            raise ValueError(
                "Splits smaller than 1% not supported for stratified group splits."
            )
        # Compute number of required fractals
        left_over = 1.0 - np.sum(split_off)
        fractions = np.asarray([left_over] + split_off)
        gcd = np.gcd.reduce(np.asarray(fractions * 100, dtype=np.int64)) / 100
        num_splits = int(1 / gcd)
        # Perform split into fractals
        splitter = skms.StratifiedGroupKFold(n_splits=num_splits, shuffle=True)
        fractal_datasets = splitter.split(samples, y=classes, groups=groups)
        # Compute numbers of fractals
        num_fractals = np.asarray(fractions / gcd, dtype=np.int64)
        fractal_splits = np.cumsum(num_fractals)
        assert fractal_splits[-1] == len(samples), "Lost sample along the way."
        # Assemble fractal datasets
        datasets = []
        i = 0
        for fs in fractal_splits:
            datasets.append(fractal_datasets[i:fs])
            i = fs
        return datasets
    # Convert relative splits into number of samples per split
    if groups:
        num_groups = len(set(groups))
        num_dataset_samples = [
            _split_to_num_split_samples(num_groups, s) for s in split_off
        ]
    else:
        num_dataset_samples = [
            _split_to_num_split_samples(num_samples, s) for s in split_off
        ]
    # Get correct splitter
    if classes and not groups:
        SplitterClass = skms.StratifiedShuffleSplit
    elif not classes and groups:
        SplitterClass = skms.GroupShuffleSplit
    else:
        SplitterClass = skms.ShuffleSplit
    # Ensure we have classes
    classes = classes or np.zeros(len(samples), dtype=np.bool)
    groups = groups or np.arange(len(samples))
    # Set initial
    datasets = []
    leftover_samples = np.asarray(samples)
    leftover_classes = np.asarray(classes)
    leftover_groups = np.asarray(groups)
    # Perform split
    for n in num_dataset_samples:
        # Compute current split
        splitter = SplitterClass(n_splits=1, test_size=n)
        split_gen = list(
            splitter.split(
                leftover_samples,
                y=leftover_classes,
                groups=leftover_groups,
            )
        )
        leftover_idx, split_idx = split_gen[0][0], split_gen[0][1]
        # Append dataset
        datasets.append(leftover_samples[split_idx].tolist())
        # Update leftover
        leftover_samples = leftover_samples[leftover_idx]
        if leftover_classes is not None:
            leftover_classes = leftover_classes[leftover_idx]
        if leftover_groups is not None:
            leftover_groups = leftover_groups[leftover_idx]
    return [leftover_samples] + datasets


def split_samples_by_subjects(
    samples, valid_split, test_split, min_percent=0.8, seed=None, verbose=True
):
    """
    Splits data paths according to subjects and data samples
    Note: So far data has to be saved as "*_subject_*.*"
    e.g. "sample_01_subject_1001.tfrecord"
    :param samples: Path to all data
    :param valid_split: desired fraction of validation data
    :param test_split: desired fraction of test data
    :param min_percent: factor: min_samples = valid_split*len(dataset_paths)*min_percent
    :param seed:
    :return: lists: train, validation, test set, dict:subject split
    """

    def extract_subjects(paths, subjects):
        final_array = []
        for i in subjects:
            tmp_array = [path for path in paths if "subject_" + i in path]
            final_array.append(tmp_array)

        return np.concatenate(final_array, axis=0)

    # TODO: For all samples
    def get_subject_id(sample):
        stem_list = sample.split(".")[0].split("_")
        subject_field = stem_list.index("subject") + 1
        return stem_list[subject_field]

    subject_ids = np.array([get_subject_id(sample) for sample in samples])
    unique_ids = np.unique(subject_ids)
    if seed is not None:
        random.seed(a=seed)
    random.shuffle(unique_ids)
    if verbose:
        print("length unique ids: ", len(unique_ids))
        print("length of dataset: ", len(samples))
    number_subjects = len(unique_ids)

    test_part = int(number_subjects * test_split)
    test_tags = unique_ids[:test_part]
    test_arr = extract_subjects(samples, test_tags)
    while len(test_arr) < test_split * len(samples) * min_percent:
        test_part += 1
        test_tags = unique_ids[:test_part]
        test_arr = extract_subjects(samples, test_tags)

    val_part = int(number_subjects * valid_split)
    val_tags = unique_ids[number_subjects - val_part :]
    val_arr = extract_subjects(samples, val_tags)
    while len(val_arr) < valid_split * len(samples) * min_percent:
        val_part += 1
        val_tags = unique_ids[number_subjects - val_part :]
        val_arr = extract_subjects(samples, val_tags)

    train_tags = unique_ids[test_part : number_subjects - val_part]
    train_arr = extract_subjects(samples, train_tags)

    split_dict = {}
    split_dict["train"] = train_tags.tolist()
    split_dict["valid"] = val_tags.tolist()
    split_dict["test"] = test_tags.tolist()
    if verbose:
        print("###########################################")
        print("Dataset split (test, val, train): ")
        print(np.shape(test_arr), np.shape(val_arr), np.shape(train_arr))
        print("Nr. Subjects: ")
        print(np.shape(test_tags), np.shape(val_tags), np.shape(train_tags))
        print("###########################################")
    return train_arr, val_arr, test_arr, split_dict


def subject_wise_cross_validation(
    dataset_paths, p_val, p_test, seed=None, verbose=True
):
    """
    get one fixed test set and k-fold validation sets for hyperparameter tuning

    Splits data paths according to subjects and data samples
    Note: So far data has to be saved as "*_subject_*.*"
    e.g. "sample_01_subject_1001.tfrecord"
    :param k: number of k-fold cross-validation
    :param dataset_paths: Path to all data
    :param p_val: desired fraction of validation data
    :param p_test: desired fraction of test data
    :param min_percent: factor: min_samples = valid_split*len(samples)*min_percent
    :param seed:
    :return: lists: train, validation, test set, dict:subject split
    """
    # TODO define variability based on mean number of trials ?
    def extract_subjects(paths, subjects):
        final_array = []
        for i in subjects:
            tmp_array = [path for path in paths if "subject_" + i in path]
            final_array.append(tmp_array)

        return np.concatenate(final_array, axis=0)

    subject_ids = np.array(
        [sample.split("_")[-4] for sample in dataset_paths]
    )  # TODO proper solution working globally
    unique_ids = np.unique(subject_ids)
    if seed is not None:
        random.seed(a=seed)

    number_subjects = len(unique_ids)
    test_subjects = int(p_test * number_subjects)
    val_subjects = int(p_val * number_subjects)
    trials_per_subject = Counter(subject_ids)
    num_test = int(p_test * len(dataset_paths))
    num_val = int(p_val * len(dataset_paths))

    if verbose:
        print("length of dataset: ", len(dataset_paths))
        print("number of subjects: ", number_subjects)
        # print("trials per subject: ", trials_per_subject)

    random.shuffle(unique_ids)
    test_ids = list(unique_ids)
    val_ids = list(reversed(unique_ids))

    # get test set
    if verbose:
        print("Aspired number of test subjects: ", test_subjects)
        print(
            "Aspired number of test trials: ",
            num_test - num_test * 0.1,
            " to ",
            num_test + num_test * 0.1,
        )
    test_tag = []
    test_trials = 0
    i = -1
    while test_trials < num_test - num_test * 0.1 or len(test_tag) < test_subjects - 2:
        i = i + 1
        # print(test_ids[i])
        temp = trials_per_subject[test_ids[i]]
        test_trials = test_trials + temp
        # print("test trials: ", test_trials)

        if test_trials < num_test + num_test * 0.1 or len(test_tag) < test_subjects + 2:
            # print("Trials: ", test_trials, "Subjects: ", len(test_tag)+1)
            test_tag.append(test_ids[i])
            # print("test tag: ", test_tag)
        else:
            test_trials = test_trials - temp
            # print("test trials new: ", test_trials)
    # print("Test set: ", test_tag)

    # get validation sets
    if verbose:
        print("Aspired number of validation subjects: ", val_subjects)
        print(
            "Aspired number of validation trials: ",
            num_val - num_val * 0.2,
            " to ",
            num_val + num_val * 0.2,
        )
    val_tags = []
    num_seed = -1
    i = -1
    while i < len(val_ids) and len(val_tags) < seed:
        num_seed = num_seed + 1
        # print(num_seed)
        # print("New validation seed")
        val_tag = []
        val_trials = 0
        while val_trials < num_val - num_val * 0.2 or len(val_tag) < val_subjects - 2:
            i = i + 1
            temp = trials_per_subject[val_ids[i]]
            val_trials = val_trials + temp
            # print("val trials: ", val_trials)
            if val_trials < num_val + num_val * 0.2 or len(val_tag) < val_subjects + 2:
                # print("Trials: ", val_trials, "Subjects: ", len(val_tag) + 1)
                if val_ids[i] in test_tag:
                    # print(val_ids[i])
                    val_trials = val_trials - temp
                else:
                    val_tag.append(val_ids[i])
            else:
                val_trials = val_trials - temp
        val_tags.append(val_tag)
    # print("val_tags: ", val_tags)

    test_arr = []
    val_arr = []
    train_arr = []

    split_dict = {}
    split_dict["train"] = []
    split_dict["test"] = []
    split_dict["valid"] = []
    for k in range(seed):
        test_arr.append(extract_subjects(dataset_paths, test_tag))
        val_arr.append(extract_subjects(dataset_paths, val_tags[k]))
        train_tags = [item for item in list(unique_ids) if item not in test_tag]
        train_tags = [item for item in train_tags if item not in val_tags[k]]
        # print("Train tags: ", train_tags)
        train_arr.append(extract_subjects(dataset_paths, train_tags))

        split_dict["train"].append(train_tags)
        split_dict["valid"].append(val_tags[k])
        split_dict["test"].append(test_tag)

        if verbose:
            print("###########################################")
            print("Dataset split (test, val, train): ")
            print(np.shape(test_arr[k]), np.shape(val_arr[k]), np.shape(train_arr[k]))
            print("Nr. Subjects: ")
            print(np.shape(test_tag), np.shape(val_tags[k]), np.shape(train_tags))
            print("###########################################")

    return train_arr, val_arr, test_arr, split_dict


def get_max_sequence_length(dataset_paths, data_format, verbose=True):
    """
    get the longest sample in the data set
    can be used to initialise data_shape for RNNs on not time-normalised data
    :param dataset_paths: list, Path to all data
    :param data_format: string, format the data is in
    :return: max_len, int
    """
    max_len = 0
    if verbose:
        print("Checking sequence lengths...")
    for sample in dataset_paths:
        # filenames = [sample]
        raw_dataset = tf.data.TFRecordDataset(sample)

        for raw_record in raw_dataset.take(1):
            example = tf.train.Example()
            example.ParseFromString(raw_record.numpy())
            length = example.features.feature["data_shape"].int64_list.value[1]
            if verbose:
                print(length)
        if length > max_len:
            max_len = length

    return max_len
