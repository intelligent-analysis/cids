# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
import random

import matplotlib.pyplot as plt
import numpy as np
from PIL import Image

from .defs import normalise_data
from .defs import prepare_skepxle_size
from .defs import scale_data


def skepxles(
    data,
    path,
    name="test",
    image_size=(224, 224),
    spatial_info=False,
    norm_idx=(None, None, None, None),
    XYZ=True,
    XY=False,
    plot=False,
    save=True,
):
    """Data format as input: matrix[sequence, features] with
        features = [feature1x, feature1y, feature1z, feature2x, ...]

    Args:
        data ([type]): [description]
        path ([type]): [description]
        name (str, optional): [description]. Defaults to "test".
        image_size (tuple, optional): [description]. Defaults to (224, 224).
        spatial_info (bool, optional): [description]. Defaults to False.
        norm_idx (list, optional): [description]. Defaults to [None, None, None, None].
        XYZ (bool, optional): [description]. Defaults to True.
        XY (bool, optional): [description]. Defaults to False.
        plot (bool, optional): [description]. Defaults to False.
        save (bool, optional): [description]. Defaults to True.

    Returns:
        [type]: [description]
    """
    if XY:
        x_data = data[:, 0:-1:2].transpose()
        y_data = data[:, 1::2].transpose()
        if spatial_info:
            # normalise data: translate and rotate
            x_data, y_data = normalise_data(
                x_data, y_data, norm_idx
            )  # TODO for 3D data

        x_scaled, y_scaled, _ = scale_data(x=x_data, y=y_data, lim=255)

        # get skepxles
        x_skepxles, y_skepxles, _, skepxles_size = prepare_skepxle_size(
            x=x_scaled, y=y_scaled
        )
        num_spatials = int(image_size[0] / skepxles_size[0])
        idx = np.arange(x_skepxles.shape[0])

        # define one single random arrangement of joints
        for combination in range(num_spatials):
            random.seed(1)
            random.shuffle(idx)

            # add temporal information
            for frame in range(np.shape(y_skepxles)[1]):
                frame_x = x_skepxles[:, frame]
                frame_y = y_skepxles[:, frame]
                new_frame_x = frame_x[idx]
                new_frame_y = frame_y[idx]

                frame_x_reshaped = np.reshape(new_frame_x, skepxles_size)
                frame_y_reshaped = np.reshape(new_frame_y, skepxles_size)
                frame_z_reshaped = np.zeros(skepxles_size) + 127

                # stack data in time dimension
                if frame == 0:
                    stacked_frames_x = frame_x_reshaped
                    stacked_frames_y = frame_y_reshaped
                    stacked_frames_z = frame_z_reshaped

                else:
                    stacked_frames_x = np.concatenate(
                        (stacked_frames_x, frame_x_reshaped), axis=1
                    )
                    stacked_frames_y = np.concatenate(
                        (stacked_frames_y, frame_y_reshaped), axis=1
                    )
                    stacked_frames_z = np.concatenate(
                        (stacked_frames_z, frame_z_reshaped), axis=1
                    )

            if combination == 0:
                spatial_x = stacked_frames_x
                spatial_y = stacked_frames_y
                spatial_z = stacked_frames_z
            else:
                spatial_x = np.concatenate((spatial_x, stacked_frames_x), axis=0)
                spatial_y = np.concatenate((spatial_y, stacked_frames_y), axis=0)
                spatial_z = np.concatenate((spatial_z, stacked_frames_z), axis=0)

        RGB = np.stack((spatial_x, spatial_y, spatial_z), axis=2)
        image_int = np.asarray(RGB, dtype="uint8")

        # resize image
        img = Image.fromarray(image_int)
        image_resized = img.resize(size=image_size)
        RGB_image = np.array(image_resized)

    if XYZ:
        x_data = data[:, 0:-2:3].transpose()
        y_data = data[:, 1:-1:3].transpose()
        z_data = data[:, 2::3].transpose()

        if spatial_info:
            # normalise data: translate and rotate
            x_data, y_data = normalise_data(x_data, y_data, z_data)  # TODO for 3D data

        # scale data to 255 to create RGB image
        x_scaled, y_scaled, z_scaled = scale_data(x=x_data, y=y_data, z=z_data, lim=255)

        # get skepxles
        x_skepxles, y_skepxles, z_skepxles, skepxles_size = prepare_skepxle_size(
            x=x_scaled, y=y_scaled, z=z_scaled
        )
        num_spatials = int(image_size[0] / skepxles_size[0])
        idx = np.arange(x_skepxles.shape[0])

        # define one single random arrangement of joints
        for combination in range(num_spatials):
            random.seed(1)
            random.shuffle(idx)

            # add temporal information
            for frame in range(np.shape(x_skepxles)[1]):
                frame_x = x_skepxles[:, frame]
                frame_y = y_skepxles[:, frame]
                frame_z = z_skepxles[:, frame]
                new_frame_x = frame_x[idx]
                new_frame_y = frame_y[idx]
                new_frame_z = frame_z[idx]

                frame_x_reshaped = np.reshape(new_frame_x, skepxles_size)
                frame_y_reshaped = np.reshape(new_frame_y, skepxles_size)
                frame_z_reshaped = np.reshape(new_frame_z, skepxles_size)

                # stack data in time dimension
                if frame == 0:
                    stacked_frames_x = frame_x_reshaped
                    stacked_frames_y = frame_y_reshaped
                    stacked_frames_z = frame_z_reshaped

                else:
                    stacked_frames_x = np.concatenate(
                        (stacked_frames_x, frame_x_reshaped), axis=1
                    )
                    stacked_frames_y = np.concatenate(
                        (stacked_frames_y, frame_y_reshaped), axis=1
                    )
                    stacked_frames_z = np.concatenate(
                        (stacked_frames_z, frame_z_reshaped), axis=1
                    )

            if combination == 0:
                spatial_x = stacked_frames_x
                spatial_y = stacked_frames_y
                spatial_z = stacked_frames_z
            else:
                spatial_x = np.concatenate((spatial_x, stacked_frames_x), axis=0)
                spatial_y = np.concatenate((spatial_y, stacked_frames_y), axis=0)
                spatial_z = np.concatenate((spatial_z, stacked_frames_z), axis=0)

        RGB = np.stack((spatial_x, spatial_y, spatial_z), axis=2)
        image_int = np.asarray(RGB, dtype="uint8")

        # resize image
        img = Image.fromarray(image_int)
        image_resized = img.resize(size=image_size)
        RGB_image = np.array(image_resized)

    if plot:
        plt.figure(figsize=(20, 20), dpi=300)
        plt.imshow(image_resized)
        plt.savefig(path + "figures/" + name)
        plt.close()

    if save:
        subdir = os.path.join(path, "skepxles")
        os.makedirs(subdir, exist_ok=True)
        np.save(os.path.join(subdir, name), RGB_image)

    return RGB_image
