# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import math

import numpy as np
import scipy.io as spio
from scipy import signal as sg
from scipy.interpolate import interp1d
from scipy.spatial.transform import Rotation as R


################################################################################
# PCA output data
# TODO online solution
def PCA_outputs(samples, subjects=False):
    if subjects:
        # subject = np.transpose(samples[[-1], :])
        samples = np.transpose(samples[:-1, :])

    # PCA
    # flatten data
    samples_flat = np.reshape(samples, [samples.shape[0], -1])
    # get mean and standard deviation and scale data
    mean = samples_flat.mean(axis=0, keepdims=True)
    std = samples_flat.std(axis=0, keepdims=True)
    features_scaled = (samples_flat - mean) / std
    # calculate covariance matrix of scaled features and perform singular value
    #  decomposition
    cov = np.matmul(features_scaled.T, features_scaled)
    U, s, _ = np.linalg.svd(cov, compute_uv=True)
    # check explained variance and choose components to maintain 95% energy
    var_explained = np.round(s**2 / np.sum(s**2), decimals=3)
    no_components = 1
    while np.sum(var_explained[:no_components]) < 0.95:
        no_components = no_components + 1
        print(np.sum(var_explained[:no_components]))

    return U, no_components, mean, std


################################################################################
# Filter data
def filter_data(data, fs=100, fc=5):
    # fs = sampling frequency
    # fc = cut-off frequency

    data_filt = np.zeros(data.shape)
    w = fc / (fs / 2)  # Normalise the frequency
    b, a = sg.butter(fc, w, "low")
    # filtfilt filters the data in both directions, forward and backward
    for i in range(data.shape[1]):
        data_filt[:, i] = sg.filtfilt(b, a, data[:, i])
    # data_filt[:,1] = sg.filtfilt(b, a, data[:,1])
    # data_filt[:,2] = sg.filtfilt(b, a, data[:,2])

    return data_filt


################################################################################
# Skepxles
def normalise_data(x_data, y_data, norm_idx):
    # translate to norm_idx (heel marker)
    x1_norm = x_data[int(norm_idx[0] / 2), 0]
    y1_norm = y_data[int(norm_idx[0] / 2), 0]
    x2_norm = x_data[int(norm_idx[1] / 2), 0]
    y2_norm = y_data[int(norm_idx[1] / 2), 0]

    # select which foot is touching the ground
    if y1_norm < y2_norm:
        x_data_translated = x_data - x1_norm
        y_data_translated = y_data - y1_norm
    else:
        x_data_translated = x_data - x2_norm
        y_data_translated = y_data - y2_norm

    # rotate with regard to specified axis (hip)
    x1_axis = x_data_translated[int(norm_idx[2] / 2), 0]
    y1_axis = y_data_translated[int(norm_idx[2] / 2), 0]
    x2_axis = x_data_translated[int(norm_idx[3] / 2), 0]
    y2_axis = y_data_translated[int(norm_idx[3] / 2), 0]
    axis1 = np.stack((x1_axis, y1_axis), axis=0)
    axis2 = np.stack((x2_axis, y2_axis), axis=0)
    axis = axis1 - axis2
    angle = np.arctan2(axis[1], axis[0])

    def rotate(x, y, angle):
        x_new = math.cos(-angle) * x - math.sin(-angle) * y
        y_new = math.sin(-angle) * x + math.cos(-angle) * y
        return x_new, y_new

    x_data_rotated = np.empty(x_data_translated.shape)
    y_data_rotated = np.empty(y_data_translated.shape)

    for i in range(x_data_translated.shape[0]):
        x_data_rotated[i, :], y_data_rotated[i, :] = rotate(
            x_data_translated[i, :], y_data_translated[i, :], angle
        )

    return x_data_rotated, y_data_rotated


def scale_data(x, y, lim, z=(0, 1)):
    min_x = np.amin(x)
    max_x = np.amax(x)
    min_y = np.amin(y)
    max_y = np.amax(y)
    min_z = np.amin(z)
    max_z = np.amax(z)

    x_scaled = lim * ((x - min_x) / (max_x - min_x))
    y_scaled = lim * ((y - min_y) / (max_y - min_y))
    z_scaled = lim * ((z - min_z) / (max_z - min_z))
    return x_scaled, y_scaled, z_scaled


def prepare_skepxle_size(x, y, z=1):
    # input data format: features x time steps
    # create a square skepxle or only a difference of one in the different dimensions
    skepxles_size = [int(np.sqrt(x.shape[0])), int(np.sqrt(x.shape[0]))]
    if skepxles_size[0] * skepxles_size[1] < x.shape[0]:
        skepxles_size[0] = skepxles_size[0] + 1
    if skepxles_size[0] * skepxles_size[1] < x.shape[0]:
        skepxles_size[1] = skepxles_size[1] + 1
    assert skepxles_size[0] * skepxles_size[1] >= x.shape[0]

    # if the number of features does not result in a square pad with zeroes
    if skepxles_size[0] * skepxles_size[1] > x.shape[0]:
        difference = skepxles_size[0] * skepxles_size[1] - x.shape[0]
        x = np.pad(x, ((0, difference), (0, 0)), constant_values=0)
        y = np.pad(y, ((0, difference), (0, 0)), constant_values=0)
        z = np.pad(z, ((0, difference), (0, 0)), constant_values=0)
    assert skepxles_size[0] * skepxles_size[1] == x.shape[0]
    return x, y, z, np.asarray(skepxles_size)


################################################################################
# Time normalise sequences
def time_normalise(sequences, frame_number):
    sequences_norm = {}
    new_length = np.linspace(0, 1, frame_number)
    keys = sequences.keys()
    for key in keys:
        temp = sequences[key]
        sequences_norm[key] = []
        if isinstance(temp, list):
            for sequence in temp:
                original_length = np.linspace(0, 1, len(sequence))
                norm = np.empty([frame_number, sequence.shape[1]])
                for j in range(sequence.shape[1]):
                    x1 = interp1d(original_length, sequence[:, j], kind="cubic")
                    norm[:, j] = x1(new_length)
                sequences_norm[key].append(norm)
        else:
            sequence = temp
            original_length = np.linspace(0, 1, len(sequence))
            norm = np.empty([frame_number, sequence.shape[1]])
            for j in range(sequence.shape[1]):
                x1 = interp1d(original_length, sequence[:, j], kind="cubic")
                norm[:, j] = x1(new_length)
            sequences_norm[key].append(norm)
    return sequences_norm


################################################################################
# Quaternion handling
def quatmultiply(quaternion1, quaternion0):
    # w0, x0, y0, z0 = quaternion0
    w0 = quaternion0[:, 0]
    x0 = quaternion0[:, 1]
    y0 = quaternion0[:, 2]
    z0 = quaternion0[:, 3]

    w1 = quaternion1[:, 0]
    x1 = quaternion1[:, 1]
    y1 = quaternion1[:, 2]
    z1 = quaternion1[:, 3]

    # w1, x1, y1, z1 = quaternion1
    return np.stack(
        [
            -x1 * x0 - y1 * y0 - z1 * z0 + w1 * w0,
            x1 * w0 + y1 * z0 - z1 * y0 + w1 * x0,
            -x1 * z0 + y1 * w0 + z1 * x0 + w1 * y0,
            x1 * y0 - y1 * x0 + z1 * w0 + w1 * z0,
        ],
        axis=-1,
    )


def quat2eulzyx(quat):
    # http://www.kostasalexis.com/frame-rotations-and-representations.html
    M, _ = quat.shape
    ret = np.zeros((M, 3))
    ret[:, 0] = np.arctan2(
        (2 * (quat[:, 0] * quat[:, 1] + quat[:, 2] * quat[:, 3])),
        (
            np.square(quat[:, 3])
            + np.square(quat[:, 0])
            - np.square(quat[:, 1])
            - np.square(quat[:, 2])
        ),
    )

    ret[:, 1] = np.arcsin(2 * ((quat[:, 0] * quat[:, 2]) - (quat[:, 1] * quat[:, 3])))

    ret[:, 2] = np.arctan2(
        (2 * (quat[:, 0] * quat[:, 3] + quat[:, 1] * quat[:, 2])),
        (
            np.square(quat[:, 0])
            + np.square(quat[:, 1])
            - np.square(quat[:, 2])
            - np.square(quat[:, 3])
        ),
    )

    return ret


def quatconj(q):
    w = q[:, 0]
    x = q[:, 1]
    y = q[:, 2]
    z = q[:, 3]
    return np.stack([w, -x, -y, -z], axis=-1)


def quatrotate_matlab(q, v):
    q0 = q[:, 0]
    q1 = q[:, 1]
    q2 = q[:, 2]
    q3 = q[:, 3]

    mat = np.array(
        [
            [
                1 - 2 * np.square(q2) - 2 * np.square(q3),
                2 * (q1 * q2 + q0 * q3),
                2 * (q1 * q3 - q0 * q2),
            ],
            [
                2 * (q1 * q2 - q0 * q3),
                1 - 2 * np.square(q1) - 2 * np.square(q3),
                2 * (q2 * q3 + q0 * q1),
            ],
            [
                2 * (q1 * q3 + q0 * q2),
                2 * (q2 * q3 - q0 * q1),
                1 - 2 * np.square(q1) - 2 * np.square(q2),
            ],
        ]
    )
    v_rot = np.empty(v.shape)
    for i in range(v.shape[1]):
        v_rot[:, i] = np.matmul(mat[:, :, i], v[:, i])
    return np.transpose(v_rot)


def quatexp(x):
    a, b, c, d = x
    cons = np.exp(a)
    v = np.array([b, c, d])
    nv = np.linalg.norm(v)
    ret = np.zeros(4)
    ret[0] = np.cos(nv)
    ret[1:] = v / nv * np.sin(nv)
    return np.nan_to_num(np.array([ret * cons]))


def quatlog(q):
    # https://math.stackexchange.com/questions/2552/the-logarithm-of-quaternion/2554#2554
    theta = np.arccos(q[:, 0])
    v = q[:, 1:4] / np.expand_dims(np.sin(theta), axis=-1)
    an = np.empty(v.shape)
    for i in range(v.shape[0]):
        an[i, :] = np.dot(v[i, :], theta[i])
    M = an.shape[0]
    ret = np.zeros((M, 4))
    ret[:, 1:] = an
    return ret


def to_quat(x):
    # Transform from 3x3xn matrices to nX4 quaternions
    r = R.from_matrix(x)
    quat = r.as_quat()
    quat[:, [1, 2, 3, 0]] = quat[:, [0, 1, 2, 3]]
    return quat


def create_tmp(x, y):
    # Define temporary joint angles
    return quat2eulzyx(quatmultiply(quatconj(x), y))


################################################################################
def generateAccelerometerData(
    sensorposition, sensororientation, frequency=100, order=2, frames=5
):
    pos = (
        sg.savgol_filter(sensorposition, frames, order, axis=0, mode="interp") / 1000
    )  # TODO filter not good
    posGrad = np.gradient(pos, axis=0)
    # Calculate velocities
    vel = posGrad * frequency
    # Calculate accelerations
    velGrad = np.gradient(vel, axis=0)
    if isinstance(velGrad, np.ndarray):
        acc = -velGrad * frequency  # pylint: disable=invalid-unary-operand-type
    else:
        acc = [-g * frequency for g in velGrad]

    acc[:, 2] -= 9.81
    return quatrotate_matlab(sensororientation, np.transpose(acc))


def generateGyroscopeData(sensororientation, frequency=100, order=2, frames=5):
    q = sg.savgol_filter(sensororientation, frames, order, axis=0, mode="interp")
    q /= np.expand_dims(np.sqrt(np.sum(q**2, axis=1)), axis=-1)
    temp = quatconj(q[:-1, :])
    deltaq = quatmultiply(temp, q[1:, :])
    tmp = 2 * quatlog(deltaq) * frequency
    gyr = np.zeros((tmp.shape[0] + 1, 3))
    gyr[:-1, :] = tmp[:, 1:4]
    gyr[-1, :] = tmp[-1, 1:4]
    return gyr


################################################################################
# TODO Temporary solution to be able to read marker data from .mat structures
# TODO Python btk wrapper


def loadmat(filename):
    """
    this function should be called instead of direct spio.loadmat
    as it cures the problem of not properly recovering python dictionaries
    from mat files. It calls the function check keys to cure all entries
    which are still mat-objects
    """
    data = spio.loadmat(filename, struct_as_record=False, squeeze_me=True)
    return _check_keys(data)


def _check_keys(dict):
    """
    checks if entries in dictionary are mat-objects. If yes
    todict is called to change them to nested dictionaries
    """
    for key in dict:
        if isinstance(dict[key], spio.matlab.mio5_params.mat_struct):
            dict[key] = _todict(dict[key])
    return dict


def _todict(matobj):
    """
    A recursive function which constructs from matobjects nested dictionaries
    """
    dict = {}
    for strg in matobj._fieldnames:
        elem = matobj.__dict__[strg]
        if isinstance(elem, spio.matlab.mio5_params.mat_struct):
            dict[strg] = _todict(elem)
        else:
            dict[strg] = elem
    return dict
