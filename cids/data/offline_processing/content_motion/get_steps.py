# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import numpy as np
from scipy.signal import find_peaks


def right_steps(markers):
    touchDowns = []
    relativeSearchRange = [-5, 10]

    # Determine z-direction second order gradients of the relevant markers
    # Heel
    heelVel = np.gradient(markers["RHEEL"][:, 2])
    heelAcc = np.gradient(heelVel)
    # Toe
    toeVel = np.gradient(markers["RTOE"][:, 2])
    toeAcc = np.gradient(toeVel)

    # Determine initial touchdown candidates based marker z-trajectories
    heelTDCandidates = find_peaks(-markers["RHEEL"][:, 2], distance=50, prominence=5)
    toeTDCandidates = find_peaks(
        -markers["RTOE"][:, 2], distance=50, prominence=3, width=40
    )

    # plt.figure(figsize=(30, 10), dpi=300)
    # plt.plot(-markers["RHEEL"][:,2])
    # plt.plot(-markers["RTOE"][:, 2])
    # plt.savefig("Test_re")
    # plt.close()

    # Make sure at least one complete step can be extracted
    if not (len(heelTDCandidates[0]) > 1 and len(toeTDCandidates[0]) >= 1) or not (
        len(heelTDCandidates[0]) >= 1 and len(toeTDCandidates[0]) > 1
    ):
        # print("no right step")
        return

    # Check if forefoot / heel touches the ground first( in time)
    if heelTDCandidates[0][0] < toeTDCandidates[0][0]:  # Heel-walker
        # Iterate over possible candidates
        numTDCandidates = len(heelTDCandidates[0])
        # print(numTDCandidates)
        for i in range(numTDCandidates):
            touchDowns.append(heelTDCandidates[0][i])
            # Try to refine the touchdown - position
            try:
                # Search for acceleration maximum around heel TD candidate
                searchRange = [
                    heelTDCandidates[0][i] + relativeSearchRange[0],
                    heelTDCandidates[0][i] + relativeSearchRange[1],
                ]
                relativeIndex = np.argmax(heelAcc[searchRange[0] : searchRange[1]])
                touchDowns[i] = (
                    heelTDCandidates[0][i] + relativeSearchRange[0] + relativeIndex
                )
            except Exception:
                pass

    else:  # Forefoot - walker
        # Iterate over possible candidates
        numTDCandidates = len(toeTDCandidates[0])
        # print(numTDCandidates)
        # try:
        for i in range(numTDCandidates):
            touchDowns.append(toeTDCandidates[0][i])
            # Try to refine the touchdown - position
            try:
                # Search for acceleration maximum around heel TD candidate
                searchRange = [
                    toeTDCandidates[i] + relativeSearchRange[0],
                    toeTDCandidates[i] + relativeSearchRange[1],
                ]
                relativeIndex = np.argmax(toeAcc[searchRange[0] : searchRange[1], :])
                touchDowns[i] = (
                    toeTDCandidates[i] + relativeSearchRange[0] + relativeIndex
                )
            except Exception:
                pass
    return touchDowns


def left_steps(markers):
    touchDowns = []
    relativeSearchRange = [-5, 10]

    # Determine z-direction second order gradients of the relevant markers
    # Heel
    heelVel = np.gradient(markers["LHEEL"][:, 2])
    heelAcc = np.gradient(heelVel)
    # Toe
    toeVel = np.gradient(markers["LTOE"][:, 2])
    toeAcc = np.gradient(toeVel)

    # Determine initial touchdown candidates based marker z-trajectories
    heelTDCandidates = find_peaks(-markers["LHEEL"][:, 2], distance=50, prominence=5)
    toeTDCandidates = find_peaks(
        -markers["LTOE"][:, 2], distance=50, prominence=3, width=40
    )

    # x = -markers["LHEEL"][:, 2]
    # y = -markers["LTOE"][:, 2]
    #
    # plt.figure(figsize=(30, 10), dpi=300)
    # plt.plot(x)
    # plt.plot(heelTDCandidates[0], x[heelTDCandidates[0]], "x")
    # plt.plot(y)
    # plt.plot(toeTDCandidates[0], y[toeTDCandidates[0]], "x")
    # plt.savefig("Test_li")
    # plt.close()

    # Make sure at least one complete step can be extracted
    if not (len(heelTDCandidates[0]) > 1 and len(toeTDCandidates[0]) >= 1) or not (
        len(heelTDCandidates[0]) >= 1 and len(toeTDCandidates[0]) > 1
    ):
        # print("no left step")
        return

    # Check if forefoot / heel touches the ground first( in time)
    if heelTDCandidates[0][0] < toeTDCandidates[0][0]:  # Heel-walker
        # Iterate over possible candidates
        numTDCandidates = len(heelTDCandidates[0])
        # print(numTDCandidates)
        for i in range(numTDCandidates):
            touchDowns.append(heelTDCandidates[0][i])
            # Try to refine the touchdown - position
            try:
                # Search for acceleration maximum around heel TD candidate
                searchRange = [
                    heelTDCandidates[0][i] + relativeSearchRange[0],
                    heelTDCandidates[0][i] + relativeSearchRange[1],
                ]
                relativeIndex = np.argmax(heelAcc[searchRange[0] : searchRange[1]])
                touchDowns[i] = (
                    heelTDCandidates[0][i] + relativeSearchRange[0] + relativeIndex
                )
            except Exception:
                pass

    else:  # Forefoot - walker
        # Iterate over possible candidates
        numTDCandidates = len(toeTDCandidates[0])
        # print(numTDCandidates)
        for i in range(numTDCandidates):
            touchDowns.append(toeTDCandidates[0][i])
            # Try to refine the touchdown - position
            try:
                # Search for acceleration maximum around heel TD candidate
                searchRange = [
                    toeTDCandidates[i] + relativeSearchRange[0],
                    toeTDCandidates[i] + relativeSearchRange[1],
                ]
                relativeIndex = np.argmax(toeAcc[searchRange[0] : searchRange[1], :])
                touchDowns[i] = (
                    toeTDCandidates[i] + relativeSearchRange[0] + relativeIndex
                )
            except Exception:
                pass
    return touchDowns


def get_sequence(data, right_TD, left_TD):
    keys = data.keys()
    sequences = {}
    for key in keys:
        temp = data[key]
        sequence = []
        try:
            for step in range(len(right_TD) - 1):
                sequence.append(temp[right_TD[step] : right_TD[step + 1], :])
        except Exception:
            pass

        try:
            for step in range(len(left_TD) - 1):
                sequence.append(temp[left_TD[step] : left_TD[step + 1], :])
        except Exception:
            return

        sequences.update({key: sequence})
    # print(len(sequences["rhip"]))
    return sequences
