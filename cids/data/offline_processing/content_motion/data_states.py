# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import math

import numpy as np

from . import defs


class Accelerometer:
    # Determine accelerometer data
    def __init__(self, position, orientation):
        self.pelvis = defs.generateAccelerometerData(
            position.pelvis, orientation.pelvis
        )
        self.rthigh = defs.generateAccelerometerData(
            position.rthigh, orientation.rthigh
        )
        self.lthigh = defs.generateAccelerometerData(
            position.lthigh, orientation.lthigh
        )
        self.rshank = defs.generateAccelerometerData(
            position.rshank, orientation.rshank
        )
        self.lshank = defs.generateAccelerometerData(
            position.lshank, orientation.lshank
        )
        self.rfoot = defs.generateAccelerometerData(position.rfoot, orientation.rfoot)
        self.lfoot = defs.generateAccelerometerData(position.lfoot, orientation.lfoot)


class Gyroscope:
    # Determine gyroscope data
    def __init__(self, orientation, frequency=100):
        self.pelvis = defs.generateGyroscopeData(orientation.pelvis, frequency)
        self.rthigh = defs.generateGyroscopeData(orientation.rthigh, frequency)
        self.lthigh = defs.generateGyroscopeData(orientation.lthigh, frequency)
        self.rshank = defs.generateGyroscopeData(orientation.rshank, frequency)
        self.lshank = defs.generateGyroscopeData(orientation.lshank, frequency)
        self.rfoot = defs.generateGyroscopeData(orientation.rfoot, frequency)
        self.lfoot = defs.generateGyroscopeData(orientation.lfoot, frequency)


class Position:
    # Determine possible IMU positions
    def __init__(self, m, jo, segcos, defaultposition):
        self.pelvis = (m["RPSI"] + m["LPSI"]) / 2 + defs.quatrotate_matlab(
            defs.quatconj(segcos.pelvis),
            np.repeat(
                np.expand_dims(np.array(defaultposition.pelvis), axis=1),
                np.shape(segcos.pelvis)[0],
                axis=1,
            ),
        )
        self.rthigh = (jo.rhip + jo.rknee) / 2 + defs.quatrotate_matlab(
            defs.quatconj(segcos.rthigh),
            np.repeat(
                np.expand_dims(np.array(defaultposition.rthigh), axis=1),
                np.shape(segcos.pelvis)[0],
                axis=1,
            ),
        )
        self.lthigh = (jo.lhip + jo.lknee) / 2 + defs.quatrotate_matlab(
            defs.quatconj(segcos.lthigh),
            np.repeat(
                np.expand_dims(np.array(defaultposition.lthigh), axis=1),
                np.shape(segcos.pelvis)[0],
                axis=1,
            ),
        )
        self.rshank = (jo.rknee + jo.rankle) / 2 + defs.quatrotate_matlab(
            defs.quatconj(segcos.rshank),
            np.repeat(
                np.expand_dims(np.array(defaultposition.rshank), axis=1),
                np.shape(segcos.pelvis)[0],
                axis=1,
            ),
        )
        self.lshank = (jo.lknee + jo.lankle) / 2 + defs.quatrotate_matlab(
            defs.quatconj(segcos.lshank),
            np.repeat(
                np.expand_dims(np.array(defaultposition.lshank), axis=1),
                np.shape(segcos.pelvis)[0],
                axis=1,
            ),
        )
        self.rfoot = (m["RTOE"] + m["RHEEL"]) / 2 + defs.quatrotate_matlab(
            defs.quatconj(segcos.rfoot),
            np.repeat(
                np.expand_dims(np.array(defaultposition.rfoot), axis=1),
                np.shape(segcos.pelvis)[0],
                axis=1,
            ),
        )
        self.lfoot = (m["LTOE"] + m["LHEEL"]) / 2 + defs.quatrotate_matlab(
            defs.quatconj(segcos.lfoot),
            np.repeat(
                np.expand_dims(np.array(defaultposition.lfoot), axis=1),
                np.shape(segcos.pelvis)[0],
                axis=1,
            ),
        )


class DefaultPosition:
    # TODO make this more flexible
    # Predefined values of position
    def __init__(self):
        self.pelvis = [-20, 0, 0]
        self.rthigh = [0, 0, 40]
        self.lthigh = [0, 0, -40]
        self.rshank = [20, 60, -40]
        self.lshank = [20, 60, 45]
        self.rfoot = [20, 20, 0]
        self.lfoot = [20, 20, 0]


class Orientation:
    # Determine possible IMU orientations
    def __init__(self, segcos, defaultorientation):
        self.pelvis = defs.quatmultiply(
            defs.quatmultiply(segcos.pelvis, defs.quatexp([0, 0, 0, math.pi / 4])),
            defs.quatexp(defaultorientation.pelvis),
        )
        self.rthigh = defs.quatmultiply(
            defs.quatmultiply(segcos.rthigh, defs.quatexp([0, math.pi / 2, 0, 0])),
            defs.quatexp(defaultorientation.rthigh),
        )
        self.lthigh = defs.quatmultiply(
            defs.quatmultiply(segcos.lthigh, defs.quatexp([0, 0, 0, math.pi / 2])),
            defs.quatexp(defaultorientation.lthigh),
        )
        self.rshank = defs.quatmultiply(
            defs.quatmultiply(segcos.rshank, defs.quatexp([0, math.pi * 6 / 13, 0, 0])),
            defs.quatexp(defaultorientation.rshank),
        )
        self.lshank = defs.quatmultiply(
            defs.quatmultiply(segcos.lshank, defs.quatexp([0, math.pi * 6 / 11, 0, 0])),
            defs.quatexp(defaultorientation.lshank),
        )
        self.rfoot = defs.quatmultiply(
            segcos.rfoot, defs.quatexp(defaultorientation.rfoot)
        )
        self.lfoot = defs.quatmultiply(
            segcos.lfoot, defs.quatexp(defaultorientation.lfoot)
        )


class DefaultOrientation:
    # TODO make this more flexible
    # Predefined values of orientation
    def __init__(self):
        self.pelvis = [0, math.pi / 4, 0, 0]
        self.rthigh = [0, math.pi * 1 / 24, 0, 0]
        self.lthigh = [0, math.pi * 1 / 28, 0, 0]
        self.rshank = [0, 0, -math.pi * 9 / 22, 0]
        self.lshank = [0, 0, -math.pi * 1 / 9, 0]
        self.rfoot = [0, 0, 0, 0]
        self.lfoot = [0, 0, 0, 0]


class JointAngles:
    def __init__(self, segcos):
        """Determine 3D hip, knee and ankle joint angles based on defines segment
        coordinate systems.
        """
        # Hip joint angles
        #  Right
        tmp = defs.create_tmp(segcos.pelvis, segcos.rthigh)
        self.rhip = np.stack([tmp[:, 2], tmp[:, 0], tmp[:, 1]], axis=-1) * 180 / math.pi
        #  Left
        tmp = defs.create_tmp(segcos.pelvis, segcos.lthigh)
        self.lhip = (
            np.stack([tmp[:, 2], -tmp[:, 0], -tmp[:, 1]], axis=-1) * 180 / math.pi
        )
        # Knee joint angles
        #  Right
        tmp = defs.create_tmp(segcos.rthigh, segcos.rshank)
        self.rknee = (
            np.stack([-tmp[:, 2], tmp[:, 0], tmp[:, 1]], axis=-1) * 180 / math.pi
        )
        #  Left
        tmp = defs.create_tmp(segcos.lthigh, segcos.lshank)
        self.lknee = (
            np.stack([-tmp[:, 2], -tmp[:, 0], -tmp[:, 1]], axis=-1) * 180 / math.pi
        )
        # Ankle joint angles
        #  Right
        tmp = defs.create_tmp(segcos.rshank, segcos.rfoot)
        self.rankle = (
            np.stack([tmp[:, 2], tmp[:, 0], tmp[:, 1]], axis=-1) * 180 / math.pi
        )
        #  Left
        tmp = defs.create_tmp(segcos.lshank, segcos.lfoot)
        self.lankle = (
            np.stack([tmp[:, 2], -tmp[:, 0], -tmp[:, 1]], axis=-1) * 180 / math.pi
        )


class SegmentCoordinateSystem:
    def __init__(self, markers, jointorigins):
        """Determine segment coordinate systems of pelvis, thighs, shanks and
        feet based on marker trajectories and joint origins.
        """
        # Determine coordinate systems as rotation matrices
        # Pelvis coordinate system
        PSI = markers["RPSI"] + 0.5 * (markers["LPSI"] - markers["RPSI"])
        M, N = markers["RASI"].shape
        tmpCOS = np.zeros([M, N, 3])
        tmp = np.cross((markers["RASI"] - PSI), (markers["LASI"] - PSI), axis=1)
        tmpCOS[:, :, 2] = markers["RASI"] - markers["LASI"]
        tmpCOS[:, :, 0] = np.cross(tmp, tmpCOS[:, :, 2], axis=1)
        tmpCOS[:, :, 1] = np.cross(tmpCOS[:, :, 2], tmpCOS[:, :, 0], axis=1)
        self.pelvis = tmpCOS / np.expand_dims(np.linalg.norm(tmpCOS, axis=1), axis=1)

        # Right thigh coordinate system
        tmpCOS[:, :, 1] = jointorigins.rhip - jointorigins.rknee
        tmp = np.cross(
            jointorigins.rhip - markers["RKNE"],
            jointorigins.rhip - markers["RKNEM"],
            axis=1,
        )
        tmpCOS[:, :, 2] = np.cross(tmp, tmpCOS[:, :, 1], axis=1)
        tmpCOS[:, :, 0] = np.cross(tmpCOS[:, :, 1], tmpCOS[:, :, 2], axis=1)
        self.rthigh = tmpCOS / np.expand_dims(np.linalg.norm(tmpCOS, axis=1), axis=1)

        # Left thigh coordinate system (Proximal = Wu)
        tmpCOS[:, :, 1] = jointorigins.lhip - jointorigins.lknee
        tmp = np.cross(
            (jointorigins.lhip - markers["LKNEM"]),
            (jointorigins.lhip - markers["LKNE"]),
            axis=1,
        )
        tmpCOS[:, :, 2] = np.cross(tmp, tmpCOS[:, :, 1], axis=1)
        tmpCOS[:, :, 0] = np.cross(tmpCOS[:, :, 1], tmpCOS[:, :, 2], axis=1)
        self.lthigh = tmpCOS / np.expand_dims(np.linalg.norm(tmpCOS, axis=1), axis=1)

        # Right shank coordinate system
        tmpCOS[:, :, 2] = markers["RANK"] - markers["RANKM"]
        tmpCOS[:, :, 0] = np.cross(
            (markers["RANK"] - jointorigins.rknee),
            (markers["RANKM"] - jointorigins.rknee),
            axis=1,
        )
        tmpCOS[:, :, 1] = np.cross(tmpCOS[:, :, 2], tmpCOS[:, :, 0], axis=1)
        self.rshank = tmpCOS / np.expand_dims(np.linalg.norm(tmpCOS, axis=1), axis=1)

        # Left shank coordinate system
        tmpCOS[:, :, 2] = markers["LANKM"] - markers["LANK"]
        tmpCOS[:, :, 0] = np.cross(
            (markers["LANKM"] - jointorigins.lknee),
            (markers["LANK"] - jointorigins.lknee),
            axis=1,
        )
        tmpCOS[:, :, 1] = np.cross(tmpCOS[:, :, 2], tmpCOS[:, :, 0], axis=1)
        self.lshank = tmpCOS / np.expand_dims(np.linalg.norm(tmpCOS, axis=1), axis=1)

        # Right foot coordinate system
        tmpCOS[:, :, 0] = markers["RTOE"] - markers["RHEEL"]
        tmpCOS[:, :, 1] = np.cross(
            (markers["RCAL"] - markers["RCALM"]), tmpCOS[:, :, 0], axis=1
        )
        tmpCOS[:, :, 2] = np.cross(tmpCOS[:, :, 0], tmpCOS[:, :, 1], axis=1)
        self.rfoot = tmpCOS / np.expand_dims(np.linalg.norm(tmpCOS, axis=1), axis=1)

        # Left foot coordinate system
        tmpCOS[:, :, 0] = markers["LTOE"] - markers["LHEEL"]
        tmpCOS[:, :, 1] = np.cross(
            tmpCOS[:, :, 0], (markers["LCAL"] - markers["LCALM"]), axis=1
        )
        tmpCOS[:, :, 2] = np.cross(tmpCOS[:, :, 0], tmpCOS[:, :, 1], axis=1)
        self.lfoot = tmpCOS / np.expand_dims(np.linalg.norm(tmpCOS, axis=1), axis=1)

        # Transform rotation matrices to quaternions
        self.pelvis = defs.to_quat(self.pelvis)
        self.rthigh = defs.to_quat(self.rthigh)
        self.lthigh = defs.to_quat(self.lthigh)
        self.rshank = defs.to_quat(self.rshank)
        self.lshank = defs.to_quat(self.lshank)
        self.rfoot = defs.to_quat(self.rfoot)
        self.lfoot = defs.to_quat(self.lfoot)


class JointOrigins:
    # Determine joint origins of hip, knee and ankle joint based on marker trajectories
    def __init__(self, markers):
        # Calculate Ankle Joint Center
        self.rankle = markers["RANK"] + 0.5 * (markers["RANKM"] - markers["RANK"])
        self.lankle = markers["LANK"] + 0.5 * (markers["LANKM"] - markers["LANK"])
        # Calculate Knee Joint Center
        self.rknee = markers["RKNE"] + 0.5 * (markers["RKNEM"] - markers["RKNE"])
        self.lknee = markers["LKNE"] + 0.5 * (markers["LKNEM"] - markers["LKNE"])
        # Calculate Hip Joint Center
        # Auxiliary Hip Joint Center
        PSI = markers["RPSI"] + 0.5 * (markers["LPSI"] - markers["RPSI"])
        ASI = markers["RASI"] + 0.5 * (markers["LASI"] - markers["RASI"])
        PelvisDepth = np.sqrt(np.sum((ASI - PSI) ** 2, axis=-1))
        PelvisWidth = np.sqrt(np.sum((markers["RASI"] - markers["LASI"]) ** 2, axis=-1))
        LegLengthRight = np.sqrt(
            np.sum((markers["RASI"] - markers["RKNEM"]) ** 2, axis=-1)
        ) + np.sqrt(np.sum((markers["RKNEM"] - markers["RANKM"]) ** 2, axis=-1))
        LegLengthLeft = np.sqrt(
            np.sum((markers["LASI"] - markers["LKNEM"]) ** 2, axis=-1)
        ) + np.sqrt(np.sum((markers["LKNEM"] - markers["LANKM"]) ** 2, axis=-1))

        M, N = markers["RASI"].shape
        PelvisCOS = np.zeros((M, N, 3))
        PelvisCOS[:, :, 2] = markers["RASI"] - markers["LASI"]
        PelvisCOS[:, :, 1] = np.cross(
            markers["RASI"] - PSI, markers["LASI"] - PSI, axis=1
        )
        PelvisCOS[:, :, 0] = np.cross(PelvisCOS[:, :, 1], PelvisCOS[:, :, 2], axis=1)
        PelvisCOS = PelvisCOS / np.expand_dims(
            np.linalg.norm(PelvisCOS, axis=1), axis=1
        )

        self.rhip = (
            ASI
            + PelvisCOS[:, :, 0] * np.array([(-0.24 * PelvisDepth - 9.9)]).T
            + PelvisCOS[:, :, 1]
            * np.array([(-0.16 * PelvisWidth - 0.04 * LegLengthRight - 7.1)]).T
            + PelvisCOS[:, :, 2]
            * np.array([(0.28 * PelvisDepth + 0.16 * PelvisWidth + 7.9)]).T
        )

        self.lhip = (
            ASI
            + PelvisCOS[:, :, 0] * np.array([(-0.24 * PelvisDepth - 9.9)]).T
            + PelvisCOS[:, :, 1]
            * np.array([(-0.16 * PelvisWidth - 0.04 * LegLengthLeft - 7.1)]).T
            - PelvisCOS[:, :, 2]
            * np.array([(0.28 * PelvisDepth + 0.16 * PelvisWidth + 7.9)]).T
        )


def calculate_joint_angles(markers):
    # Calculate kinematic quantities
    jointorigins = JointOrigins(markers)
    segmentcos = SegmentCoordinateSystem(markers, jointorigins)
    jointangles = JointAngles(segmentcos)
    return jointangles


def calculate_IMU_data(markers):
    # Calculate kinematic quantities
    jointorigins = JointOrigins(markers)
    segmentcos = SegmentCoordinateSystem(markers, jointorigins)

    # Calculate synthetic IMU data
    defaultposition = DefaultPosition()
    defaultorientation = DefaultOrientation()
    position = Position(markers, jointorigins, segmentcos, defaultposition)
    orientation = Orientation(segmentcos, defaultorientation)
    # imu = {}
    acc = vars(Accelerometer(position, orientation))
    gyr = vars(Gyroscope(orientation))
    return acc, gyr
