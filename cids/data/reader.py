# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Data reading functionality. Part of the CIDS toolbox.

    Classes:
        DataReader: Reads data from tfrecords into tensorflow datasets and into memory.

"""
import os
from collections import defaultdict
from copy import deepcopy
from pathlib import Path

import numpy as np
import psutil
import tensorflow as tf

from ..base.logging import BaseLoggerMixin


class DataReader(BaseLoggerMixin):

    VERBOSITY = 1
    INDENT = ""

    # TODO: casting to tf.float64 fails during categorical classification

    def __init__(
        self,
        data_definition,
        src_type="file",
        cast_dtype=tf.float32,
        cutoff=False,
        slice_tensors=False,
        file_type="tfr",
        csv_skip=0,
        csv_comment="#",
        prefetch="auto",
        buffer_size=0.05,
        seed=None,
    ):
        """A class that reads and processes data into tensorflow.

        DataReader uses the tensorflow dataset API to load files into
        tf.Dataset objects. The class currently supports reading of csv and
        tfrecord files. When processing tfrecords and no tfr_features is present
        it defaults to the cids default tfrecord format (see below).

        Args:
            data_definition: an object of class DataDefinition
            src_type:        source type of the data ("file" or "placeholder")
            cast_dtype:      data type to cast to after reading
            slice_tensors:   boolean: Slice tensors into multiple samples?
            csv_skip:        number of lines to skip for csv
            csv_comment:     lines to skip starting with this string for csv
            prefetch:        boolean: Preload dataset of buffer_size?
            buffer_size:     number of samples to collect for shuffling
            seed:            set a seed for shuffling. (Default None: disabled)
        """
        self._start_logging()
        self.data_definition = data_definition
        self.src_type = src_type
        self.cast_dtype = cast_dtype
        self.cutoff = cutoff
        self.slice_tensors = slice_tensors
        self.merge_dataset_features = True
        self.samples_per_file = 1
        self.skip = csv_skip
        self.comment = csv_comment
        self.file_type = file_type
        self.seed = seed
        self.prefetch = prefetch
        self.prefetch_datasets = {"train": None, "valid": None}
        self.buffer_size = buffer_size
        self.chunk_size = None
        self.chunk_stride = None
        self.incomplete_chunks = True
        self.random_chunks = False
        self.VERBOSITY = 1
        self.announced = False

    @property
    def data_shape(self):
        return self.data_definition.data_shape

    @property
    def data_format(self):
        return self.data_definition.data_format

    @property
    def batch_axis(self):
        return self.data_definition.batch_axis

    @property
    def feature_axis(self):
        return self.data_definition.feature_axis

    @property
    def sequence_axis(self):
        return self.data_definition.sequence_axis

    @property
    def spatial_axes(self):
        return self.data_definition.spatial_axes

    @property
    def iter_axis(self):
        return self.data_definition.iter_axis

    @property
    def tfr_features(self):
        return self.data_definition.tfr_features

    @property
    def tfr_features_context(self):
        return self.data_definition.tfr_features_context

    @property
    def tfr_features_sequential(self):
        return self.data_definition.tfr_features_sequential

    @property
    def features(self):
        return self.data_definition.features

    @property
    def feature_shape(self):
        return self.data_definition.feature_shape

    @property
    def input_features(self):
        return self.data_definition.input_features

    @property
    def output_features(self):
        return self.data_definition.output_features

    def _parse_tfrecord(self, tfr):
        if self.tfr_features_sequential:
            parsed_con, parsed_seq = tf.io.parse_single_sequence_example(
                tfr, self.tfr_features_context, self.tfr_features_sequential
            )
            parsed = parsed_con
            parsed.update(parsed_seq)
        else:
            parsed = tf.io.parse_single_example(tfr, self.tfr_features)

        def __parse_select(parsed, v):
            if v.dtype == tf.string:
                if v.decode_str_to is not None:
                    return self._parse_feature_tensor_bytestring(parsed, v)
                return self._parse_feature_string_bytestring(parsed, v)
            return self._parse_feature_tensor(parsed, v)

        sample = {k: __parse_select(parsed, v) for k, v in self.features.items()}
        return sample

    def _parse_feature_tensor_bytestring(self, parsed, feature):
        # Tensor parameter
        byte_string = parsed[feature.name]
        if feature.decode_str_to == tf.string:
            flat_tensor = tf.io.parse_tensor(byte_string, tf.string)
        else:
            flat_tensor = tf.io.decode_raw(byte_string, feature.decode_str_to)
        # Get tensor dynamic shape
        #   Dynamic shape is applied later to allow cutting off
        dynamic_data_shape = parsed[feature.name + "_shape"]
        dynamic_data_shape = tf.cast(dynamic_data_shape, tf.int32)
        # Reshape tensor
        tensor = tf.reshape(flat_tensor, dynamic_data_shape)
        # Cast
        if feature.decode_str_to != tf.string:
            tensor = tf.cast(tensor, self.cast_dtype)
        return tensor

    def _parse_feature_string_bytestring(self, parsed, feature):
        byte_string = parsed[feature.name]
        return byte_string

    def _parse_feature_tensor(self, parsed, feature):
        scalar = tf.cast(parsed[feature.name], self.cast_dtype)
        return scalar

    def _parse_csv_line(self, line):
        if self.cast_dtype in (tf.float32, tf.float64):
            defaults = [[0.0]] * self.data_shape[-1]
        elif self.cast_dtype in (tf.int64, tf.int32):
            defaults = [[0]] * self.data_shape[-1]
        else:
            raise ValueError("Unknown self.cast_dtype = ", self.cast_dtype)
        parsed = tf.io.decode_csv(line, record_defaults=defaults)
        parsed = tf.stack(parsed, axis=0)
        return parsed

    def _set_shape(self, x, static_shape):
        if x.dtype == tf.string and len(x.shape) == 0:
            # Fix dimensionality of scalar (non-encoded) string features
            x = tf.expand_dims(tf.expand_dims(x, 0), 0)
        x.set_shape(static_shape)
        return x

    def _cutoff_sample(self, x, static_shape):
        if x.dtype == tf.string and len(x.shape) == 0:
            # Fix dimensionality of scalar (non-encoded) string features
            x = tf.expand_dims(tf.expand_dims(x, 0), 0)
        else:
            dynamic_shape = tf.shape(x)
            tmp_shape = [
                s if s is not None else np.iinfo(np.int32).max for s in static_shape
            ]
            slice_shape = tf.minimum(dynamic_shape, tmp_shape)
            x = tf.slice(x, [0] * len(static_shape), slice_shape)
        x.set_shape(static_shape)
        return x

    def _squeeze_item(self, x, index):
        return tf.squeeze(x, index)

    def _chunk_samples(
        self,
        dataset,
        chunk_size,
        chunk_stride,
        feature_shape,
        shuffle,
        buffer_size,
        drop_remainder=False,
    ):
        # Find maximum sequence length
        sequence_length = 0
        for fk, fi in self.features.items():
            sequence_axis = fi.sequence_axis
            if sequence_axis is not None:
                assert feature_shape[fk][sequence_axis] is not None, (
                    "Data shape along sequence axis must be defined,"
                    + " when chunk_size is set."
                )
                assert chunk_size <= feature_shape[fk][sequence_axis]
                # Create multiple sequence chunks from chunkable sequences
                sequence_length = max(sequence_length, feature_shape[fk][sequence_axis])
        assert sequence_length > 0, "No sequential feature found!"
        # Chunking?
        if sequence_length <= chunk_size:
            return dataset, feature_shape
        # Select maximum and chunking shape
        max_shape = deepcopy(feature_shape)
        chunky_shape = deepcopy(feature_shape)
        for fk, fi in self.features.items():
            if fi.sequence_axis is not None:
                sequence_axis = fi.sequence_axis
                max_shape[fk][sequence_axis] = sequence_length
                chunky_shape[fk][sequence_axis] = chunk_size
        # Shuffle dataset before synthesizing more samples
        if shuffle:
            dataset = dataset.shuffle(buffer_size=buffer_size, seed=self.seed)
        # Ensure that all sequences are padded with zeros to max length
        dataset = dataset.padded_batch(
            batch_size=1, padded_shapes=max_shape, drop_remainder=drop_remainder
        )
        dataset = dataset.map(
            lambda x_dict: {k: tf.squeeze(x, 0) for k, x in x_dict.items()}
        )  # remove added batch axis
        # Create multiple sequence chunks from chunkable sequences
        num_chunks = (sequence_length - chunk_size) // chunk_stride
        if self.VERBOSITY and not self.announced:
            self.log(
                "Chunking each sequence into"
                + f" {num_chunks:d} samples of chunk length {chunk_size:d}"
                + f" (overlapping by stride {chunk_stride:d})."
            )
        # Compute chunk indices
        if self.random_chunks:
            chunk_idx = [
                tf.random.uniform([chunk_size], 0, sequence_length, dtype=tf.int32)
                for _ in range(num_chunks)
            ]
        else:
            chunk_idx = [
                tf.range(i * chunk_stride, i * chunk_stride + chunk_size)
                for i in range(num_chunks)
            ]
        dataset = dataset.map(
            lambda x_dict: {
                k: tf.concat(
                    [tf.gather(x, c, axis=sequence_axis) for c in chunk_idx], 0
                )
                if self.features[k].sequence_axis is not None
                else tf.concat(
                    [tf.stack([x] * chunk_size, axis=sequence_axis)] * num_chunks, 0
                )
                for k, x in x_dict.items()
            }
        )
        # Unbatch newly created samples
        dataset = dataset.flat_map(
            tf.data.Dataset.from_tensor_slices,
        )
        # Re-add removed batch axis
        dataset = dataset.map(
            lambda x_dict: {k: tf.expand_dims(x, axis=0) for k, x in x_dict.items()}
        )
        # Handle missing frames
        if self.incomplete_chunks:
            # Remove blank samples
            dataset = dataset.filter(
                lambda x_dict: tf.reduce_any(
                    [
                        tf.reduce_any(tf.cast(tf.sign(tf.abs(x)), tf.bool))
                        for k, x in x_dict.items()
                        if x.dtype != tf.string
                        and self.features[k].sequence_axis is not None
                    ]
                )
            )
        else:
            # Remove samples with missing frames
            dataset = dataset.filter(
                lambda x_dict: tf.reduce_any(
                    [
                        tf.reduce_any(
                            tf.reduce_all(tf.cast(tf.sign(tf.abs(x)), tf.bool), axis=1)
                        )
                        for k, x in x_dict.items()
                        if x.dtype != tf.string
                        and self.features[k].sequence_axis is not None
                    ]
                )
            )
        # Remove repeated non-sequential features
        dataset = dataset.map(
            lambda x_dict: {
                k: x
                if self.features[k].sequence_axis is not None
                else tf.gather(x, [0], axis=sequence_axis)[0]
                for k, x in x_dict.items()
            }
        )
        return dataset, chunky_shape

    def _gather_tensor_indices(self, x_dict, feature_name, merge_format, merge_shape):
        # feature_name may contain index only, thus split index off
        feature_name_split = feature_name.split("[")
        clean_key = feature_name_split[0]
        # Get corresponding tensor
        x = x_dict[clean_key]
        if len(feature_name_split) == 2:
            # Also indices are provided, gather those feature indices only
            str_indices = feature_name_split[1].replace("]", "")
            indices = [int(i) for i in str_indices.split(",")]
            x = tf.gather(x, indices, axis=self.feature_axis)
        elif len(feature_name_split) > 2:
            raise ValueError("Invalid input feature: " + feature_name)
        # Expand shape to match merge format and shape
        if isinstance(merge_shape, dict):
            for m_shape in merge_shape.values():
                if len(x.shape) == len(m_shape) and not x.shape[-1] == m_shape[-1]:
                    multiples = [1] * len(x.shape)
                    x = tf.tile(x, multiples)
        else:
            for i, (d, s) in enumerate(zip(merge_format, merge_shape)):
                if d not in self.data_definition.features[clean_key].data_format:
                    if not x.shape[i] == s:
                        # axis was not previously added (during chunking)
                        x = tf.expand_dims(x, axis=i)
                        multiples = [1] * len(x.shape)
                        multiples[i] = s
                        x = tf.tile(x, multiples)
        return x

    def get_input_shape(self, chunk_size=None):
        return self._compute_shape(
            self.data_shape["X"], self.data_format["X"], chunk_size
        )

    def get_output_shape(self, chunk_size=None):
        return self._compute_shape(
            self.data_shape["Y"], self.data_format["Y"], chunk_size
        )

    def _compute_shape(self, data_shape, data_format, chunk_size):
        data_shape = deepcopy(data_shape)
        if chunk_size is None:
            return data_shape
        if isinstance(data_format, str):
            if "S" in data_format:
                sequence_axis = data_format.index("S")
                data_shape[sequence_axis] = chunk_size
        else:
            for df in self.data_format["Y"]:
                if "S" in df:
                    sequence_axis = df.index("S")
                    data_shape[df][sequence_axis] = chunk_size
        return data_shape

    def _merge_split_features(self, x_dict, chunk_size):
        chunk_size = chunk_size or self.chunk_size
        input_shape = self.get_input_shape(chunk_size=chunk_size)
        output_shape = self.get_output_shape(chunk_size=chunk_size)
        # Gather tensors
        input_tensors = [
            self._gather_tensor_indices(
                x_dict, feature_name, self.data_format["X"], input_shape
            )
            for feature_name in self.input_features
        ]
        output_tensors = [
            self._gather_tensor_indices(
                x_dict, feature_name, self.data_format["Y"], output_shape
            )
            for feature_name in self.output_features
        ]
        # Concatenate tensors
        if isinstance(input_shape, dict) and isinstance(output_shape, dict):
            inputs = defaultdict(list)
            outputs = defaultdict(list)
            for key, value in input_shape.items():
                shared_format = key
                inputs[shared_format] = tf.concat(
                    [inp for inp in input_tensors if len(inp.shape) == len(value)], -1
                )
            input_tensor = inputs
            for key, value in output_shape.items():
                shared_format = key
                outputs[shared_format] = tf.concat(
                    [out for out in output_tensors if len(out.shape) == len(value)], -1
                )

            output_tensor = outputs
        else:
            input_tensor = tf.concat(input_tensors, self.feature_axis["X"])
            output_tensor = tf.concat(output_tensors, self.feature_axis["Y"])
        return input_tensor, output_tensor

    def _calc_min_buffer_size(self, data, mode):
        # Calculate sample size
        if isinstance(data[0], str):
            avg_sample_bytes = os.path.getsize(data[0])
        elif isinstance(data[0], Path):
            avg_sample_bytes = os.path.getsize(os.fspath(data[0]))
        elif isinstance(data[0], np.ndarray):
            avg_sample_bytes = data[0].nbytes
        elif isinstance(data[0], (list, tuple)) and isinstance(data[0][0], str):
            avg_sample_bytes = os.path.getsize(data[0][0])
        elif isinstance(data[0], (list, tuple)) and isinstance(data[0][0], Path):
            avg_sample_bytes = os.path.getsize(os.fspath(data[0][0]))
        elif isinstance(data[0], (list, tuple)) and isinstance(data[0][0], np.ndarray):
            avg_sample_bytes = data[0][0].nbytes
        else:
            raise ValueError("data must be file or ndarray")
        if self.VERBOSITY and not self.announced:
            self.log(
                f"({mode}) Estimated sample size: "
                + f"{avg_sample_bytes / 1024.0 ** 2:g} MB."
            )
        if isinstance(self.buffer_size, float):
            # Buffer size defined by fraction of total memory
            fraction = self.buffer_size
            total_bytes = psutil.virtual_memory().total
            # TODO: also calculate model size?
            # TODO: take available memory into account?
            buffer_size = int(np.ceil(fraction * total_bytes / avg_sample_bytes))
        elif isinstance(self.buffer_size, int):
            # Buffer size defined by number of samples
            buffer_size = self.buffer_size
        else:
            raise ValueError("data_reader.buffer_size must be int or float.")
        auto_cache = False
        if buffer_size == -1:
            if self.VERBOSITY and not self.announced:
                self.log(f"({mode}) buffer_size set to " + "AUTOTUNE (Will fail!).")
            return buffer_size
        min_buffer_size = min(len(data), buffer_size)
        if self.VERBOSITY and not self.announced:
            if min_buffer_size == buffer_size:
                if isinstance(self.buffer_size, float):
                    self.log(
                        f"({mode}) buffer_size set to "
                        + f"{100.0 * self.buffer_size:g}% of total memory: "
                        + f"{min_buffer_size:d} samples."
                    )
                elif isinstance(self.buffer_size, int):
                    self.log(
                        f"({mode}) Setting buffer_size to "
                        + f"{min_buffer_size:d} samples."
                    )
                else:
                    raise ValueError("data_reader.buffer_size must int or float.")
            else:
                self.log(
                    f"({mode}) buffer_size set to "
                    + f"maximum number of samples ({min_buffer_size:d})."
                )
                auto_cache = True
        return min_buffer_size, auto_cache

    def generate_batch_dataset(
        self,
        data,
        batch_size,
        chunk_size=None,
        mode="train",
        shuffle=True,
        repeats=None,
        disable_feature_merge=False,
        drop_remainder=False,
    ):
        """Read data into a dataset and concatenate samples into batches.

        The object"s DataDefinition and src_type control how data is read. This
        method creates the pipeline from the initial data (typically a list of
        file names or numpy arrays) to a dataset that can be consumed by
        CIDSModels.

        Args:
            data:                   data tensors
            batch_size:             number of samples to concatenate
                                        (can be an int or dict with keys "train"
                                        and "valid" for different modes)
            chunk_size:             sliding window size to cut sequences into
            mode:                   which mode to use (sets batch size used)
            shuffle:                shuffle the order of the samples
            repeats:                number of times the dataset should repeat
                                        (None for infinite, 0 for disable)
            disable_feature_merge:  keep features in data separate or merge

        Returns:
            dataset:    A tensorflow Dataset

        """
        min_buffer_size, auto_cache = self._calc_min_buffer_size(data, mode)
        if isinstance(batch_size, int):
            valid_batch_size = batch_size
        elif hasattr(batch_size, "keys"):
            batch_size = batch_size["train"]
            valid_batch_size = batch_size["valid"]
        else:
            raise ValueError("Variable batch_size must be type int or dict.")
        if mode == "valid":
            batch_size = valid_batch_size
        feature_shape = self.feature_shape
        # Decide to use prefetched/cached dataset
        if (
            self.prefetch
            and mode in self.prefetch_datasets
            and self.prefetch_datasets[mode] is not None
        ):
            # Use prefetched/cached dataset
            dataset = self.prefetch_datasets[mode]
        else:
            # Create new dataset and possibly prefetch/cache
            # TODO(Arnd): autodetect
            if self.src_type[:5].lower() == "file":
                dataset = self.generate_dataset_from_files(data)
            elif self.src_type[:5].lower() == "place":
                dataset = self.generate_dataset_from_placeholders(data)
            else:
                raise ValueError(
                    'Unknown src_type: must be either "file" or "placeholder".'
                )
            # Cutoff if dynamic data shape exceeds static data shape
            #   The tensor shape is set as far as the static shape is defined
            if self.cutoff:
                dataset = dataset.map(
                    lambda x_dict: {
                        k: self._cutoff_sample(x, feature_shape[k])
                        for k, x in x_dict.items()
                    }
                )
            else:
                dataset = dataset.map(
                    lambda x_dict: {
                        k: self._set_shape(x, feature_shape[k])
                        for k, x in x_dict.items()
                    }
                )
            # Cache
            if self.prefetch:
                if self.prefetch == "cache" or (self.prefetch == "auto" and auto_cache):
                    if self.VERBOSITY:
                        self.log(
                            f"({mode}) Caching dataset. Disable"
                            + " by setting model.data_reader.prefetch = True."
                            + f' Currently set to: "{self.prefetch}".'
                        )
                    dataset = dataset.cache()
                else:
                    dataset = dataset.prefetch(buffer_size=min_buffer_size)
                # Store prefetched or cached dataset in datareader
                if mode in self.prefetch_datasets:
                    self.prefetch_datasets[mode] = dataset
        # Chunk sequence dataset
        chunk_size = chunk_size or self.chunk_size
        if chunk_size:
            chunk_stride = self.chunk_stride or chunk_size // 2
            dataset, feature_shape = self._chunk_samples(
                dataset,
                chunk_size,
                chunk_stride,
                feature_shape,
                shuffle,
                min_buffer_size,
                drop_remainder=drop_remainder,
            )
        # Shuffle dataset after synthesizing samples
        if shuffle:
            dataset = dataset.shuffle(min_buffer_size, seed=self.seed)
        # Repeat dataset
        if repeats is None:
            dataset = dataset.repeat()
        elif repeats > 0:
            dataset = dataset.repeat(repeats)
        elif batch_size > len(data):
            if self.VERBOSITY and not self.announced:
                self.warn(
                    "Batch size is larger than dataset. Repeating dataset and "
                    + "dropping remainder to achieve requested batch size."
                )
            dataset = dataset.repeat(batch_size // len(data) + 1)
            drop_remainder = True
        # Create batches, padding necessary, since datasets may be from multiple sources
        dataset = dataset.map(
            lambda x_dict: {k: self._squeeze_item(x, 0) for k, x in x_dict.items()},
            num_parallel_calls=tf.data.experimental.AUTOTUNE,
        )  # remove batch axis
        sample_feature_shape = {k: fs[1:] for k, fs in feature_shape.items()}
        dataset = dataset.padded_batch(
            batch_size=batch_size,
            padded_shapes=sample_feature_shape,
            drop_remainder=drop_remainder,
        )
        if not disable_feature_merge:
            dataset = dataset.map(
                lambda x_dict: self._merge_split_features(x_dict, chunk_size),
                num_parallel_calls=tf.data.experimental.AUTOTUNE,
            )
        self.announced = True
        return dataset

    def generate_full_dataset(self, data, disable_feature_merge=False, **kwargs):
        """Read entire data without shuffling into a dataset of one big tensor.

        The object"s DataDefinition and src_type control how data is read. This
        method creates the pipeline from the initial data (typically a list of
        file names or numpy arrays) to a dataset that can be consumed by
        CIDSModels.

        Args:
            data:                   data tensors
            disable_feature_merge:  keep features in data separate or merge

        Returns:
            dataset:    A tensorflow Dataset

        """
        kwargs["chunk_size"] = kwargs.get("chunk_size", None)
        kwargs["shuffle"] = kwargs.get("shuffle", False)
        kwargs["batch_size"] = kwargs.get("batch_size", len(data))
        return self.generate_batch_dataset(
            data,
            mode="test",
            repeats=0,
            disable_feature_merge=disable_feature_merge,
            **kwargs,
        )

    def generate_dataset_from_files(self, files):
        """Create a dataset of tensors from files.

        Args:
            files:          list of filenames
        Returns:
            dataset:        a dataset of tensors
        """
        # Parse file
        if self.file_type[:3] == "tfr":
            dataset = tf.data.TFRecordDataset(files)
            dataset = dataset.map(
                self._parse_tfrecord, num_parallel_calls=tf.data.experimental.AUTOTUNE
            )
            if self.slice_tensors:
                dataset = dataset.flat_map(tf.data.Dataset.from_tensor_slices)
                # Readd remove batch axis
                dataset = dataset.map(
                    lambda x_dict: {
                        k: tf.expand_dims(x, axis=0) for k, x in x_dict.items()
                    },
                    num_parallel_calls=tf.data.experimental.AUTOTUNE,
                )
        elif self.file_type[:3] == "csv":
            dataset = tf.data.Dataset.from_tensor_slices(files)
            # Skip line and filter out comments
            dataset = dataset.flat_map(
                lambda filename: (
                    tf.data.TextLineDataset(filename)
                    .skip(self.skip)
                    .filter(
                        lambda line: tf.not_equal(tf.substr(line, 0, 1), self.comment)
                    )
                )
            )
            dataset = dataset.map(
                self._parse_csv_line, num_parallel_calls=tf.data.experimental.AUTOTUNE
            )
            if not self.slice_tensors:
                # Stack sequence
                if self.sequence_axis:
                    dataset = dataset.batch(
                        batch_size=self.data_shape[self.sequence_axis]
                    )
        else:
            raise ValueError(f'file_type "{self.file_type:s}" unknown.')
        return dataset

    def generate_dataset_from_placeholders(self, placeholders):
        """Create a dataset of tensors from placeholders.

        Args:
            placeholders:   a tensorflow placeholder/tensor for feeding
        Returns:
            dataset:        a dataset of tensors
        """
        if isinstance(placeholders, np.ndarray):
            placeholders = [placeholders]
        datasets = tuple(tf.data.Dataset.from_tensors(p) for p in placeholders)
        # Slice multiple sample tensors into single sample tensors
        if self.slice_tensors:
            datasets = tuple(
                d.flat_map(tf.data.Dataset.from_tensor_slices) for d in datasets
            )
            datasets = tuple(
                d.map(lambda x: tf.expand_dims(x, axis=0)) for d in datasets
            )
        # Cast
        datasets = tuple(
            d.map(lambda sample: tf.cast(sample, self.cast_dtype)) for d in datasets
        )
        # Zip datasets from multiple source tensors (inputs, targets) together
        if len(datasets) > 1:
            dataset = tf.data.Dataset.zip(datasets)
            dataset = dataset.map(lambda x, y: {"input": x, "output": y})
        else:
            dataset = datasets[0]
            dataset = dataset.map(lambda xy: {"data": xy})
        return dataset

    def read_tfrecords(self, tfr_files, disable_feature_merge=True, **kwargs):
        """Reads tfrecord files and return their contents.

        Args:
            tfr_files:              a list or tuple of tfrecord files
            disable_feature_merge:  keep features in data separate or merge
            **kwargs:               keyword arguments passed to generate_batch_dataset

        Returns:
            samples:                the samples after reading
        """
        # Read and batch all samples
        old_prefetch = self.prefetch
        self.prefetch = False
        dataset = self.generate_full_dataset(
            tfr_files, disable_feature_merge=disable_feature_merge, **kwargs
        )
        self.prefetch = old_prefetch

        # Extract numpy arrays from tensors
        def __bytestring_to_unicode(s):
            return s.decode("utf8")

        __array_bytestring_to_unicode = np.vectorize(__bytestring_to_unicode)

        def __extract_python_native_types(s):
            if hasattr(s, "numpy"):
                if s.dtype == tf.string:
                    return __array_bytestring_to_unicode(s.numpy())
                return s.numpy()
            if isinstance(s, tuple):
                return [__extract_python_native_types(e) for e in s]
            if hasattr(s, "items"):
                return {k: __extract_python_native_types(e) for k, e in s.items()}
            return s

        samples = [__extract_python_native_types(s) for s in dataset]
        # Unpack
        if isinstance(samples[0], dict):
            return {
                k: np.concatenate([s[k] for s in samples], axis=0)
                for k in samples[0].keys()
            }
        return [
            np.concatenate([s[i] for s in samples], axis=0)
            for i in range(len(samples[0]))
        ]

    def clear(self, reset_cached_datasets=False):
        self.announced = False
        if reset_cached_datasets:
            self.prefetch_datasets = {"train": None, "valid": None}
