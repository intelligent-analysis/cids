#!/usr/bin/env bash

IMG="intelligent_analysis2-gpu:latest"

USERNAME="${USER}"
USERID="$(id -u)"

SCRIPTNAME=$1
echo $SCRIPTNAME
TIME=$(date -u +%Y-%m-%d_UTC%H-%M-%S)
LOGNAME="RESULTS/${TIME}_$(basename ${SCRIPTNAME} .py).log"

if [ $# -eq 1 ]
then
    export GPUS="0";
    export CPUS="8.0";
elif [ $# -eq 2 ]
then
    export GPUS=$2;
    export CPUS="8.0";
elif [ $# -eq 3 ]
then
    export GPUS=$2;
    export CPUS=$3;
elif [ $# -eq 4 ]
then
    export GPUS=$2;
    export CPUS=$3;
    export IMG=$4":latest";
else
    echo $@
fi

echo "GPUs visible to CUDA:"
echo "${GPUS}"
echo "CPUS:"
echo "${CPUS}"

# Run script
docker run \
    -e "NVIDIA_VISIBLE_DEVICES=${GPUS}" \
    -e "HOSTNAME=$(cat /etc/hostname)" \
    -e "DISPLAY=${DISPLAY}" \
    -v "/tmp/.X11-unix":"/tmp/.X11-unix" \
    -v "${PWD}:/opt/intelligent_analysis" \
    -v "${PWD}/../DATA:/opt/DATA" \
    -v "${PWD}/../demos:/opt/demos" \
    -w "/opt/intelligent_analysis" \
    -u "developer" \
    --gpus='"'"device=${GPUS}"'"' \
    --cpus="${CPUS}" \
    --name "${IMG%:*}.${TIME}.${SCRIPTNAME##*/}" \
    --rm \
    ${IMG} \
    python -u "${SCRIPTNAME}" |& tee -i ${LOGNAME}

# Set Permissions correctly for download
docker run \
    -v "${PWD}:/opt/intelligent_analysis" \
    -v "${PWD}/../DATA:/opt/DATA" \
    -w "/opt/intelligent_analysis" \
    -u "developer" \
    --rm \
    ${IMG} \
    chmod -R a+rw "RESULTS" >/dev/null 2>&1; \
    chown -R "${USERID}:${USERID}" "RESULTS" >/dev/null 2>&1; \
    chmod -R a+rw "/opt/DATA" >/dev/null 2>&1; \
    chown -R "${USERID}:${USERID}" "/opt/DATA" >/dev/null 2>&1;
