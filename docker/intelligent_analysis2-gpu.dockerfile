FROM tensorflow/tensorflow:2.1.0-gpu-py3

LABEL maintainer="Arnd Koeppe <koeppe@iam.rwth-aachen.de>"

ARG DEBIAN_FRONTEND=noninteractive

# Open X11 port to local user
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/developer && \
    mkdir -p /etc/sudoers.d && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" \
      >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown ${uid}:${gid} -R /home/developer
RUN export uid=1001 gid=1001 && \
    mkdir -p /home/developer2 && \
    mkdir -p /etc/sudoers.d && \
    echo "developer2:x:${uid}:${gid}:Developer2,,,:/home/developer2:/bin/bash" \
      >> /etc/passwd && \
    echo "developer2:x:${uid}:" >> /etc/group && \
    echo "developer2 ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer2 && \
    chmod 0440 /etc/sudoers.d/developer2 && \
    chown ${uid}:${gid} -R /home/developer2

# Install python tkinter and texlive (for pretty plotting)
RUN echo ttf-mscorefonts-installer \
    msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections
RUN apt-get update && \
    apt-get install -yq --no-install-recommends \
        build-essential \
        python3-dev \
        wget \
        libmtdev-dev \
        libgl1-mesa-dev \
        libgles2-mesa-dev \
        xclip \
        libgstreamer1.0-dev \
        gstreamer1.0-plugins-base \
        gstreamer1.0-plugins-good \
        ffmpeg \
        libsdl2-dev \
        libsdl2-ttf-dev \
        libsdl2-image-dev \
        libsdl2-mixer-dev \
        libportmidi-dev \
        libswscale-dev \
        libavformat-dev \
        libavcodec-dev \
        zlib1g-dev \
        python3-tk \
        python3-opengl \
        texlive \
        texlive-latex-extra \
        texlive-fonts-recommended \
        ttf-mscorefonts-installer \
        dvipng \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

#RUN wget "http://ftp.de.debian.org/debian/pool/contrib/m/msttcorefonts/ttf-mscorefonts-installer_3.6_all.deb" && \
#    apt-get --purge remove ttf-mscorefonts-installer -yq && \
#    dpkg -i "ttf-mscorefonts-installer_3.6_all.deb" && \
#    apt-get clean && \
#    rm -rf /var/lib/apt/lists/*

## Ensure python 3 (fix devel)
#WORKDIR /usr/bin
#RUN rm python && \
#    ln -s python3 python

# Install Python packages
#    kivy and Cython versions must be compatible
RUN pip3 --no-cache-dir install -U --force-reinstall Cython==0.28.3
RUN pip3 --no-cache-dir install \
#        pyedflib \
        blist \
        sortedcontainers \
        eigency \
        keras-tuner \
        # innvestigate \    # FIXME: currently using own variant (tf2.0 compatible)
        deeplift \
        pyyaml \
        matplotlib \
        seaborn \
        scipy \
        sympy \
        deap \
        tqdm \
        colorama \
        numba \
        pympler \
        psutil \
        sklearn \
#        yappy \
        vmprof \
        line_profiler \
        natsort \
        sphinx \
        sphinx_rtd_theme \
        m2r \
		Pillow \
		xlrd
#RUN pip3 --no-cache-dir install git+https://github.com/kivy/kivy.git@master

RUN python3 -c "from matplotlib import font_manager; font_manager._rebuild()"

# Environment
ENV PATH="/opt/intelligent_analysis:${PATH}"
ENV LD_LIBRARY_PATH="/opt/intelligent_analysis:${LD_LIBRARY_PATH}"
ENV PYTHONPATH="/opt/intelligent_analysis"

USER root
ENV HOME /home/developer
WORKDIR /opt/intelligent_analysis
