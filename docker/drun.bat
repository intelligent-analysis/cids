@ECHO OFF

setlocal enabledelayedexpansion

set IMG=intelligent_analysis2:latest
set USERNAME=%USERNAME%
set USERID=%sid%
set SCRIPTNAME=%1
echo %SCRIPTNAME%
for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"
set "TIME=%YYYY%%MM%%DD%%HH%%Min%%Sec%"
set LOGNAME="RESULTS\%TIME%_%SCRIPTNAME%.py.log"

if "%2"=="" (
    set GPUS=0
    set CPUS=2.0
    ) else if "%3"=="" (
    set GPUS=%2
    set CPUS="2.0"
    ) else if "%4"=="" (
    set GPUS=%2
    set CPUS=%3
    ) else if "%5"=="" (
    set GPUS=%2;
    set CPUS=%3;
    set IMG=%4":latest"
    ) else (
    echo %*
    )

echo GPUs visible to CUDA:
echo %GPUS%
echo CPUS:
echo %CPUS%


REM Run script
docker run -v %CD%:/opt/intelligent_analysis %IMG%
    REM -e "NVIDIA_VISIBLE_DEVICES=%GPUS%" ^
    REM -e "HOSTNAME=%(cat /etc/hostname)%" ^
    REM -e "DISPLAY=%DISPLAY%" ^
    REM -v "/tmp/.X11-unix":"/tmp/.X11-unix" ^
    REM -v %CD%:/opt/intelligent_analysis ^
    REM -v %CD%/../DATA:/opt/DATA ^
    REM -v "%CD%/../demos:/opt/demos" ^
    REM -w "/opt/intelligent_analysis" ^
    REM -u "developer" ^
    REM --gpus="%GPUS%" ^
    REM --cpus="%CPUS%" ^
    REM --name "%IMG%.%TIME%.%SCRIPTNAME%" ^
    REM --rm ^
    REM %IMG% ^
    REM python -u "%SCRIPTNAME%"
	REM |& tee -i %LOGNAME%

pause

REM Set Permissions correctly for download
docker run ^
    -v "%CD%:/opt/intelligent_analysis" ^
    -v "%CD%/../DATA:/opt/DATA" ^
    -w "/opt/intelligent_analysis" ^
    -u "developer" ^
    --rm ^
    ${IMG} ^
    chmod -R a+rw "RESULTS" >/dev/null 2>&1; ^
    chown -R "%USERID%:%USERID%" "RESULTS" >/dev/null 2>&1; ^
    chmod -R a+rw "/opt/DATA" >/dev/null 2>&1; ^
    chown -R "%USERID%:%USERID%" "/opt/DATA" >/dev/null 2>&1;
