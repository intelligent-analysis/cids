FROM intelligent_analysis-gpu:latest

LABEL maintainer="Arnd Koeppe <koeppe@iam.rwth-aachen.de>"

ENV FENICS_VERSION=2019.1.0
ENV PYBIND11_VERSION=2.2.3

USER root
WORKDIR /fenics

RUN apt-get update && \
    apt-get install -yq --no-install-recommends \
        git \
        bison \
        ccache \
        cmake \
        doxygen \
        flex \
        g++ \
        gfortran \
        graphviz \
        libboost-filesystem-dev \
        libboost-iostreams-dev \
        libboost-math-dev \
        libboost-program-options-dev \
        libboost-system-dev \
        libboost-thread-dev \
        libboost-timer-dev \
        libeigen3-dev \
        libfreetype6-dev \
        liblapack-dev \
        libmpich-dev \
        libopenblas-dev \
        libpcre3-dev \
        libpng12-dev \
        libhdf5-mpich-dev \
        libgmp-dev \
        libcln-dev \
        libmpfr-dev \
        man \
        mpich \
        nano \
        pkg-config \
        wget \
        bash-completion \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN pip3 --no-cache-dir install --upgrade \
        fenics-ffc \
        sympy==1.1.1

RUN wget -nc --quiet \
    https://github.com/pybind/pybind11/archive/v${PYBIND11_VERSION}.tar.gz && \
    tar -xf v${PYBIND11_VERSION}.tar.gz && \
    cd pybind11-${PYBIND11_VERSION} && \
    mkdir build && \
    cd build && \
    cmake -DPYBIND11_TEST=off .. && \
    make install

RUN git clone --branch=$FENICS_VERSION \
    https://bitbucket.org/fenics-project/dolfin
RUN git clone --branch=$FENICS_VERSION \
    https://bitbucket.org/fenics-project/mshr
RUN mkdir dolfin/build && \
    cd dolfin/build && \
    cmake .. && \
    make install && \
    cd ../..
RUN mkdir mshr/build && \
    cd mshr/build && \
    cmake .. && \
    make install && \
    cd ../..
RUN cd dolfin/python && \
    pip3 install . && \
    cd ../..
RUN cd mshr/python && \
    pip3 install . && \
    cd ../..

#RUN git clone --branch=new_ufl \
#    https://bitbucket.org/fenics-apps/fenics-solid-mechanics/
#RUN sed -i 's/dolfin/fenics-dolfin/g' fenics-solid-mechanics/python/setup.py
#RUN cd fenics-solid-mechanics && \
#    ./cmake.local && \
#    cd ..
#ENV LD_LIBRARY_PATH=/fenics/fenics-solid-mechanics/local/lib:$LD_LIBRARY_PATH
#ENV PATH=/fenics/fenics-solid-mechanics/local/bin:$PATH
#RUN cd fenics-solid-mechanics/python && \
#    pip3 install . && \
#    cd ../..

USER root

WORKDIR /opt/intelligent_analysis
