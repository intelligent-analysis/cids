#!/usr/bin/env bash
if [ $# -eq 0 ]
then
    img="intelligent_analysis2-gpu:latest"
else
    img=${1}
    if [ $# -eq 2 ]
    then
        params=${2}
    else
        params=""
    fi
fi
docker run -it \
           -v "${PWD}:/opt/intelligent_analysis" \
           -v "${PWD}/../DATA:/opt/DATA" \
           -v "${PWD}/../demos:/opt/demos" \
           -w "/opt/intelligent_analysis" \
           -e "DISPLAY=${DISPLAY}" \
           -v "/tmp/.X11-unix":"/tmp/.X11-unix" \
           -P \
           ${params} \
           ${img} \
           /bin/bash
