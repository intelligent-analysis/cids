#!/usr/bin/env bash
if [ $# -eq 0 ]
then
    img="intelligent_analysis-gpu-fenics:latest"
else
    img=${1}
fi
docker run -v "${PWD}:/opt/intelligent_analysis" \
           -w "/opt/intelligent_analysis" \
           -u "root" \
           -i \
           ${img} \
           /bin/bash /opt/intelligent_analysis/fem/fenics-dev/install/bin/fenics-build
