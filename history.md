# List of changes

## 2023-04-18
* Improved:
  * Enabled findability of leaf files, which are in a parallel folder to branch file.
  * Adapted structure of zipped folders to be consistent to folders zipped in console.
* Fixed:
  * Unzipped folders after downloading and unzipping them from a dataset from Kadi,
    don't have repeating, nested folder name.

## Version 3.0 (September 2022)

* Published as open-source software via Gitlab, Kadi, and Zenodo
* KadiAI as interface between CIDS and Kadi
  * KadiAIProject handles synchronization, linking, and organizing data locally
  * KadiAIProjects define the ML task to be solved by CIDSModels
  * KadiAIProjects implement an ML ontology in Kadi
  * Interactive Dashboards for Kadi Workflows
  * CIDS nodes for Kadi workflows
* Improved and streamlined CIDS' DataDefinition and preprocessing capabilities
* Experimental wrapper for SKLearn
* Providing CIDS' search, tuning, and optimization to SKLearn models and Kadi workflows
