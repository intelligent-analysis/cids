import os
import shutil

from setuptools import find_packages
from setuptools import setup

__version__ = "0.0"
with open(os.path.join("cids", "_version.py"), encoding="utf8") as f:
    exec(f.read())  # pylint:disable=exec-used


with open("README.md", encoding="utf8") as f:
    long_description = f.read()


# # Generate wrapped CIDS and KERAS layers
# wrapped_layers = []
# with open(
#     os.path.join("cids", "kadi", "cidsflow", "wrap_keras_layers.py"), encoding="utf8"
# ) as f:
#     exec(f.read())


console_scripts = ["cids-flow = kadi_ai.nodes.commands:cids_flow"]
# + [f"cids._layers.{name:s} = {executable:s}" for name, executable in wrapped_layers]

all_tool_nodes = [
    "cids-flow active bayesian_oracle",
    "cids-flow active finisher",
    "cids-flow active scorer",
    "cids-flow interactive control_center",
    "cids-flow interactive define_data",
    "cids-flow interactive define_model",
    "cids-flow interactive stop",
    "cids-flow noninteractive convert_and_preprocess",
    "cids-flow noninteractive eval",
    "cids-flow noninteractive infer",
    "cids-flow noninteractive pull",
    "cids-flow noninteractive push",
    "cids-flow noninteractive search",
    "cids-flow noninteractive train",
]

setup(
    name="cids",
    version=__version__,  # noqa: F821, pylint:disable=undefined-variable
    license="Apache 2.0",
    author="Arnd Koeppe",
    description="Computational Intelligence and Data Science toolbox.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/intelligent-analysis/cids",
    packages=find_packages(),
    include_package_data=True,
    python_requires=">=3.9",
    zip_safe=False,
    classifiers=[
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
    ],
    install_requires=[
        "autoflake==2.0",
        "black==22.3",
        "click_completion",
        "colorama",
        "colorlog",
        "dash-bootstrap-components",
        "dash-editor-components",
        "dash-extensions",
        "dash<2.5",
        "deap",
        "flake8==6.0",
        "graphviz",
        "hiplot",
        "ipyfilechooser",
        "jinja2",
        "jupyter",
        "kadi-apy",
        "keras-tuner",
        "matplotlib",
        "pandas",
        "psutil",
        "pycodestyle==2.10",
        "pydot",
        "python-dateutil",
        "scikit-learn",
        "scikit-image",
        "seaborn",
        "six",
        "spm1d",
        "tensorflow==2.11",  # CUDNN 8.1, CUDA 11.2, Python 3.10
        "tqdm",
        "werkzeug==2.2.2",
        "xmlhelpy",
        "xmljson",
    ],
    extras_require={
        "dev": [
            "anybadge==1.14",
            "coverage==7.1",
            "flake8-docstrings==1.7",
            "genbadge==1.1",
            "pre-commit==3.0",
            "pylint==2.16",
            "tox",
            # "pytest-cov",
        ],
        "docs": [
            "myst-parser>=0.15",
            "recommonmark>=0.7",
            "sphinx>=3",
            "sphinx_rtd_theme>=0.5",
        ],
    },
    entry_points={"console_scripts": console_scripts},
)

# Write or extend kadistudio tools.txt
kadistudio_dir = os.path.join(os.path.expanduser("~"), ".kadistudio")
tools_file = os.path.join(kadistudio_dir, "tools.txt")
if os.path.exists(tools_file):
    shutil.copy2(tools_file, tools_file.replace(".txt", ".backup"))
    with open(tools_file, encoding="utf8") as f:
        content = f.readlines()
        content = [x.strip() for x in content]
    with open(tools_file, "a+", encoding="utf8") as f:
        for tn in all_tool_nodes:
            if tn not in content:
                f.write(tn + "\n")
else:
    os.makedirs(kadistudio_dir, exist_ok=True)
    with open(tools_file, "w", encoding="utf8") as f:
        for tn in all_tool_nodes:
            f.write(tn + "\n")
