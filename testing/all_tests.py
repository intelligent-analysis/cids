# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Suite that runs all CIDS and KadiAI tests. Part of the CIDS toolbox and KadiAI."""
from tensorflow.python.platform import test

from .test_data import DataTest
from .test_project import ProjectTest
from .test_tensorflow_layers import TFLayerTester
from .test_tensorflow_model import CIDSModelTFTest


def all_tests():
    suite = test.TestSuite()
    suite.addTest(DataTest)
    suite.addTest(ProjectTest)
    suite.addTest(TFLayerTester)
    suite.addTest(CIDSModelTFTest)
    return suite


if __name__ == "__main__":
    runner = test.TextTestRunner()
    runner.run(all_tests())
