# Copyright 2022 Arnd Koeppe
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""CIDS computational intelligence library"""
# TODO: chunking tester
# TODO: feeding tester
import numpy as np
import tensorflow as tf
from tensorflow.python.platform import test

from cids.legacy.data.datareader import DataReader
from testing.src.create_test_dataset import _NF_dataset
from testing.src.create_test_dataset import _NSF_dataset


DEBUG = True


class DataReaderTest(test.TestCase):
    """Tester for the datareader module."""

    def test_matrix_from_csv(self):
        """Tester for DataReader.matrix_from_csv."""
        paths = ["tests/src/csv/2D_dataset.csv", "tests/src/csv/2D_dataset.csv"]
        dataset, data_shape, data_format = _NF_dataset()
        batch_size = 2
        num_samples = len(paths)
        if DEBUG:
            print("paths", len(paths), "\n", paths)
            print(data_format)
            print("dataset", dataset.shape, "\n", dataset)
        # Graph
        x = tf.placeholder(tf.string, shape=(num_samples,))
        dr = DataReader(
            data_shape,
            data_format,
            batch_size,
            slice_tensors=True,
            sample_dtype=tf.float32,
            file_type="csv",
            shuffle=False,
        )
        tf_dataset = dr.generate_batch_dataset(x)
        iterator = tf_dataset.make_initializable_iterator()
        y = iterator.get_next()
        y2 = iterator.get_next()
        with self.test_session() as sess:
            sess.run(
                [
                    tf.local_variables_initializer(),
                    tf.global_variables_initializer(),
                    iterator.initializer,
                ],
                feed_dict={x: paths},
            )
            for i in range(int(2 * data_shape[0] / batch_size)):
                # Run
                Y, Y2 = sess.run([y, y2], feed_dict={x: paths})
                # Debug
                if DEBUG:
                    print("index:", i)
                    print("Y", Y.shape, "\n", Y)
                    print("Y2", Y2.shape, "\n", Y2)
                # Assertions
                self.assertAllEqual(Y.shape, (batch_size, data_shape[1]))
                assert (Y != Y2).any(), "Iterator did not return a new batch"
                for rows in Y:
                    assert (
                        (dataset.astype(np.float32) == rows).all(axis=1).any()
                    ), "Row not in original dataset"

    def test_sequence_from_csv(self):
        """Tester for DataReader.sequence_from_csv."""
        paths = tf.gfile.Glob("tests/src/csv/3D_dataset_sample*.csv")
        paths = sorted(paths)
        dataset, data_shape, data_format = _NSF_dataset()
        num_samples = len(paths)
        batch_size = 2
        if DEBUG:
            print("paths", len(paths), "\n", paths)
            print(data_format)
            print("dataset", dataset.shape, "\n", dataset)
        # Graph
        x = tf.placeholder(tf.string, shape=(num_samples,))
        dr = DataReader(
            data_shape,
            data_format,
            batch_size,
            sample_dtype=tf.float32,
            file_type="csv",
            shuffle=False,
            slice_tensors=False,
        )
        tf_dataset = dr.generate_batch_dataset(x)
        iterator = tf_dataset.make_initializable_iterator()
        y = iterator.get_next()
        y2 = iterator.get_next()
        with self.test_session() as sess:
            sess.run(
                [
                    tf.local_variables_initializer(),
                    tf.global_variables_initializer(),
                    iterator.initializer,
                ],
                feed_dict={x: paths},
            )
            for i in range(int(2 * data_shape[0] / batch_size)):
                # Run
                Y, Y2 = sess.run([y, y2], feed_dict={x: paths})
                # Debug
                if DEBUG:
                    print("index:", i)
                    print("Y", Y.shape, "\n", Y)
                # Assertions
                self.assertAllEqual(Y.shape, (batch_size, data_shape[1], data_shape[2]))
                for rows in Y:
                    assert (
                        (dataset == rows).all(axis=1).any()
                    ), "Row not in original dataset"

    def test_matrix_from_tfrecord(self):
        """Tester for DataReader.matrix_from_tfrecord."""
        paths = [
            "tests/src/tfrecord/2D_dataset.tfrecord",
            "tests/src/tfrecord/2D_dataset.tfrecord",
        ]
        dataset, data_shape, data_format = _NF_dataset()
        batch_size = 2
        num_samples = len(paths)
        if DEBUG:
            print("paths", "\n", paths)
            print(data_format)
            print("dataset", dataset.shape, "\n", dataset)
        # Graph
        x = tf.placeholder(tf.string, shape=(num_samples,))
        dr = DataReader(
            data_shape,
            data_format,
            batch_size,
            slice_tensors=True,
            file_type="tfr",
            shuffle=False,
        )
        tf_dataset = dr.generate_batch_dataset(x)
        iterator = tf_dataset.make_initializable_iterator()
        y = iterator.get_next()
        y2 = iterator.get_next()
        with self.test_session() as sess:
            sess.run(
                [
                    tf.local_variables_initializer(),
                    tf.global_variables_initializer(),
                    iterator.initializer,
                ],
                feed_dict={x: paths},
            )
            for i in range(int(2 * data_shape[0] / batch_size)):
                # Run
                Y, Y2 = sess.run([y, y2], feed_dict={x: paths})
                # Debug
                if DEBUG:
                    print("index:", i)
                    print("Y", Y.shape, "\n", Y)
                # Assertions
                self.assertAllEqual(Y.shape, (batch_size, data_shape[1]))
                for i, row in enumerate(Y):
                    print(i)
                    print("   ", row)
                    assert (
                        (dataset == row).all(axis=1).any()
                    ), "Row not in original dataset"

    def test_sequence_from_tfrecord(self):
        """Tester for DataReader.sequence_from_tfrecord."""
        paths = tf.gfile.Glob("tests/src/tfrecord/3D_dataset_sample*.tfrecord")
        paths = sorted(paths)
        dataset, data_shape, data_format = _NSF_dataset()
        num_samples = len(paths)
        batch_size = 2
        chunk_size = 3
        if DEBUG:
            print("paths", len(paths), "\n", paths)
            print(data_format)
            print("dataset", dataset.shape, "\n", dataset)
        # Graph
        x = tf.placeholder(tf.string, shape=(num_samples,))
        data_shape = list(data_shape)
        dr = DataReader(
            data_shape,
            data_format,
            batch_size,
            file_type="tfr",
            shuffle=False,
            slice_tensors=False,
            chunk_size=chunk_size,
        )
        tf_dataset = dr.generate_batch_dataset(x)
        iterator = tf_dataset.make_initializable_iterator()
        y = iterator.get_next()
        y2 = iterator.get_next()
        with self.test_session() as sess:
            sess.run(
                [
                    tf.local_variables_initializer(),
                    tf.global_variables_initializer(),
                    iterator.initializer,
                ],
                feed_dict={x: paths},
            )
            for i in range(int(2 * data_shape[0] / batch_size)):
                # Run
                Y, Y2 = sess.run([y, y2], feed_dict={x: paths})
                # Debug
                if DEBUG:
                    print("index:", i)
                    print("Y", Y.shape, "\n", Y)
                # Assertions
                self.assertAllEqual(Y.shape, (batch_size, chunk_size, data_shape[2]))
                for yrc in Y:
                    for yr in yrc:
                        assert (
                            (dataset.reshape([-1, dataset.shape[-1]]) == yr)
                            .all(axis=1)
                            .any()
                        ), "Row not in original dataset"

    def test_placeholder(self):
        """Tester for DataReader.matrix_from_tfrecord."""

        num_steps = 3
        num_features = 5
        batch_size = 2
        data_shape = [batch_size, num_features]
        data_format = "NF"

        def src_one():
            return np.ones([num_features], dtype=np.float64)

        def src_zero():
            return np.zeros([num_features], dtype=np.float64)

        def src_two():
            return 2 * src_one()

        # Graph
        x = tf.placeholder(tf.float64, shape=(None, num_features))
        dr = DataReader(
            data_shape,
            data_format,
            batch_size,
            src_type="placeholder",
            sample_dtype=tf.float64,
            cast_dtype=tf.float32,
            slice_tensors=True,
            shuffle=False,
            prefetch=False,
            buffer_size=-1,
        )
        tf_dataset = dr.generate_batch_dataset(x)
        iterator = tf_dataset.make_initializable_iterator()
        y = iterator.get_next()
        yy = tf.placeholder_with_default(x, shape=(None, num_features))
        with self.test_session() as sess:
            # Feed with zeros
            samples = [src_zero() for _ in range(batch_size)]
            sess.run(
                [
                    tf.local_variables_initializer(),
                    tf.global_variables_initializer(),
                    iterator.initializer,
                ],
                feed_dict={x: samples},
            )
            for i in range(num_steps):
                samples = [src_zero() for _ in range(batch_size)]
                # Run
                Y = sess.run(yy, feed_dict={x: samples})
                # Debug
                if DEBUG:
                    print("index:", i)
                    print("Y", Y.shape, "\n", Y)
                # Assertions
                self.assertAllEqual(Y.shape, (batch_size, data_shape[1]))
                self.assertAllEqual(Y, samples)
                # Feed with zeros
                samples = [src_zero() for _ in range(batch_size)]
                sess.run(
                    [
                        tf.local_variables_initializer(),
                        tf.global_variables_initializer(),
                        iterator.initializer,
                    ],
                    feed_dict={x: samples},
                )
            # Feed with ones
            for i in range(num_steps):
                samples = [src_one() for _ in range(batch_size)]
                sess.run([iterator.initializer], feed_dict={x: samples})
                # Run
                Y = sess.run(yy, feed_dict={x: samples})
                # Debug
                if DEBUG:
                    print("index:", i)
                    print("Y", Y.shape, "\n", Y)
                # Assertions
                self.assertAllEqual(Y.shape, (batch_size, data_shape[1]))
                self.assertAllEqual(Y, samples)

            # Feed with twos through train_feed
            for i in range(num_steps):
                samples = [src_two() for _ in range(batch_size)]
                # Run
                Y = sess.run(yy, feed_dict={yy: samples})
                # Debug
                if DEBUG:
                    print("index:", i)
                    print("Y", Y.shape, "\n", Y)
                # Assertions
                self.assertAllEqual(Y.shape, (batch_size, data_shape[1]))
                self.assertAllEqual(Y, samples)

    def test_shuffle_with_seed(self):
        """Tester for DataReader.matrix_from_csv."""
        paths = ["tests/src/csv/2D_dataset.csv", "tests/src/csv/2D_dataset.csv"]
        dataset, data_shape, data_format = _NF_dataset()
        batch_size = 4
        num_samples = len(paths)
        # Graph
        x = tf.placeholder(tf.string, shape=(num_samples,))
        dr = DataReader(
            data_shape,
            data_format,
            batch_size,
            slice_tensors=True,
            sample_dtype=tf.float32,
            file_type="csv",
            shuffle=True,
            seed=123456,
        )
        tf_dataset = dr.generate_batch_dataset(x)
        iterator = tf_dataset.make_initializable_iterator()
        y = iterator.get_next()
        first_list = []
        with self.test_session() as sess:
            sess.run(
                [
                    tf.local_variables_initializer(),
                    tf.global_variables_initializer(),
                    iterator.initializer,
                ],
                feed_dict={x: paths},
            )
            for i in range(int(2 * data_shape[0] / batch_size)):
                # Run
                Y = sess.run([y], feed_dict={x: paths})
                # Debug
                first_list.append(Y)
        #############################
        # Second graph, same settings
        x = tf.placeholder(tf.string, shape=(num_samples,))
        dr = DataReader(
            data_shape,
            data_format,
            batch_size,
            slice_tensors=True,
            sample_dtype=tf.float32,
            file_type="csv",
            shuffle=True,
            seed=123456,
        )
        tf_dataset = dr.generate_batch_dataset(x)
        iterator = tf_dataset.make_initializable_iterator()
        y = iterator.get_next()
        second_list = []
        with self.test_session() as sess:
            sess.run(
                [
                    tf.local_variables_initializer(),
                    tf.global_variables_initializer(),
                    iterator.initializer,
                ],
                feed_dict={x: paths},
            )
            for i in range(int(2 * data_shape[0] / batch_size)):
                # Run
                Y = sess.run([y], feed_dict={x: paths})
                second_list.append(Y)
        # Assertions
        assert np.shape(first_list) == np.shape(second_list)
        print(np.array(first_list) - np.array(second_list))
        assert (
            np.array(first_list) == np.array(second_list)
        ).all(), "Seed did not work properly"
        np.testing.assert_array_equal(np.array(first_list), np.array(second_list))

    def test_shuffle_without_seed(self):
        """Tester for DataReader.matrix_from_csv."""
        paths = ["tests/src/csv/2D_dataset.csv", "tests/src/csv/2D_dataset.csv"]
        dataset, data_shape, data_format = _NF_dataset()
        batch_size = 4
        num_samples = len(paths)
        # Graph
        x = tf.placeholder(tf.string, shape=(num_samples,))
        dr = DataReader(
            data_shape,
            data_format,
            batch_size,
            slice_tensors=True,
            sample_dtype=tf.float32,
            file_type="csv",
            shuffle=True,
            seed=None,
        )
        tf_dataset = dr.generate_batch_dataset(x)
        iterator = tf_dataset.make_initializable_iterator()
        y = iterator.get_next()
        first_list = []
        with self.test_session() as sess:
            sess.run(
                [
                    tf.local_variables_initializer(),
                    tf.global_variables_initializer(),
                    iterator.initializer,
                ],
                feed_dict={x: paths},
            )
            for i in range(int(2 * data_shape[0] / batch_size)):
                # Run
                Y = sess.run([y], feed_dict={x: paths})
                # Debug
                first_list.append(Y)
        #############################
        # Second graph, same settings
        x = tf.placeholder(tf.string, shape=(num_samples,))
        dr = DataReader(
            data_shape,
            data_format,
            batch_size,
            slice_tensors=True,
            sample_dtype=tf.float32,
            file_type="csv",
            shuffle=True,
            seed=123456,
        )
        tf_dataset = dr.generate_batch_dataset(x)
        iterator = tf_dataset.make_initializable_iterator()
        y = iterator.get_next()
        second_list = []
        with self.test_session() as sess:
            sess.run(
                [
                    tf.local_variables_initializer(),
                    tf.global_variables_initializer(),
                    iterator.initializer,
                ],
                feed_dict={x: paths},
            )
            for i in range(int(2 * data_shape[0] / batch_size)):
                # Run
                Y = sess.run([y], feed_dict={x: paths})
                second_list.append(Y)
        # Assertions
        assert np.shape(first_list) == np.shape(second_list)
        print(np.array(first_list) - np.array(second_list))
        assert (
            np.asarray(first_list) != np.asarray(second_list)
        ).any(), "Shuffling did not work."


if __name__ == "__main__":
    test.main()
