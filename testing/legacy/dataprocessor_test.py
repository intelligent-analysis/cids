# Copyright 2022 Arnd Koeppe
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""CIDS computational intelligence library"""
import numpy as np
import tensorflow as tf
from tensorflow.python.platform import test

from cids.legacy.data.dataprocessor import DataProcessor
from testing.src.create_test_dataset import _NF_dataset
from testing.src.create_test_dataset import _NSF_dataset


DEBUG = True


class DataProcessorTest(test.TestCase):
    """Tester for the dataprocessor module."""

    def test_online_mean_normalize_2D(self):
        """Test the online_normalize function for a 2D tensor."""
        dataset, data_shape, data_format = _NF_dataset()
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(data_shape, data_format=data_format)
        y = dp.online_normalize(x)
        freeze = False
        if DEBUG:
            print(
                "dataset_mean",
                dataset.shape[1:],
                "\n",
                np.mean(dataset, axis=0, keepdims=True),
            )
            print(
                "dataset_std",
                dataset.shape[1:],
                "\n",
                np.std(dataset, axis=0, keepdims=True),
            )
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            for i in range(0, dataset.shape[0], 2):
                # Extract multiple samples
                batch = dataset[i : i + 2, :]
                # Run
                Y, mu, sig, N, M2, frozen = sess.run(
                    [y, dp.mean, dp.std, dp.count, dp.m2, dp.freeze],
                    feed_dict={x: batch, dp.freeze: freeze},
                )
                # Debug
                if DEBUG:
                    print("index:", i, N)
                    print("dataset", batch.shape, "\n", batch)
                    print("Y", Y.shape, "\n", Y)
                    print("mu", mu.shape, "\n", mu)
                    print("sig", sig.shape, "\n", sig)
                # Assertions
                self.assertAllEqual(Y.shape, batch.shape)
                self.assertAllEqual(mu.shape[1:], dataset.shape[1:])
                self.assertAllEqual(sig.shape[1:], dataset.shape[1:])
                if i > 6:
                    self.assertLess(np.abs(Y).max(), 3.5)
                    self.assertAllEqual(np.argmax(Y, axis=0), np.argmax(batch, axis=0))
                    self.assertAllEqual(np.argmin(Y, axis=0), np.argmin(batch, axis=0))
                if i == dataset.shape[0] - 4:
                    print("Freezing normalization stats")
                    mu_frozen = mu
                    sig_frozen = sig
                    N_frozen = N
                    M2_frozen = M2
                    freeze = True
                if i == dataset.shape[0] - 2:
                    self.assertAllEqual(mu, mu_frozen)
                    self.assertAllEqual(sig, sig_frozen)
                    self.assertAllEqual(N, N_frozen)
                    self.assertAllEqual(M2, M2_frozen)

    def test_online_mean_normalize_fixed_stats(self):
        """Test the online_normalize function for a 2D tensor."""
        dataset, data_shape, data_format = _NF_dataset()
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        stat_shape = [1] + list(data_shape[1:])
        init_stats = {
            "count": 1,
            "mean": np.zeros(stat_shape, dtype=np.float64),
            "std": np.ones(stat_shape, dtype=np.float64),
            "m2": np.square(np.ones(stat_shape, dtype=np.float64)),
        }
        dp = DataProcessor(data_shape, data_format=data_format, init_stats=init_stats)
        y = dp.online_normalize(x)
        freeze = True
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            # Ensure initializing with fixed stats worked
            mu, sig, N, M2 = sess.run([dp.mean, dp.std, dp.count, dp.m2])
            self.assertAllEqual(mu, init_stats["mean"])
            self.assertAllEqual(sig, init_stats["std"])
            self.assertAllEqual(N, init_stats["count"])
            self.assertAllEqual(M2, init_stats["m2"])
            # Ensure frozen variables remain constant
            for i in range(0, dataset.shape[0], 2):
                # Extract multiple samples
                batch = dataset[i : i + 2, :]
                # Run
                Y, mu, sig, N, M2, frozen = sess.run(
                    [y, dp.mean, dp.std, dp.count, dp.m2, dp.freeze],
                    feed_dict={x: batch, dp.freeze: freeze},
                )
                # Debug
                if DEBUG:
                    print("index:", i, N)
                    print("dataset", batch.shape, "\n", batch)
                    print("Y", Y.shape, "\n", Y)
                    print("mu", mu.shape, "\n", mu)
                    print("sig", sig.shape, "\n", sig)
                # Assertions
                self.assertAllEqual(Y.shape, batch.shape)
                self.assertAllEqual(mu.shape[1:], dataset.shape[1:])
                self.assertAllEqual(sig.shape[1:], dataset.shape[1:])
                self.assertAllEqual(Y, batch.astype(np.float32))
                self.assertAllEqual(
                    mu[0, :], np.zeros(batch.shape[1:], dtype=np.float64)
                )
                self.assertAllEqual(
                    sig[0, :], np.ones(batch.shape[1:], dtype=np.float64)
                )

    def test_online_rescale_2D(self):
        """Test the online_rescale function for a 2D tensor."""
        dataset, data_shape, data_format = _NF_dataset()
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(data_shape, data_format=data_format)
        y = dp.online_normalize(x)
        y = dp.online_rescale(y)
        freeze = False
        if DEBUG:
            print(
                "dataset_mean",
                dataset.shape[1:],
                "\n",
                np.mean(dataset, axis=0, keepdims=True),
            )
            print(
                "dataset_std",
                dataset.shape[1:],
                "\n",
                np.std(dataset, axis=0, keepdims=True),
            )
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            for i in range(0, dataset.shape[0], 2):
                # Extract multiple samples
                batch = dataset[i : i + 2, :]
                # Run
                Y, mu, sig, N, M2, frozen = sess.run(
                    [y, dp.mean, dp.std, dp.count, dp.m2, dp.freeze],
                    feed_dict={x: batch, dp.freeze: freeze},
                )
                # Debug
                if DEBUG:
                    print("index:", i, N)
                    print("dataset", batch.shape, "\n", batch)
                    print("Y", Y.shape, "\n", Y)
                    print("mu", mu.shape, "\n", mu)
                    print("sig", sig.shape, "\n", sig)
                # Assertions
                self.assertAllEqual(Y.shape, batch.shape)
                self.assertAllEqual(mu.shape[1:], dataset.shape[1:])
                self.assertAllEqual(sig.shape[1:], dataset.shape[1:])
                if i > 5:
                    self.assertAllClose(Y, batch)
                    self.assertAllEqual(np.argmax(Y, axis=0), np.argmax(batch, axis=0))
                    self.assertAllEqual(np.argmin(Y, axis=0), np.argmin(batch, axis=0))
                if i == dataset.shape[0] - 4:
                    print("Freezing normalization stats")
                    mu_frozen = mu
                    sig_frozen = sig
                    N_frozen = N
                    M2_frozen = M2
                    freeze = True
                if i == dataset.shape[0] - 2:
                    self.assertAllEqual(mu, mu_frozen)
                    self.assertAllEqual(sig, sig_frozen)
                    self.assertAllEqual(N, N_frozen)
                    self.assertAllEqual(M2, M2_frozen)

    def test_online_mean_normalize_3D(self):
        """Test the online_normalize function for a 3D tensor (sequence)."""
        dataset, data_shape, data_format = _NSF_dataset()
        reduction_axes = (0, 1)
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(
            data_shape, data_format=data_format, reduction_axes=reduction_axes
        )
        y = dp.online_normalize(x)
        freeze = False
        if DEBUG:
            print(
                "dataset_mean",
                dataset.shape[1:],
                "\n",
                np.mean(dataset, axis=reduction_axes, keepdims=True),
            )
            print(
                "dataset_std",
                dataset.shape[1:],
                "\n",
                np.std(dataset, axis=reduction_axes, keepdims=True),
            )
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            for i in range(0, dataset.shape[0], 2):
                # Extract multiple samples
                batch = dataset[i : i + 2, :, :]
                # Run
                Y, mu, sig, N, M2, frozen = sess.run(
                    [y, dp.mean, dp.std, dp.count, dp.m2, dp.freeze],
                    feed_dict={x: batch, dp.freeze: freeze},
                )
                # Debug
                if DEBUG:
                    print("index:", i, N)
                    print("dataset_slice", batch.shape, "\n", batch)
                    print("Y", Y.shape, "\n", Y)
                    print("mu", mu.shape, "\n", mu)
                    print("sig", sig.shape, "\n", sig)
                # Assertions
                self.assertAllEqual(Y.shape, batch.shape)
                self.assertAllEqual(mu.shape[-1], dataset.shape[-1])
                self.assertAllEqual(sig.shape[-1], dataset.shape[-1])
                self.assertEqual(np.count_nonzero(Y), np.count_nonzero(batch))
                self.assertGreaterEqual(mu[..., 2], 1000)
                if i > 2:
                    self.assertLess(np.abs(Y).max(), 4.0)
                    for f in range(Y.shape[-1]):
                        xf = Y[:, :, f]
                        of = batch[:, :, f]
                        self.assertAllEqual(
                            np.argmax(xf[np.nonzero(xf)]), np.argmax(of[np.nonzero(of)])
                        )
                        self.assertAllEqual(
                            np.argmin(xf[np.nonzero(xf)]), np.argmin(of[np.nonzero(of)])
                        )
                if i == 7:
                    self.assertAllEqual(
                        Y[0, 4:, :], np.zeros([3, Y.shape[2]], dtype=np.float32)
                    )
                if i == dataset.shape[0] - 4:
                    print("Freezing normalization stats")
                    mu_frozen = mu
                    sig_frozen = sig
                    N_frozen = N
                    M2_frozen = M2
                    freeze = True
                if i == dataset.shape[0] - 2:
                    self.assertAllEqual(mu, mu_frozen)
                    self.assertAllEqual(sig, sig_frozen)
                    self.assertAllEqual(N, N_frozen)
                    self.assertAllEqual(M2, M2_frozen)

    def test_online_rescale_3D(self):
        """Test the online_rescale function for a 3D tensor (sequence)."""
        dataset, data_shape, data_format = _NSF_dataset()
        reduction_axes = (0, 1)
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(
            data_shape, data_format=data_format, reduction_axes=reduction_axes
        )
        y = dp.online_normalize(x)
        y = dp.online_rescale(y)
        freeze = False
        if DEBUG:
            print(
                "dataset_mean",
                dataset.shape[1:],
                "\n",
                np.mean(dataset, axis=0, keepdims=True),
            )
            print(
                "dataset_std",
                dataset.shape[1:],
                "\n",
                np.std(dataset, axis=0, keepdims=True),
            )
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            for i in range(0, dataset.shape[0], 2):
                # Extract multiple samples
                batch = dataset[i : i + 2, :]
                # Run
                Y, mu, sig, N, M2, frozen = sess.run(
                    [y, dp.mean, dp.std, dp.count, dp.m2, dp.freeze],
                    feed_dict={x: batch, dp.freeze: freeze},
                )
                # Debug
                if DEBUG:
                    print("index:", i, N)
                    print("dataset", batch.shape, "\n", batch)
                    print("Y", Y.shape, "\n", Y)
                    print("mu", mu.shape, "\n", mu)
                    print("sig", sig.shape, "\n", sig)
                # Assertions
                self.assertAllEqual(Y.shape, batch.shape)
                self.assertAllEqual(mu.shape[-1], dataset.shape[-1])
                self.assertAllEqual(sig.shape[-1], dataset.shape[-1])
                self.assertAllEqual(
                    Y[:, -1, :], np.zeros([Y.shape[0], Y.shape[2]], dtype=np.float32)
                )
                if i > 2:
                    self.assertAllEqual(
                        np.argmax(Y[np.nonzero(Y)]), np.argmax(batch[np.nonzero(batch)])
                    )
                    self.assertAllEqual(
                        np.argmin(Y[np.nonzero(Y)]), np.argmin(batch[np.nonzero(batch)])
                    )
                    self.assertAllClose(Y, batch)
                if i == 7:
                    self.assertAllEqual(
                        Y[0, 4:, :], np.zeros([3, Y.shape[2]], dtype=np.float32)
                    )
                if i == dataset.shape[0] - 4:
                    print("Freezing normalization stats")
                    mu_frozen = mu
                    sig_frozen = sig
                    N_frozen = N
                    M2_frozen = M2
                    freeze = True
                if i == dataset.shape[0] - 2:
                    self.assertAllEqual(mu, mu_frozen)
                    self.assertAllEqual(sig, sig_frozen)
                    self.assertAllEqual(N, N_frozen)
                    self.assertAllEqual(M2, M2_frozen)

    def test_online_mean_normalize_2D_minmax(self):
        """Test the online_normalize function for a 2D tensor."""
        dataset, data_shape, data_format = _NF_dataset()
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(data_shape, data_format=data_format, normalize_mode="minmax")
        y = dp.online_normalize(x)
        freeze = False
        if DEBUG:
            print(
                "dataset_mean",
                dataset.shape[1:],
                "\n",
                np.mean(dataset, axis=0, keepdims=True),
            )
            print(
                "dataset_std",
                dataset.shape[1:],
                "\n",
                np.std(dataset, axis=0, keepdims=True),
            )
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            for i in range(0, dataset.shape[0], 2):
                # Extract multiple samples
                batch = dataset[i : i + 2, :]
                # Run
                Y, mu, min, max, N, frozen = sess.run(
                    [y, dp.mean, dp.min, dp.max, dp.count, dp.freeze],
                    feed_dict={x: batch, dp.freeze: freeze},
                )
                # Debug
                if DEBUG:
                    print("index:", i, N)
                    print("dataset", batch.shape, "\n", batch)
                    print("Y", Y.shape, "\n", Y)
                    print("mu", mu.shape, "\n", mu)
                    print("min", min.shape, "\n", min)
                    print("max", max.shape, "\n", max)
                # Assertions
                self.assertAllEqual(Y.shape, batch.shape)
                self.assertAllEqual(mu.shape[1:], dataset.shape[1:])
                self.assertAllEqual(min.shape[1:], dataset.shape[1:])
                self.assertAllEqual(max.shape[1:], dataset.shape[1:])
                if i > 6:
                    self.assertLess(np.abs(Y).max(), 1.5)
                    self.assertAllEqual(np.argmax(Y, axis=0), np.argmax(batch, axis=0))
                    self.assertAllEqual(np.argmin(Y, axis=0), np.argmin(batch, axis=0))
                if i == dataset.shape[0] - 4:
                    print("Freezing normalization stats")
                    mu_frozen = mu
                    min_frozen = min
                    max_frozen = max
                    N_frozen = N
                    freeze = True
                if i == dataset.shape[0] - 2:
                    self.assertAllEqual(mu, mu_frozen)
                    self.assertAllEqual(min, min_frozen)
                    self.assertAllEqual(max, max_frozen)
                    self.assertAllEqual(N, N_frozen)

    def test_online_rescale_2D_minmax(self):
        """Test the online_rescale function for a 2D tensor with minmax."""
        dataset, data_shape, data_format = _NF_dataset()
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(data_shape, data_format=data_format, normalize_mode="minmax")
        y = dp.online_normalize(x)
        y = dp.online_rescale(y)
        freeze = False
        if DEBUG:
            print(
                "dataset_mean",
                dataset.shape[1:],
                "\n",
                np.mean(dataset, axis=0, keepdims=True),
            )
            print(
                "dataset_std",
                dataset.shape[1:],
                "\n",
                np.std(dataset, axis=0, keepdims=True),
            )
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            for i in range(0, dataset.shape[0], 2):
                # Extract multiple samples
                batch = dataset[i : i + 2, :]
                # Run
                Y, mu, min, max, N, frozen = sess.run(
                    [y, dp.mean, dp.min, dp.max, dp.count, dp.freeze],
                    feed_dict={x: batch, dp.freeze: freeze},
                )
                # Debug
                if DEBUG:
                    print("index:", i, N)
                    print("dataset", batch.shape, "\n", batch)
                    print("Y", Y.shape, "\n", Y)
                    print("mu", mu.shape, "\n", mu)
                    print("min", min.shape, "\n", min)
                    print("max", max.shape, "\n", max)
                # Assertions
                self.assertAllEqual(Y.shape, batch.shape)
                self.assertAllEqual(mu.shape[1:], dataset.shape[1:])
                self.assertAllEqual(min.shape[1:], dataset.shape[1:])
                self.assertAllEqual(max.shape[1:], dataset.shape[1:])
                if i > 5:
                    self.assertAllClose(Y, batch)
                    self.assertAllEqual(np.argmax(Y, axis=0), np.argmax(batch, axis=0))
                    self.assertAllEqual(np.argmin(Y, axis=0), np.argmin(batch, axis=0))
                if i == dataset.shape[0] - 4:
                    print("Freezing normalization stats")
                    mu_frozen = mu
                    min_frozen = min
                    max_frozen = max
                    N_frozen = N
                    freeze = True
                if i == dataset.shape[0] - 2:
                    self.assertAllEqual(mu, mu_frozen)
                    self.assertAllEqual(min, min_frozen)
                    self.assertAllEqual(max, max_frozen)
                    self.assertAllEqual(N, N_frozen)

    def test_online_mean_normalize_3D_minmax(self):
        """Test the online_normalize function for a 3D tensor (sequence) with minmax."""
        dataset, data_shape, data_format = _NSF_dataset()
        reduction_axes = (0, 1)
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(
            data_shape,
            data_format=data_format,
            reduction_axes=reduction_axes,
            normalize_mode="minmax",
        )
        y = dp.online_normalize(x)
        freeze = False
        if DEBUG:
            print(
                "dataset_mean",
                dataset.shape[1:],
                "\n",
                np.mean(dataset, axis=reduction_axes, keepdims=True),
            )
            print(
                "dataset_std",
                dataset.shape[1:],
                "\n",
                np.std(dataset, axis=reduction_axes, keepdims=True),
            )
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            for i in range(0, dataset.shape[0], 2):
                # Extract multiple samples
                batch = dataset[i : i + 2, :, :]
                # Run
                Y, mu, min, max, N, frozen = sess.run(
                    [y, dp.mean, dp.min, dp.max, dp.count, dp.freeze],
                    feed_dict={x: batch, dp.freeze: freeze},
                )
                # Debug
                if DEBUG:
                    print("index:", i, N)
                    print("dataset", batch.shape, "\n", batch)
                    print("Y", Y.shape, "\n", Y)
                    print("mu", mu.shape, "\n", mu)
                    print("min", min.shape, "\n", min)
                    print("max", max.shape, "\n", max)
                # Assertions
                self.assertAllEqual(Y.shape, batch.shape)
                self.assertAllEqual(mu.shape[-1], dataset.shape[-1])
                self.assertAllEqual(min.shape[-1], dataset.shape[-1])
                self.assertAllEqual(max.shape[-1], dataset.shape[-1])
                self.assertEqual(np.count_nonzero(Y), np.count_nonzero(batch))
                self.assertGreaterEqual(mu[..., 2], 1000)
                if i > 2:
                    self.assertLess(np.abs(Y).max(), 1.5)
                    for f in range(Y.shape[-1]):
                        xf = Y[:, :, f]
                        of = batch[:, :, f]
                        self.assertAllEqual(
                            np.argmax(xf[np.nonzero(xf)]), np.argmax(of[np.nonzero(of)])
                        )
                        self.assertAllEqual(
                            np.argmin(xf[np.nonzero(xf)]), np.argmin(of[np.nonzero(of)])
                        )
                if i == 7:
                    self.assertAllEqual(
                        Y[0, 4:, :], np.zeros([3, Y.shape[2]], dtype=np.float32)
                    )
                if i == dataset.shape[0] - 4:
                    print("Freezing normalization stats")
                    mu_frozen = mu
                    min_frozen = min
                    max_frozen = max
                    N_frozen = N
                    freeze = True
                if i == dataset.shape[0] - 2:
                    self.assertAllEqual(mu, mu_frozen)
                    self.assertAllEqual(min, min_frozen)
                    self.assertAllEqual(max, max_frozen)
                    self.assertAllEqual(N, N_frozen)

    def test_online_rescale_3D_minmax(self):
        """Test the online_rescale function for a 3D tensor (sequence) with minmax."""
        dataset, data_shape, data_format = _NSF_dataset()
        reduction_axes = (0, 1)
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(
            data_shape,
            data_format=data_format,
            reduction_axes=reduction_axes,
            normalize_mode="minmax",
        )
        y = dp.online_normalize(x)
        y = dp.online_rescale(y)
        freeze = False
        if DEBUG:
            print(
                "dataset_mean",
                dataset.shape[1:],
                "\n",
                np.mean(dataset, axis=0, keepdims=True),
            )
            print(
                "dataset_std",
                dataset.shape[1:],
                "\n",
                np.std(dataset, axis=0, keepdims=True),
            )
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            for i in range(0, dataset.shape[0], 2):
                # Extract multiple samples
                batch = dataset[i : i + 2, :]
                batch = batch
                # Run
                Y, mu, min, max, N, frozen = sess.run(
                    [y, dp.mean, dp.min, dp.max, dp.count, dp.freeze],
                    feed_dict={x: batch, dp.freeze: freeze},
                )
                # Debug
                if DEBUG:
                    print("index:", i, N)
                    print("dataset", batch.shape, "\n", batch)
                    print("Y", Y.shape, "\n", Y)
                    print("mu", mu.shape, "\n", mu)
                    print("min", min.shape, "\n", min)
                    print("max", max.shape, "\n", max)
                # Assertions
                self.assertAllEqual(Y.shape, batch.shape)
                self.assertAllEqual(mu.shape[-1], dataset.shape[-1])
                self.assertAllEqual(min.shape[-1], dataset.shape[-1])
                self.assertAllEqual(max.shape[-1], dataset.shape[-1])
                self.assertAllEqual(
                    Y[:, -1, :], np.zeros([Y.shape[0], Y.shape[2]], dtype=np.float32)
                )
                if i > 2:
                    self.assertAllEqual(
                        np.argmax(Y[np.nonzero(Y)]), np.argmax(batch[np.nonzero(batch)])
                    )
                    self.assertAllEqual(
                        np.argmin(Y[np.nonzero(Y)]), np.argmin(batch[np.nonzero(batch)])
                    )
                    self.assertAllClose(Y, batch)
                if i == 7:
                    self.assertAllEqual(
                        Y[0, 4:, :], np.zeros([3, Y.shape[2]], dtype=np.float32)
                    )
                if i == dataset.shape[0] - 4:
                    print("Freezing normalization stats")
                    mu_frozen = mu
                    min_frozen = min
                    max_frozen = max
                    N_frozen = N
                    freeze = True
                if i == dataset.shape[0] - 2:
                    self.assertAllEqual(mu, mu_frozen)
                    self.assertAllEqual(min, min_frozen)
                    self.assertAllEqual(max, max_frozen)
                    self.assertAllEqual(N, N_frozen)

    def test_batch_mean_normalize_2D(self):
        """Test the batch_normalize function for a 2D tensor."""
        dataset, data_shape, data_format = _NF_dataset()
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(data_shape, data_format=data_format)
        y, mean, std = dp.batch_normalize(x)
        # Run
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            Y, mu, sig = sess.run([y, mean, std], feed_dict={x: dataset})
        # Debug
        if DEBUG:
            print("dataset", dataset.shape, "\n", dataset)
            print("Y", Y.shape, "\n", Y)
            print("mu", mu.shape, "\n", mu)
            print("sig", sig.shape, "\n", sig)
        # Assertions
        self.assertAllEqual(Y.shape, dataset.shape)
        self.assertAllEqual(mu.shape[1:], dataset.shape[1:])
        self.assertAllEqual(sig.shape[1:], dataset.shape[1:])
        self.assertLess(np.abs(Y).max(), 3.5)
        self.assertArrayNear(Y.mean(axis=0), np.zeros_like(Y.mean(axis=0)), err=1e-6)
        self.assertAllEqual(np.argmax(Y, axis=0), np.argmax(dataset, axis=0))
        self.assertAllEqual(np.argmin(Y, axis=0), np.argmin(dataset, axis=0))

    def test_batch_rescale_2D(self):
        """Test the batch_rescale function for a 2D tensor."""
        dataset, data_shape, data_format = _NF_dataset()
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(data_shape, data_format=data_format)
        y, mean, std = dp.batch_normalize(x)
        y = dp.batch_rescale(y, mean, std)
        # Run
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            Y, mu, sig = sess.run([y, mean, std], feed_dict={x: dataset})
        # Debug
        if DEBUG:
            print("dataset", dataset.shape, "\n", dataset)
            print("Y", Y.shape, "\n", Y)
            print("mu", mu.shape, "\n", mu)
            print("sig", sig.shape, "\n", sig)
        # Assertions
        self.assertAllEqual(Y.shape, dataset.shape)
        self.assertAllEqual(mu.shape[1:], dataset.shape[1:])
        self.assertAllEqual(sig.shape[1:], dataset.shape[1:])
        self.assertAllClose(Y, dataset)
        self.assertAllEqual(np.argmax(Y, axis=0), np.argmax(dataset, axis=0))
        self.assertAllEqual(np.argmin(Y, axis=0), np.argmin(dataset, axis=0))

    def test_batch_mean_normalize_3D(self):
        """Test the batch_normalize function for a 3D tensor (sequence)."""
        dataset, data_shape, data_format = _NSF_dataset()
        reduction_axes = (0, 1)
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(
            data_shape, data_format=data_format, reduction_axes=reduction_axes
        )
        y, mean, std = dp.batch_normalize(x)
        # Run
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            Y, mu, sig = sess.run([y, mean, std], feed_dict={x: dataset})
        # Debug
        if DEBUG:
            print("dataset", dataset.shape, "\n", dataset)
            print("Y", Y.shape, "\n", Y)
            print("mu", mu.shape, "\n", mu)
            print("sig", sig.shape, "\n", sig)
        # Assertions
        self.assertAllEqual(Y.shape, dataset.shape)
        self.assertAllEqual(mu.shape[-1], dataset.shape[-1])
        self.assertAllEqual(sig.shape[-1], dataset.shape[-1])
        self.assertEqual(np.count_nonzero(Y), np.count_nonzero(dataset))
        self.assertGreaterEqual(mu[..., 2], 1000)
        self.assertLess(np.abs(Y).max(), 3.5)
        for f in range(Y.shape[-1]):
            xf = Y[:, :, f]
            df = dataset[:, :, f]
            self.assertAllEqual(
                np.argmax(xf[np.nonzero(xf)]), np.argmax(df[np.nonzero(df)])
            )
            self.assertAllEqual(
                np.argmin(xf[np.nonzero(xf)]), np.argmin(df[np.nonzero(df)])
            )
            self.assertNear(np.mean(xf[xf.nonzero()]), 0.0, err=1e-6)
        self.assertAllEqual(Y[7, 4:, :], np.zeros([3, Y.shape[2]], dtype=np.float32))

    def test_batch_rescale_3D(self):
        """Test the batch_rescale function for a 3D tensor (sequence)."""
        dataset, data_shape, data_format = _NSF_dataset()
        reduction_axes = (0, 1)
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(
            data_shape, data_format=data_format, reduction_axes=reduction_axes
        )
        y, mean, std = dp.batch_normalize(x)
        y = dp.batch_rescale(y, mean, std)
        # Run
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            Y, mu, sig = sess.run([y, mean, std], feed_dict={x: dataset})
        # Debug
        if DEBUG:
            print("dataset", dataset.shape, "\n", dataset)
            print("Y", Y.shape, "\n", Y)
            print("mu", mu.shape, "\n", mu)
            print("sig", sig.shape, "\n", sig)
        # Assertions
        self.assertAllEqual(Y.shape, dataset.shape)
        self.assertAllEqual(mu.shape[-1], dataset.shape[-1])
        self.assertAllEqual(sig.shape[-1], dataset.shape[-1])
        self.assertAllEqual(np.argmax(Y, axis=0), np.argmax(dataset, axis=0))
        self.assertAllEqual(np.argmin(Y, axis=0), np.argmin(dataset, axis=0))
        self.assertAllClose(Y, dataset)

    def test_batch_mean_normalize_2D_minmax(self):
        """Test the batch_normalize function for a 2D tensor with minmax mode."""
        dataset, data_shape, data_format = _NF_dataset()
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(data_shape, data_format=data_format, normalize_mode="minmax")
        y, mean, std = dp.batch_normalize(dataset)
        # Run
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            Y, mu, sig = sess.run([y, mean, std], feed_dict={x: dataset})
        # Debug
        if DEBUG:
            print("dataset", dataset.shape, "\n", dataset)
            print("Y", Y.shape, "\n", Y)
            print("mu", mu.shape, "\n", mu)
            print("sig", sig.shape, "\n", sig)
        # Assertions
        self.assertAllEqual(Y.shape, dataset.shape)
        self.assertAllEqual(mu.shape[1:], dataset.shape[1:])
        self.assertAllEqual(sig.shape[1:], dataset.shape[1:])
        self.assertLess(np.abs(Y).max(), 1.5)
        self.assertArrayNear(Y.mean(axis=0), np.zeros_like(Y.mean(axis=0)), err=1e-6)
        self.assertAllEqual(np.argmax(Y, axis=0), np.argmax(dataset, axis=0))
        self.assertAllEqual(np.argmin(Y, axis=0), np.argmin(dataset, axis=0))

    def test_batch_rescale_2D_minmax(self):
        """Test the batch_rescale function for a 2D tensor with minmax mode."""
        dataset, data_shape, data_format = _NF_dataset()
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(data_shape, data_format=data_format, normalize_mode="minmax")
        y, mean, std = dp.batch_normalize(dataset)
        y = dp.batch_rescale(y, mean, std)
        # Run
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            Y, mu, sig = sess.run([y, mean, std], feed_dict={x: dataset})
        # Debug
        if DEBUG:
            print("dataset", dataset.shape, "\n", dataset)
            print("Y", Y.shape, "\n", Y)
            print("mu", mu.shape, "\n", mu)
            print("sig", sig.shape, "\n", sig)
        # Assertions
        self.assertAllEqual(Y.shape, dataset.shape)
        self.assertAllEqual(mu.shape[1:], dataset.shape[1:])
        self.assertAllEqual(sig.shape[1:], dataset.shape[1:])
        self.assertAllClose(Y, dataset)
        self.assertAllEqual(np.argmax(Y, axis=0), np.argmax(dataset, axis=0))
        self.assertAllEqual(np.argmin(Y, axis=0), np.argmin(dataset, axis=0))

    def test_batch_mean_normalize_3D_minmax(self):
        """Test the batch_normalize function for a 3D tensor (sequence) with minmax mode."""
        dataset, data_shape, data_format = _NSF_dataset()
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        reduction_axes = (0, 1)
        dp = DataProcessor(
            data_shape,
            data_format=data_format,
            reduction_axes=reduction_axes,
            normalize_mode="minmax",
        )
        y, mean, std = dp.batch_normalize(dataset)
        # Run
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            Y, mu, sig = sess.run([y, mean, std], feed_dict={x: dataset})
        # Debug
        if DEBUG:
            print("dataset", dataset.shape, "\n", dataset)
            print("Y", Y.shape, "\n", Y)
            print("mu", mu.shape, "\n", mu)
            print("sig", sig.shape, "\n", sig)
        # Assertions
        self.assertAllEqual(Y.shape, dataset.shape)
        self.assertAllEqual(mu.shape[-1], dataset.shape[-1])
        self.assertAllEqual(sig.shape[-1], dataset.shape[-1])
        self.assertEqual(np.count_nonzero(Y), np.count_nonzero(dataset))
        self.assertGreaterEqual(mu[..., 2], 1000)
        self.assertLess(np.abs(Y).max(), 1.5)
        for f in range(Y.shape[-1]):
            xf = Y[:, :, f]
            df = dataset[:, :, f]
            self.assertAllEqual(
                np.argmax(xf[np.nonzero(xf)]), np.argmax(df[np.nonzero(df)])
            )
            self.assertAllEqual(
                np.argmin(xf[np.nonzero(xf)]), np.argmin(df[np.nonzero(df)])
            )
            self.assertNear(np.mean(xf[xf.nonzero()]), 0.0, err=1e-6)
        self.assertAllEqual(Y[7, 4:, :], np.zeros([3, Y.shape[2]], dtype=np.float32))

    def test_batch_rescale_3D_minmax(self):
        """Test the batch_rescale function for a 3D tensor (sequence) with minmax mode."""
        dataset, data_shape, data_format = _NSF_dataset()
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        reduction_axes = (0, 1)
        dp = DataProcessor(
            data_shape,
            data_format=data_format,
            reduction_axes=reduction_axes,
            normalize_mode="minmax",
        )
        y, mean, std = dp.batch_normalize(dataset)
        y = dp.batch_rescale(y, mean, std)
        # Run
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            Y, mu, sig = sess.run([y, mean, std], feed_dict={x: dataset})
        # Debug
        if DEBUG:
            print("dataset", dataset.shape, "\n", dataset)
            print("Y", Y.shape, "\n", Y)
            print("mu", mu.shape, "\n", mu)
            print("sig", sig.shape, "\n", sig)
        # Assertions
        self.assertAllEqual(Y.shape, dataset.shape)
        self.assertAllEqual(mu.shape[-1], dataset.shape[-1])
        self.assertAllEqual(sig.shape[-1], dataset.shape[-1])
        self.assertAllEqual(np.argmax(Y, axis=0), np.argmax(dataset, axis=0))
        self.assertAllEqual(np.argmin(Y, axis=0), np.argmin(dataset, axis=0))
        self.assertAllClose(Y, dataset)

    def test_split_features(self):
        """Test the split_features function for a 2D tensor."""
        dataset, data_shape, data_format = _NF_dataset()
        xy = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(data_shape, data_format=data_format)
        x_idx = list(range(0, data_shape[1] - 2))
        y_idx = list(range(data_shape[1] - 2, data_shape[1]))
        x, y = dp.split_features(xy, x_idx, y_idx)
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            X, Y = sess.run([x, y], feed_dict={xy: dataset})
            # Debug
            if DEBUG:
                print("dataset", dataset.shape, "\n", dataset)
                print("X", X.shape, "\n", X)
                print("Y", Y.shape, "\n", Y)
            # Assertions
            self.assertAllEqual(X.shape[:1], data_shape[:1])
            self.assertAllEqual(Y.shape[:1], data_shape[:1])
            self.assertEqual(X.shape[1] + Y.shape[1], data_shape[1])
            self.assertAllEqual(X, dataset[..., x_idx].astype(np.float32))
            self.assertAllEqual(Y, dataset[..., y_idx].astype(np.float32))

    def test_split_batch(self):
        """Test the split_batch function for a 2D tensor."""
        dataset, data_shape, data_format = _NF_dataset()
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(data_shape, data_format=data_format)
        y_train, y_valid, y_test = dp.split_batch(x, ratios=(0.78, 0.22, 0.0))
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            Y_train, Y_valid, Y_test = sess.run(
                [y_train, y_valid, y_test], feed_dict={x: dataset}
            )
            # Debug
            if DEBUG:
                print("dataset", dataset.shape, "\n", dataset)
                print("Y_train", Y_train.shape, "\n", Y_train)
                print("Y_valid", Y_valid.shape, "\n", Y_valid)
                print("Y_test", Y_test.shape, "\n", Y_test)
            # Assertions
            self.assertAllEqual(Y_train.shape[1:], data_shape[1:])
            self.assertAllEqual(Y_valid.shape[1:], data_shape[1:])
            self.assertAllEqual(Y_test.shape[1:], data_shape[1:])

    def test_shuffle_batch(self):
        """Test the shuffle_batch function for a 2D tensor."""
        dataset, data_shape, data_format = _NF_dataset()
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(data_shape, data_format=data_format, shuffle_seed=13)
        y = dp.shuffle_batch(x)
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            Y = sess.run(y, feed_dict={x: dataset})
            # Debug
            if DEBUG:
                print("dataset", dataset.shape, "\n", dataset)
                print("Y", Y.shape, "\n", Y)
            # Assertions
            self.assertAllEqual(Y.shape, data_shape)
            for yy, xx in zip(
                Y.flatten().tolist(), dataset.astype(np.float32).flatten().tolist()
            ):
                self.assertNotEqual(yy, xx)
                self.assertIn(yy, dataset.astype(np.float32))

    def test_batch_pca_transform_2D(self):
        """Test the batch_pca_transform function for a 2D tensor."""
        dataset, data_shape, data_format = _NF_dataset()
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        dp = DataProcessor(data_shape, data_format=data_format)
        num_components = 4
        y, s, u, v, cov = dp.batch_pca_transform(x, num_components=num_components)
        # Run
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            Y, S, U, V, COV = sess.run([y, s, u, v, cov], feed_dict={x: dataset})
        # Debug
        if DEBUG:
            print("dataset", dataset.shape, "\n", dataset)
            print("Y", Y.shape, "\n", Y)
            print("Y", S.shape, "\n", S)
            print("Y", U.shape, "\n", U)
            print("Y", V.shape, "\n", V)
            print("Y", COV.shape, "\n", COV)
        # Assertions
        self.assertAllEqual(Y.shape[:-1], dataset.shape[:-1])
        self.assertEqual(Y.shape[-1], num_components)
        self.assertArrayNear(Y.mean(axis=0), np.zeros_like(Y.mean(axis=0)), err=1e-6)
        self.assertLess(Y.std(axis=0).max(), 1.01)

    def test_batch_pca_transform_3D(self):
        """Test the batch_pca_transform function for a 2D tensor."""
        dataset, data_shape, data_format = _NSF_dataset()
        x = tf.placeholder(tf.float32, [None] + list(data_shape[1:]))
        num_components = 4
        reduction_axes = (0, 1)
        dp = DataProcessor(
            data_shape, data_format=data_format, reduction_axes=reduction_axes
        )
        y, s, u, v, cov = dp.batch_pca_transform(x, num_components=num_components)
        # Run
        with self.test_session() as sess:
            sess.run(
                [tf.local_variables_initializer(), tf.global_variables_initializer()]
            )
            Y, S, U, V, COV = sess.run([y, s, u, v, cov], feed_dict={x: dataset})
        # Debug
        if DEBUG:
            print("dataset", dataset.shape, "\n", dataset)
            print("Y", Y.shape, "\n", Y)
            print("Y", S.shape, "\n", S)
            print("Y", U.shape, "\n", U)
            print("Y", V.shape, "\n", V)
            print("Y", COV.shape, "\n", COV)
        # Assertions
        self.assertAllEqual(Y.shape[:-1], dataset.shape[:-1])
        self.assertEqual(Y.shape[-1], num_components)
        self.assertArrayNear(
            Y.mean(axis=(0, 1)), np.zeros_like(Y.mean(axis=(0, 1))), err=1e-6
        )
        self.assertLess(Y.std(axis=(0, 1)).max(), 2.0)


if __name__ == "__main__":
    test.main()
