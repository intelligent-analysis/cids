# Copyright 2022 Arnd Koeppe
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""CIDS computational intelligence library"""
import shutil

import numpy as np
from tensorflow.python.platform import test

from cids.legacy.data.datareader import DataReader
from cids.legacy.nn.convolutional import *
from cids.legacy.nn.recurrent import *


os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
DEBUG = True


class Benchmark(test.TestCase):
    """Tester for the neural_network module."""

    def test_linear_func_overfit(self, dim=2):
        """Tester for linear function approximation."""
        xx = np.linspace(0.0, 1.0, 20)
        dataset = np.stack([xx, -xx, 3.0 * xx, -2.0 * xx]).transpose([1, 0])
        if dim == 2:
            data_format = "NF"
            slice_tensors = True
            NN = NeuralNetwork
            layers = [
                ("fc_relu", 20),
                ("fc_relu", 60),
                ("fc_relu", 150),
                ("fc_relu", 60),
                ("fc_relu", 20),
                ("fc_out", -1),
            ]
            batch_size = 4
            buffer_size = 10000 * batch_size
        elif dim == 3:
            data_format = "NSF"
            slice_tensors = True
            dataset = np.repeat(np.expand_dims(dataset, 0), 5, axis=0)
            NN = RecNet
            cell_name = "cudnn_lstm"
            layers = [
                ("fc_relu", 20),
                (cell_name, 60),
                (cell_name, 150),
                (cell_name, 60),
                (cell_name, 20),
                ("fc_out", -1),
            ]
            batch_size = 4
            buffer_size = 10000 * batch_size
        else:
            raise ValueError("Invalid value for dim: 2 or 3 expected.")
        data_shape = dataset.shape
        print(data_shape)
        input_idx = [0, 1]
        output_idx = [2, 3]
        learning_rate = 3e-4
        grad_clip = None
        num_steps = 1000000
        report_steps = 100
        freeze_steps = 10 * report_steps
        freeze = False
        # Graph
        data_reader = DataReader(
            data_shape,
            data_format,
            batch_size,
            src_type="placeholder",
            slice_tensors=slice_tensors,
            sample_dtype=tf.float64,
            cast_dtype=tf.float32,
            buffer_size=buffer_size,
        )
        nn = NN(
            data_shape,
            input_idx,
            output_idx,
            layers,
            dr=data_reader,
            batch_size=batch_size,
            init_learning_rate=learning_rate,
            grad_clip=grad_clip,
            scope="Benchmark",
        )
        nn.build()
        config = tf.ConfigProto(allow_soft_placement=True)
        with self.test_session(use_gpu=True, config=config) as sess:
            nn.sess_initialize(sess, mode="train", feed_dict={nn.train_data: dataset})
            # Initial loss
            e0 = nn.sess_loss(sess)
            losses = []
            # Training
            try:
                for i in range(0, num_steps, 1):
                    e, _ = nn.sess_train(
                        sess, summarize=(i % report_steps == 0 or i == num_steps - 1)
                    )
                    if i == freeze_steps:
                        freeze = True
                        nn.sess_update_data_pipeline(
                            sess,
                            mode="train",
                            feed_dict={nn.idp.freeze: freeze, nn.odp.freeze: freeze},
                        )
            except KeyboardInterrupt:
                print("Training interupted by user.")
            # Do a run for a full trace
            i = num_steps
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()
            e, _ = nn.sess_train(
                sess,
                summarize=(i % report_steps == 0 or i == num_steps - 1),
                options=run_options,
                run_metadata=run_metadata,
            )
            # Check quality of results
            X, Y, Y_ = nn.sess_forward_pass(sess)
            # Outputs
            if DEBUG:
                print(
                    "X",
                    X.shape,
                )
                print(
                    "Y",
                    Y.shape,
                )
                print(
                    "Y_",
                    Y_.shape,
                )
            # Assertions
            self.assertLess(e, e0)
        # Clean up
        shutil.rmtree(nn.result_dir, ignore_errors=True)


if __name__ == "__main__":
    test.main()
