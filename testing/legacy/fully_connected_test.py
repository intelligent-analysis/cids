# Copyright 2022 Arnd Koeppe
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""CIDS computational intelligence library"""
import shutil

import numpy as np
import tensorflow as tf
from tensorflow.python.platform import test

from cids.legacy.data.datareader import DataReader
from cids.legacy.nn.fully_connected import NeuralNetwork
from cids.legacy.nn.fully_connected import SingleGPUNeuralNetwork
from testing.src.create_test_dataset import _NF_dataset


DEBUG = True


class NeuralNetworkTest(test.TestCase):
    """Tester for the neural_network module."""

    def test_SingleGPUNeuralNetwork(self):
        """Tester for SingleGPUNeuralNetwork."""
        train_paths = [
            "tests/src/tfrecord/2D_dataset.tfrecord",
            "tests/src/tfrecord/2D_dataset.tfrecord",
        ]
        valid_paths = ["tests/src/tfrecord/2D_dataset.tfrecord"]
        dataset, data_shape, data_format = _NF_dataset()
        if DEBUG:
            print("dataset", dataset.shape, "\n", dataset)
        input_idx = [0, 1]
        output_idx = [i for i in range(data_shape[-1]) if i not in input_idx]
        batch_size = 100
        layers = [("fc_relu", 5), ("fc_relu", 10), ("fc_out", -1)]
        learning_rate = 0.01
        num_steps = 100
        report_steps = 10
        # Graph
        nn = SingleGPUNeuralNetwork(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=learning_rate,
        )
        nn.name = "Test" + nn.name
        config = tf.ConfigProto(allow_soft_placement=True)
        # intra_op_parallelism_threads=10,
        # inter_op_parallelism_threads=10,
        # device_count = {"CPU": 10, "GPU": 4}
        with self.test_session(use_gpu=True, config=config) as sess:
            nn.sess_initialize(
                sess,
                mode="train",
                feed_dict={nn.train_data: train_paths, nn.valid_data: valid_paths},
            )
            # Initial loss
            l0 = nn.sess_loss(sess)
            # Training
            for i in range(0, num_steps, len(nn.comp_device)):
                l, lv = nn.sess_train(
                    sess,
                    summarize=(
                        i % report_steps == 0 or i == num_steps - len(nn.comp_device)
                    ),
                )
            # Do a run for a full trace
            i = num_steps
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()
            l, lv = nn.sess_train(sess, options=run_options, run_metadata=run_metadata)
            # Check quality of results
            X, Y, Y_ = nn.sess_forward_pass(sess)
            nn.sess_update_data_pipeline(sess, mode="valid")
            lv = nn.sess_loss(sess)
            nn.sess_update_data_pipeline(
                sess, mode="test", feed_dict={nn.feed: np.ones_like(dataset[0:1, :])}
            )
            Xb, Yb, Yb_ = nn.sess_infer(sess)
            # Save and load
            all_variables = list(tf.global_variables()) + list(tf.local_variables())
            old_weights = sess.run(all_variables, feed_dict=nn.feed_dict)
            nn.sess_save(sess, global_step=i)
            nn.sess_initialize(
                sess,
                mode="train",
                feed_dict={
                    nn.train_data: train_paths,
                    nn.valid_data: valid_paths,
                    nn.idp.freeze: True,
                    nn.odp.freeze: True,
                    nn.BYPASS: False,
                    nn.TRAIN: False,
                },
                checkpoint="last",
            )
            # nn.sess_load(sess)
            new_weights = sess.run(all_variables, feed_dict=nn.feed_dict)
            for o, n in zip(old_weights, new_weights):
                self.assertAllEqual(o, n)
            # Outputs
            if DEBUG:
                print("X", X.shape)  # , "\n", X)
                print("Y", Y.shape)  # , "\n", Y)
                print("Y_", Y_.shape)  # , "\n", Y_)
                print("Xb", Xb.shape)  # , "\n", Xb)
                print("Yb", Yb.shape)  # , "\n", Yb)
                print("Yb_", Yb_.shape)  # , "\n", Yb_)
                print("validation_loss", lv)
            # Assertions
            self.assertAllEqual(Y.shape, Y_.shape)
            self.assertAllEqual(Y.shape[1:], Yb_.shape[1:])
            self.assertEqual(Xb.shape[0], 1)
            self.assertEqual(Yb.shape[0], 1)
            self.assertEqual(Yb_.shape[0], 1)
            self.assertLess(l, l0)
        # Clean up
        shutil.rmtree(nn.result_dir, ignore_errors=True)

    def test_NeuralNetwork(self):
        """Tester for NeuralNetwork."""
        train_paths = [
            "tests/src/tfrecord/2D_dataset.tfrecord",
            "tests/src/tfrecord/2D_dataset.tfrecord",
        ]
        valid_paths = ["tests/src/tfrecord/2D_dataset.tfrecord"]
        dataset, data_shape, data_format = _NF_dataset()
        if DEBUG:
            print("dataset", dataset.shape, "\n", dataset)
        input_idx = [0, 1]
        output_idx = [i for i in range(data_shape[-1]) if i not in input_idx]
        batch_size = 100
        layers = [("fc_relu", 5), ("fc_relu", 10), ("fc_out", -1)]
        learning_rate = 0.01
        num_steps = 100
        report_steps = 10
        # Graph
        nn = NeuralNetwork(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=learning_rate,
        )
        nn.build()
        nn.name = "Test" + nn.name
        config = tf.ConfigProto(  # intra_op_parallelism_threads=10,
            # inter_op_parallelism_threads=10,
            allow_soft_placement=True
        )
        # device_count = {"CPU": 10, "GPU": 4}
        with self.test_session(use_gpu=True, config=config) as sess:
            nn.sess_initialize(
                sess,
                mode="train",
                feed_dict={nn.train_data: train_paths, nn.valid_data: valid_paths},
            )
            # Initial loss
            l0 = nn.sess_loss(sess)
            # Training
            for i in range(0, num_steps, len(nn.comp_devices)):
                l, lv = nn.sess_train(
                    sess,
                    summarize=(
                        i % report_steps == 0 or i == num_steps - len(nn.comp_devices)
                    ),
                )
            # Do a run for a full trace
            i = num_steps
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()
            l, lv = nn.sess_train(sess, options=run_options, run_metadata=run_metadata)
            # Check quality of results
            X, Y, Y_ = nn.sess_forward_pass(sess)
            nn.sess_update_data_pipeline(sess, mode="valid")
            lv = nn.sess_loss(sess)
            nn.sess_update_data_pipeline(
                sess, mode="test", feed_dict={nn.feed: np.ones_like(dataset[0:1, :])}
            )
            Xb, Yb, Yb_ = nn.sess_infer(sess)
            # Save and load
            all_variables = list(tf.global_variables()) + list(tf.local_variables())
            old_weights = sess.run(all_variables, feed_dict=nn.feed_dict)
            nn.sess_save(sess, global_step=i)
            nn.sess_initialize(
                sess,
                mode="train",
                feed_dict={
                    nn.train_data: train_paths,
                    nn.valid_data: valid_paths,
                    nn.idp.freeze: True,
                    nn.odp.freeze: True,
                    nn.BYPASS: False,
                    nn.TRAIN: False,
                },
                checkpoint="last",
            )
            # nn.sess_load(sess)
            new_weights = sess.run(all_variables, feed_dict=nn.feed_dict)
            for o, n in zip(old_weights, new_weights):
                self.assertAllEqual(o, n)
            # Outputs
            if DEBUG:
                print("X", X.shape)  # , "\n", X)
                print("Y", Y.shape)  # , "\n", Y)
                print("Y_", Y_.shape)  # , "\n", Y_)
                print("Xb", Xb.shape)  # , "\n", Xb)
                print("Yb", Yb.shape)  # , "\n", Yb)
                print("Yb_", Yb_.shape)  # , "\n", Yb_)
                print("validation_loss", lv)
            # Assertions
            self.assertAllEqual(Y.shape, Y_.shape)
            self.assertAllEqual(Y.shape[1:], Yb_.shape[1:])
            self.assertEqual(Xb.shape[0], 1)
            self.assertEqual(Yb.shape[0], 1)
            self.assertEqual(Yb_.shape[0], 1)
            self.assertLess(l, l0)
        # Clean up
        shutil.rmtree(nn.result_dir, ignore_errors=True)

    def test_Leakage(self):
        """Tester for against leakage of validation set into training."""
        train_paths = [
            "tests/src/tfrecord/2D_dataset.tfrecord",
            "tests/src/tfrecord/2D_dataset.tfrecord",
        ]
        valid_paths = ["tests/src/tfrecord/2D_dataset.tfrecord"]
        dataset, data_shape, data_format = _NF_dataset()
        if DEBUG:
            print("dataset", dataset.shape, "\n", dataset)
        input_idx = [0, 1]
        output_idx = [i for i in range(data_shape[-1]) if i not in input_idx]
        layers = [("fc_relu", 5), ("fc_relu", 10), ("fc_out", -1)]
        num_steps = 30
        report_steps = 10
        # Graph
        nn = NeuralNetwork(data_shape, input_idx, output_idx, layers)
        nn.build()
        nn.name = "Test" + nn.name
        with self.test_session(use_gpu=True) as sess:
            nn.sess_initialize(
                sess,
                feed_dict={
                    nn.train_data: train_paths,
                    nn.valid_data: valid_paths,
                    nn.TRAIN: True,
                    nn.BYPASS: False,
                },
            )
            # Initial loss
            nn.sess_loss(sess)
            # Training
            for i in range(0, num_steps, len(nn.comp_devices)):
                nn.sess_train(
                    sess,
                    summarize=(
                        i % report_steps == 0 or i == num_steps - len(nn.comp_devices)
                    ),
                )

            # Check quality of results
            nn.sess_update_data_pipeline(sess, "valid")
            _, _, N_stage = sess.run(
                [nn.stage, nn.loss, nn.towers[0].gpu_gradients_staging_area.size()],
                feed_dict=nn.feed_dict,
            )
            for i in range(0, num_steps):
                N_stage_old = N_stage
                _, N_stage = sess.run(
                    [nn.loss, nn.towers[0].gpu_gradients_staging_area.size()],
                    feed_dict=nn.feed_dict,
                )
                # Outputs
                if DEBUG:
                    print("N_stage", N_stage)
                # Assertions
                self.assertEqual(N_stage, N_stage_old)
        # Clean up
        shutil.rmtree(nn.result_dir, ignore_errors=True)

    def test_flags(self):
        data_shape = [None, 2]
        train_src = 1.0 * np.ones([10, data_shape[-1]])
        valid_src = 3.0 * np.ones([5, data_shape[-1]])
        input_idx = [0]
        output_idx = [1]
        layers = [("fc_relu", 5), ("fc_relu", 10), ("fc_out", -1)]
        # Graph
        dr = DataReader(data_shape, "NF", 5, src_type="placeholder", slice_tensors=True)
        nn = NeuralNetwork(data_shape, input_idx, output_idx, layers, dr=dr)
        nn.build()
        nn.name = "Test" + nn.name
        with self.test_session(use_gpu=True) as sess:
            nn.sess_initialize(
                sess, feed_dict={nn.train_data: train_src, nn.valid_data: valid_src}
            )
            for i1 in [True, False]:
                for i2 in [True, False]:
                    o1, o2 = sess.run(
                        [nn.TRAIN, nn.BYPASS], feed_dict={nn.TRAIN: i1, nn.BYPASS: i2}
                    )
                    print(i1, " == ", o1)
                    print(i2, " == ", o2)
                    assert o1 == i1
                    assert o2 == i2
        # Clean up
        shutil.rmtree(nn.result_dir, ignore_errors=True)

    def test_data_handling(self):
        """Tester for against leakage of validation set into training."""
        data_shape = [None, 2]
        batch_size = 5
        train_src = 1.0 * np.ones([batch_size, data_shape[-1]])
        train_src2 = 2.0 * np.ones([batch_size, data_shape[-1]])
        valid_src = 3.0 * np.ones([batch_size, data_shape[-1]])
        valid_src2 = 4.0 * np.ones([batch_size, data_shape[-1]])
        input_idx = [0]
        output_idx = [1]
        layers = [("fc_relu", 5), ("fc_relu", 10), ("fc_out", -1)]
        num_steps = 30
        # Graph
        dr = DataReader(
            data_shape, "NF", batch_size, src_type="placeholder", slice_tensors=True
        )
        nn = NeuralNetwork(data_shape, input_idx, output_idx, layers, dr=dr)
        nn.build()
        nn.name = "Test" + nn.name
        with self.test_session(use_gpu=True) as sess:
            nn.sess_initialize(
                sess,
                feed_dict={
                    nn.train_data: train_src,
                    nn.valid_data: valid_src,
                    nn.TRAIN: True,
                    nn.BYPASS: True,
                },
            )
            # Test input train_src
            nn.sess_update_data_pipeline(
                sess, "train", feed_dict={nn.train_data: train_src}
            )
            x, y, y_ = nn.sess_infer(sess)
            np.testing.assert_array_equal(x, train_src[:, input_idx])
            np.testing.assert_array_equal(y, train_src[:, output_idx])
            # Test input valid_src
            nn.sess_update_data_pipeline(
                sess, "valid", feed_dict={nn.valid_data: valid_src}
            )
            x, y, y_ = nn.sess_infer(sess)
            np.testing.assert_array_equal(x, valid_src[:, input_idx])
            np.testing.assert_array_equal(y, valid_src[:, output_idx])
            # Test input train_src2
            nn.sess_update_data_pipeline(
                sess, "train", feed_dict={nn.train_data: train_src2}
            )
            x, y, y_ = nn.sess_infer(sess)
            np.testing.assert_array_equal(x, train_src2[:, input_idx])
            np.testing.assert_array_equal(y, train_src2[:, output_idx])
            # Test input valid_src2
            nn.sess_update_data_pipeline(
                sess, "valid", feed_dict={nn.valid_data: valid_src2}
            )
            x, y, y_ = nn.sess_infer(sess)
            np.testing.assert_array_equal(x, valid_src2[:, input_idx])
            np.testing.assert_array_equal(y, valid_src2[:, output_idx])
            # Training with train_src
            nn.sess_update_data_pipeline(
                sess,
                "train",
                feed_dict={nn.train_data: train_src, nn.valid_data: valid_src},
            )
            for i in range(0, num_steps, len(nn.comp_devices)):
                nn.sess_train(sess, summarize=True)
            # Training with train_src2
            nn.sess_update_data_pipeline(
                sess,
                "train",
                feed_dict={nn.train_data: train_src2, nn.valid_data: valid_src2},
            )
            for i in range(0, num_steps, len(nn.comp_devices)):
                nn.sess_train(sess, summarize=True)
        # Clean up
        shutil.rmtree(nn.result_dir, ignore_errors=True)

    def test_data_processor_freezing(self):
        """Tester for against leakage of validation set into training."""
        data_shape = [None, 2]
        batch_size = 5
        train_src = 1.0 * np.ones([batch_size, data_shape[-1]])
        valid_src = 2.0 * np.ones([batch_size, data_shape[-1]])
        test_src = 3.0 * np.ones([batch_size, data_shape[-1]])
        input_idx = [0]
        output_idx = [1]
        layers = [("fc_relu", 5), ("fc_relu", 10), ("fc_out", -1)]
        num_steps = 30
        # Graph
        dr = DataReader(
            data_shape, "NF", batch_size, src_type="placeholder", slice_tensors=True
        )
        nn = NeuralNetwork(data_shape, input_idx, output_idx, layers, dr=dr)
        nn.build()
        nn.name = "Test" + nn.name
        with self.test_session(use_gpu=True) as sess:
            nn.sess_initialize(
                sess,
                mode="train",
                feed_dict={nn.train_data: train_src, nn.valid_data: valid_src},
            )
            # Training with train_src, no summaries
            for i in range(0, num_steps, len(nn.comp_devices)):
                nn.sess_train(sess, summarize=False)
                if nn.idp.freeze in nn.feed_dict.keys():
                    assert not nn.feed_dict[nn.idp.freeze]
                    assert not nn.feed_dict[nn.odp.freeze]
            # Train with summaries: should freeze during valid and unfreeze after
            nn.sess_train(sess, summarize=False)
            if nn.idp.freeze in nn.feed_dict.keys():
                assert not nn.feed_dict[nn.idp.freeze]
                assert not nn.feed_dict[nn.odp.freeze]
            # Training with train_src, no summaries
            for i in range(0, num_steps, len(nn.comp_devices)):
                nn.sess_train(sess, summarize=False)
                if nn.idp.freeze in nn.feed_dict.keys():
                    assert not nn.feed_dict[nn.idp.freeze]
                    assert not nn.feed_dict[nn.odp.freeze]
            # Test inference with test data: should be frozen
            nn.sess_update_data_pipeline(
                sess, "test", feed_dict={nn.test_data: test_src}
            )
            x, y, y_ = nn.sess_infer(sess)
            assert nn.idp.freeze in nn.feed_dict.keys()
            assert nn.odp.freeze in nn.feed_dict.keys()
            assert nn.feed_dict[nn.idp.freeze]
            assert nn.feed_dict[nn.odp.freeze]
            # Reset data pipeline to training: should be unfrozen
            nn.sess_update_data_pipeline(sess, "train")
            if nn.idp.freeze in nn.feed_dict.keys():
                assert not nn.feed_dict[nn.idp.freeze]
                assert not nn.feed_dict[nn.odp.freeze]
            # Set data pipeline to training and manually freeze
            nn.sess_update_data_pipeline(
                sess, "train", feed_dict={nn.idp.freeze: True, nn.odp.freeze: True}
            )
            assert nn.idp.freeze in nn.feed_dict.keys()
            assert nn.odp.freeze in nn.feed_dict.keys()
            assert nn.feed_dict[nn.idp.freeze]
            assert nn.feed_dict[nn.odp.freeze]
            # Train with frozen data pipeline
            for i in range(0, num_steps, len(nn.comp_devices)):
                nn.sess_train(sess, summarize=False)
                assert nn.feed_dict[nn.idp.freeze]
                assert nn.feed_dict[nn.odp.freeze]
        # Clean up
        shutil.rmtree(nn.result_dir, ignore_errors=True)


if __name__ == "__main__":
    test.main()
