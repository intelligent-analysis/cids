# Copyright 2022 Arnd Koeppe
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""CIDS computational intelligence library"""
import shutil
import unittest

import numpy as np
from tensorflow.python.platform import test

from cids.legacy.nn.recurrent import *
from testing.src.create_test_dataset import _NSF_dataset


DEBUG = True


class RecurrentTest(test.TestCase):
    """Tester for the recurrent module."""

    def test_SingleGPULSTM(self):
        """Tester for SingleGPULSTM."""
        train_paths = tf.gfile.Glob("tests/src/tfrecord/3D_dataset_sample*.tfr*")
        train_paths = sorted(train_paths)
        valid_paths = ["tests/src/tfrecord/3D_dataset_sample_00.tfrecord"]
        dataset, data_shape, data_format = _NSF_dataset()
        if DEBUG:
            print("dataset", dataset.shape)  # , "\n", dataset)
        input_idx = [0, 1]
        output_idx = [i for i in range(data_shape[-1]) if i not in input_idx]
        batch_size = 100
        layers = [("fc_relu", 5), ("lstm", 10), ("fc_out", -1)]
        learning_rate = 0.01
        num_steps = 100
        report_steps = 10
        # Graph
        nn = SingleGPURecNet(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=learning_rate,
        )
        nn.build()
        config = tf.ConfigProto(allow_soft_placement=True)
        with self.test_session(use_gpu=True, config=config) as sess:
            nn.sess_initialize(
                sess, feed_dict={nn.train_data: train_paths, nn.valid_data: valid_paths}
            )
            # Initial loss
            l0 = nn.sess_loss(sess)
            # Training
            for i in range(0, num_steps, 1):
                l, lv = nn.sess_train(
                    sess, summarize=(i % report_steps == 0 or i == num_steps - 1)
                )
            # Do a run for a full trace
            i = num_steps
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()
            l, lv = nn.sess_train(sess, options=run_options, run_metadata=run_metadata)
            # Check quality of results
            X, Y, Y_ = nn.sess_forward_pass(sess)
            nn.sess_update_data_pipeline(sess, mode="valid")
            lv = nn.sess_loss(sess)
            nn.sess_update_data_pipeline(
                sess, mode="test", feed_dict={nn.feed: np.ones_like(dataset[0:1, :])}
            )
            Xb, Yb, Yb_ = nn.sess_infer(sess)
            # Save and load
            nn.sess_save(sess, global_step=i)
            nn.sess_load(sess)
            # Outputs
            if DEBUG:
                print(
                    "X",
                    X.shape,
                )  # "\n", X)
                print(
                    "Y",
                    Y.shape,
                )  # "\n", Y)
                print(
                    "Y_",
                    Y_.shape,
                )  # "\n", Y_)
                print(
                    "Xb",
                    Xb.shape,
                )  # "\n", Xb)
                print(
                    "Yb",
                    Yb.shape,
                )  # "\n", Yb)
                print(
                    "Yb_",
                    Yb_.shape,
                )  # "\n", Yb_)
                print("validation_error", lv)
            # Assertions
            self.assertAllEqual(Y.shape, Y_.shape)
            self.assertAllEqual(Y.shape[1:], Yb_.shape[1:])
            self.assertEqual(Xb.shape[0], 1)
            self.assertEqual(Yb.shape[0], 1)
            self.assertEqual(Yb_.shape[0], 1)
            self.assertLess(l, l0)
        # Clean up
        shutil.rmtree(nn.result_dir, ignore_errors=True)

    def test_LSTM(self):
        """Tester for LSTM."""
        train_paths = tf.gfile.Glob("tests/src/tfrecord/3D_dataset_sample*.tfr*")
        train_paths = sorted(train_paths)
        valid_paths = ["tests/src/tfrecord/3D_dataset_sample_00.tfrecord"]
        dataset, data_shape, data_format = _NSF_dataset()
        if DEBUG:
            print("dataset", dataset.shape)  # , "\n", dataset)
        input_idx = [0, 1]
        output_idx = [i for i in range(data_shape[-1]) if i not in input_idx]
        batch_size = 100
        layers = [("fc_relu", 5), ("lstm", 10), ("fc_out", -1)]
        learning_rate = 0.01
        num_steps = 100
        report_steps = 10
        # Graph
        nn = RecNet(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=learning_rate,
        )
        nn.build()
        config = tf.ConfigProto(allow_soft_placement=True)
        with self.test_session(use_gpu=True, config=config) as sess:
            nn.sess_initialize(
                sess, feed_dict={nn.train_data: train_paths, nn.valid_data: valid_paths}
            )
            # Initial loss
            l0 = nn.sess_loss(sess)
            # Training
            for i in range(0, num_steps, 1):
                l, lv = nn.sess_train(
                    sess, summarize=(i % report_steps == 0 or i == num_steps - 1)
                )
            # Do a run for a full trace
            i = num_steps
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()
            l, lv = nn.sess_train(sess, options=run_options, run_metadata=run_metadata)
            # Check quality of results
            X, Y, Y_ = nn.sess_forward_pass(sess)
            nn.sess_update_data_pipeline(sess, mode="valid")
            lv = nn.sess_loss(sess)
            nn.sess_update_data_pipeline(
                sess, mode="test", feed_dict={nn.feed: np.ones_like(dataset[0:1, :])}
            )
            Xb, Yb, Yb_ = nn.sess_infer(sess)
            # Save and load
            nn.sess_save(sess, global_step=i)
            nn.sess_load(sess)
            # Outputs
            if DEBUG:
                print(
                    "X",
                    X.shape,
                )  # "\n", X)
                print(
                    "Y",
                    Y.shape,
                )  # "\n", Y)
                print(
                    "Y_",
                    Y_.shape,
                )  # "\n", Y_)
                print(
                    "Xb",
                    Xb.shape,
                )  # "\n", Xb)
                print(
                    "Yb",
                    Yb.shape,
                )  # "\n", Yb)
                print(
                    "Yb_",
                    Yb_.shape,
                )  # "\n", Yb_)
                print("validation_error", lv)
            # Assertions
            self.assertAllEqual(Y.shape, Y_.shape)
            self.assertAllEqual(Y.shape[1:], Yb_.shape[1:])
            self.assertEqual(Xb.shape[0], 1)
            self.assertEqual(Yb.shape[0], 1)
            self.assertEqual(Yb_.shape[0], 1)
            self.assertLess(l, l0)
        # Clean up
        shutil.rmtree(nn.result_dir, ignore_errors=True)

    def test_SingleGPUGRU(self):
        """Tester for SingleGPUGRU."""
        train_paths = tf.gfile.Glob("tests/src/tfrecord/3D_dataset_sample*.tfr*")
        train_paths = sorted(train_paths)
        valid_paths = ["tests/src/tfrecord/3D_dataset_sample_00.tfrecord"]
        dataset, data_shape, data_format = _NSF_dataset()
        if DEBUG:
            print("dataset", dataset.shape)  # , "\n", dataset)
        input_idx = [0, 1]
        output_idx = [i for i in range(data_shape[-1]) if i not in input_idx]
        batch_size = 100
        layers = [("fc_relu", 5), ("gru", 10), ("fc_out", -1)]
        learning_rate = 0.01
        num_steps = 100
        report_steps = 10
        # Graph
        nn = SingleGPURecNet(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=learning_rate,
        )
        nn.build()
        config = tf.ConfigProto(allow_soft_placement=True)
        with self.test_session(use_gpu=True, config=config) as sess:
            nn.sess_initialize(
                sess, feed_dict={nn.train_data: train_paths, nn.valid_data: valid_paths}
            )
            # Initial loss
            l0 = nn.sess_loss(sess)
            # Training
            for i in range(0, num_steps, 1):
                l, lv = nn.sess_train(
                    sess, summarize=(i % report_steps == 0 or i == num_steps - 1)
                )
            # Do a run for a full trace
            i = num_steps
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()
            l, lv = nn.sess_train(sess, options=run_options, run_metadata=run_metadata)
            # Check quality of results
            X, Y, Y_ = nn.sess_forward_pass(sess)
            nn.sess_update_data_pipeline(sess, mode="valid")
            lv = nn.sess_loss(sess)
            nn.sess_update_data_pipeline(
                sess, mode="test", feed_dict={nn.feed: np.ones_like(dataset[0:1, :])}
            )
            Xb, Yb, Yb_ = nn.sess_infer(sess)
            # Save and load
            nn.sess_save(sess, global_step=i)
            nn.sess_load(sess)
            # Outputs
            if DEBUG:
                print(
                    "X",
                    X.shape,
                )  # "\n", X)
                print(
                    "Y",
                    Y.shape,
                )  # "\n", Y)
                print(
                    "Y_",
                    Y_.shape,
                )  # "\n", Y_)
                print(
                    "Xb",
                    Xb.shape,
                )  # "\n", Xb)
                print(
                    "Yb",
                    Yb.shape,
                )  # "\n", Yb)
                print(
                    "Yb_",
                    Yb_.shape,
                )  # "\n", Yb_)
                print("validation_error", lv)
            # Assertions
            self.assertAllEqual(Y.shape, Y_.shape)
            self.assertAllEqual(Y.shape[1:], Yb_.shape[1:])
            self.assertEqual(Xb.shape[0], 1)
            self.assertEqual(Yb.shape[0], 1)
            self.assertEqual(Yb_.shape[0], 1)
            self.assertLess(l, l0)
        # Clean up
        shutil.rmtree(nn.result_dir, ignore_errors=True)

    def test_GRU(self):
        """Tester for GRU."""
        train_paths = tf.gfile.Glob("tests/src/tfrecord/3D_dataset_sample*.tfr*")
        train_paths = sorted(train_paths)
        valid_paths = ["tests/src/tfrecord/3D_dataset_sample_00.tfrecord"]
        dataset, data_shape, data_format = _NSF_dataset()
        if DEBUG:
            print("dataset", dataset.shape)  # , "\n", dataset)
        input_idx = [0, 1]
        output_idx = [i for i in range(data_shape[-1]) if i not in input_idx]
        batch_size = 100
        layers = [("fc_relu", 5), ("lstm", 10), ("fc_out", -1)]
        learning_rate = 0.01
        num_steps = 100
        report_steps = 10
        # Graph
        nn = RecNet(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=learning_rate,
        )
        nn.build()
        config = tf.ConfigProto(allow_soft_placement=True)
        with self.test_session(use_gpu=True, config=config) as sess:
            nn.sess_initialize(
                sess, feed_dict={nn.train_data: train_paths, nn.valid_data: valid_paths}
            )
            # Initial loss
            l0 = nn.sess_loss(sess)
            # Training
            for i in range(0, num_steps, 1):
                l, lv = nn.sess_train(
                    sess, summarize=(i % report_steps == 0 or i == num_steps - 1)
                )
            # Do a run for a full trace
            i = num_steps
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()
            l, lv = nn.sess_train(sess, options=run_options, run_metadata=run_metadata)
            # Check quality of results
            X, Y, Y_ = nn.sess_forward_pass(sess)
            nn.sess_update_data_pipeline(sess, mode="valid")
            lv = nn.sess_loss(sess)
            nn.sess_update_data_pipeline(
                sess, mode="test", feed_dict={nn.feed: np.ones_like(dataset[0:1, :])}
            )
            Xb, Yb, Yb_ = nn.sess_infer(sess)
            # Save and load
            nn.sess_save(sess, global_step=i)
            nn.sess_load(sess)
            # Outputs
            if DEBUG:
                print(
                    "X",
                    X.shape,
                )  # "\n", X)
                print(
                    "Y",
                    Y.shape,
                )  # "\n", Y)
                print(
                    "Y_",
                    Y_.shape,
                )  # "\n", Y_)
                print(
                    "Xb",
                    Xb.shape,
                )  # "\n", Xb)
                print(
                    "Yb",
                    Yb.shape,
                )  # "\n", Yb)
                print(
                    "Yb_",
                    Yb_.shape,
                )  # "\n", Yb_)
                print("validation_error", lv)
            # Assertions
            self.assertAllEqual(Y.shape, Y_.shape)
            self.assertAllEqual(Y.shape[1:], Yb_.shape[1:])
            self.assertEqual(Xb.shape[0], 1)
            self.assertEqual(Yb.shape[0], 1)
            self.assertEqual(Yb_.shape[0], 1)
            self.assertLess(l, l0)
        # Clean up
        shutil.rmtree(nn.result_dir, ignore_errors=True)

    @unittest.skipUnless(
        test.is_built_with_cuda(), "Test only applicable when running on GPUs"
    )
    def test_SingleGPU_CUDNN_LSTM(self):
        """Tester for the CUDNN LSTM."""
        train_paths = tf.gfile.Glob("tests/src/tfrecord/3D_dataset_sample*.tfr*")
        train_paths = sorted(train_paths)
        valid_paths = ["tests/src/tfrecord/3D_dataset_sample_00.tfrecord"]
        dataset, data_shape, data_format = _NSF_dataset()
        if DEBUG:
            print("dataset", dataset.shape)  # , "\n", dataset)
        input_idx = [0, 1]
        output_idx = [i for i in range(data_shape[-1]) if i not in input_idx]
        batch_size = 100
        layers = [("fc_relu", 5), ("cudnn_lstm", 10), ("fc_out", -1)]
        learning_rate = 0.01
        num_steps = 100
        report_steps = 10
        # Graph
        nn = SingleGPURecNet(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=learning_rate,
        )
        nn.build()
        config = tf.ConfigProto(allow_soft_placement=True)
        with self.test_session(use_gpu=True, config=config) as sess:
            nn.sess_initialize(
                sess, feed_dict={nn.train_data: train_paths, nn.valid_data: valid_paths}
            )
            # Initial loss
            l0 = nn.sess_loss(sess)
            # Training
            for i in range(0, num_steps, 1):
                l, lv = nn.sess_train(
                    sess, summarize=(i % report_steps == 0 or i == num_steps - 1)
                )
            # Do a run for a full trace
            i = num_steps
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()
            l, lv = nn.sess_train(sess, options=run_options, run_metadata=run_metadata)
            # Check quality of results
            X, Y, Y_ = nn.sess_forward_pass(sess)
            nn.sess_update_data_pipeline(sess, mode="valid")
            lv = nn.sess_loss(sess)
            nn.sess_update_data_pipeline(
                sess, mode="test", feed_dict={nn.feed: np.ones_like(dataset[0:1, :])}
            )
            Xb, Yb, Yb_ = nn.sess_infer(sess)
            # Save and load
            nn.sess_save(sess, global_step=i)
            nn.sess_load(sess)
            # Outputs
            if DEBUG:
                print(
                    "X",
                    X.shape,
                )  # "\n", X)
                print(
                    "Y",
                    Y.shape,
                )  # "\n", Y)
                print(
                    "Y_",
                    Y_.shape,
                )  # "\n", Y_)
                print(
                    "Xb",
                    Xb.shape,
                )  # "\n", Xb)
                print(
                    "Yb",
                    Yb.shape,
                )  # "\n", Yb)
                print(
                    "Yb_",
                    Yb_.shape,
                )  # "\n", Yb_)
                print("validation_error", lv)
            # Assertions
            self.assertAllEqual(Y.shape, Y_.shape)
            self.assertAllEqual(Y.shape[1:], Yb_.shape[1:])
            self.assertEqual(Xb.shape[0], 1)
            self.assertEqual(Yb.shape[0], 1)
            self.assertEqual(Yb_.shape[0], 1)
            self.assertLess(l, l0)
        # Clean up
        shutil.rmtree(nn.result_dir, ignore_errors=True)

    @unittest.skipUnless(
        test.is_built_with_cuda(), "Test only applicable when running on GPUs"
    )
    def test_CUDNN_LSTM(self):
        """Tester for the CUDNN LSTM."""
        train_paths = tf.gfile.Glob("tests/src/tfrecord/3D_dataset_sample*.tfr*")
        train_paths = sorted(train_paths)
        valid_paths = ["tests/src/tfrecord/3D_dataset_sample_00.tfrecord"]
        dataset, data_shape, data_format = _NSF_dataset()
        if DEBUG:
            print("dataset", dataset.shape)  # , "\n", dataset)
        input_idx = [0, 1]
        output_idx = [i for i in range(data_shape[-1]) if i not in input_idx]
        batch_size = 100
        layers = [("fc_relu", 5), ("cudnn_lstm", 10), ("fc_out", -1)]
        learning_rate = 0.01
        num_steps = 100
        report_steps = 10
        # Graph
        nn = RecNet(
            data_shape,
            input_idx,
            output_idx,
            layers,
            batch_size=batch_size,
            init_learning_rate=learning_rate,
        )
        nn.build()
        config = tf.ConfigProto(allow_soft_placement=True)
        with self.test_session(use_gpu=True, config=config) as sess:
            nn.sess_initialize(
                sess, feed_dict={nn.train_data: train_paths, nn.valid_data: valid_paths}
            )
            # Initial loss
            l0 = nn.sess_loss(sess)
            # Training
            for i in range(0, num_steps, 1):
                l, lv = nn.sess_train(
                    sess, summarize=(i % report_steps == 0 or i == num_steps - 1)
                )
            # Do a run for a full trace
            i = num_steps
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()
            l, lv = nn.sess_train(sess, options=run_options, run_metadata=run_metadata)
            # Check quality of results
            X, Y, Y_ = nn.sess_forward_pass(sess)
            nn.sess_update_data_pipeline(sess, mode="valid")
            lv = nn.sess_loss(sess)
            nn.sess_update_data_pipeline(
                sess, mode="test", feed_dict={nn.feed: np.ones_like(dataset[0:1, :])}
            )
            Xb, Yb, Yb_ = nn.sess_infer(sess)
            # Save and load
            nn.sess_save(sess, global_step=i)
            nn.sess_load(sess)
            # Outputs
            if DEBUG:
                print(
                    "X",
                    X.shape,
                )  # "\n", X)
                print(
                    "Y",
                    Y.shape,
                )  # "\n", Y)
                print(
                    "Y_",
                    Y_.shape,
                )  # "\n", Y_)
                print(
                    "Xb",
                    Xb.shape,
                )  # "\n", Xb)
                print(
                    "Yb",
                    Yb.shape,
                )  # "\n", Yb)
                print(
                    "Yb_",
                    Yb_.shape,
                )  # "\n", Yb_)
                print("validation_error", lv)
            # Assertions
            self.assertAllEqual(Y.shape, Y_.shape)
            self.assertAllEqual(Y.shape[1:], Yb_.shape[1:])
            self.assertEqual(Xb.shape[0], 1)
            self.assertEqual(Yb.shape[0], 1)
            self.assertEqual(Yb_.shape[0], 1)
            self.assertLess(l, l0)
        # Clean up
        shutil.rmtree(nn.result_dir, ignore_errors=True)


if __name__ == "__main__":
    test.main()
