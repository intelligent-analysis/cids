# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Create dummy datasets used for testing. Part of the CIDS toolbox and KadiAI."""
import os

import numpy as np
import tensorflow as tf

from cids.data import DataWriter


def _benchmark_dataset(write=False):
    """A benchmark 2D dataset."""
    num_samples = 1000
    dataset = np.asarray(
        [
            np.linspace(0.0, 1.0, num_samples),
            np.linspace(-5.0, 5.0, num_samples),
            np.linspace(-1.0, 0.0, num_samples),
        ]
    ).T
    data_shape = dataset.shape
    print(dataset.shape)
    dataset = dataset.astype("float64")
    data_format = "NF"
    if write:
        os.makedirs("tests/src/csv", exist_ok=True)
        os.makedirs("tests/src/tfrecord", exist_ok=True)
        np.savetxt(
            "tests/src/csv/benchmark_dataset.csv",
            dataset.astype("float32"),
            delimiter=",",
        )
        proto = DataWriter.convert_ndarray_to_proto(dataset, data_format=data_format)
        writer = tf.python_io.TFRecordWriter(
            "tests/src/tfrecord/benchmark_dataset.tfrecord"
        )
        writer.write(proto.SerializeToString())
        writer.close()
    return dataset, data_shape, data_format


def _NF_dataset(write=False):
    """A dummy 2D dataset."""
    dataset_3D, _, _ = _NSF_dataset(False)
    dataset = dataset_3D[:, 0, :]
    data_shape = dataset.shape
    print(dataset.shape)
    dataset = dataset.astype("float64")
    data_format = "NF"
    if write:
        os.makedirs("tests/src/csv", exist_ok=True)
        os.makedirs("tests/src/tfrecord", exist_ok=True)
        np.savetxt(
            "tests/src/csv/2D_dataset.csv", dataset.astype("float32"), delimiter=","
        )
        proto = DataWriter.convert_ndarray_to_proto(dataset, data_format=data_format)
        writer = tf.python_io.TFRecordWriter("tests/src/tfrecord/2D_dataset.tfrecord")
        writer.write(proto.SerializeToString())
        writer.close()
    return dataset, data_shape, data_format


def _NSF_dataset(write=False):
    """A dummy 3D dataset with padded values."""
    dataset = np.pad(
        np.transpose(
            np.asarray(
                [
                    np.reshape(np.arange(1, 121, 1, dtype=np.float64), (6, 20)),
                    -1.0
                    * np.reshape(np.arange(1, 121, 1, dtype=np.float64) ** 2, (6, 20)),
                    np.reshape(np.linspace(1000, 1001, 120, dtype=np.float64), (6, 20)),
                    np.reshape(np.arange(20, 140, 1, dtype=np.float64) ** -1, (6, 20)),
                    np.reshape(np.arange(1, 121, 1, dtype=np.float64) ** 0.5, (6, 20)),
                ]
            ),
            [2, 1, 0],
        ),
        ((0, 0), (0, 1), (0, 0)),
        "constant",
    )
    # shorten sample sequence
    dataset[7, 4:6, :] = np.zeros([2, 5], dtype=np.float64)
    data_shape = dataset.shape
    dataset = dataset.astype("float64")
    data_format = "NSF"
    if write:
        os.makedirs("tests/src/csv", exist_ok=True)
        os.makedirs("tests/src/tfrecord", exist_ok=True)
        for i, A in enumerate(np.split(dataset, dataset.shape[0], axis=0)):
            np.savetxt(
                f"tests/src/csv/3D_dataset_sample_{i:02d}.csv",
                np.squeeze(A.astype("float32"), axis=0),
                delimiter=",",
            )
            proto = DataWriter.convert_ndarray_to_proto(A, data_format=data_format)
            writer = tf.python_io.TFRecordWriter(
                f"tests/src/tfrecord/3D_dataset_sample_{i:02d}.tfrecord"
            )
            writer.write(proto.SerializeToString())
            writer.close()
    return dataset, data_shape, data_format


def _NSF_binary_class_dataset():
    dataset, data_shape, data_format = _NSF_dataset()
    pseudo_classes = dataset[..., 0:1]
    pseudo_classes = np.asarray(
        np.mean(pseudo_classes - np.mean(pseudo_classes), axis=1) > 0.0,
        dtype=np.float64,
    )
    mask = np.asarray(np.all(dataset != 0.0, axis=-1, keepdims=True), dtype=np.float64)
    pseudo_classes = np.stack([pseudo_classes] * data_shape[1], axis=1)
    pseudo_classes *= mask
    dataset = np.concatenate([dataset[..., :2], pseudo_classes], axis=-1)
    dataset = dataset.astype("float64")
    data_shape = dataset.shape
    return dataset, data_shape, data_format


def _NSF_categorical_class_dataset(num_classes):
    dataset, data_shape, data_format = _NSF_dataset()
    pseudo_classes = np.zeros_like(dataset[..., 0:1])
    boundaries = [
        np.float64(i / num_classes) * np.max(dataset[..., 0:1])
        for i in range(1, num_classes)
    ]
    for i, b in enumerate(boundaries):
        pseudo_classes[dataset[..., 0:1] > b] = np.float64(i + 1)
    dataset = np.asarray(
        np.concatenate([dataset[..., :2], pseudo_classes], axis=-1), dtype=np.float64
    )
    dataset = dataset.astype("float64")
    data_shape = dataset.shape
    return dataset, data_shape, data_format


def _NXYF_binary_class_dataset():
    dataset = np.ones([20, 9, 9, 2])
    dataset = dataset * np.reshape(np.arange(20), [-1, 1, 1, 1])
    pseudo_classes = dataset[:, 0:1, 0:1, 0:1] > np.mean(dataset)
    pseudo_classes = pseudo_classes.astype("float64")
    pseudo_classes = np.tile(pseudo_classes, [1, 9, 9, 1])
    dataset = np.concatenate([dataset, pseudo_classes], axis=-1)
    dataset = dataset.astype("float64")
    data_format = "NXYF"
    data_shape = dataset.shape
    return dataset, data_shape, data_format


def _NXYF_categorical_class_dataset(num_classes):
    dataset = np.ones([20, 9, 9, 2])
    dataset = dataset * np.reshape(np.arange(20), [-1, 1, 1, 1])
    pseudo_classes = np.zeros_like(dataset[..., 0:1])
    boundaries = [
        np.float64(i / num_classes) * np.max(dataset[..., 0:1])
        for i in range(1, num_classes)
    ]
    for i, b in enumerate(boundaries):
        pseudo_classes[dataset[..., 0:1] > b] = np.float64(i + 1)
    pseudo_classes = pseudo_classes.astype("float64")
    dataset = np.concatenate([dataset, pseudo_classes], axis=-1)
    dataset = dataset.astype("float64")
    data_format = "NXYF"
    data_shape = dataset.shape
    return dataset, data_shape, data_format


def _4D_dataset(write=False):
    """A dummy 4D dataset with padded values."""
    dataset, _, _ = _NSF_dataset(False)
    dataset = np.stack([dataset, -dataset, -0.5 * dataset, 0.5 * dataset], axis=-1)
    dataset = np.tile(dataset, [1, 2, 2, 1])
    data_shape = dataset.shape
    dataset = dataset.astype("float64")
    data_format = "NHWC"
    if write:
        os.makedirs("tests/src/csv", exist_ok=True)
        os.makedirs("tests/src/tfrecord", exist_ok=True)
        for i, A in enumerate(np.split(dataset, dataset.shape[0], axis=0)):
            proto = DataWriter.convert_ndarray_to_proto(A, data_format=data_format)
            writer = tf.python_io.TFRecordWriter(
                f"tests/src/tfrecord/4D_dataset_sample_{i:02d}.tfrecord"
            )
            writer.write(proto.SerializeToString())
            writer.close()
    return dataset, data_shape, data_format


def _normalize_test_dataset():
    dataset = np.concatenate([np.ones([10, 3]), 0.5 * np.ones([10, 3])], axis=0)
    data_shape = dataset.shape
    data_format = "NF"
    return dataset, data_shape, data_format
