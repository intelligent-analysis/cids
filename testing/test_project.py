# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Tester for the KadiAIProject class. Part of KadiAI."""
import os
import shutil
import tempfile

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

from pathlib import Path
from tensorflow.python.platform import test

from kadi_ai.projects import KadiAIProject
from cids.data import DataDefinition
from cids.data import Feature


DEBUG = True


class ProjectTest(test.TestCase):
    """Tester for the neural_network module."""

    def test_project_logging(self):
        with tempfile.TemporaryDirectory(prefix="cids-test-") as tmpdir:
            project = KadiAIProject("test", root=tmpdir)
            # Check logging
            log_string = "This should be logged."
            project.log(log_string)
            assert project.log_dir.exists() and project.log_dir.is_dir()
            assert project.log_file.exists() and project.log_file.is_file()
            with project.log_file.open("r", encoding="utf8") as f:
                content = f.read()
                assert log_string in content, "Logging failed."
                project.log("Logging successful.")

    def test_project_serialization(self):
        with tempfile.TemporaryDirectory(prefix="cids-test-") as tmpdir:
            project = KadiAIProject("test", root=tmpdir)
            # Serialize and reload
            project.to_json(write_data_definition=False)
            project2 = KadiAIProject.from_json(project.project_file)
            # Check init params
            assert project.name == project2.name
            assert project.seed == project2.seed
            assert project.log_file == project2.log_file
            assert project.root_dir == project2.root_dir
            assert project._runscript == project2._runscript
            assert project.gitcommit == project2.gitcommit
            # Check run params
            assert project.input_dir == project2.input_dir
            assert project.src_dir == project2.src_dir
            assert project.tfrecord_dir == project2.tfrecord_dir
            assert project.result_dir == project2.result_dir
            assert project.log_dir == project2.log_dir
            assert project.plot_dir == project2.plot_dir

    def test_project_data_definition(self):
        with tempfile.TemporaryDirectory(prefix="cids-test-") as tmpdir:
            project = KadiAIProject("test", root=tmpdir)
            # Serialize and deserialize data definition
            project.data_definition = DataDefinition(
                Feature("test_input", [None, 1]), Feature("test_target", [None, 2])
            )
            print(os.fspath(project.data_definition.file))
            project.data_definition.to_json()
            data_definition2 = DataDefinition.from_json(project.data_definition.file)
            # Check content
            assert data_definition2 == project.data_definition
            # Check file paths
            assert project.data_definition.file == data_definition2.file

    def test_project_data_definition_with_samples(self):
        with tempfile.TemporaryDirectory(prefix="cids-test-") as tmpdir:
            project = KadiAIProject("test", root=tmpdir)
            # Setup files
            shutil.copytree(
                Path(__file__).parent / "src" / "tfrecord",
                project.input_dir / "tfrecord_copied",
            )
            # Serialize and deserialize data definition
            project.data_definition = DataDefinition(
                Feature("test_input", [None, 1]), Feature("test_target", [None, 2])
            )
            print(os.fspath(project.data_definition.file))
            project.data_definition.to_json()
            data_definition2 = DataDefinition.from_json(project.data_definition.file)
            # Discover files automatically
            data_definition3 = DataDefinition(
                Feature("test_input", [None, 1]), Feature("test_target", [None, 2])
            )
            data_definition3.file = project.tfrecord_dir / "data_definition3.json"
            discovered_samples = data_definition3.samples
            # Check features
            assert data_definition2 == project.data_definition
            # Check samples stored in data definition
            for s1, s2, s3 in zip(
                project.data_definition.samples,
                data_definition2.samples,
                discovered_samples,
            ):
                print(s1)
                assert s1 == s2
                assert s1 == s3
            # Check file paths
            assert project.data_definition.file == data_definition2.file

    def test_project_serialization_with_samples(self):
        with tempfile.TemporaryDirectory(prefix="cids-test-") as tmpdir:
            project = KadiAIProject("test", root=tmpdir)
            # Setup files
            shutil.copytree(
                Path(__file__).parent / "src" / "tfrecord",
                project.input_dir / "tfrecord_copied",
            )
            # Set data definition
            project.data_definition = DataDefinition(
                Feature("test_input", [None, 1]), Feature("test_target", [None, 2])
            )
            project.to_json(write_data_definition=True)
            project2 = KadiAIProject.from_json(project.project_file)

            # Check features
            assert project2.data_definition == project.data_definition
            # Check samples stored in data definition
            for s1, s2 in zip(
                project.data_definition.samples,
                project2.data_definition.samples,
            ):
                print(s1)
                assert s1 == s2
            # Check file paths
            assert project.data_definition.file == project2.data_definition.file


if __name__ == "__main__":
    test.main()
