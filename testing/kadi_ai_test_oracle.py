# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import json
import shutil
from pathlib import Path

import numpy as np
from click.core import Context
from kadi_apy import KadiManager
from kadi_apy.lib.exceptions import KadiAPYInputError
from kadi_apy.lib.exceptions import KadiAPYRequestError
from kadi_apy.lib.miscellaneous import Miscellaneous
from kadi_apy.lib.resources.records import Record

from kadi_ai.nodes.commands import bayesian_oracle
from kadi_ai.nodes.commands import finisher
from kadi_ai.nodes.commands import scorer
from kadi_ai.nodes.properties import DEFAULT_KADI_PROPERTY_FILE
from kadi_ai.nodes.properties import read_kadi_properties_file


def black_box_function1D(x):
    """Function with unknown internals we wish to maximize.

    This is just serving as an example, for all intents and
    purposes think of the internals of this function, i.e.: the process
    which generates its output values, as unknown.
    """
    from math import sin

    return -(1.4 - 3.0 * x) * sin(18.0 * x)


def black_box_function2D(x):
    return x[..., 0] ** 2.0 + x[..., 1] ** 2.0


def _create_blank_record(manager, identifier):
    # Remove record
    try:
        record = Record(manager, identifier=identifier, create=False)
        record.delete()
    except (KadiAPYRequestError, KadiAPYInputError):
        pass
    # Empty trash
    misc = Miscellaneous(manager)
    request = misc.get_deleted_resources(filter=identifier)
    items = request.json()["items"]
    for item in items:
        misc.purge(Record, item["id"])
    # Create new record
    record = Record(manager, identifier=identifier, create=True)
    return record


if __name__ == "__main__":

    max_steps = 10
    dimensionality = 2
    kadi_instance = "my_kadi_demo_instance"
    record_identifier = "tmp-search-space"
    target_value = 0.0
    objective = "score"
    monitor = "Tortuosity"
    direction = "min"

    search_space = [
        {
            "key": "x",
            "type": "dict",
            "value": [
                {"key": "min", "type": "float", "value": -1.0},
                {"key": "max", "type": "float", "value": 1.0},
            ],
        }
    ]
    if dimensionality > 1:
        search_space.append(
            {
                "key": "y",
                "type": "dict",
                "value": [
                    {"key": "min", "type": "float", "value": -1.0},
                    {"key": "max", "type": "float", "value": 1.0},
                ],
            }
        )

    # Oracle static parameters
    ctx_oracle = Context(command=bayesian_oracle)
    ctx_oracle.params["search_space_identifier"] = record_identifier
    ctx_oracle.params["objective"] = objective
    ctx_oracle.params["direction"] = direction
    ctx_oracle.params["exploration_factor"] = 2.6
    ctx_oracle.params["max_index"] = 100
    ctx_oracle.params["num_initial_points"] = 2
    ctx_oracle.params["plot"] = False  # FIXME

    # Finisher static parameters
    ctx_finisher = Context(command=finisher)

    # Scorer static parameters
    ctx_scorer = Context(command=scorer)
    ctx_scorer.params["objective"] = objective
    ctx_scorer.params["monitor"] = monitor
    ctx_scorer.params["direction"] = direction
    ctx_scorer.params["target_value"] = target_value

    # Remove kadi_properties file
    kadi_prop_file = Path.cwd() / DEFAULT_KADI_PROPERTY_FILE
    kadi_prop_file.unlink(missing_ok=True)

    # Clean search directory
    shutil.rmtree(Path() / "active_search", ignore_errors=True)

    # Create Kadi Record on demo instance
    manager = KadiManager(instance=kadi_instance)
    record = _create_blank_record(manager, record_identifier)
    record.edit(description="Umbrella record for Bayesian Optimization test.")
    record.add_metadata(search_space, force=True)

    # Manual loop
    print(f"Performing {max_steps} iterations of Bayesian Optimization.")
    for i in range(max_steps):

        print(i)

        # NODE: Predict next trial and write to kadi properties file
        bayesian_oracle.callback(**ctx_oracle.params)

        # PLACEHOLDER: Simulate a trial run
        #  In KadiStudio, the process engine reads the file, sets environment variables
        #  and runs the program. The user is responsible for adding environment
        #  variable placeholders as input parameters to the nodes in the trial run.

        # Read kadi properties file, extract values and update callback contexts
        root_element = read_kadi_properties_file(kadi_prop_file)
        environment_element = root_element.findall("environment")
        env = {param.get("key"): param.text for param in environment_element}
        ctx_scorer.params["trial_id"] = env["trial_id"]
        ctx_finisher.params["trial_id"] = env["trial_id"]

        # Simulate a result was computed random results
        score = None
        if dimensionality == 1:
            x = float(env["x"])
            score = black_box_function1D(x)
        elif dimensionality == 2:
            x = float(env["x"])
            y = float(env["y"])
            score = black_box_function2D(np.asarray([[x, y]]))

        # results in .json file format
        results_file = Path(env["trial_dir"]) / "results.json"
        data_out = [
            {
                "Frame": "3",
                "Time": "2.500000e+00",
                str(monitor): ",".join(map(str, score.tolist())),
                "Error": "7.408606e-01",
                "Porosity": "3.377920e-01",
                "CG_iterations": "1",
                "CG_residual": "1.600390e-02",
            }
        ]
        json_string = json.dumps(data_out)
        with open(results_file, "w", encoding="utf8") as f:
            f.write(json_string)
        ctx_scorer.params["results_file"] = results_file

        # NODE: Finish trial (only necessary for remote execution, updates status)
        finisher.callback(**ctx_finisher.params)

        # NODE: Compute score and update record
        scorer.callback(**ctx_scorer.params)
