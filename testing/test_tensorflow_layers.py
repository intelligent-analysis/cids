# Copyright 2022 Arnd Koeppe and the CIDS team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Tester for the CIDSModelTF class. Part of the CIDS toolbox."""
import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

import numpy as np
import tensorflow as tf

# import tensorflow as tf
# import shutil
# import inspect

# from pathlib import Path
from tensorflow.python.platform import test

from cids.tensorflow import layers as clayers

# from cids.data import DataDefinition, Feature
# from tests.src.create_test_dataset import (
#     _NF_dataset,
#     _NSF_dataset,
#     _NSF_binary_class_dataset,
#     _NSF_categorical_class_dataset,
#     _NXYF_binary_class_dataset,
#     _NXYF_categorical_class_dataset,
#     _benchmark_dataset,
#     _normalize_test_dataset,
# )


DEBUG = True


class TFLayerTester(test.TestCase):
    """Tester for the neural_network module."""

    def test_sampling_layer(self):
        latent_dim = 1000
        ########################################################################
        print("Sampling: concat")
        sampling = clayers.Sampling(
            add_sampling_loss=False,
            mode="concat",
            latent_dim=latent_dim,
            use_input_as_seed=True,
        )
        x = tf.ones([5, 2])
        y_ = sampling(x)
        # Array shape
        np.testing.assert_array_equal([x.shape[0], x.shape[1] + latent_dim], y_.shape)
        # Value
        np.testing.assert_array_equal(x, y_[..., 0 : x.shape[-1]])
        # Mean
        np.testing.assert_allclose(np.mean(y_[..., 1:], axis=-1), 0.0, atol=0.3)
        # Standard deviation
        np.testing.assert_array_less(np.std(y_[..., 1:], axis=-1), 3.0)
        np.testing.assert_array_less(0.3, np.std(y_[..., 1:], axis=-1))
        ########################################################################
        print("Sampling: drop")
        sampling = clayers.Sampling(
            add_sampling_loss=False,
            mode="drop",
            latent_dim=latent_dim,
            use_input_as_seed=True,
        )
        x = tf.ones([5, 2])
        y_ = sampling(x)
        print(y_)
        # Array shape
        np.testing.assert_array_equal([x.shape[0], latent_dim], y_.shape)
        # Mean
        np.testing.assert_allclose(np.mean(y_[..., 1:], axis=-1), 0.0, atol=0.3)
        # Standard deviation
        np.testing.assert_array_less(np.std(y_[..., 1:], axis=-1), 3.0)
        np.testing.assert_array_less(0.3, np.std(y_[..., 1:], axis=-1))
        ########################################################################
        print("Sampling: repar")
        sampling = clayers.Sampling(
            add_sampling_loss=False,
            mode="repar",
            latent_dim=latent_dim,
            use_input_as_seed=True,
        )
        x = tf.ones([5, latent_dim])
        y_ = sampling(x)
        # Array shape
        np.testing.assert_array_equal([x.shape[0], x.shape[1] // 2], y_.shape)
        # Mean
        np.testing.assert_allclose(np.mean(y_, axis=-1), 1.0, atol=0.3)
        # Standard deviation
        np.testing.assert_array_less(np.std(y_, axis=-1), 3.0)
        np.testing.assert_array_less(0.3, np.std(y_, axis=-1))
        ########################################################################
        print("Sampling: reparaugment")
        sampling = clayers.Sampling(
            add_sampling_loss=False,
            mode="reparaugment",
            latent_dim=latent_dim,
            use_input_as_seed=True,
        )
        x = tf.ones([5, latent_dim])
        y_ = sampling(x)
        # Array shape
        np.testing.assert_array_equal([x.shape[0], x.shape[1] // 2 * 3], y_.shape)
        # Mean
        np.testing.assert_allclose(np.mean(y_, axis=-1), 1.0, atol=0.3)
        # Standard deviation
        np.testing.assert_array_less(np.std(y_, axis=-1), 3.0)
        np.testing.assert_array_less(0.3, np.std(y_, axis=-1))
        print("done")


if __name__ == "__main__":
    test.main()
